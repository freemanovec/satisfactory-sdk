#pragma once

#include "../Utils.h"
// Name: Satisfactory, Version: 1.0.0

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace SDK
{
//---------------------------------------------------------------------------
// Classes
//---------------------------------------------------------------------------

// Class FactoryGame.FGSubsystem
// 0x427D5600
class FGSubsystem
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static FGSubsystem ReadAsMe(const uintptr_t address)
	{
		FGSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGActorRepresentationManager
// 0x42804400
class FGActorRepresentationManager
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGActorRepresentationManager ReadAsMe(const uintptr_t address)
	{
		FGActorRepresentationManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGActorRepresentationManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAdminInterface
// 0x427D5600
class FGAdminInterface
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static FGAdminInterface ReadAsMe(const uintptr_t address)
	{
		FGAdminInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAdminInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGActorRepresentationInterface
// 0x427D4600
class FGActorRepresentationInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGActorRepresentationInterface ReadAsMe(const uintptr_t address)
	{
		FGActorRepresentationInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGActorRepresentationInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGActorRepresentation
// 0x40FE0C00
class FGActorRepresentation
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGActorRepresentation ReadAsMe(const uintptr_t address)
	{
		FGActorRepresentation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGActorRepresentation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAISystem
// 0x4280C000
class FGAISystem
{
public:
	unsigned char                                      UnknownData00[0x4280C000];                                // 0x0000(0x4280C000) MISSED OFFSET

	static FGAISystem ReadAsMe(const uintptr_t address)
	{
		FGAISystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAISystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAggroTargetInterface
// 0x427D4600
class FGAggroTargetInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGAggroTargetInterface ReadAsMe(const uintptr_t address)
	{
		FGAggroTargetInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAggroTargetInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAmbientVolume
// 0x40FEC600
class FGAmbientVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static FGAmbientVolume ReadAsMe(const uintptr_t address)
	{
		FGAmbientVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAmbientVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAnimInstanceTrainDocking
// 0x427EA800
class FGAnimInstanceTrainDocking
{
public:
	unsigned char                                      UnknownData00[0x427EA800];                                // 0x0000(0x427EA800) MISSED OFFSET

	static FGAnimInstanceTrainDocking ReadAsMe(const uintptr_t address)
	{
		FGAnimInstanceTrainDocking ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAnimInstanceTrainDocking& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAnimInstanceTruckStation
// 0x427EA800
class FGAnimInstanceTruckStation
{
public:
	unsigned char                                      UnknownData00[0x427EA800];                                // 0x0000(0x427EA800) MISSED OFFSET

	static FGAnimInstanceTruckStation ReadAsMe(const uintptr_t address)
	{
		FGAnimInstanceTruckStation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAnimInstanceTruckStation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAmbientSoundSpline
// 0x40FE8000
class FGAmbientSoundSpline
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGAmbientSoundSpline ReadAsMe(const uintptr_t address)
	{
		FGAmbientSoundSpline ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAmbientSoundSpline& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAnimNotify_FootDown
// 0x427D3400
class FGAnimNotify_FootDown
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static FGAnimNotify_FootDown ReadAsMe(const uintptr_t address)
	{
		FGAnimNotify_FootDown ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAnimNotify_FootDown& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCharacterAnimInstance
// 0x427EA800
class FGCharacterAnimInstance
{
public:
	unsigned char                                      UnknownData00[0x427EA800];                                // 0x0000(0x427EA800) MISSED OFFSET

	static FGCharacterAnimInstance ReadAsMe(const uintptr_t address)
	{
		FGCharacterAnimInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCharacterAnimInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGItemDescriptor
// 0x40FE0C00
class FGItemDescriptor
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGItemDescriptor ReadAsMe(const uintptr_t address)
	{
		FGItemDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGItemDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAnimNotify_Attack
// 0x427D3400
class FGAnimNotify_Attack
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static FGAnimNotify_Attack ReadAsMe(const uintptr_t address)
	{
		FGAnimNotify_Attack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAnimNotify_Attack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAmbientSettings
// 0x40FE0C00
class FGAmbientSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGAmbientSettings ReadAsMe(const uintptr_t address)
	{
		FGAmbientSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAmbientSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAnimNotify_Landed
// 0x427D3400
class FGAnimNotify_Landed
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static FGAnimNotify_Landed ReadAsMe(const uintptr_t address)
	{
		FGAnimNotify_Landed ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAnimNotify_Landed& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAnimPlayer
// 0x4280AA00
class FGAnimPlayer
{
public:
	unsigned char                                      UnknownData00[0x4280AA00];                                // 0x0000(0x4280AA00) MISSED OFFSET

	static FGAnimPlayer ReadAsMe(const uintptr_t address)
	{
		FGAnimPlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAnimPlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAssetManager
// 0x4280A200
class FGAssetManager
{
public:
	unsigned char                                      UnknownData00[0x4280A200];                                // 0x0000(0x4280A200) MISSED OFFSET

	static FGAssetManager ReadAsMe(const uintptr_t address)
	{
		FGAssetManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAssetManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHologram
// 0x40FE8000
class FGHologram
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGHologram ReadAsMe(const uintptr_t address)
	{
		FGHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAnyUndefinedDescriptor
// 0x4280A600
class FGAnyUndefinedDescriptor
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGAnyUndefinedDescriptor ReadAsMe(const uintptr_t address)
	{
		FGAnyUndefinedDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAnyUndefinedDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAtmosphereUpdater
// 0x40FE0C00
class FGAtmosphereUpdater
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGAtmosphereUpdater ReadAsMe(const uintptr_t address)
	{
		FGAtmosphereUpdater ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAtmosphereUpdater& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableHologram
// 0x42809400
class FGBuildableHologram
{
public:
	unsigned char                                      UnknownData00[0x42809400];                                // 0x0000(0x42809400) MISSED OFFSET

	static FGBuildableHologram ReadAsMe(const uintptr_t address)
	{
		FGBuildableHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAtmosphereVolume
// 0x40FEC600
class FGAtmosphereVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static FGAtmosphereVolume ReadAsMe(const uintptr_t address)
	{
		FGAtmosphereVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAtmosphereVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFactoryHologram
// 0x42809600
class FGFactoryHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGFactoryHologram ReadAsMe(const uintptr_t address)
	{
		FGFactoryHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFactoryHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConveyorAttachmentHologram
// 0x42809800
class FGConveyorAttachmentHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGConveyorAttachmentHologram ReadAsMe(const uintptr_t address)
	{
		FGConveyorAttachmentHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConveyorAttachmentHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAttachmentSplitterHologram
// 0x42809A00
class FGAttachmentSplitterHologram
{
public:
	unsigned char                                      UnknownData00[0x42809A00];                                // 0x0000(0x42809A00) MISSED OFFSET

	static FGAttachmentSplitterHologram ReadAsMe(const uintptr_t address)
	{
		FGAttachmentSplitterHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAttachmentSplitterHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAttack
// 0x40FE0C00
class FGAttack
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGAttack ReadAsMe(const uintptr_t address)
	{
		FGAttack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAttack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAttackRanged
// 0x42809200
class FGAttackRanged
{
public:
	unsigned char                                      UnknownData00[0x42809200];                                // 0x0000(0x42809200) MISSED OFFSET

	static FGAttackRanged ReadAsMe(const uintptr_t address)
	{
		FGAttackRanged ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAttackRanged& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAttackCustomRadial
// 0x42809200
class FGAttackCustomRadial
{
public:
	unsigned char                                      UnknownData00[0x42809200];                                // 0x0000(0x42809200) MISSED OFFSET

	static FGAttackCustomRadial ReadAsMe(const uintptr_t address)
	{
		FGAttackCustomRadial ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAttackCustomRadial& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAttentionPingActor
// 0x40FE8000
class FGAttentionPingActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGAttentionPingActor ReadAsMe(const uintptr_t address)
	{
		FGAttentionPingActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAttentionPingActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAttackMeleeJump
// 0x42808E00
class FGAttackMeleeJump
{
public:
	unsigned char                                      UnknownData00[0x42808E00];                                // 0x0000(0x42808E00) MISSED OFFSET

	static FGAttackMeleeJump ReadAsMe(const uintptr_t address)
	{
		FGAttackMeleeJump ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAttackMeleeJump& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMessageBase
// 0x4280FC00
class FGMessageBase
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGMessageBase ReadAsMe(const uintptr_t address)
	{
		FGMessageBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMessageBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAudioMessage
// 0x4280FE00
class FGAudioMessage
{
public:
	unsigned char                                      UnknownData00[0x4280FE00];                                // 0x0000(0x4280FE00) MISSED OFFSET

	static FGAudioMessage ReadAsMe(const uintptr_t address)
	{
		FGAudioMessage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAudioMessage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAudioVolumeSubsystem
// 0x42804400
class FGAudioVolumeSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGAudioVolumeSubsystem ReadAsMe(const uintptr_t address)
	{
		FGAudioVolumeSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAudioVolumeSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAvailabilityDependency
// 0x40FE0C00
class FGAvailabilityDependency
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGAvailabilityDependency ReadAsMe(const uintptr_t address)
	{
		FGAvailabilityDependency ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAvailabilityDependency& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAttackMelee
// 0x42809200
class FGAttackMelee
{
public:
	unsigned char                                      UnknownData00[0x42809200];                                // 0x0000(0x42809200) MISSED OFFSET

	static FGAttackMelee ReadAsMe(const uintptr_t address)
	{
		FGAttackMelee ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAttackMelee& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBlueprintFunctionLibrary
// 0x40FEE400
class FGBlueprintFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGBlueprintFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		FGBlueprintFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBlueprintFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGAutoJsonExportSettings
// 0x427D8E00
class FGAutoJsonExportSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGAutoJsonExportSettings ReadAsMe(const uintptr_t address)
	{
		FGAutoJsonExportSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGAutoJsonExportSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBaseUI
// 0x4280FC00
class FGBaseUI
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGBaseUI ReadAsMe(const uintptr_t address)
	{
		FGBaseUI ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBaseUI& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBeacon
// 0x40FE8000
class FGBeacon
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGBeacon ReadAsMe(const uintptr_t address)
	{
		FGBeacon ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBeacon& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildable
// 0x40FE8000
class FGBuildable
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGBuildable ReadAsMe(const uintptr_t address)
	{
		FGBuildable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableConveyorAttachment
// 0x4280E400
class FGBuildableConveyorAttachment
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableConveyorAttachment ReadAsMe(const uintptr_t address)
	{
		FGBuildableConveyorAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableConveyorAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableAttachmentMerger
// 0x4280E600
class FGBuildableAttachmentMerger
{
public:
	unsigned char                                      UnknownData00[0x4280E600];                                // 0x0000(0x4280E600) MISSED OFFSET

	static FGBuildableAttachmentMerger ReadAsMe(const uintptr_t address)
	{
		FGBuildableAttachmentMerger ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableAttachmentMerger& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBoundedTextRenderComponent
// 0x4280EC00
class FGBoundedTextRenderComponent
{
public:
	unsigned char                                      UnknownData00[0x4280EC00];                                // 0x0000(0x4280EC00) MISSED OFFSET

	static FGBoundedTextRenderComponent ReadAsMe(const uintptr_t address)
	{
		FGBoundedTextRenderComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBoundedTextRenderComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableAttachmentSplitter
// 0x4280E600
class FGBuildableAttachmentSplitter
{
public:
	unsigned char                                      UnknownData00[0x4280E600];                                // 0x0000(0x4280E600) MISSED OFFSET

	static FGBuildableAttachmentSplitter ReadAsMe(const uintptr_t address)
	{
		FGBuildableAttachmentSplitter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableAttachmentSplitter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableCheatFluidSink
// 0x4280E400
class FGBuildableCheatFluidSink
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableCheatFluidSink ReadAsMe(const uintptr_t address)
	{
		FGBuildableCheatFluidSink ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableCheatFluidSink& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableManufacturer
// 0x4280E400
class FGBuildableManufacturer
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableManufacturer ReadAsMe(const uintptr_t address)
	{
		FGBuildableManufacturer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableManufacturer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableCheatFluidSpawner
// 0x4280E400
class FGBuildableCheatFluidSpawner
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableCheatFluidSpawner ReadAsMe(const uintptr_t address)
	{
		FGBuildableCheatFluidSpawner ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableCheatFluidSpawner& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableAutomatedWorkBench
// 0x4280DE00
class FGBuildableAutomatedWorkBench
{
public:
	unsigned char                                      UnknownData00[0x4280DE00];                                // 0x0000(0x4280DE00) MISSED OFFSET

	static FGBuildableAutomatedWorkBench ReadAsMe(const uintptr_t address)
	{
		FGBuildableAutomatedWorkBench ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableAutomatedWorkBench& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableFactory
// 0x4280EA00
class FGBuildableFactory
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableFactory ReadAsMe(const uintptr_t address)
	{
		FGBuildableFactory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableFactory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConveyorRemoteCallObject
// 0x4280D400
class FGConveyorRemoteCallObject
{
public:
	unsigned char                                      UnknownData00[0x4280D400];                                // 0x0000(0x4280D400) MISSED OFFSET

	static FGConveyorRemoteCallObject ReadAsMe(const uintptr_t address)
	{
		FGConveyorRemoteCallObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConveyorRemoteCallObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.PresistentConveyorPackagingData
// 0x40FE0C00
class PresistentConveyorPackagingData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PresistentConveyorPackagingData ReadAsMe(const uintptr_t address)
	{
		PresistentConveyorPackagingData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PresistentConveyorPackagingData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_ConveyorBeltValid
// 0x4280CC00
class FGUseState_ConveyorBeltValid
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_ConveyorBeltValid ReadAsMe(const uintptr_t address)
	{
		FGUseState_ConveyorBeltValid ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_ConveyorBeltValid& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableConverter
// 0x4280DE00
class FGBuildableConverter
{
public:
	unsigned char                                      UnknownData00[0x4280DE00];                                // 0x0000(0x4280DE00) MISSED OFFSET

	static FGBuildableConverter ReadAsMe(const uintptr_t address)
	{
		FGBuildableConverter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableConverter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRemoteCallObject
// 0x40FE0C00
class FGRemoteCallObject
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGRemoteCallObject ReadAsMe(const uintptr_t address)
	{
		FGRemoteCallObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRemoteCallObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_ConveyorBeltEmpty
// 0x4280CC00
class FGUseState_ConveyorBeltEmpty
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_ConveyorBeltEmpty ReadAsMe(const uintptr_t address)
	{
		FGUseState_ConveyorBeltEmpty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_ConveyorBeltEmpty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState
// 0x40FE0C00
class FGUseState
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGUseState ReadAsMe(const uintptr_t address)
	{
		FGUseState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableConveyorBase
// 0x4280EA00
class FGBuildableConveyorBase
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableConveyorBase ReadAsMe(const uintptr_t address)
	{
		FGBuildableConveyorBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableConveyorBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableDecor
// 0x4280EA00
class FGBuildableDecor
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableDecor ReadAsMe(const uintptr_t address)
	{
		FGBuildableDecor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableDecor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableDockingStation
// 0x4280E400
class FGBuildableDockingStation
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableDockingStation ReadAsMe(const uintptr_t address)
	{
		FGBuildableDockingStation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableDockingStation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableConveyorBelt
// 0x4280D000
class FGBuildableConveyorBelt
{
public:
	unsigned char                                      UnknownData00[0x4280D000];                                // 0x0000(0x4280D000) MISSED OFFSET

	static FGBuildableConveyorBelt ReadAsMe(const uintptr_t address)
	{
		FGBuildableConveyorBelt ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableConveyorBelt& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableGeneratorFuel
// 0x42823600
class FGBuildableGeneratorFuel
{
public:
	unsigned char                                      UnknownData00[0x42823600];                                // 0x0000(0x42823600) MISSED OFFSET

	static FGBuildableGeneratorFuel ReadAsMe(const uintptr_t address)
	{
		FGBuildableGeneratorFuel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableGeneratorFuel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableGeneratorGeoThermal
// 0x42823600
class FGBuildableGeneratorGeoThermal
{
public:
	unsigned char                                      UnknownData00[0x42823600];                                // 0x0000(0x42823600) MISSED OFFSET

	static FGBuildableGeneratorGeoThermal ReadAsMe(const uintptr_t address)
	{
		FGBuildableGeneratorGeoThermal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableGeneratorGeoThermal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableFactoryBuilding
// 0x4280EA00
class FGBuildableFactoryBuilding
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableFactoryBuilding ReadAsMe(const uintptr_t address)
	{
		FGBuildableFactoryBuilding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableFactoryBuilding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_ConveyorBeltFullInventory
// 0x4280CC00
class FGUseState_ConveyorBeltFullInventory
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_ConveyorBeltFullInventory ReadAsMe(const uintptr_t address)
	{
		FGUseState_ConveyorBeltFullInventory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_ConveyorBeltFullInventory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableGenerator
// 0x4280E400
class FGBuildableGenerator
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableGenerator ReadAsMe(const uintptr_t address)
	{
		FGBuildableGenerator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableGenerator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableConveyorLift
// 0x4280D000
class FGBuildableConveyorLift
{
public:
	unsigned char                                      UnknownData00[0x4280D000];                                // 0x0000(0x4280D000) MISSED OFFSET

	static FGBuildableConveyorLift ReadAsMe(const uintptr_t address)
	{
		FGBuildableConveyorLift ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableConveyorLift& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableGeneratorNuclear
// 0x42823400
class FGBuildableGeneratorNuclear
{
public:
	unsigned char                                      UnknownData00[0x42823400];                                // 0x0000(0x42823400) MISSED OFFSET

	static FGBuildableGeneratorNuclear ReadAsMe(const uintptr_t address)
	{
		FGBuildableGeneratorNuclear ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableGeneratorNuclear& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipeBase
// 0x4280EA00
class FGBuildablePipeBase
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildablePipeBase ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipeBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipeBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableFoundation
// 0x42823A00
class FGBuildableFoundation
{
public:
	unsigned char                                      UnknownData00[0x42823A00];                                // 0x0000(0x42823A00) MISSED OFFSET

	static FGBuildableFoundation ReadAsMe(const uintptr_t address)
	{
		FGBuildableFoundation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableFoundation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipeHyper
// 0x42822C00
class FGBuildablePipeHyper
{
public:
	unsigned char                                      UnknownData00[0x42822C00];                                // 0x0000(0x42822C00) MISSED OFFSET

	static FGBuildablePipeHyper ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipeHyper ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipeHyper& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipelineJunction
// 0x42822600
class FGBuildablePipelineJunction
{
public:
	unsigned char                                      UnknownData00[0x42822600];                                // 0x0000(0x42822600) MISSED OFFSET

	static FGBuildablePipelineJunction ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipelineJunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipelineJunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipelinePump
// 0x42822600
class FGBuildablePipelinePump
{
public:
	unsigned char                                      UnknownData00[0x42822600];                                // 0x0000(0x42822600) MISSED OFFSET

	static FGBuildablePipelinePump ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipelinePump ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipelinePump& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipeline
// 0x42822C00
class FGBuildablePipeline
{
public:
	unsigned char                                      UnknownData00[0x42822C00];                                // 0x0000(0x42822C00) MISSED OFFSET

	static FGBuildablePipeline ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipeline ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipeline& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipelineAttachment
// 0x4280E400
class FGBuildablePipelineAttachment
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildablePipelineAttachment ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipelineAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipelineAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableHubTerminal
// 0x4280EA00
class FGBuildableHubTerminal
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableHubTerminal ReadAsMe(const uintptr_t address)
	{
		FGBuildableHubTerminal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableHubTerminal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipeHyperPart
// 0x42821C00
class FGBuildablePipeHyperPart
{
public:
	unsigned char                                      UnknownData00[0x42821C00];                                // 0x0000(0x42821C00) MISSED OFFSET

	static FGBuildablePipeHyperPart ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipeHyperPart ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipeHyperPart& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePoleBase
// 0x4280EA00
class FGBuildablePoleBase
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildablePoleBase ReadAsMe(const uintptr_t address)
	{
		FGBuildablePoleBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePoleBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipeReservoir
// 0x4280E400
class FGBuildablePipeReservoir
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildablePipeReservoir ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipeReservoir ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipeReservoir& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipelineSupport
// 0x42821E00
class FGBuildablePipelineSupport
{
public:
	unsigned char                                      UnknownData00[0x42821E00];                                // 0x0000(0x42821E00) MISSED OFFSET

	static FGBuildablePipelineSupport ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipelineSupport ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipelineSupport& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePipePart
// 0x4280E400
class FGBuildablePipePart
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildablePipePart ReadAsMe(const uintptr_t address)
	{
		FGBuildablePipePart ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePipePart& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePowerPole
// 0x4280EA00
class FGBuildablePowerPole
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildablePowerPole ReadAsMe(const uintptr_t address)
	{
		FGBuildablePowerPole ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePowerPole& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePole
// 0x42821E00
class FGBuildablePole
{
public:
	unsigned char                                      UnknownData00[0x42821E00];                                // 0x0000(0x42821E00) MISSED OFFSET

	static FGBuildablePole ReadAsMe(const uintptr_t address)
	{
		FGBuildablePole ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePole& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableRadarTower
// 0x4280E400
class FGBuildableRadarTower
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableRadarTower ReadAsMe(const uintptr_t address)
	{
		FGBuildableRadarTower ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableRadarTower& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableWall
// 0x42823A00
class FGBuildableWall
{
public:
	unsigned char                                      UnknownData00[0x42823A00];                                // 0x0000(0x42823A00) MISSED OFFSET

	static FGBuildableWall ReadAsMe(const uintptr_t address)
	{
		FGBuildableWall ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableWall& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildablePoweredWall
// 0x42821200
class FGBuildablePoweredWall
{
public:
	unsigned char                                      UnknownData00[0x42821200];                                // 0x0000(0x42821200) MISSED OFFSET

	static FGBuildablePoweredWall ReadAsMe(const uintptr_t address)
	{
		FGBuildablePoweredWall ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildablePoweredWall& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableRailroadBridge
// 0x4280EA00
class FGBuildableRailroadBridge
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableRailroadBridge ReadAsMe(const uintptr_t address)
	{
		FGBuildableRailroadBridge ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableRailroadBridge& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableRailroadSignal
// 0x4280EA00
class FGBuildableRailroadSignal
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableRailroadSignal ReadAsMe(const uintptr_t address)
	{
		FGBuildableRailroadSignal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableRailroadSignal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableRailroadStation
// 0x42820600
class FGBuildableRailroadStation
{
public:
	unsigned char                                      UnknownData00[0x42820600];                                // 0x0000(0x42820600) MISSED OFFSET

	static FGBuildableRailroadStation ReadAsMe(const uintptr_t address)
	{
		FGBuildableRailroadStation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableRailroadStation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableTrainPlatform
// 0x4280E400
class FGBuildableTrainPlatform
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableTrainPlatform ReadAsMe(const uintptr_t address)
	{
		FGBuildableTrainPlatform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableTrainPlatform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableRamp
// 0x42823800
class FGBuildableRamp
{
public:
	unsigned char                                      UnknownData00[0x42823800];                                // 0x0000(0x42823800) MISSED OFFSET

	static FGBuildableRamp ReadAsMe(const uintptr_t address)
	{
		FGBuildableRamp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableRamp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableResourceExtractor
// 0x4280E400
class FGBuildableResourceExtractor
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableResourceExtractor ReadAsMe(const uintptr_t address)
	{
		FGBuildableResourceExtractor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableResourceExtractor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableRailroadSwitchControl
// 0x4280E400
class FGBuildableRailroadSwitchControl
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableRailroadSwitchControl ReadAsMe(const uintptr_t address)
	{
		FGBuildableRailroadSwitchControl ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableRailroadSwitchControl& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableRailroadTrack
// 0x4280EA00
class FGBuildableRailroadTrack
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableRailroadTrack ReadAsMe(const uintptr_t address)
	{
		FGBuildableRailroadTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableRailroadTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableRoad
// 0x4280EA00
class FGBuildableRoad
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableRoad ReadAsMe(const uintptr_t address)
	{
		FGBuildableRoad ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableRoad& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableSpaceElevator
// 0x4280E400
class FGBuildableSpaceElevator
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableSpaceElevator ReadAsMe(const uintptr_t address)
	{
		FGBuildableSpaceElevator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableSpaceElevator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableSignWall
// 0x42821200
class FGBuildableSignWall
{
public:
	unsigned char                                      UnknownData00[0x42821200];                                // 0x0000(0x42821200) MISSED OFFSET

	static FGBuildableSignWall ReadAsMe(const uintptr_t address)
	{
		FGBuildableSignWall ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableSignWall& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableResourceSinkShop
// 0x4280E400
class FGBuildableResourceSinkShop
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableResourceSinkShop ReadAsMe(const uintptr_t address)
	{
		FGBuildableResourceSinkShop ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableResourceSinkShop& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableSpeedSign
// 0x4280EA00
class FGBuildableSpeedSign
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableSpeedSign ReadAsMe(const uintptr_t address)
	{
		FGBuildableSpeedSign ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableSpeedSign& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableSplitterSmart
// 0x4280E200
class FGBuildableSplitterSmart
{
public:
	unsigned char                                      UnknownData00[0x4280E200];                                // 0x0000(0x4280E200) MISSED OFFSET

	static FGBuildableSplitterSmart ReadAsMe(const uintptr_t address)
	{
		FGBuildableSplitterSmart ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableSplitterSmart& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableResourceSink
// 0x4280E400
class FGBuildableResourceSink
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableResourceSink ReadAsMe(const uintptr_t address)
	{
		FGBuildableResourceSink ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableResourceSink& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableStair
// 0x42823800
class FGBuildableStair
{
public:
	unsigned char                                      UnknownData00[0x42823800];                                // 0x0000(0x42823800) MISSED OFFSET

	static FGBuildableStair ReadAsMe(const uintptr_t address)
	{
		FGBuildableStair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableStair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableStorage
// 0x4280E400
class FGBuildableStorage
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableStorage ReadAsMe(const uintptr_t address)
	{
		FGBuildableStorage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableStorage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableSubsystem
// 0x42804400
class FGBuildableSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGBuildableSubsystem ReadAsMe(const uintptr_t address)
	{
		FGBuildableSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableTradingPost
// 0x4280E400
class FGBuildableTradingPost
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableTradingPost ReadAsMe(const uintptr_t address)
	{
		FGBuildableTradingPost ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableTradingPost& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableTrainPlatformCargo
// 0x42820600
class FGBuildableTrainPlatformCargo
{
public:
	unsigned char                                      UnknownData00[0x42820600];                                // 0x0000(0x42820600) MISSED OFFSET

	static FGBuildableTrainPlatformCargo ReadAsMe(const uintptr_t address)
	{
		FGBuildableTrainPlatformCargo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableTrainPlatformCargo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableWalkway
// 0x42823A00
class FGBuildableWalkway
{
public:
	unsigned char                                      UnknownData00[0x42823A00];                                // 0x0000(0x42823A00) MISSED OFFSET

	static FGBuildableWalkway ReadAsMe(const uintptr_t address)
	{
		FGBuildableWalkway ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableWalkway& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableStandaloneSign
// 0x4280EA00
class FGBuildableStandaloneSign
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableStandaloneSign ReadAsMe(const uintptr_t address)
	{
		FGBuildableStandaloneSign ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableStandaloneSign& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableWindTurbine
// 0x4280E400
class FGBuildableWindTurbine
{
public:
	unsigned char                                      UnknownData00[0x4280E400];                                // 0x0000(0x4280E400) MISSED OFFSET

	static FGBuildableWindTurbine ReadAsMe(const uintptr_t address)
	{
		FGBuildableWindTurbine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableWindTurbine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableWire
// 0x4280EA00
class FGBuildableWire
{
public:
	unsigned char                                      UnknownData00[0x4280EA00];                                // 0x0000(0x4280EA00) MISSED OFFSET

	static FGBuildableWire ReadAsMe(const uintptr_t address)
	{
		FGBuildableWire ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableWire& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildCategory
// 0x40FE0C00
class FGBuildCategory
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGBuildCategory ReadAsMe(const uintptr_t address)
	{
		FGBuildCategory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildCategory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildableTrainPlatformEmpty
// 0x42820600
class FGBuildableTrainPlatformEmpty
{
public:
	unsigned char                                      UnknownData00[0x42820600];                                // 0x0000(0x42820600) MISSED OFFSET

	static FGBuildableTrainPlatformEmpty ReadAsMe(const uintptr_t address)
	{
		FGBuildableTrainPlatformEmpty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildableTrainPlatformEmpty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildDescriptor
// 0x4280A600
class FGBuildDescriptor
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGBuildDescriptor ReadAsMe(const uintptr_t address)
	{
		FGBuildDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildEffectSpline
// 0x40FE8000
class FGBuildEffectSpline
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGBuildEffectSpline ReadAsMe(const uintptr_t address)
	{
		FGBuildEffectSpline ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildEffectSpline& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildGuide
// 0x40FE8000
class FGBuildGuide
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGBuildGuide ReadAsMe(const uintptr_t address)
	{
		FGBuildGuide ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildGuide& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEquipment
// 0x40FE8000
class FGEquipment
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGEquipment ReadAsMe(const uintptr_t address)
	{
		FGEquipment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEquipment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEquipmentAttachment
// 0x40FE8000
class FGEquipmentAttachment
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGEquipmentAttachment ReadAsMe(const uintptr_t address)
	{
		FGEquipmentAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEquipmentAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildGunAttachment
// 0x42824E00
class FGBuildGunAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGBuildGunAttachment ReadAsMe(const uintptr_t address)
	{
		FGBuildGunAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildGunAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildGunStateDismantle
// 0x42825600
class FGBuildGunStateDismantle
{
public:
	unsigned char                                      UnknownData00[0x42825600];                                // 0x0000(0x42825600) MISSED OFFSET

	static FGBuildGunStateDismantle ReadAsMe(const uintptr_t address)
	{
		FGBuildGunStateDismantle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildGunStateDismantle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildGunState
// 0x40FE0C00
class FGBuildGunState
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGBuildGunState ReadAsMe(const uintptr_t address)
	{
		FGBuildGunState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildGunState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildSubCategory
// 0x42825E00
class FGBuildSubCategory
{
public:
	unsigned char                                      UnknownData00[0x42825E00];                                // 0x0000(0x42825E00) MISSED OFFSET

	static FGBuildSubCategory ReadAsMe(const uintptr_t address)
	{
		FGBuildSubCategory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildSubCategory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGButtonWidget
// 0x4280FC00
class FGButtonWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGButtonWidget ReadAsMe(const uintptr_t address)
	{
		FGButtonWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGButtonWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWeapon
// 0x42825200
class FGWeapon
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGWeapon ReadAsMe(const uintptr_t address)
	{
		FGWeapon ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWeapon& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildGunStateBuild
// 0x42825600
class FGBuildGunStateBuild
{
public:
	unsigned char                                      UnknownData00[0x42825600];                                // 0x0000(0x42825600) MISSED OFFSET

	static FGBuildGunStateBuild ReadAsMe(const uintptr_t address)
	{
		FGBuildGunStateBuild ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildGunStateBuild& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGC4Explosive
// 0x40FE8000
class FGC4Explosive
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGC4Explosive ReadAsMe(const uintptr_t address)
	{
		FGC4Explosive ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGC4Explosive& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildGun
// 0x42825200
class FGBuildGun
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGBuildGun ReadAsMe(const uintptr_t address)
	{
		FGBuildGun ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildGun& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGBuildingDescriptor
// 0x42825C00
class FGBuildingDescriptor
{
public:
	unsigned char                                      UnknownData00[0x42825C00];                                // 0x0000(0x42825C00) MISSED OFFSET

	static FGBuildingDescriptor ReadAsMe(const uintptr_t address)
	{
		FGBuildingDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGBuildingDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCameraModifierLimitLook
// 0x427E2C00
class FGCameraModifierLimitLook
{
public:
	unsigned char                                      UnknownData00[0x427E2C00];                                // 0x0000(0x427E2C00) MISSED OFFSET

	static FGCameraModifierLimitLook ReadAsMe(const uintptr_t address)
	{
		FGCameraModifierLimitLook ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCameraModifierLimitLook& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCentralStorageContainer
// 0x42826E00
class FGCentralStorageContainer
{
public:
	unsigned char                                      UnknownData00[0x42826E00];                                // 0x0000(0x42826E00) MISSED OFFSET

	static FGCentralStorageContainer ReadAsMe(const uintptr_t address)
	{
		FGCentralStorageContainer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCentralStorageContainer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGC4Dispenser
// 0x4282C400
class FGC4Dispenser
{
public:
	unsigned char                                      UnknownData00[0x4282C400];                                // 0x0000(0x4282C400) MISSED OFFSET

	static FGC4Dispenser ReadAsMe(const uintptr_t address)
	{
		FGC4Dispenser ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGC4Dispenser& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCameraModifierSlide
// 0x427E2C00
class FGCameraModifierSlide
{
public:
	unsigned char                                      UnknownData00[0x427E2C00];                                // 0x0000(0x427E2C00) MISSED OFFSET

	static FGCameraModifierSlide ReadAsMe(const uintptr_t address)
	{
		FGCameraModifierSlide ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCameraModifierSlide& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCentralStorageSubsystem
// 0x42804400
class FGCentralStorageSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGCentralStorageSubsystem ReadAsMe(const uintptr_t address)
	{
		FGCentralStorageSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCentralStorageSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGChainsaw
// 0x42825200
class FGChainsaw
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGChainsaw ReadAsMe(const uintptr_t address)
	{
		FGChainsaw ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGChainsaw& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCharacterBase
// 0x427E5A00
class FGCharacterBase
{
public:
	unsigned char                                      UnknownData00[0x427E5A00];                                // 0x0000(0x427E5A00) MISSED OFFSET

	static FGCharacterBase ReadAsMe(const uintptr_t address)
	{
		FGCharacterBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCharacterBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCharacterMovementComponent
// 0x4282B000
class FGCharacterMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x4282B000];                                // 0x0000(0x4282B000) MISSED OFFSET

	static FGCharacterMovementComponent ReadAsMe(const uintptr_t address)
	{
		FGCharacterMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCharacterMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_ReviveInvalid_PlayerNotDead
// 0x4280CC00
class FGUseState_ReviveInvalid_PlayerNotDead
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_ReviveInvalid_PlayerNotDead ReadAsMe(const uintptr_t address)
	{
		FGUseState_ReviveInvalid_PlayerNotDead ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_ReviveInvalid_PlayerNotDead& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGChatManager
// 0x42804400
class FGChatManager
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGChatManager ReadAsMe(const uintptr_t address)
	{
		FGChatManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGChatManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGChainsawableInterface
// 0x427D4600
class FGChainsawableInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGChainsawableInterface ReadAsMe(const uintptr_t address)
	{
		FGChainsawableInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGChainsawableInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCharacterPlayer
// 0x4282B400
class FGCharacterPlayer
{
public:
	unsigned char                                      UnknownData00[0x4282B400];                                // 0x0000(0x4282B400) MISSED OFFSET

	static FGCharacterPlayer ReadAsMe(const uintptr_t address)
	{
		FGCharacterPlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCharacterPlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_ReviveValid
// 0x4280CC00
class FGUseState_ReviveValid
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_ReviveValid ReadAsMe(const uintptr_t address)
	{
		FGUseState_ReviveValid ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_ReviveValid& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCheatManager
// 0x4282A000
class FGCheatManager
{
public:
	unsigned char                                      UnknownData00[0x4282A000];                                // 0x0000(0x4282A000) MISSED OFFSET

	static FGCheatManager ReadAsMe(const uintptr_t address)
	{
		FGCheatManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCheatManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCircuitConnectionComponent
// 0x42829A00
class FGCircuitConnectionComponent
{
public:
	unsigned char                                      UnknownData00[0x42829A00];                                // 0x0000(0x42829A00) MISSED OFFSET

	static FGCircuitConnectionComponent ReadAsMe(const uintptr_t address)
	{
		FGCircuitConnectionComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCircuitConnectionComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCircuitSubsystem
// 0x42804400
class FGCircuitSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGCircuitSubsystem ReadAsMe(const uintptr_t address)
	{
		FGCircuitSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCircuitSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCircuit
// 0x40FE0C00
class FGCircuit
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGCircuit ReadAsMe(const uintptr_t address)
	{
		FGCircuit ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCircuit& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConnectionComponent
// 0x40FEF800
class FGConnectionComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGConnectionComponent ReadAsMe(const uintptr_t address)
	{
		FGConnectionComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConnectionComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCollectionParamUniformFloat
// 0x42829400
class FGCollectionParamUniformFloat
{
public:
	unsigned char                                      UnknownData00[0x42829400];                                // 0x0000(0x42829400) MISSED OFFSET

	static FGCollectionParamUniformFloat ReadAsMe(const uintptr_t address)
	{
		FGCollectionParamUniformFloat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCollectionParamUniformFloat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGColoredInstanceManager
// 0x40FEF800
class FGColoredInstanceManager
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGColoredInstanceManager ReadAsMe(const uintptr_t address)
	{
		FGColoredInstanceManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGColoredInstanceManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGColoredInstanceMeshProxy
// 0x42828C00
class FGColoredInstanceMeshProxy
{
public:
	unsigned char                                      UnknownData00[0x42828C00];                                // 0x0000(0x42828C00) MISSED OFFSET

	static FGColoredInstanceMeshProxy ReadAsMe(const uintptr_t address)
	{
		FGColoredInstanceMeshProxy ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGColoredInstanceMeshProxy& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGColorInterface
// 0x427D4600
class FGColorInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGColorInterface ReadAsMe(const uintptr_t address)
	{
		FGColorInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGColorInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCombatFunctionLibrary
// 0x40FEE400
class FGCombatFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGCombatFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		FGCombatFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCombatFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCompassWidget
// 0x4280FC00
class FGCompassWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGCompassWidget ReadAsMe(const uintptr_t address)
	{
		FGCompassWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCompassWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPlayerController
// 0x4282F400
class FGPlayerController
{
public:
	unsigned char                                      UnknownData00[0x4282F400];                                // 0x0000(0x4282F400) MISSED OFFSET

	static FGPlayerController ReadAsMe(const uintptr_t address)
	{
		FGPlayerController ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPlayerController& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCompassObjectWidget
// 0x4280FC00
class FGCompassObjectWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGCompassObjectWidget ReadAsMe(const uintptr_t address)
	{
		FGCompassObjectWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCompassObjectWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWeaponInstantFire
// 0x4282C400
class FGWeaponInstantFire
{
public:
	unsigned char                                      UnknownData00[0x4282C400];                                // 0x0000(0x4282C400) MISSED OFFSET

	static FGWeaponInstantFire ReadAsMe(const uintptr_t address)
	{
		FGWeaponInstantFire ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWeaponInstantFire& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPlayerControllerBase
// 0x4282A400
class FGPlayerControllerBase
{
public:
	unsigned char                                      UnknownData00[0x4282A400];                                // 0x0000(0x4282A400) MISSED OFFSET

	static FGPlayerControllerBase ReadAsMe(const uintptr_t address)
	{
		FGPlayerControllerBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPlayerControllerBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConstructDisqualifier
// 0x40FE0C00
class FGConstructDisqualifier
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGConstructDisqualifier ReadAsMe(const uintptr_t address)
	{
		FGConstructDisqualifier ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConstructDisqualifier& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConsoleCommandManager
// 0x40FE0C00
class FGConsoleCommandManager
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGConsoleCommandManager ReadAsMe(const uintptr_t address)
	{
		FGConsoleCommandManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConsoleCommandManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDInitializing
// 0x4282F200
class FGCDInitializing
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDInitializing ReadAsMe(const uintptr_t address)
	{
		FGCDInitializing ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDInitializing& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDEncroachingClearance
// 0x4282F200
class FGCDEncroachingClearance
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDEncroachingClearance ReadAsMe(const uintptr_t address)
	{
		FGCDEncroachingClearance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDEncroachingClearance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDInvalidAimLocation
// 0x4282F200
class FGCDInvalidAimLocation
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDInvalidAimLocation ReadAsMe(const uintptr_t address)
	{
		FGCDInvalidAimLocation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDInvalidAimLocation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDUniqueBuilding
// 0x4282F200
class FGCDUniqueBuilding
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDUniqueBuilding ReadAsMe(const uintptr_t address)
	{
		FGCDUniqueBuilding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDUniqueBuilding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDInvalidFloor
// 0x4282F200
class FGCDInvalidFloor
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDInvalidFloor ReadAsMe(const uintptr_t address)
	{
		FGCDInvalidFloor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDInvalidFloor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDEncroachingPlayer
// 0x4282F200
class FGCDEncroachingPlayer
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDEncroachingPlayer ReadAsMe(const uintptr_t address)
	{
		FGCDEncroachingPlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDEncroachingPlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDMustSnap
// 0x4282F200
class FGCDMustSnap
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDMustSnap ReadAsMe(const uintptr_t address)
	{
		FGCDMustSnap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDMustSnap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDNeedsResourceNode
// 0x4282F200
class FGCDNeedsResourceNode
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDNeedsResourceNode ReadAsMe(const uintptr_t address)
	{
		FGCDNeedsResourceNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDNeedsResourceNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDUnaffordable
// 0x4282F200
class FGCDUnaffordable
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDUnaffordable ReadAsMe(const uintptr_t address)
	{
		FGCDUnaffordable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDUnaffordable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDInvalidPlacement
// 0x4282F200
class FGCDInvalidPlacement
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDInvalidPlacement ReadAsMe(const uintptr_t address)
	{
		FGCDInvalidPlacement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDInvalidPlacement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDNeedsWaterVolume
// 0x4282F200
class FGCDNeedsWaterVolume
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDNeedsWaterVolume ReadAsMe(const uintptr_t address)
	{
		FGCDNeedsWaterVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDNeedsWaterVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDShouldntSnap
// 0x4282F200
class FGCDShouldntSnap
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDShouldntSnap ReadAsMe(const uintptr_t address)
	{
		FGCDShouldntSnap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDShouldntSnap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDResourceDeposit
// 0x4282F200
class FGCDResourceDeposit
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDResourceDeposit ReadAsMe(const uintptr_t address)
	{
		FGCDResourceDeposit ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDResourceDeposit& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDResourceIsTooShallow
// 0x4282F200
class FGCDResourceIsTooShallow
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDResourceIsTooShallow ReadAsMe(const uintptr_t address)
	{
		FGCDResourceIsTooShallow ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDResourceIsTooShallow& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDWireSnap
// 0x4282F200
class FGCDWireSnap
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDWireSnap ReadAsMe(const uintptr_t address)
	{
		FGCDWireSnap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDWireSnap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDWireTooLong
// 0x4282F200
class FGCDWireTooLong
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDWireTooLong ReadAsMe(const uintptr_t address)
	{
		FGCDWireTooLong ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDWireTooLong& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDResourceNodeIsOccuped
// 0x4282F200
class FGCDResourceNodeIsOccuped
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDResourceNodeIsOccuped ReadAsMe(const uintptr_t address)
	{
		FGCDResourceNodeIsOccuped ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDResourceNodeIsOccuped& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDWireTooManyConnections
// 0x4282F200
class FGCDWireTooManyConnections
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDWireTooManyConnections ReadAsMe(const uintptr_t address)
	{
		FGCDWireTooManyConnections ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDWireTooManyConnections& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDBeltMustSnap
// 0x4282F200
class FGCDBeltMustSnap
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDBeltMustSnap ReadAsMe(const uintptr_t address)
	{
		FGCDBeltMustSnap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDBeltMustSnap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGColorGun
// 0x42828800
class FGColorGun
{
public:
	unsigned char                                      UnknownData00[0x42828800];                                // 0x0000(0x42828800) MISSED OFFSET

	static FGColorGun ReadAsMe(const uintptr_t address)
	{
		FGColorGun ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGColorGun& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDConveyorTooShort
// 0x4282F200
class FGCDConveyorTooShort
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDConveyorTooShort ReadAsMe(const uintptr_t address)
	{
		FGCDConveyorTooShort ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDConveyorTooShort& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDConveyorInvalidShape
// 0x4282F200
class FGCDConveyorInvalidShape
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDConveyorInvalidShape ReadAsMe(const uintptr_t address)
	{
		FGCDConveyorInvalidShape ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDConveyorInvalidShape& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDMustHaveRailRoadTrack
// 0x4282F200
class FGCDMustHaveRailRoadTrack
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDMustHaveRailRoadTrack ReadAsMe(const uintptr_t address)
	{
		FGCDMustHaveRailRoadTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDMustHaveRailRoadTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDConveyorAttachmentTooSharpTurn
// 0x4282F200
class FGCDConveyorAttachmentTooSharpTurn
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDConveyorAttachmentTooSharpTurn ReadAsMe(const uintptr_t address)
	{
		FGCDConveyorAttachmentTooSharpTurn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDConveyorAttachmentTooSharpTurn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDTrackTooLong
// 0x4282F200
class FGCDTrackTooLong
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDTrackTooLong ReadAsMe(const uintptr_t address)
	{
		FGCDTrackTooLong ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDTrackTooLong& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDConveyorTooSteep
// 0x4282F200
class FGCDConveyorTooSteep
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDConveyorTooSteep ReadAsMe(const uintptr_t address)
	{
		FGCDConveyorTooSteep ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDConveyorTooSteep& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDConveyorTooLong
// 0x4282F200
class FGCDConveyorTooLong
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDConveyorTooLong ReadAsMe(const uintptr_t address)
	{
		FGCDConveyorTooLong ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDConveyorTooLong& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDMustAttachToTrainPlatform
// 0x4282F200
class FGCDMustAttachToTrainPlatform
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDMustAttachToTrainPlatform ReadAsMe(const uintptr_t address)
	{
		FGCDMustAttachToTrainPlatform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDMustAttachToTrainPlatform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDTrackTooShort
// 0x4282F200
class FGCDTrackTooShort
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDTrackTooShort ReadAsMe(const uintptr_t address)
	{
		FGCDTrackTooShort ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDTrackTooShort& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDPipeTooShort
// 0x4282F200
class FGCDPipeTooShort
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDPipeTooShort ReadAsMe(const uintptr_t address)
	{
		FGCDPipeTooShort ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDPipeTooShort& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDTrackTooSteep
// 0x4282F200
class FGCDTrackTooSteep
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDTrackTooSteep ReadAsMe(const uintptr_t address)
	{
		FGCDTrackTooSteep ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDTrackTooSteep& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDTrackTrunToSharp
// 0x4282F200
class FGCDTrackTrunToSharp
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDTrackTrunToSharp ReadAsMe(const uintptr_t address)
	{
		FGCDTrackTrunToSharp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDTrackTrunToSharp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDPipeTooLong
// 0x4282F200
class FGCDPipeTooLong
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDPipeTooLong ReadAsMe(const uintptr_t address)
	{
		FGCDPipeTooLong ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDPipeTooLong& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDPipeAttachmentTooSharpTurn
// 0x4282F200
class FGCDPipeAttachmentTooSharpTurn
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDPipeAttachmentTooSharpTurn ReadAsMe(const uintptr_t address)
	{
		FGCDPipeAttachmentTooSharpTurn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDPipeAttachmentTooSharpTurn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDPipeMustSnap
// 0x4282F200
class FGCDPipeMustSnap
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDPipeMustSnap ReadAsMe(const uintptr_t address)
	{
		FGCDPipeMustSnap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDPipeMustSnap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDPipeFluidTypeMismatch
// 0x4282F200
class FGCDPipeFluidTypeMismatch
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDPipeFluidTypeMismatch ReadAsMe(const uintptr_t address)
	{
		FGCDPipeFluidTypeMismatch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDPipeFluidTypeMismatch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConstructionMessageInterface
// 0x427D4600
class FGConstructionMessageInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGConstructionMessageInterface ReadAsMe(const uintptr_t address)
	{
		FGConstructionMessageInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConstructionMessageInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEquipmentDescriptor
// 0x4280A600
class FGEquipmentDescriptor
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGEquipmentDescriptor ReadAsMe(const uintptr_t address)
	{
		FGEquipmentDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEquipmentDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDPipeNoPathFound
// 0x4282F200
class FGCDPipeNoPathFound
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDPipeNoPathFound ReadAsMe(const uintptr_t address)
	{
		FGCDPipeNoPathFound ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDPipeNoPathFound& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCDPipeInvalidShape
// 0x4282F200
class FGCDPipeInvalidShape
{
public:
	unsigned char                                      UnknownData00[0x4282F200];                                // 0x0000(0x4282F200) MISSED OFFSET

	static FGCDPipeInvalidShape ReadAsMe(const uintptr_t address)
	{
		FGCDPipeInvalidShape ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCDPipeInvalidShape& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSplineHologram
// 0x42809600
class FGSplineHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGSplineHologram ReadAsMe(const uintptr_t address)
	{
		FGSplineHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSplineHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConveyorBeltHologram
// 0x42841800
class FGConveyorBeltHologram
{
public:
	unsigned char                                      UnknownData00[0x42841800];                                // 0x0000(0x42841800) MISSED OFFSET

	static FGConveyorBeltHologram ReadAsMe(const uintptr_t address)
	{
		FGConveyorBeltHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConveyorBeltHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConsumableDescriptor
// 0x42841E00
class FGConsumableDescriptor
{
public:
	unsigned char                                      UnknownData00[0x42841E00];                                // 0x0000(0x42841E00) MISSED OFFSET

	static FGConsumableDescriptor ReadAsMe(const uintptr_t address)
	{
		FGConsumableDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConsumableDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConveyorLiftHologram
// 0x42841800
class FGConveyorLiftHologram
{
public:
	unsigned char                                      UnknownData00[0x42841800];                                // 0x0000(0x42841800) MISSED OFFSET

	static FGConveyorLiftHologram ReadAsMe(const uintptr_t address)
	{
		FGConveyorLiftHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConveyorLiftHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConsumableEquipment
// 0x42825200
class FGConsumableEquipment
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGConsumableEquipment ReadAsMe(const uintptr_t address)
	{
		FGConsumableEquipment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConsumableEquipment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPoleHologram
// 0x42809800
class FGPoleHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGPoleHologram ReadAsMe(const uintptr_t address)
	{
		FGPoleHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPoleHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCreature
// 0x4282B400
class FGCreature
{
public:
	unsigned char                                      UnknownData00[0x4282B400];                                // 0x0000(0x4282B400) MISSED OFFSET

	static FGCreature ReadAsMe(const uintptr_t address)
	{
		FGCreature ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCreature& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEnemy
// 0x42840800
class FGEnemy
{
public:
	unsigned char                                      UnknownData00[0x42840800];                                // 0x0000(0x42840800) MISSED OFFSET

	static FGEnemy ReadAsMe(const uintptr_t address)
	{
		FGEnemy ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEnemy& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCrabHatcher
// 0x42840A00
class FGCrabHatcher
{
public:
	unsigned char                                      UnknownData00[0x42840A00];                                // 0x0000(0x42840A00) MISSED OFFSET

	static FGCrabHatcher ReadAsMe(const uintptr_t address)
	{
		FGCrabHatcher ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCrabHatcher& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCrashSiteDebris
// 0x40FE8000
class FGCrashSiteDebris
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGCrashSiteDebris ReadAsMe(const uintptr_t address)
	{
		FGCrashSiteDebris ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCrashSiteDebris& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConveyorMultiPoleHologram
// 0x42809600
class FGConveyorMultiPoleHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGConveyorMultiPoleHologram ReadAsMe(const uintptr_t address)
	{
		FGConveyorMultiPoleHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConveyorMultiPoleHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConveyorPoleStackable
// 0x42821600
class FGConveyorPoleStackable
{
public:
	unsigned char                                      UnknownData00[0x42821600];                                // 0x0000(0x42821600) MISSED OFFSET

	static FGConveyorPoleStackable ReadAsMe(const uintptr_t address)
	{
		FGConveyorPoleStackable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConveyorPoleStackable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCrate
// 0x42848400
class FGCrate
{
public:
	unsigned char                                      UnknownData00[0x42848400];                                // 0x0000(0x42848400) MISSED OFFSET

	static FGCrate ReadAsMe(const uintptr_t address)
	{
		FGCrate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCrate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInteractActor
// 0x40FE8000
class FGInteractActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGInteractActor ReadAsMe(const uintptr_t address)
	{
		FGInteractActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInteractActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDriveablePawn
// 0x427E5800
class FGDriveablePawn
{
public:
	unsigned char                                      UnknownData00[0x427E5800];                                // 0x0000(0x427E5800) MISSED OFFSET

	static FGDriveablePawn ReadAsMe(const uintptr_t address)
	{
		FGDriveablePawn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDriveablePawn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCrashSiteDebrisActor
// 0x40FE8000
class FGCrashSiteDebrisActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGCrashSiteDebrisActor ReadAsMe(const uintptr_t address)
	{
		FGCrashSiteDebrisActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCrashSiteDebrisActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCreatureSeat
// 0x42847C00
class FGCreatureSeat
{
public:
	unsigned char                                      UnknownData00[0x42847C00];                                // 0x0000(0x42847C00) MISSED OFFSET

	static FGCreatureSeat ReadAsMe(const uintptr_t address)
	{
		FGCreatureSeat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCreatureSeat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCreatureSpawner
// 0x40FE8000
class FGCreatureSpawner
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGCreatureSpawner ReadAsMe(const uintptr_t address)
	{
		FGCreatureSpawner ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCreatureSpawner& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGConveyorPoleHologram
// 0x42841000
class FGConveyorPoleHologram
{
public:
	unsigned char                                      UnknownData00[0x42841000];                                // 0x0000(0x42841000) MISSED OFFSET

	static FGConveyorPoleHologram ReadAsMe(const uintptr_t address)
	{
		FGConveyorPoleHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGConveyorPoleHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDamageOverTime
// 0x40FE0C00
class FGDamageOverTime
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGDamageOverTime ReadAsMe(const uintptr_t address)
	{
		FGDamageOverTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDamageOverTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCreatureSpawnerDebugComponent
// 0x40FEF600
class FGCreatureSpawnerDebugComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static FGCreatureSpawnerDebugComponent ReadAsMe(const uintptr_t address)
	{
		FGCreatureSpawnerDebugComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCreatureSpawnerDebugComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDamageType
// 0x42847000
class FGDamageType
{
public:
	unsigned char                                      UnknownData00[0x42847000];                                // 0x0000(0x42847000) MISSED OFFSET

	static FGDamageType ReadAsMe(const uintptr_t address)
	{
		FGDamageType ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDamageType& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDecorationDescriptor
// 0x42841E00
class FGDecorationDescriptor
{
public:
	unsigned char                                      UnknownData00[0x42841E00];                                // 0x0000(0x42841E00) MISSED OFFSET

	static FGDecorationDescriptor ReadAsMe(const uintptr_t address)
	{
		FGDecorationDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDecorationDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGCreatureController
// 0x42848000
class FGCreatureController
{
public:
	unsigned char                                      UnknownData00[0x42848000];                                // 0x0000(0x42848000) MISSED OFFSET

	static FGCreatureController ReadAsMe(const uintptr_t address)
	{
		FGCreatureController ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGCreatureController& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDecorHologram
// 0x42809600
class FGDecorHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGDecorHologram ReadAsMe(const uintptr_t address)
	{
		FGDecorHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDecorHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDecorationActor
// 0x42848400
class FGDecorationActor
{
public:
	unsigned char                                      UnknownData00[0x42848400];                                // 0x0000(0x42848400) MISSED OFFSET

	static FGDecorationActor ReadAsMe(const uintptr_t address)
	{
		FGDecorationActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDecorationActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDestructibleActor
// 0x427D1200
class FGDestructibleActor
{
public:
	unsigned char                                      UnknownData00[0x427D1200];                                // 0x0000(0x427D1200) MISSED OFFSET

	static FGDestructibleActor ReadAsMe(const uintptr_t address)
	{
		FGDestructibleActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDestructibleActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDamageOverTimeVolume
// 0x40FEC600
class FGDamageOverTimeVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static FGDamageOverTimeVolume ReadAsMe(const uintptr_t address)
	{
		FGDamageOverTimeVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDamageOverTimeVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDecorDescriptor
// 0x42824800
class FGDecorDescriptor
{
public:
	unsigned char                                      UnknownData00[0x42824800];                                // 0x0000(0x42824800) MISSED OFFSET

	static FGDecorDescriptor ReadAsMe(const uintptr_t address)
	{
		FGDecorDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDecorDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDestructiveProjectile
// 0x42846200
class FGDestructiveProjectile
{
public:
	unsigned char                                      UnknownData00[0x42846200];                                // 0x0000(0x42846200) MISSED OFFSET

	static FGDestructiveProjectile ReadAsMe(const uintptr_t address)
	{
		FGDestructiveProjectile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDestructiveProjectile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDotComponent
// 0x40FEF800
class FGDotComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGDotComponent ReadAsMe(const uintptr_t address)
	{
		FGDotComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDotComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGProjectile
// 0x40FE8000
class FGProjectile
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGProjectile ReadAsMe(const uintptr_t address)
	{
		FGProjectile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGProjectile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDowsingStick
// 0x42825200
class FGDowsingStick
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGDowsingStick ReadAsMe(const uintptr_t address)
	{
		FGDowsingStick ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDowsingStick& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDismantleInterface
// 0x427D4600
class FGDismantleInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGDismantleInterface ReadAsMe(const uintptr_t address)
	{
		FGDismantleInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDismantleInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDowsingStickAttachment
// 0x42824E00
class FGDowsingStickAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGDowsingStickAttachment ReadAsMe(const uintptr_t address)
	{
		FGDowsingStickAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDowsingStickAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDynamicOptionsRow
// 0x4280FC00
class FGDynamicOptionsRow
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGDynamicOptionsRow ReadAsMe(const uintptr_t address)
	{
		FGDynamicOptionsRow ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDynamicOptionsRow& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDockableInterface
// 0x427D4600
class FGDockableInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGDockableInterface ReadAsMe(const uintptr_t address)
	{
		FGDockableInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDockableInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDropPod
// 0x40FE8000
class FGDropPod
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGDropPod ReadAsMe(const uintptr_t address)
	{
		FGDropPod ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDropPod& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGDropPodSettings
// 0x42845200
class FGDropPodSettings
{
public:
	unsigned char                                      UnknownData00[0x42845200];                                // 0x0000(0x42845200) MISSED OFFSET

	static FGDropPodSettings ReadAsMe(const uintptr_t address)
	{
		FGDropPodSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGDropPodSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEditableText
// 0x42844C00
class FGEditableText
{
public:
	unsigned char                                      UnknownData00[0x42844C00];                                // 0x0000(0x42844C00) MISSED OFFSET

	static FGEditableText ReadAsMe(const uintptr_t address)
	{
		FGEditableText ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEditableText& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEnemyController
// 0x42848200
class FGEnemyController
{
public:
	unsigned char                                      UnknownData00[0x42848200];                                // 0x0000(0x42848200) MISSED OFFSET

	static FGEnemyController ReadAsMe(const uintptr_t address)
	{
		FGEnemyController ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEnemyController& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEnvironmentSettings
// 0x42845200
class FGEnvironmentSettings
{
public:
	unsigned char                                      UnknownData00[0x42845200];                                // 0x0000(0x42845200) MISSED OFFSET

	static FGEnvironmentSettings ReadAsMe(const uintptr_t address)
	{
		FGEnvironmentSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEnvironmentSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSettings
// 0x40FE0C00
class FGSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGSettings ReadAsMe(const uintptr_t address)
	{
		FGSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEquipmentChild
// 0x40FE8000
class FGEquipmentChild
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGEquipmentChild ReadAsMe(const uintptr_t address)
	{
		FGEquipmentChild ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEquipmentChild& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEnvQueryGenerator_ForAngle
// 0x42844400
class FGEnvQueryGenerator_ForAngle
{
public:
	unsigned char                                      UnknownData00[0x42844400];                                // 0x0000(0x42844400) MISSED OFFSET

	static FGEnvQueryGenerator_ForAngle ReadAsMe(const uintptr_t address)
	{
		FGEnvQueryGenerator_ForAngle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEnvQueryGenerator_ForAngle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEquipmentStunSpear
// 0x42825200
class FGEquipmentStunSpear
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGEquipmentStunSpear ReadAsMe(const uintptr_t address)
	{
		FGEquipmentStunSpear ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEquipmentStunSpear& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGExplosiveDestroyableInterface
// 0x427D4600
class FGExplosiveDestroyableInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGExplosiveDestroyableInterface ReadAsMe(const uintptr_t address)
	{
		FGExplosiveDestroyableInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGExplosiveDestroyableInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGErrorMessage
// 0x40FE0C00
class FGErrorMessage
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGErrorMessage ReadAsMe(const uintptr_t address)
	{
		FGErrorMessage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGErrorMessage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGExtractableResourceInterface
// 0x427D4600
class FGExtractableResourceInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGExtractableResourceInterface ReadAsMe(const uintptr_t address)
	{
		FGExtractableResourceInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGExtractableResourceInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEquipmentDecoration
// 0x42825200
class FGEquipmentDecoration
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGEquipmentDecoration ReadAsMe(const uintptr_t address)
	{
		FGEquipmentDecoration ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEquipmentDecoration& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEnvQueryTest_ItemDescription
// 0x4284C000
class FGEnvQueryTest_ItemDescription
{
public:
	unsigned char                                      UnknownData00[0x4284C000];                                // 0x0000(0x4284C000) MISSED OFFSET

	static FGEnvQueryTest_ItemDescription ReadAsMe(const uintptr_t address)
	{
		FGEnvQueryTest_ItemDescription ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEnvQueryTest_ItemDescription& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFactoryMaterialInstanceManager
// 0x40FE0C00
class FGFactoryMaterialInstanceManager
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGFactoryMaterialInstanceManager ReadAsMe(const uintptr_t address)
	{
		FGFactoryMaterialInstanceManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFactoryMaterialInstanceManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFactoryBuildingHologram
// 0x42809600
class FGFactoryBuildingHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGFactoryBuildingHologram ReadAsMe(const uintptr_t address)
	{
		FGFactoryBuildingHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFactoryBuildingHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFactorySettings
// 0x42845200
class FGFactorySettings
{
public:
	unsigned char                                      UnknownData00[0x42845200];                                // 0x0000(0x42845200) MISSED OFFSET

	static FGFactorySettings ReadAsMe(const uintptr_t address)
	{
		FGFactorySettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFactorySettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFAnimInstanceFactory
// 0x427EA800
class FGFAnimInstanceFactory
{
public:
	unsigned char                                      UnknownData00[0x427EA800];                                // 0x0000(0x427EA800) MISSED OFFSET

	static FGFAnimInstanceFactory ReadAsMe(const uintptr_t address)
	{
		FGFAnimInstanceFactory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFAnimInstanceFactory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFluffActor
// 0x40FE8000
class FGFluffActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGFluffActor ReadAsMe(const uintptr_t address)
	{
		FGFluffActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFluffActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFactoryConnectionComponent
// 0x42829A00
class FGFactoryConnectionComponent
{
public:
	unsigned char                                      UnknownData00[0x42829A00];                                // 0x0000(0x42829A00) MISSED OFFSET

	static FGFactoryConnectionComponent ReadAsMe(const uintptr_t address)
	{
		FGFactoryConnectionComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFactoryConnectionComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFactoryLegsComponent
// 0x40FEF800
class FGFactoryLegsComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGFactoryLegsComponent ReadAsMe(const uintptr_t address)
	{
		FGFactoryLegsComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFactoryLegsComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageIdentifier_Pickupable
// 0x4284A000
class FGFoliageIdentifier_Pickupable
{
public:
	unsigned char                                      UnknownData00[0x4284A000];                                // 0x0000(0x4284A000) MISSED OFFSET

	static FGFoliageIdentifier_Pickupable ReadAsMe(const uintptr_t address)
	{
		FGFoliageIdentifier_Pickupable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageIdentifier_Pickupable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageIdentifier_Chainsawable
// 0x4284A200
class FGFoliageIdentifier_Chainsawable
{
public:
	unsigned char                                      UnknownData00[0x4284A200];                                // 0x0000(0x4284A200) MISSED OFFSET

	static FGFoliageIdentifier_Chainsawable ReadAsMe(const uintptr_t address)
	{
		FGFoliageIdentifier_Chainsawable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageIdentifier_Chainsawable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFluidIntegrantInterface
// 0x427D4600
class FGFluidIntegrantInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGFluidIntegrantInterface ReadAsMe(const uintptr_t address)
	{
		FGFluidIntegrantInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFluidIntegrantInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageIdentifier_RemovableByBuildings
// 0x4284A200
class FGFoliageIdentifier_RemovableByBuildings
{
public:
	unsigned char                                      UnknownData00[0x4284A200];                                // 0x0000(0x4284A200) MISSED OFFSET

	static FGFoliageIdentifier_RemovableByBuildings ReadAsMe(const uintptr_t address)
	{
		FGFoliageIdentifier_RemovableByBuildings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageIdentifier_RemovableByBuildings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageIdentifier_VehicleDestroyable
// 0x4284A200
class FGFoliageIdentifier_VehicleDestroyable
{
public:
	unsigned char                                      UnknownData00[0x4284A200];                                // 0x0000(0x4284A200) MISSED OFFSET

	static FGFoliageIdentifier_VehicleDestroyable ReadAsMe(const uintptr_t address)
	{
		FGFoliageIdentifier_VehicleDestroyable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageIdentifier_VehicleDestroyable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliagePickup
// 0x40FE8000
class FGFoliagePickup
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGFoliagePickup ReadAsMe(const uintptr_t address)
	{
		FGFoliagePickup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliagePickup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageLibrary
// 0x40FE0C00
class FGFoliageLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGFoliageLibrary ReadAsMe(const uintptr_t address)
	{
		FGFoliageLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageRemovalSubsystem
// 0x42804400
class FGFoliageRemovalSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGFoliageRemovalSubsystem ReadAsMe(const uintptr_t address)
	{
		FGFoliageRemovalSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageRemovalSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageIdentifier
// 0x40FE0C00
class FGFoliageIdentifier
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGFoliageIdentifier ReadAsMe(const uintptr_t address)
	{
		FGFoliageIdentifier ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageIdentifier& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageResourceUserData
// 0x427EEC00
class FGFoliageResourceUserData
{
public:
	unsigned char                                      UnknownData00[0x427EEC00];                                // 0x0000(0x427EEC00) MISSED OFFSET

	static FGFoliageResourceUserData ReadAsMe(const uintptr_t address)
	{
		FGFoliageResourceUserData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageResourceUserData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageIdentifier_ExplosiveDestroyable
// 0x4284A200
class FGFoliageIdentifier_ExplosiveDestroyable
{
public:
	unsigned char                                      UnknownData00[0x4284A200];                                // 0x0000(0x4284A200) MISSED OFFSET

	static FGFoliageIdentifier_ExplosiveDestroyable ReadAsMe(const uintptr_t address)
	{
		FGFoliageIdentifier_ExplosiveDestroyable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageIdentifier_ExplosiveDestroyable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVehicle
// 0x42847C00
class FGVehicle
{
public:
	unsigned char                                      UnknownData00[0x42847C00];                                // 0x0000(0x42847C00) MISSED OFFSET

	static FGVehicle ReadAsMe(const uintptr_t address)
	{
		FGVehicle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVehicle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoundationHologram
// 0x4284B200
class FGFoundationHologram
{
public:
	unsigned char                                      UnknownData00[0x4284B200];                                // 0x0000(0x4284B200) MISSED OFFSET

	static FGFoundationHologram ReadAsMe(const uintptr_t address)
	{
		FGFoundationHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoundationHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageRemoval
// 0x40FE8000
class FGFoliageRemoval
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGFoliageRemoval ReadAsMe(const uintptr_t address)
	{
		FGFoliageRemoval ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageRemoval& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadVehicle
// 0x4284FC00
class FGRailroadVehicle
{
public:
	unsigned char                                      UnknownData00[0x4284FC00];                                // 0x0000(0x4284FC00) MISSED OFFSET

	static FGRailroadVehicle ReadAsMe(const uintptr_t address)
	{
		FGRailroadVehicle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadVehicle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameEngine
// 0x4284F800
class FGGameEngine
{
public:
	unsigned char                                      UnknownData00[0x4284F800];                                // 0x0000(0x4284F800) MISSED OFFSET

	static FGGameEngine ReadAsMe(const uintptr_t address)
	{
		FGGameEngine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameEngine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameInstance
// 0x427D7000
class FGGameInstance
{
public:
	unsigned char                                      UnknownData00[0x427D7000];                                // 0x0000(0x427D7000) MISSED OFFSET

	static FGGameInstance ReadAsMe(const uintptr_t address)
	{
		FGGameInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFreightWagon
// 0x4284FE00
class FGFreightWagon
{
public:
	unsigned char                                      UnknownData00[0x4284FE00];                                // 0x0000(0x4284FE00) MISSED OFFSET

	static FGFreightWagon ReadAsMe(const uintptr_t address)
	{
		FGFreightWagon ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFreightWagon& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoliageIdentifier_ChainsawableXmasTree
// 0x42849C00
class FGFoliageIdentifier_ChainsawableXmasTree
{
public:
	unsigned char                                      UnknownData00[0x42849C00];                                // 0x0000(0x42849C00) MISSED OFFSET

	static FGFoliageIdentifier_ChainsawableXmasTree ReadAsMe(const uintptr_t address)
	{
		FGFoliageIdentifier_ChainsawableXmasTree ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoliageIdentifier_ChainsawableXmasTree& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGamePhaseReachedDependency
// 0x4280F600
class FGGamePhaseReachedDependency
{
public:
	unsigned char                                      UnknownData00[0x4280F600];                                // 0x0000(0x4280F600) MISSED OFFSET

	static FGGamePhaseReachedDependency ReadAsMe(const uintptr_t address)
	{
		FGGamePhaseReachedDependency ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGamePhaseReachedDependency& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFoundationSubsystem
// 0x42804400
class FGFoundationSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGFoundationSubsystem ReadAsMe(const uintptr_t address)
	{
		FGFoundationSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFoundationSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameplayTask_Base
// 0x4284E000
class FGGameplayTask_Base
{
public:
	unsigned char                                      UnknownData00[0x4284E000];                                // 0x0000(0x4284E000) MISSED OFFSET

	static FGGameplayTask_Base ReadAsMe(const uintptr_t address)
	{
		FGGameplayTask_Base ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameplayTask_Base& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameMode
// 0x4284F000
class FGGameMode
{
public:
	unsigned char                                      UnknownData00[0x4284F000];                                // 0x0000(0x4284F000) MISSED OFFSET

	static FGGameMode ReadAsMe(const uintptr_t address)
	{
		FGGameMode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameMode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameplayTask_Attack
// 0x4284E200
class FGGameplayTask_Attack
{
public:
	unsigned char                                      UnknownData00[0x4284E200];                                // 0x0000(0x4284E200) MISSED OFFSET

	static FGGameplayTask_Attack ReadAsMe(const uintptr_t address)
	{
		FGGameplayTask_Attack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameplayTask_Attack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameplayTask_AttackJump
// 0x4284E400
class FGGameplayTask_AttackJump
{
public:
	unsigned char                                      UnknownData00[0x4284E400];                                // 0x0000(0x4284E400) MISSED OFFSET

	static FGGameplayTask_AttackJump ReadAsMe(const uintptr_t address)
	{
		FGGameplayTask_AttackJump ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameplayTask_AttackJump& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameplayTask_AttackMelee
// 0x4284E400
class FGGameplayTask_AttackMelee
{
public:
	unsigned char                                      UnknownData00[0x4284E400];                                // 0x0000(0x4284E400) MISSED OFFSET

	static FGGameplayTask_AttackMelee ReadAsMe(const uintptr_t address)
	{
		FGGameplayTask_AttackMelee ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameplayTask_AttackMelee& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGameplayTaskResource_Attack
// 0x4284E600
class FGameplayTaskResource_Attack
{
public:
	unsigned char                                      UnknownData00[0x4284E600];                                // 0x0000(0x4284E600) MISSED OFFSET

	static FGameplayTaskResource_Attack ReadAsMe(const uintptr_t address)
	{
		FGameplayTaskResource_Attack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGameplayTaskResource_Attack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameUI
// 0x4280F400
class FGGameUI
{
public:
	unsigned char                                      UnknownData00[0x4280F400];                                // 0x0000(0x4280F400) MISSED OFFSET

	static FGGameUI ReadAsMe(const uintptr_t address)
	{
		FGGameUI ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameUI& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameState
// 0x4284D400
class FGGameState
{
public:
	unsigned char                                      UnknownData00[0x4284D400];                                // 0x0000(0x4284D400) MISSED OFFSET

	static FGGameState ReadAsMe(const uintptr_t address)
	{
		FGGameState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameSession
// 0x4284D800
class FGGameSession
{
public:
	unsigned char                                      UnknownData00[0x4284D800];                                // 0x0000(0x4284D800) MISSED OFFSET

	static FGGameSession ReadAsMe(const uintptr_t address)
	{
		FGGameSession ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameSession& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameViewportClient
// 0x4284C800
class FGGameViewportClient
{
public:
	unsigned char                                      UnknownData00[0x4284C800];                                // 0x0000(0x4284C800) MISSED OFFSET

	static FGGameViewportClient ReadAsMe(const uintptr_t address)
	{
		FGGameViewportClient ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameViewportClient& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGasMask
// 0x42825200
class FGGasMask
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGGasMask ReadAsMe(const uintptr_t address)
	{
		FGGasMask ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGasMask& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGasPillar
// 0x40FE8000
class FGGasPillar
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGGasPillar ReadAsMe(const uintptr_t address)
	{
		FGGasPillar ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGasPillar& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGamePhaseManager
// 0x42804400
class FGGamePhaseManager
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGGamePhaseManager ReadAsMe(const uintptr_t address)
	{
		FGGamePhaseManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGamePhaseManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGolfCartDispenser
// 0x42825200
class FGGolfCartDispenser
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGGolfCartDispenser ReadAsMe(const uintptr_t address)
	{
		FGGolfCartDispenser ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGolfCartDispenser& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGameUserSettings
// 0x4284CC00
class FGGameUserSettings
{
public:
	unsigned char                                      UnknownData00[0x4284CC00];                                // 0x0000(0x4284CC00) MISSED OFFSET

	static FGGameUserSettings ReadAsMe(const uintptr_t address)
	{
		FGGameUserSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGameUserSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceExtractorHologram
// 0x42809800
class FGResourceExtractorHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGResourceExtractorHologram ReadAsMe(const uintptr_t address)
	{
		FGResourceExtractorHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceExtractorHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGlobalSettings
// 0x40FE0C00
class FGGlobalSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGGlobalSettings ReadAsMe(const uintptr_t address)
	{
		FGGlobalSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGlobalSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGasMaskAttachment
// 0x42824E00
class FGGasMaskAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGGasMaskAttachment ReadAsMe(const uintptr_t address)
	{
		FGGasMaskAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGasMaskAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHardDriveSettings
// 0x42845200
class FGHardDriveSettings
{
public:
	unsigned char                                      UnknownData00[0x42845200];                                // 0x0000(0x42845200) MISSED OFFSET

	static FGHardDriveSettings ReadAsMe(const uintptr_t address)
	{
		FGHardDriveSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHardDriveSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHookshot
// 0x42825200
class FGHookshot
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGHookshot ReadAsMe(const uintptr_t address)
	{
		FGHookshot ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHookshot& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHealthComponent
// 0x40FEF600
class FGHealthComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static FGHealthComponent ReadAsMe(const uintptr_t address)
	{
		FGHealthComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHealthComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHUD
// 0x42852800
class FGHUD
{
public:
	unsigned char                                      UnknownData00[0x42852800];                                // 0x0000(0x42852800) MISSED OFFSET

	static FGHUD ReadAsMe(const uintptr_t address)
	{
		FGHUD ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHUD& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHUDBase
// 0x42852600
class FGHUDBase
{
public:
	unsigned char                                      UnknownData00[0x42852600];                                // 0x0000(0x42852600) MISSED OFFSET

	static FGHUDBase ReadAsMe(const uintptr_t address)
	{
		FGHUDBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHUDBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInputLibrary
// 0x40FEE400
class FGInputLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGInputLibrary ReadAsMe(const uintptr_t address)
	{
		FGInputLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInputLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGGeoThermalGeneratorHologram
// 0x42853800
class FGGeoThermalGeneratorHologram
{
public:
	unsigned char                                      UnknownData00[0x42853800];                                // 0x0000(0x42853800) MISSED OFFSET

	static FGGeoThermalGeneratorHologram ReadAsMe(const uintptr_t address)
	{
		FGGeoThermalGeneratorHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGGeoThermalGeneratorHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHotbarShortcut
// 0x40FE0C00
class FGHotbarShortcut
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGHotbarShortcut ReadAsMe(const uintptr_t address)
	{
		FGHotbarShortcut ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHotbarShortcut& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInteractableMarker
// 0x40FE8000
class FGInteractableMarker
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGInteractableMarker ReadAsMe(const uintptr_t address)
	{
		FGInteractableMarker ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInteractableMarker& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInteractWidget
// 0x4280FC00
class FGInteractWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGInteractWidget ReadAsMe(const uintptr_t address)
	{
		FGInteractWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInteractWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInventoryComponentBeltSlot
// 0x42851E00
class FGInventoryComponentBeltSlot
{
public:
	unsigned char                                      UnknownData00[0x42851E00];                                // 0x0000(0x42851E00) MISSED OFFSET

	static FGInventoryComponentBeltSlot ReadAsMe(const uintptr_t address)
	{
		FGInventoryComponentBeltSlot ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInventoryComponentBeltSlot& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInventoryComponentEquipment
// 0x42851E00
class FGInventoryComponentEquipment
{
public:
	unsigned char                                      UnknownData00[0x42851E00];                                // 0x0000(0x42851E00) MISSED OFFSET

	static FGInventoryComponentEquipment ReadAsMe(const uintptr_t address)
	{
		FGInventoryComponentEquipment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInventoryComponentEquipment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGItemCategory
// 0x40FE0C00
class FGItemCategory
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGItemCategory ReadAsMe(const uintptr_t address)
	{
		FGItemCategory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGItemCategory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInventoryLibrary
// 0x40FEE400
class FGInventoryLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGInventoryLibrary ReadAsMe(const uintptr_t address)
	{
		FGInventoryLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInventoryLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGItemDescriptorNuclearFuel
// 0x4280A600
class FGItemDescriptorNuclearFuel
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGItemDescriptorNuclearFuel ReadAsMe(const uintptr_t address)
	{
		FGItemDescriptorNuclearFuel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGItemDescriptorNuclearFuel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInventoryComponent
// 0x40FEF600
class FGInventoryComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static FGInventoryComponent ReadAsMe(const uintptr_t address)
	{
		FGInventoryComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInventoryComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGItemPickedUpDependency
// 0x4280F600
class FGItemPickedUpDependency
{
public:
	unsigned char                                      UnknownData00[0x4280F600];                                // 0x0000(0x4280F600) MISSED OFFSET

	static FGItemPickedUpDependency ReadAsMe(const uintptr_t address)
	{
		FGItemPickedUpDependency ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGItemPickedUpDependency& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInventoryComponentTrash
// 0x42851E00
class FGInventoryComponentTrash
{
public:
	unsigned char                                      UnknownData00[0x42851E00];                                // 0x0000(0x42851E00) MISSED OFFSET

	static FGInventoryComponentTrash ReadAsMe(const uintptr_t address)
	{
		FGInventoryComponentTrash ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInventoryComponentTrash& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_Collecting
// 0x4280CC00
class FGUseState_Collecting
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_Collecting ReadAsMe(const uintptr_t address)
	{
		FGUseState_Collecting ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_Collecting& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGItemPickup
// 0x40FE8000
class FGItemPickup
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGItemPickup ReadAsMe(const uintptr_t address)
	{
		FGItemPickup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGItemPickup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_FullInventory
// 0x4280CC00
class FGUseState_FullInventory
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_FullInventory ReadAsMe(const uintptr_t address)
	{
		FGUseState_FullInventory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_FullInventory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGItemPickup_Spawnable
// 0x42850800
class FGItemPickup_Spawnable
{
public:
	unsigned char                                      UnknownData00[0x42850800];                                // 0x0000(0x42850800) MISSED OFFSET

	static FGItemPickup_Spawnable ReadAsMe(const uintptr_t address)
	{
		FGItemPickup_Spawnable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGItemPickup_Spawnable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGItemRegrowSubsystem
// 0x42804400
class FGItemRegrowSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGItemRegrowSubsystem ReadAsMe(const uintptr_t address)
	{
		FGItemRegrowSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGItemRegrowSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGJetPackAttachment
// 0x42824E00
class FGJetPackAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGJetPackAttachment ReadAsMe(const uintptr_t address)
	{
		FGJetPackAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGJetPackAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGItemDescriptorBiomass
// 0x4280A600
class FGItemDescriptorBiomass
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGItemDescriptorBiomass ReadAsMe(const uintptr_t address)
	{
		FGItemDescriptorBiomass ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGItemDescriptorBiomass& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGJetPack
// 0x42825200
class FGJetPack
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGJetPack ReadAsMe(const uintptr_t address)
	{
		FGJetPack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGJetPack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGJumpingStilts
// 0x42825200
class FGJumpingStilts
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGJumpingStilts ReadAsMe(const uintptr_t address)
	{
		FGJumpingStilts ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGJumpingStilts& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGListView
// 0x40FEEE00
class FGListView
{
public:
	unsigned char                                      UnknownData00[0x40FEEE00];                                // 0x0000(0x40FEEE00) MISSED OFFSET

	static FGListView ReadAsMe(const uintptr_t address)
	{
		FGListView ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGListView& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGJumpingStiltsAttachment
// 0x42824E00
class FGJumpingStiltsAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGJumpingStiltsAttachment ReadAsMe(const uintptr_t address)
	{
		FGJumpingStiltsAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGJumpingStiltsAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGLadderComponent
// 0x42857C00
class FGLadderComponent
{
public:
	unsigned char                                      UnknownData00[0x42857C00];                                // 0x0000(0x42857C00) MISSED OFFSET

	static FGLadderComponent ReadAsMe(const uintptr_t address)
	{
		FGLadderComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGLadderComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEM_LoggedOutFromOnlineService
// 0x4284B800
class FGEM_LoggedOutFromOnlineService
{
public:
	unsigned char                                      UnknownData00[0x4284B800];                                // 0x0000(0x4284B800) MISSED OFFSET

	static FGEM_LoggedOutFromOnlineService ReadAsMe(const uintptr_t address)
	{
		FGEM_LoggedOutFromOnlineService ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEM_LoggedOutFromOnlineService& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEM_FailedToLoginToOnlineService
// 0x4284B800
class FGEM_FailedToLoginToOnlineService
{
public:
	unsigned char                                      UnknownData00[0x4284B800];                                // 0x0000(0x4284B800) MISSED OFFSET

	static FGEM_FailedToLoginToOnlineService ReadAsMe(const uintptr_t address)
	{
		FGEM_FailedToLoginToOnlineService ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEM_FailedToLoginToOnlineService& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGListViewSlot
// 0x42857400
class FGListViewSlot
{
public:
	unsigned char                                      UnknownData00[0x42857400];                                // 0x0000(0x42857400) MISSED OFFSET

	static FGListViewSlot ReadAsMe(const uintptr_t address)
	{
		FGListViewSlot ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGListViewSlot& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGEM_LostConnectionWithOnlineService
// 0x4284B800
class FGEM_LostConnectionWithOnlineService
{
public:
	unsigned char                                      UnknownData00[0x4284B800];                                // 0x0000(0x4284B800) MISSED OFFSET

	static FGEM_LostConnectionWithOnlineService ReadAsMe(const uintptr_t address)
	{
		FGEM_LostConnectionWithOnlineService ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGEM_LostConnectionWithOnlineService& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGLocalPlayer
// 0x42856A00
class FGLocalPlayer
{
public:
	unsigned char                                      UnknownData00[0x42856A00];                                // 0x0000(0x42856A00) MISSED OFFSET

	static FGLocalPlayer ReadAsMe(const uintptr_t address)
	{
		FGLocalPlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGLocalPlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGLocalSettings
// 0x427D8E00
class FGLocalSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGLocalSettings ReadAsMe(const uintptr_t address)
	{
		FGLocalSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGLocalSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadVehicleMovementComponent
// 0x42805E00
class FGRailroadVehicleMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x42805E00];                                // 0x0000(0x42805E00) MISSED OFFSET

	static FGRailroadVehicleMovementComponent ReadAsMe(const uintptr_t address)
	{
		FGRailroadVehicleMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadVehicleMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGLocomotiveMovementComponent
// 0x42856200
class FGLocomotiveMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x42856200];                                // 0x0000(0x42856200) MISSED OFFSET

	static FGLocomotiveMovementComponent ReadAsMe(const uintptr_t address)
	{
		FGLocomotiveMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGLocomotiveMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGLootSettings
// 0x40FE0C00
class FGLootSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGLootSettings ReadAsMe(const uintptr_t address)
	{
		FGLootSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGLootSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGManta
// 0x40FE8000
class FGManta
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGManta ReadAsMe(const uintptr_t address)
	{
		FGManta ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGManta& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGManufacturingButton
// 0x4280FC00
class FGManufacturingButton
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGManufacturingButton ReadAsMe(const uintptr_t address)
	{
		FGManufacturingButton ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGManufacturingButton& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMapAreaTexture
// 0x40FE0C00
class FGMapAreaTexture
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGMapAreaTexture ReadAsMe(const uintptr_t address)
	{
		FGMapAreaTexture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMapAreaTexture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMapCompassSettings
// 0x427D8E00
class FGMapCompassSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGMapCompassSettings ReadAsMe(const uintptr_t address)
	{
		FGMapCompassSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMapCompassSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMapFunctionLibrary
// 0x40FEE400
class FGMapFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGMapFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		FGMapFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMapFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMapArea
// 0x40FE0C00
class FGMapArea
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGMapArea ReadAsMe(const uintptr_t address)
	{
		FGMapArea ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMapArea& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMapManager
// 0x42804400
class FGMapManager
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGMapManager ReadAsMe(const uintptr_t address)
	{
		FGMapManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMapManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMapWidget
// 0x4280FC00
class FGMapWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGMapWidget ReadAsMe(const uintptr_t address)
	{
		FGMapWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMapWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGLocomotive
// 0x4284FE00
class FGLocomotive
{
public:
	unsigned char                                      UnknownData00[0x4284FE00];                                // 0x0000(0x4284FE00) MISSED OFFSET

	static FGLocomotive ReadAsMe(const uintptr_t address)
	{
		FGLocomotive ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGLocomotive& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMapObjectWidget
// 0x4280FC00
class FGMapObjectWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGMapObjectWidget ReadAsMe(const uintptr_t address)
	{
		FGMapObjectWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMapObjectWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMaterialEffectComponent
// 0x40FEF600
class FGMaterialEffectComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static FGMaterialEffectComponent ReadAsMe(const uintptr_t address)
	{
		FGMaterialEffectComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMaterialEffectComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMaterialFlowAnalysisFunctionLibrary
// 0x40FEE400
class FGMaterialFlowAnalysisFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGMaterialFlowAnalysisFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		FGMaterialFlowAnalysisFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMaterialFlowAnalysisFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMenuBase
// 0x4280FC00
class FGMenuBase
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGMenuBase ReadAsMe(const uintptr_t address)
	{
		FGMenuBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMenuBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMessageSender
// 0x40FE0C00
class FGMessageSender
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGMessageSender ReadAsMe(const uintptr_t address)
	{
		FGMessageSender ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMessageSender& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHeightWaterUserData
// 0x427EEC00
class FGHeightWaterUserData
{
public:
	unsigned char                                      UnknownData00[0x427EEC00];                                // 0x0000(0x427EEC00) MISSED OFFSET

	static FGHeightWaterUserData ReadAsMe(const uintptr_t address)
	{
		FGHeightWaterUserData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHeightWaterUserData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMigrationSettings
// 0x427D8E00
class FGMigrationSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGMigrationSettings ReadAsMe(const uintptr_t address)
	{
		FGMigrationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMigrationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHeightFoliageUserData
// 0x427EEC00
class FGHeightFoliageUserData
{
public:
	unsigned char                                      UnknownData00[0x427EEC00];                                // 0x0000(0x427EEC00) MISSED OFFSET

	static FGHeightFoliageUserData ReadAsMe(const uintptr_t address)
	{
		FGHeightFoliageUserData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHeightFoliageUserData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGHeightHideUserData
// 0x427EEC00
class FGHeightHideUserData
{
public:
	unsigned char                                      UnknownData00[0x427EEC00];                                // 0x0000(0x427EEC00) MISSED OFFSET

	static FGHeightHideUserData ReadAsMe(const uintptr_t address)
	{
		FGHeightHideUserData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGHeightHideUserData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMaterialEffect_Build
// 0x42854800
class FGMaterialEffect_Build
{
public:
	unsigned char                                      UnknownData00[0x42854800];                                // 0x0000(0x42854800) MISSED OFFSET

	static FGMaterialEffect_Build ReadAsMe(const uintptr_t address)
	{
		FGMaterialEffect_Build ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMaterialEffect_Build& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMinimapCaptureActor
// 0x4285BA00
class FGMinimapCaptureActor
{
public:
	unsigned char                                      UnknownData00[0x4285BA00];                                // 0x0000(0x4285BA00) MISSED OFFSET

	static FGMinimapCaptureActor ReadAsMe(const uintptr_t address)
	{
		FGMinimapCaptureActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMinimapCaptureActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMusicManager
// 0x40FE0C00
class FGMusicManager
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGMusicManager ReadAsMe(const uintptr_t address)
	{
		FGMusicManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMusicManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNavArea_HardNature
// 0x4285AE00
class FGNavArea_HardNature
{
public:
	unsigned char                                      UnknownData00[0x4285AE00];                                // 0x0000(0x4285AE00) MISSED OFFSET

	static FGNavArea_HardNature ReadAsMe(const uintptr_t address)
	{
		FGNavArea_HardNature ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNavArea_HardNature& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNavArea_Factory
// 0x4285AE00
class FGNavArea_Factory
{
public:
	unsigned char                                      UnknownData00[0x4285AE00];                                // 0x0000(0x4285AE00) MISSED OFFSET

	static FGNavArea_Factory ReadAsMe(const uintptr_t address)
	{
		FGNavArea_Factory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNavArea_Factory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNetConstructionFunctionLibrary
// 0x40FEE400
class FGNetConstructionFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGNetConstructionFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		FGNetConstructionFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNetConstructionFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPresenceLibrary
// 0x40FEE400
class FGPresenceLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGPresenceLibrary ReadAsMe(const uintptr_t address)
	{
		FGPresenceLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPresenceLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGFriendsLibrary
// 0x40FEE400
class FGFriendsLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGFriendsLibrary ReadAsMe(const uintptr_t address)
	{
		FGFriendsLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGFriendsLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNavArea_Water
// 0x4285AE00
class FGNavArea_Water
{
public:
	unsigned char                                      UnknownData00[0x4285AE00];                                // 0x0000(0x4285AE00) MISSED OFFSET

	static FGNavArea_Water ReadAsMe(const uintptr_t address)
	{
		FGNavArea_Water ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNavArea_Water& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSessionLibrary
// 0x40FEE400
class FGSessionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGSessionLibrary ReadAsMe(const uintptr_t address)
	{
		FGSessionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSessionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGInviteLibrary
// 0x40FEE400
class FGInviteLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGInviteLibrary ReadAsMe(const uintptr_t address)
	{
		FGInviteLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGInviteLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNewsFeedActor
// 0x40FE8000
class FGNewsFeedActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGNewsFeedActor ReadAsMe(const uintptr_t address)
	{
		FGNewsFeedActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNewsFeedActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMultiplayerVerticalBox
// 0x4285B400
class FGMultiplayerVerticalBox
{
public:
	unsigned char                                      UnknownData00[0x4285B400];                                // 0x0000(0x4285B400) MISSED OFFSET

	static FGMultiplayerVerticalBox ReadAsMe(const uintptr_t address)
	{
		FGMultiplayerVerticalBox ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMultiplayerVerticalBox& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNobeliskDetonator
// 0x4282C400
class FGNobeliskDetonator
{
public:
	unsigned char                                      UnknownData00[0x4282C400];                                // 0x0000(0x4282C400) MISSED OFFSET

	static FGNobeliskDetonator ReadAsMe(const uintptr_t address)
	{
		FGNobeliskDetonator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNobeliskDetonator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNetworkLibrary
// 0x40FEE400
class FGNetworkLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGNetworkLibrary ReadAsMe(const uintptr_t address)
	{
		FGNetworkLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNetworkLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWeaponAttachment
// 0x42824E00
class FGWeaponAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGWeaponAttachment ReadAsMe(const uintptr_t address)
	{
		FGWeaponAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWeaponAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNobeliskExplosive
// 0x42846400
class FGNobeliskExplosive
{
public:
	unsigned char                                      UnknownData00[0x42846400];                                // 0x0000(0x42846400) MISSED OFFSET

	static FGNobeliskExplosive ReadAsMe(const uintptr_t address)
	{
		FGNobeliskExplosive ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNobeliskExplosive& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNobeliskExplosiveAttachment
// 0x42859400
class FGNobeliskExplosiveAttachment
{
public:
	unsigned char                                      UnknownData00[0x42859400];                                // 0x0000(0x42859400) MISSED OFFSET

	static FGNobeliskExplosiveAttachment ReadAsMe(const uintptr_t address)
	{
		FGNobeliskExplosiveAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNobeliskExplosiveAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNoneDescriptor
// 0x4280A600
class FGNoneDescriptor
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGNoneDescriptor ReadAsMe(const uintptr_t address)
	{
		FGNoneDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNoneDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNotificationSettings
// 0x427D8E00
class FGNotificationSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGNotificationSettings ReadAsMe(const uintptr_t address)
	{
		FGNotificationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNotificationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGObjectScannerAttachment
// 0x42824E00
class FGObjectScannerAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGObjectScannerAttachment ReadAsMe(const uintptr_t address)
	{
		FGObjectScannerAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGObjectScannerAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGOptionsLibrary
// 0x40FEE400
class FGOptionsLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGOptionsLibrary ReadAsMe(const uintptr_t address)
	{
		FGOptionsLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGOptionsLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGObjectScanner
// 0x42825200
class FGObjectScanner
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGObjectScanner ReadAsMe(const uintptr_t address)
	{
		FGObjectScanner ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGObjectScanner& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGOnlineSessionClient
// 0x427D8C00
class FGOnlineSessionClient
{
public:
	unsigned char                                      UnknownData00[0x427D8C00];                                // 0x0000(0x427D8C00) MISSED OFFSET

	static FGOnlineSessionClient ReadAsMe(const uintptr_t address)
	{
		FGOnlineSessionClient ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGOnlineSessionClient& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGOptionsValueController
// 0x4280FC00
class FGOptionsValueController
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGOptionsValueController ReadAsMe(const uintptr_t address)
	{
		FGOptionsValueController ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGOptionsValueController& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGOutlineComponent
// 0x40FEF800
class FGOutlineComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGOutlineComponent ReadAsMe(const uintptr_t address)
	{
		FGOutlineComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGOutlineComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGOptionsSettings
// 0x427D8E00
class FGOptionsSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGOptionsSettings ReadAsMe(const uintptr_t address)
	{
		FGOptionsSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGOptionsSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGNobeliskDetonatorAttachment
// 0x42859400
class FGNobeliskDetonatorAttachment
{
public:
	unsigned char                                      UnknownData00[0x42859400];                                // 0x0000(0x42859400) MISSED OFFSET

	static FGNobeliskDetonatorAttachment ReadAsMe(const uintptr_t address)
	{
		FGNobeliskDetonatorAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGNobeliskDetonatorAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGParachuteCameraShake
// 0x4285F200
class FGParachuteCameraShake
{
public:
	unsigned char                                      UnknownData00[0x4285F200];                                // 0x0000(0x4285F200) MISSED OFFSET

	static FGParachuteCameraShake ReadAsMe(const uintptr_t address)
	{
		FGParachuteCameraShake ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGParachuteCameraShake& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGParachute
// 0x42825200
class FGParachute
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGParachute ReadAsMe(const uintptr_t address)
	{
		FGParachute ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGParachute& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGOverflowDescriptor
// 0x4280A600
class FGOverflowDescriptor
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGOverflowDescriptor ReadAsMe(const uintptr_t address)
	{
		FGOverflowDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGOverflowDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGParachuteAttachment
// 0x42824E00
class FGParachuteAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGParachuteAttachment ReadAsMe(const uintptr_t address)
	{
		FGParachuteAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGParachuteAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPassengerSeat
// 0x42847C00
class FGPassengerSeat
{
public:
	unsigned char                                      UnknownData00[0x42847C00];                                // 0x0000(0x42847C00) MISSED OFFSET

	static FGPassengerSeat ReadAsMe(const uintptr_t address)
	{
		FGPassengerSeat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPassengerSeat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeBuilder
// 0x4284FC00
class FGPipeBuilder
{
public:
	unsigned char                                      UnknownData00[0x4284FC00];                                // 0x0000(0x4284FC00) MISSED OFFSET

	static FGPipeBuilder ReadAsMe(const uintptr_t address)
	{
		FGPipeBuilder ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeBuilder& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeBuilderTrail
// 0x40FE8000
class FGPipeBuilderTrail
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGPipeBuilderTrail ReadAsMe(const uintptr_t address)
	{
		FGPipeBuilderTrail ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeBuilderTrail& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeAttachmentSnapTargetInterface
// 0x427D4600
class FGPipeAttachmentSnapTargetInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGPipeAttachmentSnapTargetInterface ReadAsMe(const uintptr_t address)
	{
		FGPipeAttachmentSnapTargetInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeAttachmentSnapTargetInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeConnectionComponent
// 0x4285E400
class FGPipeConnectionComponent
{
public:
	unsigned char                                      UnknownData00[0x4285E400];                                // 0x0000(0x4285E400) MISSED OFFSET

	static FGPipeConnectionComponent ReadAsMe(const uintptr_t address)
	{
		FGPipeConnectionComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeConnectionComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeConnectionComponentBase
// 0x42829A00
class FGPipeConnectionComponentBase
{
public:
	unsigned char                                      UnknownData00[0x42829A00];                                // 0x0000(0x42829A00) MISSED OFFSET

	static FGPipeConnectionComponentBase ReadAsMe(const uintptr_t address)
	{
		FGPipeConnectionComponentBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeConnectionComponentBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeConnectionFactory
// 0x4285E200
class FGPipeConnectionFactory
{
public:
	unsigned char                                      UnknownData00[0x4285E200];                                // 0x0000(0x4285E200) MISSED OFFSET

	static FGPipeConnectionFactory ReadAsMe(const uintptr_t address)
	{
		FGPipeConnectionFactory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeConnectionFactory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeHyperInterface
// 0x427D4600
class FGPipeHyperInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGPipeHyperInterface ReadAsMe(const uintptr_t address)
	{
		FGPipeHyperInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeHyperInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeHyperStart
// 0x42821A00
class FGPipeHyperStart
{
public:
	unsigned char                                      UnknownData00[0x42821A00];                                // 0x0000(0x42821A00) MISSED OFFSET

	static FGPipeHyperStart ReadAsMe(const uintptr_t address)
	{
		FGPipeHyperStart ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeHyperStart& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGMapAreaZoneDescriptor
// 0x40FE0C00
class FGMapAreaZoneDescriptor
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGMapAreaZoneDescriptor ReadAsMe(const uintptr_t address)
	{
		FGMapAreaZoneDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGMapAreaZoneDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipelineHologram
// 0x42841800
class FGPipelineHologram
{
public:
	unsigned char                                      UnknownData00[0x42841800];                                // 0x0000(0x42841800) MISSED OFFSET

	static FGPipelineHologram ReadAsMe(const uintptr_t address)
	{
		FGPipelineHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipelineHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipelineFlowIndicatorComponent
// 0x42828C00
class FGPipelineFlowIndicatorComponent
{
public:
	unsigned char                                      UnknownData00[0x42828C00];                                // 0x0000(0x42828C00) MISSED OFFSET

	static FGPipelineFlowIndicatorComponent ReadAsMe(const uintptr_t address)
	{
		FGPipelineFlowIndicatorComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipelineFlowIndicatorComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeNetwork
// 0x427D5600
class FGPipeNetwork
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static FGPipeNetwork ReadAsMe(const uintptr_t address)
	{
		FGPipeNetwork ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeNetwork& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipelineSupportHologram
// 0x42809800
class FGPipelineSupportHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGPipelineSupportHologram ReadAsMe(const uintptr_t address)
	{
		FGPipelineSupportHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipelineSupportHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeSubsystem
// 0x42804400
class FGPipeSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGPipeSubsystem ReadAsMe(const uintptr_t address)
	{
		FGPipeSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipePartHologram
// 0x42809800
class FGPipePartHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGPipePartHologram ReadAsMe(const uintptr_t address)
	{
		FGPipePartHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipePartHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPlanet
// 0x40FE8000
class FGPlanet
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGPlanet ReadAsMe(const uintptr_t address)
	{
		FGPlanet ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPlanet& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipeReservoirHologram
// 0x42809800
class FGPipeReservoirHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGPipeReservoirHologram ReadAsMe(const uintptr_t address)
	{
		FGPipeReservoirHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipeReservoirHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipelineJunctionHologram
// 0x4285DA00
class FGPipelineJunctionHologram
{
public:
	unsigned char                                      UnknownData00[0x4285DA00];                                // 0x0000(0x4285DA00) MISSED OFFSET

	static FGPipelineJunctionHologram ReadAsMe(const uintptr_t address)
	{
		FGPipelineJunctionHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipelineJunctionHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPipelineAttachmentHologram
// 0x42809800
class FGPipelineAttachmentHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGPipelineAttachmentHologram ReadAsMe(const uintptr_t address)
	{
		FGPipelineAttachmentHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPipelineAttachmentHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPlayerStartTradingPost
// 0x42863E00
class FGPlayerStartTradingPost
{
public:
	unsigned char                                      UnknownData00[0x42863E00];                                // 0x0000(0x42863E00) MISSED OFFSET

	static FGPlayerStartTradingPost ReadAsMe(const uintptr_t address)
	{
		FGPlayerStartTradingPost ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPlayerStartTradingPost& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPlayerState
// 0x42863800
class FGPlayerState
{
public:
	unsigned char                                      UnknownData00[0x42863800];                                // 0x0000(0x42863800) MISSED OFFSET

	static FGPlayerState ReadAsMe(const uintptr_t address)
	{
		FGPlayerState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPlayerState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPoleDescriptor
// 0x42824800
class FGPoleDescriptor
{
public:
	unsigned char                                      UnknownData00[0x42824800];                                // 0x0000(0x42824800) MISSED OFFSET

	static FGPoleDescriptor ReadAsMe(const uintptr_t address)
	{
		FGPoleDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPoleDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPopupWidgetContent
// 0x4280FC00
class FGPopupWidgetContent
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGPopupWidgetContent ReadAsMe(const uintptr_t address)
	{
		FGPopupWidgetContent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPopupWidgetContent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPopupInstigatorInterface
// 0x427D4600
class FGPopupInstigatorInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGPopupInstigatorInterface ReadAsMe(const uintptr_t address)
	{
		FGPopupInstigatorInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPopupInstigatorInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPopupConnectAccounts
// 0x42863200
class FGPopupConnectAccounts
{
public:
	unsigned char                                      UnknownData00[0x42863200];                                // 0x0000(0x42863200) MISSED OFFSET

	static FGPopupConnectAccounts ReadAsMe(const uintptr_t address)
	{
		FGPopupConnectAccounts ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPopupConnectAccounts& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPlayerSettings
// 0x427D8E00
class FGPlayerSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGPlayerSettings ReadAsMe(const uintptr_t address)
	{
		FGPlayerSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPlayerSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPortableMiner
// 0x40FE8000
class FGPortableMiner
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGPortableMiner ReadAsMe(const uintptr_t address)
	{
		FGPortableMiner ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPortableMiner& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPortableMinerDispenser
// 0x42825200
class FGPortableMinerDispenser
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGPortableMinerDispenser ReadAsMe(const uintptr_t address)
	{
		FGPortableMinerDispenser ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPortableMinerDispenser& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPopupWidget
// 0x42852000
class FGPopupWidget
{
public:
	unsigned char                                      UnknownData00[0x42852000];                                // 0x0000(0x42852000) MISSED OFFSET

	static FGPopupWidget ReadAsMe(const uintptr_t address)
	{
		FGPopupWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPopupWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPowerConnectionComponent
// 0x42829C00
class FGPowerConnectionComponent
{
public:
	unsigned char                                      UnknownData00[0x42829C00];                                // 0x0000(0x42829C00) MISSED OFFSET

	static FGPowerConnectionComponent ReadAsMe(const uintptr_t address)
	{
		FGPowerConnectionComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPowerConnectionComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWallHologram
// 0x4284B200
class FGWallHologram
{
public:
	unsigned char                                      UnknownData00[0x4284B200];                                // 0x0000(0x4284B200) MISSED OFFSET

	static FGWallHologram ReadAsMe(const uintptr_t address)
	{
		FGWallHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWallHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPoweredWallHologram
// 0x42862000
class FGPoweredWallHologram
{
public:
	unsigned char                                      UnknownData00[0x42862000];                                // 0x0000(0x42862000) MISSED OFFSET

	static FGPoweredWallHologram ReadAsMe(const uintptr_t address)
	{
		FGPoweredWallHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPoweredWallHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPowerCircuit
// 0x42829E00
class FGPowerCircuit
{
public:
	unsigned char                                      UnknownData00[0x42829E00];                                // 0x0000(0x42829E00) MISSED OFFSET

	static FGPowerCircuit ReadAsMe(const uintptr_t address)
	{
		FGPowerCircuit ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPowerCircuit& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPowerCircuitWidget
// 0x4280FC00
class FGPowerCircuitWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGPowerCircuitWidget ReadAsMe(const uintptr_t address)
	{
		FGPowerCircuitWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPowerCircuitWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGProceduralStaticMeshActor
// 0x42861800
class FGProceduralStaticMeshActor
{
public:
	unsigned char                                      UnknownData00[0x42861800];                                // 0x0000(0x42861800) MISSED OFFSET

	static FGProceduralStaticMeshActor ReadAsMe(const uintptr_t address)
	{
		FGProceduralStaticMeshActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGProceduralStaticMeshActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPowerInfoComponent
// 0x40FEF600
class FGPowerInfoComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static FGPowerInfoComponent ReadAsMe(const uintptr_t address)
	{
		FGPowerInfoComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPowerInfoComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGProductionIndicatorComponent
// 0x40FEF600
class FGProductionIndicatorComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static FGProductionIndicatorComponent ReadAsMe(const uintptr_t address)
	{
		FGProductionIndicatorComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGProductionIndicatorComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGPowerPoleHologram
// 0x42809600
class FGPowerPoleHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGPowerPoleHologram ReadAsMe(const uintptr_t address)
	{
		FGPowerPoleHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPowerPoleHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGProductionIndicatorInstanceManager
// 0x40FEF800
class FGProductionIndicatorInstanceManager
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGProductionIndicatorInstanceManager ReadAsMe(const uintptr_t address)
	{
		FGProductionIndicatorInstanceManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGProductionIndicatorInstanceManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGProfileSpline
// 0x40FE8000
class FGProfileSpline
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGProfileSpline ReadAsMe(const uintptr_t address)
	{
		FGProfileSpline ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGProfileSpline& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRadiationInterface
// 0x427D4600
class FGRadiationInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGRadiationInterface ReadAsMe(const uintptr_t address)
	{
		FGRadiationInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRadiationInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRadioactiveActor
// 0x40FE8000
class FGRadioactiveActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGRadioactiveActor ReadAsMe(const uintptr_t address)
	{
		FGRadioactiveActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRadioactiveActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRadioactivitySubsystem
// 0x42804400
class FGRadioactivitySubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGRadioactivitySubsystem ReadAsMe(const uintptr_t address)
	{
		FGRadioactivitySubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRadioactivitySubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadBridgeHologram
// 0x42841800
class FGRailroadBridgeHologram
{
public:
	unsigned char                                      UnknownData00[0x42841800];                                // 0x0000(0x42841800) MISSED OFFSET

	static FGRailroadBridgeHologram ReadAsMe(const uintptr_t address)
	{
		FGRailroadBridgeHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadBridgeHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGProximitySubsystem
// 0x40FE8000
class FGProximitySubsystem
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGProximitySubsystem ReadAsMe(const uintptr_t address)
	{
		FGProximitySubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGProximitySubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRadiationSettings
// 0x427D8E00
class FGRadiationSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGRadiationSettings ReadAsMe(const uintptr_t address)
	{
		FGRadiationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRadiationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadTimeTable
// 0x427D5600
class FGRailroadTimeTable
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static FGRailroadTimeTable ReadAsMe(const uintptr_t address)
	{
		FGRailroadTimeTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadTimeTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadSubsystem
// 0x42804400
class FGRailroadSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGRailroadSubsystem ReadAsMe(const uintptr_t address)
	{
		FGRailroadSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGProductionIndicatorInstanceComponent
// 0x42828C00
class FGProductionIndicatorInstanceComponent
{
public:
	unsigned char                                      UnknownData00[0x42828C00];                                // 0x0000(0x42828C00) MISSED OFFSET

	static FGProductionIndicatorInstanceComponent ReadAsMe(const uintptr_t address)
	{
		FGProductionIndicatorInstanceComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGProductionIndicatorInstanceComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadTrackHologram
// 0x42841800
class FGRailroadTrackHologram
{
public:
	unsigned char                                      UnknownData00[0x42841800];                                // 0x0000(0x42841800) MISSED OFFSET

	static FGRailroadTrackHologram ReadAsMe(const uintptr_t address)
	{
		FGRailroadTrackHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadTrackHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadFunctionLibrary
// 0x40FEE400
class FGRailroadFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGRailroadFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		FGRailroadFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVehicleHologram
// 0x42809400
class FGVehicleHologram
{
public:
	unsigned char                                      UnknownData00[0x42809400];                                // 0x0000(0x42809400) MISSED OFFSET

	static FGVehicleHologram ReadAsMe(const uintptr_t address)
	{
		FGVehicleHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVehicleHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadTrackConnectionComponent
// 0x42829A00
class FGRailroadTrackConnectionComponent
{
public:
	unsigned char                                      UnknownData00[0x42829A00];                                // 0x0000(0x42829A00) MISSED OFFSET

	static FGRailroadTrackConnectionComponent ReadAsMe(const uintptr_t address)
	{
		FGRailroadTrackConnectionComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadTrackConnectionComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailRoadVehicleAnim
// 0x427EA800
class FGRailRoadVehicleAnim
{
public:
	unsigned char                                      UnknownData00[0x427EA800];                                // 0x0000(0x427EA800) MISSED OFFSET

	static FGRailRoadVehicleAnim ReadAsMe(const uintptr_t address)
	{
		FGRailRoadVehicleAnim ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailRoadVehicleAnim& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadVehicleSoundComponent
// 0x40FEF800
class FGRailroadVehicleSoundComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGRailroadVehicleSoundComponent ReadAsMe(const uintptr_t address)
	{
		FGRailroadVehicleSoundComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadVehicleSoundComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRecipeManager
// 0x42804400
class FGRecipeManager
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGRecipeManager ReadAsMe(const uintptr_t address)
	{
		FGRecipeManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRecipeManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRecipeUnlockedDependency
// 0x4280F600
class FGRecipeUnlockedDependency
{
public:
	unsigned char                                      UnknownData00[0x4280F600];                                // 0x0000(0x4280F600) MISSED OFFSET

	static FGRecipeUnlockedDependency ReadAsMe(const uintptr_t address)
	{
		FGRecipeUnlockedDependency ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRecipeUnlockedDependency& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRecipe
// 0x40FE0C00
class FGRecipe
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGRecipe ReadAsMe(const uintptr_t address)
	{
		FGRecipe ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRecipe& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRecipeProducerInterface
// 0x427D4600
class FGRecipeProducerInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGRecipeProducerInterface ReadAsMe(const uintptr_t address)
	{
		FGRecipeProducerInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRecipeProducerInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRenderTargetStage
// 0x40FE8000
class FGRenderTargetStage
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGRenderTargetStage ReadAsMe(const uintptr_t address)
	{
		FGRenderTargetStage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRenderTargetStage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActor_BuildableFactory
// 0x42866400
class FGReplicationDetailActor_BuildableFactory
{
public:
	unsigned char                                      UnknownData00[0x42866400];                                // 0x0000(0x42866400) MISSED OFFSET

	static FGReplicationDetailActor_BuildableFactory ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActor_BuildableFactory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActor_BuildableFactory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRecipeShortcut
// 0x42852C00
class FGRecipeShortcut
{
public:
	unsigned char                                      UnknownData00[0x42852C00];                                // 0x0000(0x42852C00) MISSED OFFSET

	static FGRecipeShortcut ReadAsMe(const uintptr_t address)
	{
		FGRecipeShortcut ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRecipeShortcut& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRepDetailActor_Extractor
// 0x42866600
class FGRepDetailActor_Extractor
{
public:
	unsigned char                                      UnknownData00[0x42866600];                                // 0x0000(0x42866600) MISSED OFFSET

	static FGRepDetailActor_Extractor ReadAsMe(const uintptr_t address)
	{
		FGRepDetailActor_Extractor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRepDetailActor_Extractor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActor
// 0x40FE8000
class FGReplicationDetailActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGReplicationDetailActor ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDependencyActorInterface
// 0x427D4600
class FGReplicationDependencyActorInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGReplicationDependencyActorInterface ReadAsMe(const uintptr_t address)
	{
		FGReplicationDependencyActorInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDependencyActorInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActor_CargoPlatform
// 0x42866600
class FGReplicationDetailActor_CargoPlatform
{
public:
	unsigned char                                      UnknownData00[0x42866600];                                // 0x0000(0x42866600) MISSED OFFSET

	static FGReplicationDetailActor_CargoPlatform ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActor_CargoPlatform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActor_CargoPlatform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActor_DockingStation
// 0x42866600
class FGReplicationDetailActor_DockingStation
{
public:
	unsigned char                                      UnknownData00[0x42866600];                                // 0x0000(0x42866600) MISSED OFFSET

	static FGReplicationDetailActor_DockingStation ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActor_DockingStation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActor_DockingStation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActor_GeneratorNuclear
// 0x42865C00
class FGReplicationDetailActor_GeneratorNuclear
{
public:
	unsigned char                                      UnknownData00[0x42865C00];                                // 0x0000(0x42865C00) MISSED OFFSET

	static FGReplicationDetailActor_GeneratorNuclear ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActor_GeneratorNuclear ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActor_GeneratorNuclear& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRailroadVehicleHologram
// 0x42867800
class FGRailroadVehicleHologram
{
public:
	unsigned char                                      UnknownData00[0x42867800];                                // 0x0000(0x42867800) MISSED OFFSET

	static FGRailroadVehicleHologram ReadAsMe(const uintptr_t address)
	{
		FGRailroadVehicleHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRailroadVehicleHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActor_Storage
// 0x42866600
class FGReplicationDetailActor_Storage
{
public:
	unsigned char                                      UnknownData00[0x42866600];                                // 0x0000(0x42866600) MISSED OFFSET

	static FGReplicationDetailActor_Storage ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActor_Storage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActor_Storage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActor_GeneratorFuel
// 0x42866600
class FGReplicationDetailActor_GeneratorFuel
{
public:
	unsigned char                                      UnknownData00[0x42866600];                                // 0x0000(0x42866600) MISSED OFFSET

	static FGReplicationDetailActor_GeneratorFuel ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActor_GeneratorFuel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActor_GeneratorFuel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActorOwnerInterface
// 0x427D4600
class FGReplicationDetailActorOwnerInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGReplicationDetailActorOwnerInterface ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActorOwnerInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActorOwnerInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailInventoryComponent
// 0x40FEF600
class FGReplicationDetailInventoryComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static FGReplicationDetailInventoryComponent ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailInventoryComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailInventoryComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationGraphNode_ConditionallyAlwaysRelevant
// 0x40FE9A00
class FGReplicationGraphNode_ConditionallyAlwaysRelevant
{
public:
	unsigned char                                      UnknownData00[0x40FE9A00];                                // 0x0000(0x40FE9A00) MISSED OFFSET

	static FGReplicationGraphNode_ConditionallyAlwaysRelevant ReadAsMe(const uintptr_t address)
	{
		FGReplicationGraphNode_ConditionallyAlwaysRelevant ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationGraphNode_ConditionallyAlwaysRelevant& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResearchMachine
// 0x40FEF800
class FGResearchMachine
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGResearchMachine ReadAsMe(const uintptr_t address)
	{
		FGResearchMachine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResearchMachine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationGraphNode_AlwaysRelevant_ForConnection
// 0x40FE8A00
class FGReplicationGraphNode_AlwaysRelevant_ForConnection
{
public:
	unsigned char                                      UnknownData00[0x40FE8A00];                                // 0x0000(0x40FE8A00) MISSED OFFSET

	static FGReplicationGraphNode_AlwaysRelevant_ForConnection ReadAsMe(const uintptr_t address)
	{
		FGReplicationGraphNode_AlwaysRelevant_ForConnection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationGraphNode_AlwaysRelevant_ForConnection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResearchManager
// 0x42804400
class FGResearchManager
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGResearchManager ReadAsMe(const uintptr_t address)
	{
		FGResearchManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResearchManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationGraph
// 0x40FEA000
class FGReplicationGraph
{
public:
	unsigned char                                      UnknownData00[0x40FEA000];                                // 0x0000(0x40FEA000) MISSED OFFSET

	static FGReplicationGraph ReadAsMe(const uintptr_t address)
	{
		FGReplicationGraph ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationGraph& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResearchTree
// 0x40FE0C00
class FGResearchTree
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGResearchTree ReadAsMe(const uintptr_t address)
	{
		FGResearchTree ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResearchTree& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResearchTreeNode
// 0x40FE0C00
class FGResearchTreeNode
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGResearchTreeNode ReadAsMe(const uintptr_t address)
	{
		FGResearchTreeNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResearchTreeNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceNode
// 0x40FE8000
class FGResourceNode
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGResourceNode ReadAsMe(const uintptr_t address)
	{
		FGResourceNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResearchRecipe
// 0x42867400
class FGResearchRecipe
{
public:
	unsigned char                                      UnknownData00[0x42867400];                                // 0x0000(0x42867400) MISSED OFFSET

	static FGResearchRecipe ReadAsMe(const uintptr_t address)
	{
		FGResearchRecipe ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResearchRecipe& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceDescriptorGeyser
// 0x4286C000
class FGResourceDescriptorGeyser
{
public:
	unsigned char                                      UnknownData00[0x4286C000];                                // 0x0000(0x4286C000) MISSED OFFSET

	static FGResourceDescriptorGeyser ReadAsMe(const uintptr_t address)
	{
		FGResourceDescriptorGeyser ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceDescriptorGeyser& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGReplicationDetailActor_Manufacturing
// 0x42866600
class FGReplicationDetailActor_Manufacturing
{
public:
	unsigned char                                      UnknownData00[0x42866600];                                // 0x0000(0x42866600) MISSED OFFSET

	static FGReplicationDetailActor_Manufacturing ReadAsMe(const uintptr_t address)
	{
		FGReplicationDetailActor_Manufacturing ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGReplicationDetailActor_Manufacturing& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceMiner
// 0x42825200
class FGResourceMiner
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGResourceMiner ReadAsMe(const uintptr_t address)
	{
		FGResourceMiner ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceMiner& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_NonConveyorResource
// 0x4280CC00
class FGUseState_NonConveyorResource
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_NonConveyorResource ReadAsMe(const uintptr_t address)
	{
		FGUseState_NonConveyorResource ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_NonConveyorResource& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceDeposit
// 0x4286C200
class FGResourceDeposit
{
public:
	unsigned char                                      UnknownData00[0x4286C200];                                // 0x0000(0x4286C200) MISSED OFFSET

	static FGResourceDeposit ReadAsMe(const uintptr_t address)
	{
		FGResourceDeposit ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceDeposit& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_NodeFullInventory
// 0x4280CC00
class FGUseState_NodeFullInventory
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_NodeFullInventory ReadAsMe(const uintptr_t address)
	{
		FGUseState_NodeFullInventory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_NodeFullInventory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceSettings
// 0x42845200
class FGResourceSettings
{
public:
	unsigned char                                      UnknownData00[0x42845200];                                // 0x0000(0x42845200) MISSED OFFSET

	static FGResourceSettings ReadAsMe(const uintptr_t address)
	{
		FGResourceSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceSinkCreditDescriptor
// 0x4280A600
class FGResourceSinkCreditDescriptor
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGResourceSinkCreditDescriptor ReadAsMe(const uintptr_t address)
	{
		FGResourceSinkCreditDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceSinkCreditDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceScanner
// 0x42825200
class FGResourceScanner
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGResourceScanner ReadAsMe(const uintptr_t address)
	{
		FGResourceScanner ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceScanner& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceSinkSettings
// 0x427D8E00
class FGResourceSinkSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGResourceSinkSettings ReadAsMe(const uintptr_t address)
	{
		FGResourceSinkSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceSinkSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceSinkSubsystem
// 0x42804400
class FGResourceSinkSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGResourceSinkSubsystem ReadAsMe(const uintptr_t address)
	{
		FGResourceSinkSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceSinkSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRoadConnectionComponent
// 0x42829A00
class FGRoadConnectionComponent
{
public:
	unsigned char                                      UnknownData00[0x42829A00];                                // 0x0000(0x42829A00) MISSED OFFSET

	static FGRoadConnectionComponent ReadAsMe(const uintptr_t address)
	{
		FGRoadConnectionComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRoadConnectionComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceDescriptor
// 0x4280A600
class FGResourceDescriptor
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGResourceDescriptor ReadAsMe(const uintptr_t address)
	{
		FGResourceDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGResourceNodeGeyser
// 0x4286C200
class FGResourceNodeGeyser
{
public:
	unsigned char                                      UnknownData00[0x4286C200];                                // 0x0000(0x4286C200) MISSED OFFSET

	static FGResourceNodeGeyser ReadAsMe(const uintptr_t address)
	{
		FGResourceNodeGeyser ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGResourceNodeGeyser& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRoadHologram
// 0x42841800
class FGRoadHologram
{
public:
	unsigned char                                      UnknownData00[0x42841800];                                // 0x0000(0x42841800) MISSED OFFSET

	static FGRoadHologram ReadAsMe(const uintptr_t address)
	{
		FGRoadHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRoadHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSaveInterface
// 0x427D4600
class FGSaveInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGSaveInterface ReadAsMe(const uintptr_t address)
	{
		FGSaveInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSaveInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSaveSession
// 0x40FE0C00
class FGSaveSession
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGSaveSession ReadAsMe(const uintptr_t address)
	{
		FGSaveSession ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSaveSession& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGRiverSpline
// 0x40FE8000
class FGRiverSpline
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGRiverSpline ReadAsMe(const uintptr_t address)
	{
		FGRiverSpline ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGRiverSpline& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSaveSystem
// 0x40FE0C00
class FGSaveSystem
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGSaveSystem ReadAsMe(const uintptr_t address)
	{
		FGSaveSystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSaveSystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSchematic
// 0x40FE0C00
class FGSchematic
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGSchematic ReadAsMe(const uintptr_t address)
	{
		FGSchematic ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSchematic& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSchematicManager
// 0x42804400
class FGSchematicManager
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGSchematicManager ReadAsMe(const uintptr_t address)
	{
		FGSchematicManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSchematicManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSharedPostProcessSettings
// 0x40FE0C00
class FGSharedPostProcessSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGSharedPostProcessSettings ReadAsMe(const uintptr_t address)
	{
		FGSharedPostProcessSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSharedPostProcessSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSchematicCategory
// 0x40FE0C00
class FGSchematicCategory
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGSchematicCategory ReadAsMe(const uintptr_t address)
	{
		FGSchematicCategory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSchematicCategory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignElementListWidget
// 0x4280FC00
class FGSignElementListWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGSignElementListWidget ReadAsMe(const uintptr_t address)
	{
		FGSignElementListWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignElementListWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignElementSettingsWidget
// 0x4280FC00
class FGSignElementSettingsWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGSignElementSettingsWidget ReadAsMe(const uintptr_t address)
	{
		FGSignElementSettingsWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignElementSettingsWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSchematicPurchasedDependency
// 0x4280F600
class FGSchematicPurchasedDependency
{
public:
	unsigned char                                      UnknownData00[0x4280F600];                                // 0x0000(0x4280F600) MISSED OFFSET

	static FGSchematicPurchasedDependency ReadAsMe(const uintptr_t address)
	{
		FGSchematicPurchasedDependency ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSchematicPurchasedDependency& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignElementWidget
// 0x4280FC00
class FGSignElementWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGSignElementWidget ReadAsMe(const uintptr_t address)
	{
		FGSignElementWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignElementWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignElementDragDrop
// 0x42869000
class FGSignElementDragDrop
{
public:
	unsigned char                                      UnknownData00[0x42869000];                                // 0x0000(0x42869000) MISSED OFFSET

	static FGSignElementDragDrop ReadAsMe(const uintptr_t address)
	{
		FGSignElementDragDrop ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignElementDragDrop& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignCanvasWidget
// 0x4280FC00
class FGSignCanvasWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGSignCanvasWidget ReadAsMe(const uintptr_t address)
	{
		FGSignCanvasWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignCanvasWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignElementDragWidget
// 0x4280FC00
class FGSignElementDragWidget
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGSignElementDragWidget ReadAsMe(const uintptr_t address)
	{
		FGSignElementDragWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignElementDragWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignificanceManager
// 0x427E6A00
class FGSignificanceManager
{
public:
	unsigned char                                      UnknownData00[0x427E6A00];                                // 0x0000(0x427E6A00) MISSED OFFSET

	static FGSignificanceManager ReadAsMe(const uintptr_t address)
	{
		FGSignificanceManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignificanceManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignInteractWidget
// 0x42852000
class FGSignInteractWidget
{
public:
	unsigned char                                      UnknownData00[0x42852000];                                // 0x0000(0x42852000) MISSED OFFSET

	static FGSignInteractWidget ReadAsMe(const uintptr_t address)
	{
		FGSignInteractWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignInteractWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignElementData
// 0x40FE0C00
class FGSignElementData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGSignElementData ReadAsMe(const uintptr_t address)
	{
		FGSignElementData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignElementData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignTextData
// 0x4286F800
class FGSignTextData
{
public:
	unsigned char                                      UnknownData00[0x4286F800];                                // 0x0000(0x4286F800) MISSED OFFSET

	static FGSignTextData ReadAsMe(const uintptr_t address)
	{
		FGSignTextData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignTextData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignLayer
// 0x40FE0C00
class FGSignLayer
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGSignLayer ReadAsMe(const uintptr_t address)
	{
		FGSignLayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignLayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignIconData
// 0x4286F800
class FGSignIconData
{
public:
	unsigned char                                      UnknownData00[0x4286F800];                                // 0x0000(0x4286F800) MISSED OFFSET

	static FGSignIconData ReadAsMe(const uintptr_t address)
	{
		FGSignIconData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignIconData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignPixelData
// 0x4286F800
class FGSignPixelData
{
public:
	unsigned char                                      UnknownData00[0x4286F800];                                // 0x0000(0x4286F800) MISSED OFFSET

	static FGSignPixelData ReadAsMe(const uintptr_t address)
	{
		FGSignPixelData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignPixelData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignInterface
// 0x427D4600
class FGSignInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGSignInterface ReadAsMe(const uintptr_t address)
	{
		FGSignInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSkySphere
// 0x40FE8000
class FGSkySphere
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGSkySphere ReadAsMe(const uintptr_t address)
	{
		FGSkySphere ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSkySphere& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignTextWidget
// 0x42868800
class FGSignTextWidget
{
public:
	unsigned char                                      UnknownData00[0x42868800];                                // 0x0000(0x42868800) MISSED OFFSET

	static FGSignTextWidget ReadAsMe(const uintptr_t address)
	{
		FGSignTextWidget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignTextWidget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSoundSplineComponent
// 0x40FEFA00
class FGSoundSplineComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEFA00];                                // 0x0000(0x40FEFA00) MISSED OFFSET

	static FGSoundSplineComponent ReadAsMe(const uintptr_t address)
	{
		FGSoundSplineComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSoundSplineComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignSettings
// 0x42845200
class FGSignSettings
{
public:
	unsigned char                                      UnknownData00[0x42845200];                                // 0x0000(0x42845200) MISSED OFFSET

	static FGSignSettings ReadAsMe(const uintptr_t address)
	{
		FGSignSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSplineMeshGenerationLibrary
// 0x40FEE400
class FGSplineMeshGenerationLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGSplineMeshGenerationLibrary ReadAsMe(const uintptr_t address)
	{
		FGSplineMeshGenerationLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSplineMeshGenerationLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSignificanceInterface
// 0x427D4600
class FGSignificanceInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGSignificanceInterface ReadAsMe(const uintptr_t address)
	{
		FGSignificanceInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSignificanceInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSpaceElevatorHologram
// 0x42809800
class FGSpaceElevatorHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGSpaceElevatorHologram ReadAsMe(const uintptr_t address)
	{
		FGSpaceElevatorHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSpaceElevatorHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSporeFlower
// 0x40FE8000
class FGSporeFlower
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGSporeFlower ReadAsMe(const uintptr_t address)
	{
		FGSporeFlower ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSporeFlower& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGStairHologram
// 0x42848A00
class FGStairHologram
{
public:
	unsigned char                                      UnknownData00[0x42848A00];                                // 0x0000(0x42848A00) MISSED OFFSET

	static FGStairHologram ReadAsMe(const uintptr_t address)
	{
		FGStairHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGStairHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGStandaloneSignHologram
// 0x42809600
class FGStandaloneSignHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGStandaloneSignHologram ReadAsMe(const uintptr_t address)
	{
		FGStandaloneSignHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGStandaloneSignHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGStartingPod
// 0x40FE8000
class FGStartingPod
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGStartingPod ReadAsMe(const uintptr_t address)
	{
		FGStartingPod ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGStartingPod& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSplineComponent
// 0x427EBC00
class FGSplineComponent
{
public:
	unsigned char                                      UnknownData00[0x427EBC00];                                // 0x0000(0x427EBC00) MISSED OFFSET

	static FGSplineComponent ReadAsMe(const uintptr_t address)
	{
		FGSplineComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSplineComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGStingerWidgetRewardData
// 0x40FE0C00
class FGStingerWidgetRewardData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGStingerWidgetRewardData ReadAsMe(const uintptr_t address)
	{
		FGStingerWidgetRewardData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGStingerWidgetRewardData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGStorySubsystem
// 0x42804400
class FGStorySubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGStorySubsystem ReadAsMe(const uintptr_t address)
	{
		FGStorySubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGStorySubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGStatHat
// 0x40FE0C00
class FGStatHat
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGStatHat ReadAsMe(const uintptr_t address)
	{
		FGStatHat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGStatHat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSubsystemClasses
// 0x42845200
class FGSubsystemClasses
{
public:
	unsigned char                                      UnknownData00[0x42845200];                                // 0x0000(0x42845200) MISSED OFFSET

	static FGSubsystemClasses ReadAsMe(const uintptr_t address)
	{
		FGSubsystemClasses ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSubsystemClasses& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSuitBase
// 0x42825200
class FGSuitBase
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGSuitBase ReadAsMe(const uintptr_t address)
	{
		FGSuitBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSuitBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSplinePath
// 0x40FE8000
class FGSplinePath
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGSplinePath ReadAsMe(const uintptr_t address)
	{
		FGSplinePath ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSplinePath& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGSuitBaseAttachment
// 0x42824E00
class FGSuitBaseAttachment
{
public:
	unsigned char                                      UnknownData00[0x42824E00];                                // 0x0000(0x42824E00) MISSED OFFSET

	static FGSuitBaseAttachment ReadAsMe(const uintptr_t address)
	{
		FGSuitBaseAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGSuitBaseAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTargetPointLinkedList
// 0x40FE0C00
class FGTargetPointLinkedList
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGTargetPointLinkedList ReadAsMe(const uintptr_t address)
	{
		FGTargetPointLinkedList ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTargetPointLinkedList& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTargetPoint
// 0x40FE8000
class FGTargetPoint
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGTargetPoint ReadAsMe(const uintptr_t address)
	{
		FGTargetPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTargetPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGToolBelt
// 0x42825200
class FGToolBelt
{
public:
	unsigned char                                      UnknownData00[0x42825200];                                // 0x0000(0x42825200) MISSED OFFSET

	static FGToolBelt ReadAsMe(const uintptr_t address)
	{
		FGToolBelt ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGToolBelt& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTimeOfDaySubsystem
// 0x42804400
class FGTimeOfDaySubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGTimeOfDaySubsystem ReadAsMe(const uintptr_t address)
	{
		FGTimeOfDaySubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTimeOfDaySubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTradingPostHologram
// 0x42809800
class FGTradingPostHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGTradingPostHologram ReadAsMe(const uintptr_t address)
	{
		FGTradingPostHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTradingPostHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTitleButton
// 0x42824400
class FGTitleButton
{
public:
	unsigned char                                      UnknownData00[0x42824400];                                // 0x0000(0x42824400) MISSED OFFSET

	static FGTitleButton ReadAsMe(const uintptr_t address)
	{
		FGTitleButton ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTitleButton& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTrainPlatformConnection
// 0x42829A00
class FGTrainPlatformConnection
{
public:
	unsigned char                                      UnknownData00[0x42829A00];                                // 0x0000(0x42829A00) MISSED OFFSET

	static FGTrainPlatformConnection ReadAsMe(const uintptr_t address)
	{
		FGTrainPlatformConnection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTrainPlatformConnection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTrainStationHologram
// 0x42873400
class FGTrainStationHologram
{
public:
	unsigned char                                      UnknownData00[0x42873400];                                // 0x0000(0x42873400) MISSED OFFSET

	static FGTrainStationHologram ReadAsMe(const uintptr_t address)
	{
		FGTrainStationHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTrainStationHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTrainStationIdentifier
// 0x427D5600
class FGTrainStationIdentifier
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static FGTrainStationIdentifier ReadAsMe(const uintptr_t address)
	{
		FGTrainStationIdentifier ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTrainStationIdentifier& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTutorialIntroManager
// 0x42804400
class FGTutorialIntroManager
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGTutorialIntroManager ReadAsMe(const uintptr_t address)
	{
		FGTutorialIntroManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTutorialIntroManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTutorialSubsystem
// 0x40FE0C00
class FGTutorialSubsystem
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGTutorialSubsystem ReadAsMe(const uintptr_t address)
	{
		FGTutorialSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTutorialSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTrain
// 0x40FE8000
class FGTrain
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static FGTrain ReadAsMe(const uintptr_t address)
	{
		FGTrain ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTrain& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGTrainPlatformHologram
// 0x42809800
class FGTrainPlatformHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGTrainPlatformHologram ReadAsMe(const uintptr_t address)
	{
		FGTrainPlatformHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGTrainPlatformHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockArmEquipmentSlot
// 0x42872800
class FGUnlockArmEquipmentSlot
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockArmEquipmentSlot ReadAsMe(const uintptr_t address)
	{
		FGUnlockArmEquipmentSlot ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockArmEquipmentSlot& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGStackableStorageHologram
// 0x42809800
class FGStackableStorageHologram
{
public:
	unsigned char                                      UnknownData00[0x42809800];                                // 0x0000(0x42809800) MISSED OFFSET

	static FGStackableStorageHologram ReadAsMe(const uintptr_t address)
	{
		FGStackableStorageHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGStackableStorageHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockBuildEfficiency
// 0x42872800
class FGUnlockBuildEfficiency
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockBuildEfficiency ReadAsMe(const uintptr_t address)
	{
		FGUnlockBuildEfficiency ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockBuildEfficiency& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockGiveItem
// 0x42872800
class FGUnlockGiveItem
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockGiveItem ReadAsMe(const uintptr_t address)
	{
		FGUnlockGiveItem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockGiveItem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlock
// 0x40FE0C00
class FGUnlock
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGUnlock ReadAsMe(const uintptr_t address)
	{
		FGUnlock ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlock& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockInventorySlot
// 0x42872800
class FGUnlockInventorySlot
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockInventorySlot ReadAsMe(const uintptr_t address)
	{
		FGUnlockInventorySlot ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockInventorySlot& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockMap
// 0x42872800
class FGUnlockMap
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockMap ReadAsMe(const uintptr_t address)
	{
		FGUnlockMap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockMap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockBuildOverclock
// 0x42872800
class FGUnlockBuildOverclock
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockBuildOverclock ReadAsMe(const uintptr_t address)
	{
		FGUnlockBuildOverclock ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockBuildOverclock& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockSchematic
// 0x42872800
class FGUnlockSchematic
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockSchematic ReadAsMe(const uintptr_t address)
	{
		FGUnlockSchematic ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockSchematic& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockSubsystem
// 0x42804400
class FGUnlockSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGUnlockSubsystem ReadAsMe(const uintptr_t address)
	{
		FGUnlockSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockScannableResource
// 0x42872800
class FGUnlockScannableResource
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockScannableResource ReadAsMe(const uintptr_t address)
	{
		FGUnlockScannableResource ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockScannableResource& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_Valid
// 0x4280CC00
class FGUseState_Valid
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_Valid ReadAsMe(const uintptr_t address)
	{
		FGUseState_Valid ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_Valid& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseableInterface
// 0x427D4600
class FGUseableInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGUseableInterface ReadAsMe(const uintptr_t address)
	{
		FGUseableInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseableInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUnlockRecipe
// 0x42872800
class FGUnlockRecipe
{
public:
	unsigned char                                      UnknownData00[0x42872800];                                // 0x0000(0x42872800) MISSED OFFSET

	static FGUnlockRecipe ReadAsMe(const uintptr_t address)
	{
		FGUnlockRecipe ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUnlockRecipe& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUISettings
// 0x427D8E00
class FGUISettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static FGUISettings ReadAsMe(const uintptr_t address)
	{
		FGUISettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUISettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_VehicleHasDriver
// 0x4280CC00
class FGUseState_VehicleHasDriver
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_VehicleHasDriver ReadAsMe(const uintptr_t address)
	{
		FGUseState_VehicleHasDriver ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_VehicleHasDriver& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVehicleCollisionBoxComponent
// 0x42857C00
class FGVehicleCollisionBoxComponent
{
public:
	unsigned char                                      UnknownData00[0x42857C00];                                // 0x0000(0x42857C00) MISSED OFFSET

	static FGVehicleCollisionBoxComponent ReadAsMe(const uintptr_t address)
	{
		FGVehicleCollisionBoxComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVehicleCollisionBoxComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_VehicleOccupied
// 0x4280CC00
class FGUseState_VehicleOccupied
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_VehicleOccupied ReadAsMe(const uintptr_t address)
	{
		FGUseState_VehicleOccupied ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_VehicleOccupied& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGUseState_VehicleInWater
// 0x4280CC00
class FGUseState_VehicleInWater
{
public:
	unsigned char                                      UnknownData00[0x4280CC00];                                // 0x0000(0x4280CC00) MISSED OFFSET

	static FGUseState_VehicleInWater ReadAsMe(const uintptr_t address)
	{
		FGUseState_VehicleInWater ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGUseState_VehicleInWater& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVehicleWheel
// 0x42805400
class FGVehicleWheel
{
public:
	unsigned char                                      UnknownData00[0x42805400];                                // 0x0000(0x42805400) MISSED OFFSET

	static FGVehicleWheel ReadAsMe(const uintptr_t address)
	{
		FGVehicleWheel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVehicleWheel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVehicleDestroyableInterface
// 0x427D4600
class FGVehicleDestroyableInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGVehicleDestroyableInterface ReadAsMe(const uintptr_t address)
	{
		FGVehicleDestroyableInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVehicleDestroyableInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVirtualCursorFunctionLibrary
// 0x40FEE400
class FGVirtualCursorFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGVirtualCursorFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		FGVirtualCursorFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVirtualCursorFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVehicleSubsystem
// 0x42804400
class FGVehicleSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42804400];                                // 0x0000(0x42804400) MISSED OFFSET

	static FGVehicleSubsystem ReadAsMe(const uintptr_t address)
	{
		FGVehicleSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVehicleSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWallAttachmentHologram
// 0x42809600
class FGWallAttachmentHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGWallAttachmentHologram ReadAsMe(const uintptr_t address)
	{
		FGWallAttachmentHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWallAttachmentHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVersionFunctionLibrary
// 0x40FEE400
class FGVersionFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static FGVersionFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		FGVersionFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVersionFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVolumeMapArea
// 0x40FEC600
class FGVolumeMapArea
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static FGVolumeMapArea ReadAsMe(const uintptr_t address)
	{
		FGVolumeMapArea ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVolumeMapArea& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWaterVolume
// 0x42877400
class FGWaterVolume
{
public:
	unsigned char                                      UnknownData00[0x42877400];                                // 0x0000(0x42877400) MISSED OFFSET

	static FGWaterVolume ReadAsMe(const uintptr_t address)
	{
		FGWaterVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWaterVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWeaponAttachmentProjectile
// 0x42859400
class FGWeaponAttachmentProjectile
{
public:
	unsigned char                                      UnknownData00[0x42859400];                                // 0x0000(0x42859400) MISSED OFFSET

	static FGWeaponAttachmentProjectile ReadAsMe(const uintptr_t address)
	{
		FGWeaponAttachmentProjectile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWeaponAttachmentProjectile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWaterAudio
// 0x40FE0C00
class FGWaterAudio
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FGWaterAudio ReadAsMe(const uintptr_t address)
	{
		FGWaterAudio ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWaterAudio& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWheeledVehicle
// 0x4284FC00
class FGWheeledVehicle
{
public:
	unsigned char                                      UnknownData00[0x4284FC00];                                // 0x0000(0x4284FC00) MISSED OFFSET

	static FGWheeledVehicle ReadAsMe(const uintptr_t address)
	{
		FGWheeledVehicle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWheeledVehicle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWeaponChild
// 0x4284BE00
class FGWeaponChild
{
public:
	unsigned char                                      UnknownData00[0x4284BE00];                                // 0x0000(0x4284BE00) MISSED OFFSET

	static FGWeaponChild ReadAsMe(const uintptr_t address)
	{
		FGWeaponChild ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWeaponChild& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWalkwayHologram
// 0x4284B200
class FGWalkwayHologram
{
public:
	unsigned char                                      UnknownData00[0x4284B200];                                // 0x0000(0x4284B200) MISSED OFFSET

	static FGWalkwayHologram ReadAsMe(const uintptr_t address)
	{
		FGWalkwayHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWalkwayHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWheeledVehicleHologram
// 0x42867800
class FGWheeledVehicleHologram
{
public:
	unsigned char                                      UnknownData00[0x42867800];                                // 0x0000(0x42867800) MISSED OFFSET

	static FGWheeledVehicleHologram ReadAsMe(const uintptr_t address)
	{
		FGWheeledVehicleHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWheeledVehicleHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWheeledVehicleMovementComponent4W
// 0x42805000
class FGWheeledVehicleMovementComponent4W
{
public:
	unsigned char                                      UnknownData00[0x42805000];                                // 0x0000(0x42805000) MISSED OFFSET

	static FGWheeledVehicleMovementComponent4W ReadAsMe(const uintptr_t address)
	{
		FGWheeledVehicleMovementComponent4W ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWheeledVehicleMovementComponent4W& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWidgetMultiplayer
// 0x4280FC00
class FGWidgetMultiplayer
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGWidgetMultiplayer ReadAsMe(const uintptr_t address)
	{
		FGWidgetMultiplayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWidgetMultiplayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWidgetSwitcher
// 0x42876000
class FGWidgetSwitcher
{
public:
	unsigned char                                      UnknownData00[0x42876000];                                // 0x0000(0x42876000) MISSED OFFSET

	static FGWidgetSwitcher ReadAsMe(const uintptr_t address)
	{
		FGWidgetSwitcher ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWidgetSwitcher& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWheeledVehicleMovementComponent6W
// 0x42806000
class FGWheeledVehicleMovementComponent6W
{
public:
	unsigned char                                      UnknownData00[0x42806000];                                // 0x0000(0x42806000) MISSED OFFSET

	static FGWheeledVehicleMovementComponent6W ReadAsMe(const uintptr_t address)
	{
		FGWheeledVehicleMovementComponent6W ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWheeledVehicleMovementComponent6W& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWindow
// 0x4280FC00
class FGWindow
{
public:
	unsigned char                                      UnknownData00[0x4280FC00];                                // 0x0000(0x4280FC00) MISSED OFFSET

	static FGWindow ReadAsMe(const uintptr_t address)
	{
		FGWindow ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWindow& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWorkBench
// 0x40FEF800
class FGWorkBench
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static FGWorkBench ReadAsMe(const uintptr_t address)
	{
		FGWorkBench ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWorkBench& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWireHologram
// 0x42809600
class FGWireHologram
{
public:
	unsigned char                                      UnknownData00[0x42809600];                                // 0x0000(0x42809600) MISSED OFFSET

	static FGWireHologram ReadAsMe(const uintptr_t address)
	{
		FGWireHologram ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWireHologram& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWildCardDescriptor
// 0x4280A600
class FGWildCardDescriptor
{
public:
	unsigned char                                      UnknownData00[0x4280A600];                                // 0x0000(0x4280A600) MISSED OFFSET

	static FGWildCardDescriptor ReadAsMe(const uintptr_t address)
	{
		FGWildCardDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWildCardDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWorldSettings
// 0x42875200
class FGWorldSettings
{
public:
	unsigned char                                      UnknownData00[0x42875200];                                // 0x0000(0x42875200) MISSED OFFSET

	static FGWorldSettings ReadAsMe(const uintptr_t address)
	{
		FGWorldSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWorldSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWorldCreationInterface
// 0x427D4600
class FGWorldCreationInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static FGWorldCreationInterface ReadAsMe(const uintptr_t address)
	{
		FGWorldCreationInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWorldCreationInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGWeaponProjectileFire
// 0x4282C400
class FGWeaponProjectileFire
{
public:
	unsigned char                                      UnknownData00[0x4282C400];                                // 0x0000(0x4282C400) MISSED OFFSET

	static FGWeaponProjectileFire ReadAsMe(const uintptr_t address)
	{
		FGWeaponProjectileFire ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGWeaponProjectileFire& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class FactoryGame.FGVehicleDescriptor
// 0x42825C00
class FGVehicleDescriptor
{
public:
	unsigned char                                      UnknownData00[0x42825C00];                                // 0x0000(0x42825C00) MISSED OFFSET

	static FGVehicleDescriptor ReadAsMe(const uintptr_t address)
	{
		FGVehicleDescriptor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGVehicleDescriptor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
