#pragma once

#include "../Utils.h"
// Name: Satisfactory, Version: 1.0.0

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace SDK
{
//---------------------------------------------------------------------------
// Classes
//---------------------------------------------------------------------------

// Class CoreUObject.Object
// 0x0000
class UObject
{
public:
	static FUObjectArray*                              GObjects;                                                 // 0x0000(0x0000)
	void*                                              Vtable;                                                   // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int32_t                                            ObjectFlags;                                              // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int32_t                                            InternalIndex;                                            // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	class UClass*                                      Class;                                                    // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	FName                                              Name;                                                     // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	class UObject*                                     Outer;                                                    // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY

	static inline TUObjectArray& GetGlobalObjects()
	{
		return GObjects->ObjObjects;
	}

	std::string GetName() const;

	std::string GetFullName() const;

	template<typename T>
	static T* FindObject(const std::string& name)
	{
		for (int i = 0; i < GetGlobalObjects().Num(); ++i)
		{
			auto object = GetGlobalObjects().GetByIndex(i);
	
			if (object == nullptr)
			{
				continue;
			}
	
			if (object->GetFullName() == name)
			{
				return static_cast<T*>(object);
			}
		}
		return nullptr;
	}

	template<typename T>
	static T* FindObject()
	{
		auto v = T::StaticClass();
		for (int i = 0; i < SDK::UObject::GetGlobalObjects().Num(); ++i)
		{
			auto object = SDK::UObject::GetGlobalObjects().GetByIndex(i);

			if (object == nullptr)
			{
				continue;
			}

			if (object->IsA(v))
			{
				return static_cast<T*>(object);
			}
		}
		return nullptr;
	}

	template<typename T>
	static std::vector<T*> FindObjects(const std::string& name)
	{
		std::vector<T*> ret;
		for (int i = 0; i < GetGlobalObjects().Num(); ++i)
		{
			auto object = GetGlobalObjects().GetByIndex(i);

			if (object == nullptr)
			{
				continue;
			}

			if (object->GetFullName() == name)
			{
				ret.push_back(static_cast<T*>(object));
			}
		}
		return ret;
	}

	template<typename T>
	static std::vector<T*> FindObjects()
	{
		std::vector<T*> ret;
		auto v = T::StaticClass();
		for (int i = 0; i < SDK::UObject::GetGlobalObjects().Num(); ++i)
		{
			auto object = SDK::UObject::GetGlobalObjects().GetByIndex(i);

			if (object == nullptr)
			{
				continue;
			}

			if (object->IsA(v))
			{
				ret.push_back(static_cast<T*>(object));
			}
		}
		return ret;
	}

	static UClass* FindClass(const std::string& name)
	{
		return FindObject<UClass>(name);
	}

	template<typename T>
	static T* GetObjectCasted(std::size_t index)
	{
		return static_cast<T*>(GetGlobalObjects().GetByIndex(index));
	}

	bool IsA(UClass* cmp) const;

	static UObject ReadAsMe(const uintptr_t address)
	{
		UObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, UObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Interface
// 0x40FE0C00
class Interface
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Interface ReadAsMe(const uintptr_t address)
	{
		Interface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Interface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.TextBuffer
// 0x40FE0C00
class TextBuffer
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static TextBuffer ReadAsMe(const uintptr_t address)
	{
		TextBuffer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextBuffer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Struct
// 0x42874C00
class Struct
{
public:
	class UStruct*                                     SuperField;                                               // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	class UField*                                      Children;                                                 // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int32_t                                            PropertySize;                                             // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int32_t                                            MinAlignment;                                             // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	unsigned char                                      UnknownData0x0048[0x40];                                  // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY

	static Struct ReadAsMe(const uintptr_t address)
	{
		Struct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Struct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Field
// 0x40FE0C00
class Field
{
public:
	class UField*                                      Next;                                                     // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY

	static Field ReadAsMe(const uintptr_t address)
	{
		Field ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Field& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.GCObjectReferencer
// 0x40FE0C00
class GCObjectReferencer
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static GCObjectReferencer ReadAsMe(const uintptr_t address)
	{
		GCObjectReferencer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GCObjectReferencer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Function
// 0x42874A00
class Function
{
public:
	int32_t                                            FunctionFlags;                                            // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int16_t                                            RepOffset;                                                // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int8_t                                             NumParms;                                                 // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int16_t                                            ParmsSize;                                                // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int16_t                                            ReturnValueOffset;                                        // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int16_t                                            RPCId;                                                    // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int16_t                                            RPCResponseId;                                            // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	class UProperty*                                   FirstPropertyToInit;                                      // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	class UFunction*                                   EventGraphFunction;                                       // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	int32_t                                            EventGraphCallOffset;                                     // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY
	void*                                              Func;                                                     // 0x0000(0x0000) NOT AUTO-GENERATED PROPERTY

	static Function ReadAsMe(const uintptr_t address)
	{
		Function ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Function& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Class
// 0x42874A00
class Class
{
public:
	unsigned char                                      UnknownData00[0x42874A00];                                // 0x0000(0x42874A00) MISSED OFFSET

	template<typename T>
	inline T* CreateDefaultObject()
	{
		return static_cast<T*>(CreateDefaultObject());
	}

	static Class ReadAsMe(const uintptr_t address)
	{
		Class ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Class& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.ScriptStruct
// 0x42874A00
class ScriptStruct
{
public:
	unsigned char                                      UnknownData00[0x42874A00];                                // 0x0000(0x42874A00) MISSED OFFSET

	static ScriptStruct ReadAsMe(const uintptr_t address)
	{
		ScriptStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ScriptStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.DelegateFunction
// 0x4287C600
class DelegateFunction
{
public:
	unsigned char                                      UnknownData00[0x4287C600];                                // 0x0000(0x4287C600) MISSED OFFSET

	static DelegateFunction ReadAsMe(const uintptr_t address)
	{
		DelegateFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DelegateFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Package
// 0x40FE0C00
class Package
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Package ReadAsMe(const uintptr_t address)
	{
		Package ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Package& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Enum
// 0x42874C00
class Enum
{
public:
	unsigned char                                      UnknownData00[0x42874C00];                                // 0x0000(0x42874C00) MISSED OFFSET

	static Enum ReadAsMe(const uintptr_t address)
	{
		Enum ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Enum& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.EnumProperty
// 0x4287BA00
class EnumProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static EnumProperty ReadAsMe(const uintptr_t address)
	{
		EnumProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EnumProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.LinkerPlaceholderExportObject
// 0x40FE0C00
class LinkerPlaceholderExportObject
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static LinkerPlaceholderExportObject ReadAsMe(const uintptr_t address)
	{
		LinkerPlaceholderExportObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LinkerPlaceholderExportObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.LinkerPlaceholderClass
// 0x42874600
class LinkerPlaceholderClass
{
public:
	unsigned char                                      UnknownData00[0x42874600];                                // 0x0000(0x42874600) MISSED OFFSET

	static LinkerPlaceholderClass ReadAsMe(const uintptr_t address)
	{
		LinkerPlaceholderClass ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LinkerPlaceholderClass& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.DynamicClass
// 0x42874600
class DynamicClass
{
public:
	unsigned char                                      UnknownData00[0x42874600];                                // 0x0000(0x42874600) MISSED OFFSET

	static DynamicClass ReadAsMe(const uintptr_t address)
	{
		DynamicClass ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DynamicClass& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.PackageMap
// 0x40FE0C00
class PackageMap
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PackageMap ReadAsMe(const uintptr_t address)
	{
		PackageMap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PackageMap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Property
// 0x42874C00
class Property
{
public:
	unsigned char                                      UnknownData00[0x42874C00];                                // 0x0000(0x42874C00) MISSED OFFSET

	static Property ReadAsMe(const uintptr_t address)
	{
		Property ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Property& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.ArrayProperty
// 0x4287BA00
class ArrayProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static ArrayProperty ReadAsMe(const uintptr_t address)
	{
		ArrayProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ArrayProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.ObjectPropertyBase
// 0x4287BA00
class ObjectPropertyBase
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static ObjectPropertyBase ReadAsMe(const uintptr_t address)
	{
		ObjectPropertyBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ObjectPropertyBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.ObjectRedirector
// 0x40FE0C00
class ObjectRedirector
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ObjectRedirector ReadAsMe(const uintptr_t address)
	{
		ObjectRedirector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ObjectRedirector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.MetaData
// 0x40FE0C00
class MetaData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static MetaData ReadAsMe(const uintptr_t address)
	{
		MetaData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MetaData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.NumericProperty
// 0x4287BA00
class NumericProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static NumericProperty ReadAsMe(const uintptr_t address)
	{
		NumericProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NumericProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.ByteProperty
// 0x4287A600
class ByteProperty
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static ByteProperty ReadAsMe(const uintptr_t address)
	{
		ByteProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ByteProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.BoolProperty
// 0x4287BA00
class BoolProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static BoolProperty ReadAsMe(const uintptr_t address)
	{
		BoolProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BoolProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.ClassProperty
// 0x4287A200
class ClassProperty
{
public:
	unsigned char                                      UnknownData00[0x4287A200];                                // 0x0000(0x4287A200) MISSED OFFSET

	static ClassProperty ReadAsMe(const uintptr_t address)
	{
		ClassProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ClassProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.DelegateProperty
// 0x4287BA00
class DelegateProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static DelegateProperty ReadAsMe(const uintptr_t address)
	{
		DelegateProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DelegateProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.FloatProperty
// 0x4287A600
class FloatProperty
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static FloatProperty ReadAsMe(const uintptr_t address)
	{
		FloatProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FloatProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.IntProperty
// 0x4287A600
class IntProperty
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static IntProperty ReadAsMe(const uintptr_t address)
	{
		IntProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, IntProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Int16Property
// 0x4287A600
class Int16Property
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static Int16Property ReadAsMe(const uintptr_t address)
	{
		Int16Property ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Int16Property& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.DoubleProperty
// 0x4287A600
class DoubleProperty
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static DoubleProperty ReadAsMe(const uintptr_t address)
	{
		DoubleProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DoubleProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Int8Property
// 0x4287A600
class Int8Property
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static Int8Property ReadAsMe(const uintptr_t address)
	{
		Int8Property ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Int8Property& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.LinkerPlaceholderFunction
// 0x4287C600
class LinkerPlaceholderFunction
{
public:
	unsigned char                                      UnknownData00[0x4287C600];                                // 0x0000(0x4287C600) MISSED OFFSET

	static LinkerPlaceholderFunction ReadAsMe(const uintptr_t address)
	{
		LinkerPlaceholderFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LinkerPlaceholderFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.MapProperty
// 0x4287BA00
class MapProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static MapProperty ReadAsMe(const uintptr_t address)
	{
		MapProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MapProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.MulticastDelegateProperty
// 0x4287BA00
class MulticastDelegateProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static MulticastDelegateProperty ReadAsMe(const uintptr_t address)
	{
		MulticastDelegateProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MulticastDelegateProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.Int64Property
// 0x4287A600
class Int64Property
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static Int64Property ReadAsMe(const uintptr_t address)
	{
		Int64Property ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Int64Property& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.NameProperty
// 0x4287BA00
class NameProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static NameProperty ReadAsMe(const uintptr_t address)
	{
		NameProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NameProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.SetProperty
// 0x4287BA00
class SetProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static SetProperty ReadAsMe(const uintptr_t address)
	{
		SetProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SetProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.InterfaceProperty
// 0x4287BA00
class InterfaceProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static InterfaceProperty ReadAsMe(const uintptr_t address)
	{
		InterfaceProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterfaceProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.SoftObjectProperty
// 0x4287AC00
class SoftObjectProperty
{
public:
	unsigned char                                      UnknownData00[0x4287AC00];                                // 0x0000(0x4287AC00) MISSED OFFSET

	static SoftObjectProperty ReadAsMe(const uintptr_t address)
	{
		SoftObjectProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoftObjectProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.StrProperty
// 0x4287BA00
class StrProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static StrProperty ReadAsMe(const uintptr_t address)
	{
		StrProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StrProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.StructProperty
// 0x4287BA00
class StructProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static StructProperty ReadAsMe(const uintptr_t address)
	{
		StructProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StructProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.UInt16Property
// 0x4287A600
class UInt16Property
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static UInt16Property ReadAsMe(const uintptr_t address)
	{
		UInt16Property ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, UInt16Property& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.UInt32Property
// 0x4287A600
class UInt32Property
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static UInt32Property ReadAsMe(const uintptr_t address)
	{
		UInt32Property ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, UInt32Property& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.WeakObjectProperty
// 0x4287AC00
class WeakObjectProperty
{
public:
	unsigned char                                      UnknownData00[0x4287AC00];                                // 0x0000(0x4287AC00) MISSED OFFSET

	static WeakObjectProperty ReadAsMe(const uintptr_t address)
	{
		WeakObjectProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, WeakObjectProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.UInt64Property
// 0x4287A600
class UInt64Property
{
public:
	unsigned char                                      UnknownData00[0x4287A600];                                // 0x0000(0x4287A600) MISSED OFFSET

	static UInt64Property ReadAsMe(const uintptr_t address)
	{
		UInt64Property ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, UInt64Property& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.TextProperty
// 0x4287BA00
class TextProperty
{
public:
	unsigned char                                      UnknownData00[0x4287BA00];                                // 0x0000(0x4287BA00) MISSED OFFSET

	static TextProperty ReadAsMe(const uintptr_t address)
	{
		TextProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.LazyObjectProperty
// 0x4287AC00
class LazyObjectProperty
{
public:
	unsigned char                                      UnknownData00[0x4287AC00];                                // 0x0000(0x4287AC00) MISSED OFFSET

	static LazyObjectProperty ReadAsMe(const uintptr_t address)
	{
		LazyObjectProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LazyObjectProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.SoftClassProperty
// 0x4287FE00
class SoftClassProperty
{
public:
	unsigned char                                      UnknownData00[0x4287FE00];                                // 0x0000(0x4287FE00) MISSED OFFSET

	static SoftClassProperty ReadAsMe(const uintptr_t address)
	{
		SoftClassProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoftClassProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class CoreUObject.ObjectProperty
// 0x4287AC00
class ObjectProperty
{
public:
	unsigned char                                      UnknownData00[0x4287AC00];                                // 0x0000(0x4287AC00) MISSED OFFSET

	static ObjectProperty ReadAsMe(const uintptr_t address)
	{
		ObjectProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ObjectProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
