#pragma once

#include "../Utils.h"
// Name: Satisfactory, Version: 1.0.0

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace SDK
{
//---------------------------------------------------------------------------
// Classes
//---------------------------------------------------------------------------

// Class Engine.ReplicationDriver
// 0x40FE0C00
class ReplicationDriver
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ReplicationDriver ReadAsMe(const uintptr_t address)
	{
		ReplicationDriver ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ReplicationDriver& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ReplicationConnectionDriver
// 0x40FE0C00
class ReplicationConnectionDriver
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ReplicationConnectionDriver ReadAsMe(const uintptr_t address)
	{
		ReplicationConnectionDriver ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ReplicationConnectionDriver& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Actor
// 0x40FE0C00
class AActor
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AActor ReadAsMe(const uintptr_t address)
	{
		AActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ActorComponent
// 0x40FE0C00
class ActorComponent
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ActorComponent ReadAsMe(const uintptr_t address)
	{
		ActorComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ActorComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SceneComponent
// 0x40FEF600
class SceneComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static SceneComponent ReadAsMe(const uintptr_t address)
	{
		SceneComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SceneComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlueprintFunctionLibrary
// 0x40FE0C00
class BlueprintFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static BlueprintFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		BlueprintFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlueprintFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Brush
// 0x40FE8000
class Brush
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static Brush ReadAsMe(const uintptr_t address)
	{
		Brush ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Brush& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Volume
// 0x40FEC400
class Volume
{
public:
	unsigned char                                      UnknownData00[0x40FEC400];                                // 0x0000(0x40FEC400) MISSED OFFSET

	static Volume ReadAsMe(const uintptr_t address)
	{
		Volume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Volume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.World
// 0x40FE0C00
class World
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static World ReadAsMe(const uintptr_t address)
	{
		World ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, World& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotify
// 0x40FE0C00
class AnimNotify
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimNotify ReadAsMe(const uintptr_t address)
	{
		AnimNotify ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotify& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrack
// 0x40FE0C00
class InterpTrack
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InterpTrack ReadAsMe(const uintptr_t address)
	{
		InterpTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackVectorBase
// 0x427D2E00
class InterpTrackVectorBase
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackVectorBase ReadAsMe(const uintptr_t address)
	{
		InterpTrackVectorBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackVectorBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackFloatBase
// 0x427D2E00
class InterpTrackFloatBase
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackFloatBase ReadAsMe(const uintptr_t address)
	{
		InterpTrackFloatBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackFloatBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInst
// 0x40FE0C00
class InterpTrackInst
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InterpTrackInst ReadAsMe(const uintptr_t address)
	{
		InterpTrackInst ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInst& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PrimitiveComponent
// 0x40FEF800
class PrimitiveComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static PrimitiveComponent ReadAsMe(const uintptr_t address)
	{
		PrimitiveComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PrimitiveComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkinnedMeshComponent
// 0x427D0C00
class SkinnedMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0C00];                                // 0x0000(0x427D0C00) MISSED OFFSET

	static SkinnedMeshComponent ReadAsMe(const uintptr_t address)
	{
		SkinnedMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkinnedMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkeletalMesh
// 0x40FE0C00
class SkeletalMesh
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SkeletalMesh ReadAsMe(const uintptr_t address)
	{
		SkeletalMesh ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkeletalMesh& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MeshComponent
// 0x427D0A00
class MeshComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static MeshComponent ReadAsMe(const uintptr_t address)
	{
		MeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameInstance
// 0x40FE0C00
class GameInstance
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static GameInstance ReadAsMe(const uintptr_t address)
	{
		GameInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Info
// 0x40FE8000
class Info
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static Info ReadAsMe(const uintptr_t address)
	{
		Info ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Info& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.OnlineBlueprintCallProxyBase
// 0x40FE0C00
class OnlineBlueprintCallProxyBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static OnlineBlueprintCallProxyBase ReadAsMe(const uintptr_t address)
	{
		OnlineBlueprintCallProxyBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, OnlineBlueprintCallProxyBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Player
// 0x40FE0C00
class Player
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Player ReadAsMe(const uintptr_t address)
	{
		Player ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Player& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NetDriver
// 0x40FE0C00
class NetDriver
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NetDriver ReadAsMe(const uintptr_t address)
	{
		NetDriver ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NetDriver& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NetConnection
// 0x427DA800
class NetConnection
{
public:
	unsigned char                                      UnknownData00[0x427DA800];                                // 0x0000(0x427DA800) MISSED OFFSET

	static NetConnection ReadAsMe(const uintptr_t address)
	{
		NetConnection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NetConnection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlueprintAsyncActionBase
// 0x40FE0C00
class BlueprintAsyncActionBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static BlueprintAsyncActionBase ReadAsMe(const uintptr_t address)
	{
		BlueprintAsyncActionBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlueprintAsyncActionBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.OnlineEngineInterface
// 0x40FE0C00
class OnlineEngineInterface
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static OnlineEngineInterface ReadAsMe(const uintptr_t address)
	{
		OnlineEngineInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, OnlineEngineInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DeveloperSettings
// 0x40FE0C00
class DeveloperSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DeveloperSettings ReadAsMe(const uintptr_t address)
	{
		DeveloperSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DeveloperSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.OnlineSession
// 0x40FE0C00
class OnlineSession
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static OnlineSession ReadAsMe(const uintptr_t address)
	{
		OnlineSession ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, OnlineSession& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CameraModifier
// 0x40FE0C00
class CameraModifier
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CameraModifier ReadAsMe(const uintptr_t address)
	{
		CameraModifier ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CameraModifier& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureSample
// 0x427E6000
class MaterialExpressionTextureSample
{
public:
	unsigned char                                      UnknownData00[0x427E6000];                                // 0x0000(0x427E6000) MISSED OFFSET

	static MaterialExpressionTextureSample ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureSample ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureSample& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Pawn
// 0x40FE8000
class Pawn
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static Pawn ReadAsMe(const uintptr_t address)
	{
		Pawn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Pawn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureSampleParameter
// 0x427E6200
class MaterialExpressionTextureSampleParameter
{
public:
	unsigned char                                      UnknownData00[0x427E6200];                                // 0x0000(0x427E6200) MISSED OFFSET

	static MaterialExpressionTextureSampleParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureSampleParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureSampleParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Character
// 0x427E5800
class Character
{
public:
	unsigned char                                      UnknownData00[0x427E5800];                                // 0x0000(0x427E5800) MISSED OFFSET

	static Character ReadAsMe(const uintptr_t address)
	{
		Character ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Character& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureBase
// 0x427E5E00
class MaterialExpressionTextureBase
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTextureBase ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureSampleParameter2D
// 0x427E6400
class MaterialExpressionTextureSampleParameter2D
{
public:
	unsigned char                                      UnknownData00[0x427E6400];                                // 0x0000(0x427E6400) MISSED OFFSET

	static MaterialExpressionTextureSampleParameter2D ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureSampleParameter2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureSampleParameter2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DataAsset
// 0x40FE0C00
class DataAsset
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DataAsset ReadAsMe(const uintptr_t address)
	{
		DataAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DataAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SplineComponent
// 0x427D0A00
class SplineComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static SplineComponent ReadAsMe(const uintptr_t address)
	{
		SplineComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SplineComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimInstance
// 0x40FE0C00
class AnimInstance
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimInstance ReadAsMe(const uintptr_t address)
	{
		AnimInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkeletalMeshComponent
// 0x427D0E00
class SkeletalMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0E00];                                // 0x0000(0x427D0E00) MISSED OFFSET

	static SkeletalMeshComponent ReadAsMe(const uintptr_t address)
	{
		SkeletalMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkeletalMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AudioComponent
// 0x40FEF800
class AudioComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static AudioComponent ReadAsMe(const uintptr_t address)
	{
		AudioComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AudioComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AssetImportData
// 0x40FE0C00
class AssetImportData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AssetImportData ReadAsMe(const uintptr_t address)
	{
		AssetImportData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AssetImportData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AssetUserData
// 0x40FE0C00
class AssetUserData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AssetUserData ReadAsMe(const uintptr_t address)
	{
		AssetUserData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AssetUserData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpression
// 0x40FE0C00
class MaterialExpression
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static MaterialExpression ReadAsMe(const uintptr_t address)
	{
		MaterialExpression ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpression& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MovementComponent
// 0x40FEF600
class MovementComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static MovementComponent ReadAsMe(const uintptr_t address)
	{
		MovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AISystemBase
// 0x40FE0C00
class AISystemBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AISystemBase ReadAsMe(const uintptr_t address)
	{
		AISystemBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AISystemBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PawnMovementComponent
// 0x42805C00
class PawnMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x42805C00];                                // 0x0000(0x42805C00) MISSED OFFSET

	static PawnMovementComponent ReadAsMe(const uintptr_t address)
	{
		PawnMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PawnMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavMovementComponent
// 0x42805A00
class NavMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x42805A00];                                // 0x0000(0x42805A00) MISSED OFFSET

	static NavMovementComponent ReadAsMe(const uintptr_t address)
	{
		NavMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AssetManager
// 0x40FE0C00
class AssetManager
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AssetManager ReadAsMe(const uintptr_t address)
	{
		AssetManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AssetManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextRenderComponent
// 0x427D0A00
class TextRenderComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static TextRenderComponent ReadAsMe(const uintptr_t address)
	{
		TextRenderComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextRenderComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CharacterMovementComponent
// 0x42805E00
class CharacterMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x42805E00];                                // 0x0000(0x42805E00) MISSED OFFSET

	static CharacterMovementComponent ReadAsMe(const uintptr_t address)
	{
		CharacterMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CharacterMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Controller
// 0x40FE8000
class Controller
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static Controller ReadAsMe(const uintptr_t address)
	{
		Controller ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Controller& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlayerController
// 0x4282A200
class PlayerController
{
public:
	unsigned char                                      UnknownData00[0x4282A200];                                // 0x0000(0x4282A200) MISSED OFFSET

	static PlayerController ReadAsMe(const uintptr_t address)
	{
		PlayerController ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlayerController& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CheatManager
// 0x40FE0C00
class CheatManager
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CheatManager ReadAsMe(const uintptr_t address)
	{
		CheatManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CheatManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Distribution
// 0x40FE0C00
class Distribution
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Distribution ReadAsMe(const uintptr_t address)
	{
		Distribution ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Distribution& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.StaticMeshComponent
// 0x427D0C00
class StaticMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0C00];                                // 0x0000(0x427D0C00) MISSED OFFSET

	static StaticMeshComponent ReadAsMe(const uintptr_t address)
	{
		StaticMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StaticMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionVector
// 0x42829200
class DistributionVector
{
public:
	unsigned char                                      UnknownData00[0x42829200];                                // 0x0000(0x42829200) MISSED OFFSET

	static DistributionVector ReadAsMe(const uintptr_t address)
	{
		DistributionVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DamageType
// 0x40FE0C00
class DamageType
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DamageType ReadAsMe(const uintptr_t address)
	{
		DamageType ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DamageType& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Engine
// 0x40FE0C00
class Engine
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Engine ReadAsMe(const uintptr_t address)
	{
		Engine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Engine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameModeBase
// 0x427D5600
class GameModeBase
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static GameModeBase ReadAsMe(const uintptr_t address)
	{
		GameModeBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameModeBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameMode
// 0x4284EE00
class GameMode
{
public:
	unsigned char                                      UnknownData00[0x4284EE00];                                // 0x0000(0x4284EE00) MISSED OFFSET

	static GameMode ReadAsMe(const uintptr_t address)
	{
		GameMode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameMode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameEngine
// 0x4284F600
class GameEngine
{
public:
	unsigned char                                      UnknownData00[0x4284F600];                                // 0x0000(0x4284F600) MISSED OFFSET

	static GameEngine ReadAsMe(const uintptr_t address)
	{
		GameEngine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameEngine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameSession
// 0x427D5600
class GameSession
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static GameSession ReadAsMe(const uintptr_t address)
	{
		GameSession ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameSession& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameState
// 0x4284D200
class GameState
{
public:
	unsigned char                                      UnknownData00[0x4284D200];                                // 0x0000(0x4284D200) MISSED OFFSET

	static GameState ReadAsMe(const uintptr_t address)
	{
		GameState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ScriptViewportClient
// 0x40FE0C00
class ScriptViewportClient
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ScriptViewportClient ReadAsMe(const uintptr_t address)
	{
		ScriptViewportClient ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ScriptViewportClient& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameViewportClient
// 0x42854200
class GameViewportClient
{
public:
	unsigned char                                      UnknownData00[0x42854200];                                // 0x0000(0x42854200) MISSED OFFSET

	static GameViewportClient ReadAsMe(const uintptr_t address)
	{
		GameViewportClient ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameViewportClient& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameUserSettings
// 0x40FE0C00
class GameUserSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static GameUserSettings ReadAsMe(const uintptr_t address)
	{
		GameUserSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameUserSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameStateBase
// 0x427D5600
class GameStateBase
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static GameStateBase ReadAsMe(const uintptr_t address)
	{
		GameStateBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameStateBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HUD
// 0x40FE8000
class HUD
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static HUD ReadAsMe(const uintptr_t address)
	{
		HUD ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HUD& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BoxComponent
// 0x42857A00
class BoxComponent
{
public:
	unsigned char                                      UnknownData00[0x42857A00];                                // 0x0000(0x42857A00) MISSED OFFSET

	static BoxComponent ReadAsMe(const uintptr_t address)
	{
		BoxComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BoxComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ShapeComponent
// 0x427D0A00
class ShapeComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static ShapeComponent ReadAsMe(const uintptr_t address)
	{
		ShapeComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ShapeComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LocalPlayer
// 0x427DA800
class LocalPlayer
{
public:
	unsigned char                                      UnknownData00[0x427DA800];                                // 0x0000(0x427DA800) MISSED OFFSET

	static LocalPlayer ReadAsMe(const uintptr_t address)
	{
		LocalPlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LocalPlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SceneCapture2D
// 0x4285B800
class SceneCapture2D
{
public:
	unsigned char                                      UnknownData00[0x4285B800];                                // 0x0000(0x4285B800) MISSED OFFSET

	static SceneCapture2D ReadAsMe(const uintptr_t address)
	{
		SceneCapture2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SceneCapture2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SceneCapture
// 0x40FE8000
class SceneCapture
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static SceneCapture ReadAsMe(const uintptr_t address)
	{
		SceneCapture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SceneCapture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavAreaBase
// 0x40FE0C00
class NavAreaBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NavAreaBase ReadAsMe(const uintptr_t address)
	{
		NavAreaBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavAreaBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CameraShake
// 0x40FE0C00
class CameraShake
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CameraShake ReadAsMe(const uintptr_t address)
	{
		CameraShake ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CameraShake& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavigationObjectBase
// 0x40FE8000
class NavigationObjectBase
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static NavigationObjectBase ReadAsMe(const uintptr_t address)
	{
		NavigationObjectBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavigationObjectBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlayerState
// 0x427D5600
class PlayerState
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static PlayerState ReadAsMe(const uintptr_t address)
	{
		PlayerState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlayerState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlayerStart
// 0x42863C00
class PlayerStart
{
public:
	unsigned char                                      UnknownData00[0x42863C00];                                // 0x0000(0x42863C00) MISSED OFFSET

	static PlayerStart ReadAsMe(const uintptr_t address)
	{
		PlayerStart ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlayerStart& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.StaticMeshActor
// 0x40FE8000
class StaticMeshActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static StaticMeshActor ReadAsMe(const uintptr_t address)
	{
		StaticMeshActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StaticMeshActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsVolume
// 0x40FEC600
class PhysicsVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static PhysicsVolume ReadAsMe(const uintptr_t address)
	{
		PhysicsVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.WorldSettings
// 0x427D5600
class WorldSettings
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static WorldSettings ReadAsMe(const uintptr_t address)
	{
		WorldSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, WorldSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkyLight
// 0x427D5600
class SkyLight
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static SkyLight ReadAsMe(const uintptr_t address)
	{
		SkyLight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkyLight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Texture
// 0x40FE0C00
class Texture
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Texture ReadAsMe(const uintptr_t address)
	{
		Texture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Texture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextureCube
// 0x42881000
class TextureCube
{
public:
	unsigned char                                      UnknownData00[0x42881000];                                // 0x0000(0x42881000) MISSED OFFSET

	static TextureCube ReadAsMe(const uintptr_t address)
	{
		TextureCube ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextureCube& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InstancedStaticMeshComponent
// 0x42828C00
class InstancedStaticMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x42828C00];                                // 0x0000(0x42828C00) MISSED OFFSET

	static InstancedStaticMeshComponent ReadAsMe(const uintptr_t address)
	{
		InstancedStaticMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InstancedStaticMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HierarchicalInstancedStaticMeshComponent
// 0x42886200
class HierarchicalInstancedStaticMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x42886200];                                // 0x0000(0x42886200) MISSED OFFSET

	static HierarchicalInstancedStaticMeshComponent ReadAsMe(const uintptr_t address)
	{
		HierarchicalInstancedStaticMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HierarchicalInstancedStaticMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialInstanceConstant
// 0x4288AE00
class MaterialInstanceConstant
{
public:
	unsigned char                                      UnknownData00[0x4288AE00];                                // 0x0000(0x4288AE00) MISSED OFFSET

	static MaterialInstanceConstant ReadAsMe(const uintptr_t address)
	{
		MaterialInstanceConstant ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialInstanceConstant& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialInterface
// 0x40FE0C00
class MaterialInterface
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static MaterialInterface ReadAsMe(const uintptr_t address)
	{
		MaterialInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCustomOutput
// 0x427E5E00
class MaterialExpressionCustomOutput
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionCustomOutput ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCustomOutput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCustomOutput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialInstance
// 0x4288AC00
class MaterialInstance
{
public:
	unsigned char                                      UnknownData00[0x4288AC00];                                // 0x0000(0x4288AC00) MISSED OFFSET

	static MaterialInstance ReadAsMe(const uintptr_t address)
	{
		MaterialInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EngineCustomTimeStep
// 0x40FE0C00
class EngineCustomTimeStep
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static EngineCustomTimeStep ReadAsMe(const uintptr_t address)
	{
		EngineCustomTimeStep ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EngineCustomTimeStep& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotifyState
// 0x40FE0C00
class AnimNotifyState
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimNotifyState ReadAsMe(const uintptr_t address)
	{
		AnimNotifyState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotifyState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DynamicBlueprintBinding
// 0x40FE0C00
class DynamicBlueprintBinding
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DynamicBlueprintBinding ReadAsMe(const uintptr_t address)
	{
		DynamicBlueprintBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DynamicBlueprintBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlueprintGeneratedClass
// 0x42874600
class BlueprintGeneratedClass
{
public:
	unsigned char                                      UnknownData00[0x42874600];                                // 0x0000(0x42874600) MISSED OFFSET

	static BlueprintGeneratedClass ReadAsMe(const uintptr_t address)
	{
		BlueprintGeneratedClass ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlueprintGeneratedClass& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CameraActor
// 0x40FE8000
class CameraActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static CameraActor ReadAsMe(const uintptr_t address)
	{
		CameraActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CameraActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundEffectPreset
// 0x40FE0C00
class SoundEffectPreset
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundEffectPreset ReadAsMe(const uintptr_t address)
	{
		SoundEffectPreset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundEffectPreset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CameraComponent
// 0x40FEF800
class CameraComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static CameraComponent ReadAsMe(const uintptr_t address)
	{
		CameraComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CameraComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundBase
// 0x40FE0C00
class SoundBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundBase ReadAsMe(const uintptr_t address)
	{
		SoundBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundWaveProcedural
// 0x428B6E00
class SoundWaveProcedural
{
public:
	unsigned char                                      UnknownData00[0x428B6E00];                                // 0x0000(0x428B6E00) MISSED OFFSET

	static SoundWaveProcedural ReadAsMe(const uintptr_t address)
	{
		SoundWaveProcedural ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundWaveProcedural& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundEffectSubmixPreset
// 0x428B7800
class SoundEffectSubmixPreset
{
public:
	unsigned char                                      UnknownData00[0x428B7800];                                // 0x0000(0x428B7800) MISSED OFFSET

	static SoundEffectSubmixPreset ReadAsMe(const uintptr_t address)
	{
		SoundEffectSubmixPreset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundEffectSubmixPreset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundWave
// 0x428B6C00
class SoundWave
{
public:
	unsigned char                                      UnknownData00[0x428B6C00];                                // 0x0000(0x428B6C00) MISSED OFFSET

	static SoundWave ReadAsMe(const uintptr_t address)
	{
		SoundWave ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundWave& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Blueprint
// 0x428BA600
class Blueprint
{
public:
	unsigned char                                      UnknownData00[0x428BA600];                                // 0x0000(0x428BA600) MISSED OFFSET

	static Blueprint ReadAsMe(const uintptr_t address)
	{
		Blueprint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Blueprint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlueprintCore
// 0x40FE0C00
class BlueprintCore
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static BlueprintCore ReadAsMe(const uintptr_t address)
	{
		BlueprintCore ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlueprintCore& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Model
// 0x40FE0C00
class Model
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Model ReadAsMe(const uintptr_t address)
	{
		Model ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Model& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Channel
// 0x40FE0C00
class Channel
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Channel ReadAsMe(const uintptr_t address)
	{
		Channel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Channel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ActorChannel
// 0x428BE800
class ActorChannel
{
public:
	unsigned char                                      UnknownData00[0x428BE800];                                // 0x0000(0x428BE800) MISSED OFFSET

	static ActorChannel ReadAsMe(const uintptr_t address)
	{
		ActorChannel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ActorChannel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlendSpace
// 0x428BE200
class BlendSpace
{
public:
	unsigned char                                      UnknownData00[0x428BE200];                                // 0x0000(0x428BE200) MISSED OFFSET

	static BlendSpace ReadAsMe(const uintptr_t address)
	{
		BlendSpace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlendSpace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AimOffsetBlendSpace
// 0x428BE400
class AimOffsetBlendSpace
{
public:
	unsigned char                                      UnknownData00[0x428BE400];                                // 0x0000(0x428BE400) MISSED OFFSET

	static AimOffsetBlendSpace ReadAsMe(const uintptr_t address)
	{
		AimOffsetBlendSpace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AimOffsetBlendSpace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlendSpaceBase
// 0x428BE000
class BlendSpaceBase
{
public:
	unsigned char                                      UnknownData00[0x428BE000];                                // 0x0000(0x428BE000) MISSED OFFSET

	static BlendSpaceBase ReadAsMe(const uintptr_t address)
	{
		BlendSpaceBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlendSpaceBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AimOffsetBlendSpace1D
// 0x428BDC00
class AimOffsetBlendSpace1D
{
public:
	unsigned char                                      UnknownData00[0x428BDC00];                                // 0x0000(0x428BDC00) MISSED OFFSET

	static AimOffsetBlendSpace1D ReadAsMe(const uintptr_t address)
	{
		AimOffsetBlendSpace1D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AimOffsetBlendSpace1D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimationSettings
// 0x427D8E00
class AnimationSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static AnimationSettings ReadAsMe(const uintptr_t address)
	{
		AnimationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlendSpace1D
// 0x428BE200
class BlendSpace1D
{
public:
	unsigned char                                      UnknownData00[0x428BE200];                                // 0x0000(0x428BE200) MISSED OFFSET

	static BlendSpace1D ReadAsMe(const uintptr_t address)
	{
		BlendSpace1D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlendSpace1D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimBlueprint
// 0x428BA800
class AnimBlueprint
{
public:
	unsigned char                                      UnknownData00[0x428BA800];                                // 0x0000(0x428BA800) MISSED OFFSET

	static AnimBlueprint ReadAsMe(const uintptr_t address)
	{
		AnimBlueprint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimBlueprint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimClassInterface
// 0x427D4600
class AnimClassInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static AnimClassInterface ReadAsMe(const uintptr_t address)
	{
		AnimClassInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimClassInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimationAsset
// 0x40FE0C00
class AnimationAsset
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimationAsset ReadAsMe(const uintptr_t address)
	{
		AnimationAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimationAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimBlueprintGeneratedClass
// 0x428B1E00
class AnimBlueprintGeneratedClass
{
public:
	unsigned char                                      UnknownData00[0x428B1E00];                                // 0x0000(0x428B1E00) MISSED OFFSET

	static AnimBlueprintGeneratedClass ReadAsMe(const uintptr_t address)
	{
		AnimBlueprintGeneratedClass ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimBlueprintGeneratedClass& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimSequenceBase
// 0x428BE000
class AnimSequenceBase
{
public:
	unsigned char                                      UnknownData00[0x428BE000];                                // 0x0000(0x428BE000) MISSED OFFSET

	static AnimSequenceBase ReadAsMe(const uintptr_t address)
	{
		AnimSequenceBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimSequenceBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimClassData
// 0x40FE0C00
class AnimClassData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimClassData ReadAsMe(const uintptr_t address)
	{
		AnimClassData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimClassData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AmbientSound
// 0x40FE8000
class AmbientSound
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static AmbientSound ReadAsMe(const uintptr_t address)
	{
		AmbientSound ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AmbientSound& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompress
// 0x40FE0C00
class AnimCompress
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimCompress ReadAsMe(const uintptr_t address)
	{
		AnimCompress ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompress& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompositeBase
// 0x428BCA00
class AnimCompositeBase
{
public:
	unsigned char                                      UnknownData00[0x428BCA00];                                // 0x0000(0x428BCA00) MISSED OFFSET

	static AnimCompositeBase ReadAsMe(const uintptr_t address)
	{
		AnimCompositeBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompositeBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimComposite
// 0x428BCC00
class AnimComposite
{
public:
	unsigned char                                      UnknownData00[0x428BCC00];                                // 0x0000(0x428BCC00) MISSED OFFSET

	static AnimComposite ReadAsMe(const uintptr_t address)
	{
		AnimComposite ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimComposite& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompress_BitwiseCompressOnly
// 0x428BC800
class AnimCompress_BitwiseCompressOnly
{
public:
	unsigned char                                      UnknownData00[0x428BC800];                                // 0x0000(0x428BC800) MISSED OFFSET

	static AnimCompress_BitwiseCompressOnly ReadAsMe(const uintptr_t address)
	{
		AnimCompress_BitwiseCompressOnly ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompress_BitwiseCompressOnly& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompress_Automatic
// 0x428BC800
class AnimCompress_Automatic
{
public:
	unsigned char                                      UnknownData00[0x428BC800];                                // 0x0000(0x428BC800) MISSED OFFSET

	static AnimCompress_Automatic ReadAsMe(const uintptr_t address)
	{
		AnimCompress_Automatic ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompress_Automatic& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompress_LeastDestructive
// 0x428BC800
class AnimCompress_LeastDestructive
{
public:
	unsigned char                                      UnknownData00[0x428BC800];                                // 0x0000(0x428BC800) MISSED OFFSET

	static AnimCompress_LeastDestructive ReadAsMe(const uintptr_t address)
	{
		AnimCompress_LeastDestructive ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompress_LeastDestructive& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompress_PerTrackCompression
// 0x42903A00
class AnimCompress_PerTrackCompression
{
public:
	unsigned char                                      UnknownData00[0x42903A00];                                // 0x0000(0x42903A00) MISSED OFFSET

	static AnimCompress_PerTrackCompression ReadAsMe(const uintptr_t address)
	{
		AnimCompress_PerTrackCompression ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompress_PerTrackCompression& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompress_RemoveEverySecondKey
// 0x428BC800
class AnimCompress_RemoveEverySecondKey
{
public:
	unsigned char                                      UnknownData00[0x428BC800];                                // 0x0000(0x428BC800) MISSED OFFSET

	static AnimCompress_RemoveEverySecondKey ReadAsMe(const uintptr_t address)
	{
		AnimCompress_RemoveEverySecondKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompress_RemoveEverySecondKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompress_RemoveTrivialKeys
// 0x428BC800
class AnimCompress_RemoveTrivialKeys
{
public:
	unsigned char                                      UnknownData00[0x428BC800];                                // 0x0000(0x428BC800) MISSED OFFSET

	static AnimCompress_RemoveTrivialKeys ReadAsMe(const uintptr_t address)
	{
		AnimCompress_RemoveTrivialKeys ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompress_RemoveTrivialKeys& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCurveCompressionCodec
// 0x40FE0C00
class AnimCurveCompressionCodec
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimCurveCompressionCodec ReadAsMe(const uintptr_t address)
	{
		AnimCurveCompressionCodec ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCurveCompressionCodec& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCompress_RemoveLinearKeys
// 0x428BC800
class AnimCompress_RemoveLinearKeys
{
public:
	unsigned char                                      UnknownData00[0x428BC800];                                // 0x0000(0x428BC800) MISSED OFFSET

	static AnimCompress_RemoveLinearKeys ReadAsMe(const uintptr_t address)
	{
		AnimCompress_RemoveLinearKeys ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCompress_RemoveLinearKeys& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCurveCompressionSettings
// 0x40FE0C00
class AnimCurveCompressionSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimCurveCompressionSettings ReadAsMe(const uintptr_t address)
	{
		AnimCurveCompressionSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCurveCompressionSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimMetaData
// 0x40FE0C00
class AnimMetaData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimMetaData ReadAsMe(const uintptr_t address)
	{
		AnimMetaData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimMetaData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotify_PauseClothingSimulation
// 0x427D3400
class AnimNotify_PauseClothingSimulation
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static AnimNotify_PauseClothingSimulation ReadAsMe(const uintptr_t address)
	{
		AnimNotify_PauseClothingSimulation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotify_PauseClothingSimulation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotify_PlayParticleEffect
// 0x427D3400
class AnimNotify_PlayParticleEffect
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static AnimNotify_PlayParticleEffect ReadAsMe(const uintptr_t address)
	{
		AnimNotify_PlayParticleEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotify_PlayParticleEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotify_PlaySound
// 0x427D3400
class AnimNotify_PlaySound
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static AnimNotify_PlaySound ReadAsMe(const uintptr_t address)
	{
		AnimNotify_PlaySound ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotify_PlaySound& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCurveCompressionCodec_CompressedRichCurve
// 0x42903400
class AnimCurveCompressionCodec_CompressedRichCurve
{
public:
	unsigned char                                      UnknownData00[0x42903400];                                // 0x0000(0x42903400) MISSED OFFSET

	static AnimCurveCompressionCodec_CompressedRichCurve ReadAsMe(const uintptr_t address)
	{
		AnimCurveCompressionCodec_CompressedRichCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCurveCompressionCodec_CompressedRichCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimMontage
// 0x428BCC00
class AnimMontage
{
public:
	unsigned char                                      UnknownData00[0x428BCC00];                                // 0x0000(0x428BCC00) MISSED OFFSET

	static AnimMontage ReadAsMe(const uintptr_t address)
	{
		AnimMontage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimMontage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotify_ResumeClothingSimulation
// 0x427D3400
class AnimNotify_ResumeClothingSimulation
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static AnimNotify_ResumeClothingSimulation ReadAsMe(const uintptr_t address)
	{
		AnimNotify_ResumeClothingSimulation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotify_ResumeClothingSimulation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimCurveCompressionCodec_UniformlySampled
// 0x42903400
class AnimCurveCompressionCodec_UniformlySampled
{
public:
	unsigned char                                      UnknownData00[0x42903400];                                // 0x0000(0x42903400) MISSED OFFSET

	static AnimCurveCompressionCodec_UniformlySampled ReadAsMe(const uintptr_t address)
	{
		AnimCurveCompressionCodec_UniformlySampled ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimCurveCompressionCodec_UniformlySampled& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotify_ResetClothingSimulation
// 0x427D3400
class AnimNotify_ResetClothingSimulation
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static AnimNotify_ResetClothingSimulation ReadAsMe(const uintptr_t address)
	{
		AnimNotify_ResetClothingSimulation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotify_ResetClothingSimulation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotifyState_Trail
// 0x4288DC00
class AnimNotifyState_Trail
{
public:
	unsigned char                                      UnknownData00[0x4288DC00];                                // 0x0000(0x4288DC00) MISSED OFFSET

	static AnimNotifyState_Trail ReadAsMe(const uintptr_t address)
	{
		AnimNotifyState_Trail ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotifyState_Trail& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotifyState_TimedParticleEffect
// 0x4288DC00
class AnimNotifyState_TimedParticleEffect
{
public:
	unsigned char                                      UnknownData00[0x4288DC00];                                // 0x0000(0x4288DC00) MISSED OFFSET

	static AnimNotifyState_TimedParticleEffect ReadAsMe(const uintptr_t address)
	{
		AnimNotifyState_TimedParticleEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotifyState_TimedParticleEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotify_ResetDynamics
// 0x427D3400
class AnimNotify_ResetDynamics
{
public:
	unsigned char                                      UnknownData00[0x427D3400];                                // 0x0000(0x427D3400) MISSED OFFSET

	static AnimNotify_ResetDynamics ReadAsMe(const uintptr_t address)
	{
		AnimNotify_ResetDynamics ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotify_ResetDynamics& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimSequence
// 0x428BCA00
class AnimSequence
{
public:
	unsigned char                                      UnknownData00[0x428BCA00];                                // 0x0000(0x428BCA00) MISSED OFFSET

	static AnimSequence ReadAsMe(const uintptr_t address)
	{
		AnimSequence ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimSequence& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimStateMachineTypes
// 0x40FE0C00
class AnimStateMachineTypes
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimStateMachineTypes ReadAsMe(const uintptr_t address)
	{
		AnimStateMachineTypes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimStateMachineTypes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ApplicationLifecycleComponent
// 0x40FEF600
class ApplicationLifecycleComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static ApplicationLifecycleComponent ReadAsMe(const uintptr_t address)
	{
		ApplicationLifecycleComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ApplicationLifecycleComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimSingleNodeInstance
// 0x427EA800
class AnimSingleNodeInstance
{
public:
	unsigned char                                      UnknownData00[0x427EA800];                                // 0x0000(0x427EA800) MISSED OFFSET

	static AnimSingleNodeInstance ReadAsMe(const uintptr_t address)
	{
		AnimSingleNodeInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimSingleNodeInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimSet
// 0x40FE0C00
class AnimSet
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AnimSet ReadAsMe(const uintptr_t address)
	{
		AnimSet ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimSet& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AnimNotifyState_DisableRootMotion
// 0x4288DC00
class AnimNotifyState_DisableRootMotion
{
public:
	unsigned char                                      UnknownData00[0x4288DC00];                                // 0x0000(0x4288DC00) MISSED OFFSET

	static AnimNotifyState_DisableRootMotion ReadAsMe(const uintptr_t address)
	{
		AnimNotifyState_DisableRootMotion ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AnimNotifyState_DisableRootMotion& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ArrowComponent
// 0x427D0A00
class ArrowComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static ArrowComponent ReadAsMe(const uintptr_t address)
	{
		ArrowComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ArrowComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AssetManagerSettings
// 0x427D8E00
class AssetManagerSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static AssetManagerSettings ReadAsMe(const uintptr_t address)
	{
		AssetManagerSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AssetManagerSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AssetExportTask
// 0x40FE0C00
class AssetExportTask
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AssetExportTask ReadAsMe(const uintptr_t address)
	{
		AssetExportTask ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AssetExportTask& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AssetMappingTable
// 0x40FE0C00
class AssetMappingTable
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AssetMappingTable ReadAsMe(const uintptr_t address)
	{
		AssetMappingTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AssetMappingTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AsyncActionLoadPrimaryAssetBase
// 0x427D9800
class AsyncActionLoadPrimaryAssetBase
{
public:
	unsigned char                                      UnknownData00[0x427D9800];                                // 0x0000(0x427D9800) MISSED OFFSET

	static AsyncActionLoadPrimaryAssetBase ReadAsMe(const uintptr_t address)
	{
		AsyncActionLoadPrimaryAssetBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AsyncActionLoadPrimaryAssetBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AsyncActionLoadPrimaryAssetList
// 0x42900400
class AsyncActionLoadPrimaryAssetList
{
public:
	unsigned char                                      UnknownData00[0x42900400];                                // 0x0000(0x42900400) MISSED OFFSET

	static AsyncActionLoadPrimaryAssetList ReadAsMe(const uintptr_t address)
	{
		AsyncActionLoadPrimaryAssetList ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AsyncActionLoadPrimaryAssetList& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AsyncActionLoadPrimaryAsset
// 0x42900400
class AsyncActionLoadPrimaryAsset
{
public:
	unsigned char                                      UnknownData00[0x42900400];                                // 0x0000(0x42900400) MISSED OFFSET

	static AsyncActionLoadPrimaryAsset ReadAsMe(const uintptr_t address)
	{
		AsyncActionLoadPrimaryAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AsyncActionLoadPrimaryAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AsyncActionLoadPrimaryAssetClassList
// 0x42900400
class AsyncActionLoadPrimaryAssetClassList
{
public:
	unsigned char                                      UnknownData00[0x42900400];                                // 0x0000(0x42900400) MISSED OFFSET

	static AsyncActionLoadPrimaryAssetClassList ReadAsMe(const uintptr_t address)
	{
		AsyncActionLoadPrimaryAssetClassList ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AsyncActionLoadPrimaryAssetClassList& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AtmosphericFog
// 0x427D5600
class AtmosphericFog
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static AtmosphericFog ReadAsMe(const uintptr_t address)
	{
		AtmosphericFog ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AtmosphericFog& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AudioSettings
// 0x427D8E00
class AudioSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static AudioSettings ReadAsMe(const uintptr_t address)
	{
		AudioSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AudioSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AtmosphericFogComponent
// 0x40FEF800
class AtmosphericFogComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static AtmosphericFogComponent ReadAsMe(const uintptr_t address)
	{
		AtmosphericFogComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AtmosphericFogComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AsyncActionChangePrimaryAssetBundles
// 0x42900400
class AsyncActionChangePrimaryAssetBundles
{
public:
	unsigned char                                      UnknownData00[0x42900400];                                // 0x0000(0x42900400) MISSED OFFSET

	static AsyncActionChangePrimaryAssetBundles ReadAsMe(const uintptr_t address)
	{
		AsyncActionChangePrimaryAssetBundles ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AsyncActionChangePrimaryAssetBundles& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AvoidanceManager
// 0x40FE0C00
class AvoidanceManager
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AvoidanceManager ReadAsMe(const uintptr_t address)
	{
		AvoidanceManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AvoidanceManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AsyncActionLoadPrimaryAssetClass
// 0x42900400
class AsyncActionLoadPrimaryAssetClass
{
public:
	unsigned char                                      UnknownData00[0x42900400];                                // 0x0000(0x42900400) MISSED OFFSET

	static AsyncActionLoadPrimaryAssetClass ReadAsMe(const uintptr_t address)
	{
		AsyncActionLoadPrimaryAssetClass ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AsyncActionLoadPrimaryAssetClass& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlendableInterface
// 0x427D4600
class BlendableInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static BlendableInterface ReadAsMe(const uintptr_t address)
	{
		BlendableInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlendableInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Skeleton
// 0x40FE0C00
class Skeleton
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Skeleton ReadAsMe(const uintptr_t address)
	{
		Skeleton ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Skeleton& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BillboardComponent
// 0x427D0A00
class BillboardComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static BillboardComponent ReadAsMe(const uintptr_t address)
	{
		BillboardComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BillboardComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AutomationTestSettings
// 0x40FE0C00
class AutomationTestSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AutomationTestSettings ReadAsMe(const uintptr_t address)
	{
		AutomationTestSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AutomationTestSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AudioVolume
// 0x40FEC600
class AudioVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static AudioVolume ReadAsMe(const uintptr_t address)
	{
		AudioVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AudioVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlockingVolume
// 0x40FEC600
class BlockingVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static BlockingVolume ReadAsMe(const uintptr_t address)
	{
		BlockingVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlockingVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlueprintPlatformLibrary
// 0x40FEE400
class BlueprintPlatformLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static BlueprintPlatformLibrary ReadAsMe(const uintptr_t address)
	{
		BlueprintPlatformLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlueprintPlatformLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlendProfile
// 0x40FE0C00
class BlendProfile
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static BlendProfile ReadAsMe(const uintptr_t address)
	{
		BlendProfile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlendProfile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BodySetup
// 0x40FE0C00
class BodySetup
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static BodySetup ReadAsMe(const uintptr_t address)
	{
		BodySetup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BodySetup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlueprintMapLibrary
// 0x40FEE400
class BlueprintMapLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static BlueprintMapLibrary ReadAsMe(const uintptr_t address)
	{
		BlueprintMapLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlueprintMapLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlatformGameInstance
// 0x427D7000
class PlatformGameInstance
{
public:
	unsigned char                                      UnknownData00[0x427D7000];                                // 0x0000(0x427D7000) MISSED OFFSET

	static PlatformGameInstance ReadAsMe(const uintptr_t address)
	{
		PlatformGameInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlatformGameInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BookmarkBase
// 0x40FE0C00
class BookmarkBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static BookmarkBase ReadAsMe(const uintptr_t address)
	{
		BookmarkBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BookmarkBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BoneMaskFilter
// 0x40FE0C00
class BoneMaskFilter
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static BoneMaskFilter ReadAsMe(const uintptr_t address)
	{
		BoneMaskFilter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BoneMaskFilter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BookMark
// 0x42905600
class BookMark
{
public:
	unsigned char                                      UnknownData00[0x42905600];                                // 0x0000(0x42905600) MISSED OFFSET

	static BookMark ReadAsMe(const uintptr_t address)
	{
		BookMark ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BookMark& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BoxReflectionCapture
// 0x42905000
class BoxReflectionCapture
{
public:
	unsigned char                                      UnknownData00[0x42905000];                                // 0x0000(0x42905000) MISSED OFFSET

	static BoxReflectionCapture ReadAsMe(const uintptr_t address)
	{
		BoxReflectionCapture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BoxReflectionCapture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlueprintPathsLibrary
// 0x40FEE400
class BlueprintPathsLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static BlueprintPathsLibrary ReadAsMe(const uintptr_t address)
	{
		BlueprintPathsLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlueprintPathsLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ReflectionCaptureComponent
// 0x40FEF800
class ReflectionCaptureComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static ReflectionCaptureComponent ReadAsMe(const uintptr_t address)
	{
		ReflectionCaptureComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ReflectionCaptureComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BoxReflectionCaptureComponent
// 0x42904C00
class BoxReflectionCaptureComponent
{
public:
	unsigned char                                      UnknownData00[0x42904C00];                                // 0x0000(0x42904C00) MISSED OFFSET

	static BoxReflectionCaptureComponent ReadAsMe(const uintptr_t address)
	{
		BoxReflectionCaptureComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BoxReflectionCaptureComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BlueprintSetLibrary
// 0x40FEE400
class BlueprintSetLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static BlueprintSetLibrary ReadAsMe(const uintptr_t address)
	{
		BlueprintSetLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BlueprintSetLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Breakpoint
// 0x40FE0C00
class Breakpoint
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Breakpoint ReadAsMe(const uintptr_t address)
	{
		Breakpoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Breakpoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BrushBuilder
// 0x40FE0C00
class BrushBuilder
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static BrushBuilder ReadAsMe(const uintptr_t address)
	{
		BrushBuilder ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BrushBuilder& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BrushComponent
// 0x427D0A00
class BrushComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static BrushComponent ReadAsMe(const uintptr_t address)
	{
		BrushComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BrushComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BrushShape
// 0x40FEC400
class BrushShape
{
public:
	unsigned char                                      UnknownData00[0x40FEC400];                                // 0x0000(0x40FEC400) MISSED OFFSET

	static BrushShape ReadAsMe(const uintptr_t address)
	{
		BrushShape ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BrushShape& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ButtonStyleAsset
// 0x40FE0C00
class ButtonStyleAsset
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ButtonStyleAsset ReadAsMe(const uintptr_t address)
	{
		ButtonStyleAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ButtonStyleAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ReflectionCapture
// 0x40FE8000
class ReflectionCapture
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static ReflectionCapture ReadAsMe(const uintptr_t address)
	{
		ReflectionCapture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ReflectionCapture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CameraBlockingVolume
// 0x40FEC600
class CameraBlockingVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static CameraBlockingVolume ReadAsMe(const uintptr_t address)
	{
		CameraBlockingVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CameraBlockingVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CameraAnim
// 0x40FE0C00
class CameraAnim
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CameraAnim ReadAsMe(const uintptr_t address)
	{
		CameraAnim ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CameraAnim& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CameraAnimInst
// 0x40FE0C00
class CameraAnimInst
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CameraAnimInst ReadAsMe(const uintptr_t address)
	{
		CameraAnimInst ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CameraAnimInst& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextureRenderTarget
// 0x42881000
class TextureRenderTarget
{
public:
	unsigned char                                      UnknownData00[0x42881000];                                // 0x0000(0x42881000) MISSED OFFSET

	static TextureRenderTarget ReadAsMe(const uintptr_t address)
	{
		TextureRenderTarget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextureRenderTarget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.BookMark2D
// 0x42905600
class BookMark2D
{
public:
	unsigned char                                      UnknownData00[0x42905600];                                // 0x0000(0x42905600) MISSED OFFSET

	static BookMark2D ReadAsMe(const uintptr_t address)
	{
		BookMark2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, BookMark2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Canvas
// 0x40FE0C00
class Canvas
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Canvas ReadAsMe(const uintptr_t address)
	{
		Canvas ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Canvas& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CapsuleComponent
// 0x42857A00
class CapsuleComponent
{
public:
	unsigned char                                      UnknownData00[0x42857A00];                                // 0x0000(0x42857A00) MISSED OFFSET

	static CapsuleComponent ReadAsMe(const uintptr_t address)
	{
		CapsuleComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CapsuleComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CheckBoxStyleAsset
// 0x40FE0C00
class CheckBoxStyleAsset
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CheckBoxStyleAsset ReadAsMe(const uintptr_t address)
	{
		CheckBoxStyleAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CheckBoxStyleAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ChildActorComponent
// 0x40FEF800
class ChildActorComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static ChildActorComponent ReadAsMe(const uintptr_t address)
	{
		ChildActorComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ChildActorComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ChildConnection
// 0x427DAA00
class ChildConnection
{
public:
	unsigned char                                      UnknownData00[0x427DAA00];                                // 0x0000(0x427DAA00) MISSED OFFSET

	static ChildConnection ReadAsMe(const uintptr_t address)
	{
		ChildConnection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ChildConnection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CameraModifier_CameraShake
// 0x427E2C00
class CameraModifier_CameraShake
{
public:
	unsigned char                                      UnknownData00[0x427E2C00];                                // 0x0000(0x427E2C00) MISSED OFFSET

	static CameraModifier_CameraShake ReadAsMe(const uintptr_t address)
	{
		CameraModifier_CameraShake ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CameraModifier_CameraShake& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CloudStorageBase
// 0x4290AA00
class CloudStorageBase
{
public:
	unsigned char                                      UnknownData00[0x4290AA00];                                // 0x0000(0x4290AA00) MISSED OFFSET

	static CloudStorageBase ReadAsMe(const uintptr_t address)
	{
		CloudStorageBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CloudStorageBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlatformInterfaceBase
// 0x40FE0C00
class PlatformInterfaceBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PlatformInterfaceBase ReadAsMe(const uintptr_t address)
	{
		PlatformInterfaceBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlatformInterfaceBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CollisionProfile
// 0x427D8E00
class CollisionProfile
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static CollisionProfile ReadAsMe(const uintptr_t address)
	{
		CollisionProfile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CollisionProfile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Commandlet
// 0x40FE0C00
class Commandlet
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Commandlet ReadAsMe(const uintptr_t address)
	{
		Commandlet ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Commandlet& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurveTable
// 0x40FE0C00
class CurveTable
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CurveTable ReadAsMe(const uintptr_t address)
	{
		CurveTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurveTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextureRenderTarget2D
// 0x4290B600
class TextureRenderTarget2D
{
public:
	unsigned char                                      UnknownData00[0x4290B600];                                // 0x0000(0x4290B600) MISSED OFFSET

	static TextureRenderTarget2D ReadAsMe(const uintptr_t address)
	{
		TextureRenderTarget2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextureRenderTarget2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CompositeCurveTable
// 0x4290A000
class CompositeCurveTable
{
public:
	unsigned char                                      UnknownData00[0x4290A000];                                // 0x0000(0x4290A000) MISSED OFFSET

	static CompositeCurveTable ReadAsMe(const uintptr_t address)
	{
		CompositeCurveTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CompositeCurveTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CompositeDataTable
// 0x42909C00
class CompositeDataTable
{
public:
	unsigned char                                      UnknownData00[0x42909C00];                                // 0x0000(0x42909C00) MISSED OFFSET

	static CompositeDataTable ReadAsMe(const uintptr_t address)
	{
		CompositeDataTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CompositeDataTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Console
// 0x40FE0C00
class Console
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Console ReadAsMe(const uintptr_t address)
	{
		Console ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Console& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DataTable
// 0x40FE0C00
class DataTable
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DataTable ReadAsMe(const uintptr_t address)
	{
		DataTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DataTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ComponentDelegateBinding
// 0x428B2400
class ComponentDelegateBinding
{
public:
	unsigned char                                      UnknownData00[0x428B2400];                                // 0x0000(0x428B2400) MISSED OFFSET

	static ComponentDelegateBinding ReadAsMe(const uintptr_t address)
	{
		ComponentDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ComponentDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.StreamingSettings
// 0x427D8E00
class StreamingSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static StreamingSettings ReadAsMe(const uintptr_t address)
	{
		StreamingSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StreamingSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GarbageCollectionSettings
// 0x427D8E00
class GarbageCollectionSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static GarbageCollectionSettings ReadAsMe(const uintptr_t address)
	{
		GarbageCollectionSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GarbageCollectionSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ControlRigInterface
// 0x427D4600
class ControlRigInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static ControlRigInterface ReadAsMe(const uintptr_t address)
	{
		ControlRigInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ControlRigInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurveBase
// 0x40FE0C00
class CurveBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CurveBase ReadAsMe(const uintptr_t address)
	{
		CurveBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurveBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurveEdPresetCurve
// 0x40FE0C00
class CurveEdPresetCurve
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static CurveEdPresetCurve ReadAsMe(const uintptr_t address)
	{
		CurveEdPresetCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurveEdPresetCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ControlChannel
// 0x428BE800
class ControlChannel
{
public:
	unsigned char                                      UnknownData00[0x428BE800];                                // 0x0000(0x428BE800) MISSED OFFSET

	static ControlChannel ReadAsMe(const uintptr_t address)
	{
		ControlChannel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ControlChannel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CullDistanceVolume
// 0x40FEC600
class CullDistanceVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static CullDistanceVolume ReadAsMe(const uintptr_t address)
	{
		CullDistanceVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CullDistanceVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurveFloat
// 0x42908E00
class CurveFloat
{
public:
	unsigned char                                      UnknownData00[0x42908E00];                                // 0x0000(0x42908E00) MISSED OFFSET

	static CurveFloat ReadAsMe(const uintptr_t address)
	{
		CurveFloat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurveFloat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurveLinearColor
// 0x42908E00
class CurveLinearColor
{
public:
	unsigned char                                      UnknownData00[0x42908E00];                                // 0x0000(0x42908E00) MISSED OFFSET

	static CurveLinearColor ReadAsMe(const uintptr_t address)
	{
		CurveLinearColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurveLinearColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Texture2D
// 0x42881000
class Texture2D
{
public:
	unsigned char                                      UnknownData00[0x42881000];                                // 0x0000(0x42881000) MISSED OFFSET

	static Texture2D ReadAsMe(const uintptr_t address)
	{
		Texture2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Texture2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurveLinearColorAtlas
// 0x4290FE00
class CurveLinearColorAtlas
{
public:
	unsigned char                                      UnknownData00[0x4290FE00];                                // 0x0000(0x4290FE00) MISSED OFFSET

	static CurveLinearColorAtlas ReadAsMe(const uintptr_t address)
	{
		CurveLinearColorAtlas ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurveLinearColorAtlas& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CanvasRenderTarget2D
// 0x4290B800
class CanvasRenderTarget2D
{
public:
	unsigned char                                      UnknownData00[0x4290B800];                                // 0x0000(0x4290B800) MISSED OFFSET

	static CanvasRenderTarget2D ReadAsMe(const uintptr_t address)
	{
		CanvasRenderTarget2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CanvasRenderTarget2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurvePanningInterface
// 0x427D4600
class CurvePanningInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static CurvePanningInterface ReadAsMe(const uintptr_t address)
	{
		CurvePanningInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurvePanningInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurveSourceInterface
// 0x427D4600
class CurveSourceInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static CurveSourceInterface ReadAsMe(const uintptr_t address)
	{
		CurveSourceInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurveSourceInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PrimaryDataAsset
// 0x427EC000
class PrimaryDataAsset
{
public:
	unsigned char                                      UnknownData00[0x427EC000];                                // 0x0000(0x427EC000) MISSED OFFSET

	static PrimaryDataAsset ReadAsMe(const uintptr_t address)
	{
		PrimaryDataAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PrimaryDataAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.CurveVector
// 0x42908E00
class CurveVector
{
public:
	unsigned char                                      UnknownData00[0x42908E00];                                // 0x0000(0x42908E00) MISSED OFFSET

	static CurveVector ReadAsMe(const uintptr_t address)
	{
		CurveVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, CurveVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DataTableFunctionLibrary
// 0x40FEE400
class DataTableFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static DataTableFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		DataTableFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DataTableFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DebugCameraHUD
// 0x42852600
class DebugCameraHUD
{
public:
	unsigned char                                      UnknownData00[0x42852600];                                // 0x0000(0x42852600) MISSED OFFSET

	static DebugCameraHUD ReadAsMe(const uintptr_t address)
	{
		DebugCameraHUD ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DebugCameraHUD& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DecalActor
// 0x40FE8000
class DecalActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static DecalActor ReadAsMe(const uintptr_t address)
	{
		DecalActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DecalActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DebugCameraController
// 0x4282A400
class DebugCameraController
{
public:
	unsigned char                                      UnknownData00[0x4282A400];                                // 0x0000(0x4282A400) MISSED OFFSET

	static DebugCameraController ReadAsMe(const uintptr_t address)
	{
		DebugCameraController ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DebugCameraController& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DebugDrawService
// 0x40FEE400
class DebugDrawService
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static DebugDrawService ReadAsMe(const uintptr_t address)
	{
		DebugDrawService ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DebugDrawService& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DecalComponent
// 0x40FEF800
class DecalComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static DecalComponent ReadAsMe(const uintptr_t address)
	{
		DecalComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DecalComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DefaultPawn
// 0x427E5800
class DefaultPawn
{
public:
	unsigned char                                      UnknownData00[0x427E5800];                                // 0x0000(0x427E5800) MISSED OFFSET

	static DefaultPawn ReadAsMe(const uintptr_t address)
	{
		DefaultPawn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DefaultPawn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DefaultPhysicsVolume
// 0x42877400
class DefaultPhysicsVolume
{
public:
	unsigned char                                      UnknownData00[0x42877400];                                // 0x0000(0x42877400) MISSED OFFSET

	static DefaultPhysicsVolume ReadAsMe(const uintptr_t address)
	{
		DefaultPhysicsVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DefaultPhysicsVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DemoNetConnection
// 0x427DAA00
class DemoNetConnection
{
public:
	unsigned char                                      UnknownData00[0x427DAA00];                                // 0x0000(0x427DAA00) MISSED OFFSET

	static DemoNetConnection ReadAsMe(const uintptr_t address)
	{
		DemoNetConnection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DemoNetConnection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PendingNetGame
// 0x40FE0C00
class PendingNetGame
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PendingNetGame ReadAsMe(const uintptr_t address)
	{
		PendingNetGame ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PendingNetGame& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DemoNetDriver
// 0x427DA400
class DemoNetDriver
{
public:
	unsigned char                                      UnknownData00[0x427DA400];                                // 0x0000(0x427DA400) MISSED OFFSET

	static DemoNetDriver ReadAsMe(const uintptr_t address)
	{
		DemoNetDriver ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DemoNetDriver& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DemoPendingNetGame
// 0x4290DE00
class DemoPendingNetGame
{
public:
	unsigned char                                      UnknownData00[0x4290DE00];                                // 0x0000(0x4290DE00) MISSED OFFSET

	static DemoPendingNetGame ReadAsMe(const uintptr_t address)
	{
		DemoPendingNetGame ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DemoPendingNetGame& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DeviceProfile
// 0x4290D800
class DeviceProfile
{
public:
	unsigned char                                      UnknownData00[0x4290D800];                                // 0x0000(0x4290D800) MISSED OFFSET

	static DeviceProfile ReadAsMe(const uintptr_t address)
	{
		DeviceProfile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DeviceProfile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DeviceProfileManager
// 0x40FE0C00
class DeviceProfileManager
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DeviceProfileManager ReadAsMe(const uintptr_t address)
	{
		DeviceProfileManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DeviceProfileManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DialogueSoundWaveProxy
// 0x428B6C00
class DialogueSoundWaveProxy
{
public:
	unsigned char                                      UnknownData00[0x428B6C00];                                // 0x0000(0x428B6C00) MISSED OFFSET

	static DialogueSoundWaveProxy ReadAsMe(const uintptr_t address)
	{
		DialogueSoundWaveProxy ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DialogueSoundWaveProxy& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DialogueVoice
// 0x40FE0C00
class DialogueVoice
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DialogueVoice ReadAsMe(const uintptr_t address)
	{
		DialogueVoice ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DialogueVoice& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Light
// 0x40FE8000
class Light
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static Light ReadAsMe(const uintptr_t address)
	{
		Light ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Light& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DestructibleInterface
// 0x427D4600
class DestructibleInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static DestructibleInterface ReadAsMe(const uintptr_t address)
	{
		DestructibleInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DestructibleInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DialogueWave
// 0x40FE0C00
class DialogueWave
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DialogueWave ReadAsMe(const uintptr_t address)
	{
		DialogueWave ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DialogueWave& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextureLODSettings
// 0x40FE0C00
class TextureLODSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static TextureLODSettings ReadAsMe(const uintptr_t address)
	{
		TextureLODSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextureLODSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightComponentBase
// 0x40FEF800
class LightComponentBase
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static LightComponentBase ReadAsMe(const uintptr_t address)
	{
		LightComponentBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightComponentBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionFloat
// 0x42829200
class DistributionFloat
{
public:
	unsigned char                                      UnknownData00[0x42829200];                                // 0x0000(0x42829200) MISSED OFFSET

	static DistributionFloat ReadAsMe(const uintptr_t address)
	{
		DistributionFloat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionFloat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightComponent
// 0x42924200
class LightComponent
{
public:
	unsigned char                                      UnknownData00[0x42924200];                                // 0x0000(0x42924200) MISSED OFFSET

	static LightComponent ReadAsMe(const uintptr_t address)
	{
		LightComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DirectionalLight
// 0x4290CC00
class DirectionalLight
{
public:
	unsigned char                                      UnknownData00[0x4290CC00];                                // 0x0000(0x4290CC00) MISSED OFFSET

	static DirectionalLight ReadAsMe(const uintptr_t address)
	{
		DirectionalLight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DirectionalLight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionFloatParameterBase
// 0x42923E00
class DistributionFloatParameterBase
{
public:
	unsigned char                                      UnknownData00[0x42923E00];                                // 0x0000(0x42923E00) MISSED OFFSET

	static DistributionFloatParameterBase ReadAsMe(const uintptr_t address)
	{
		DistributionFloatParameterBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionFloatParameterBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionFloatConstant
// 0x42924000
class DistributionFloatConstant
{
public:
	unsigned char                                      UnknownData00[0x42924000];                                // 0x0000(0x42924000) MISSED OFFSET

	static DistributionFloatConstant ReadAsMe(const uintptr_t address)
	{
		DistributionFloatConstant ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionFloatConstant& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionFloatUniform
// 0x42924000
class DistributionFloatUniform
{
public:
	unsigned char                                      UnknownData00[0x42924000];                                // 0x0000(0x42924000) MISSED OFFSET

	static DistributionFloatUniform ReadAsMe(const uintptr_t address)
	{
		DistributionFloatUniform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionFloatUniform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionFloatConstantCurve
// 0x42924000
class DistributionFloatConstantCurve
{
public:
	unsigned char                                      UnknownData00[0x42924000];                                // 0x0000(0x42924000) MISSED OFFSET

	static DistributionFloatConstantCurve ReadAsMe(const uintptr_t address)
	{
		DistributionFloatConstantCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionFloatConstantCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionFloatUniformCurve
// 0x42924000
class DistributionFloatUniformCurve
{
public:
	unsigned char                                      UnknownData00[0x42924000];                                // 0x0000(0x42924000) MISSED OFFSET

	static DistributionFloatUniformCurve ReadAsMe(const uintptr_t address)
	{
		DistributionFloatUniformCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionFloatUniformCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionFloatParticleParameter
// 0x42923A00
class DistributionFloatParticleParameter
{
public:
	unsigned char                                      UnknownData00[0x42923A00];                                // 0x0000(0x42923A00) MISSED OFFSET

	static DistributionFloatParticleParameter ReadAsMe(const uintptr_t address)
	{
		DistributionFloatParticleParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionFloatParticleParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionVectorParameterBase
// 0x42923200
class DistributionVectorParameterBase
{
public:
	unsigned char                                      UnknownData00[0x42923200];                                // 0x0000(0x42923200) MISSED OFFSET

	static DistributionVectorParameterBase ReadAsMe(const uintptr_t address)
	{
		DistributionVectorParameterBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionVectorParameterBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionVectorConstantCurve
// 0x42829400
class DistributionVectorConstantCurve
{
public:
	unsigned char                                      UnknownData00[0x42829400];                                // 0x0000(0x42829400) MISSED OFFSET

	static DistributionVectorConstantCurve ReadAsMe(const uintptr_t address)
	{
		DistributionVectorConstantCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionVectorConstantCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionVectorUniform
// 0x42829400
class DistributionVectorUniform
{
public:
	unsigned char                                      UnknownData00[0x42829400];                                // 0x0000(0x42829400) MISSED OFFSET

	static DistributionVectorUniform ReadAsMe(const uintptr_t address)
	{
		DistributionVectorUniform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionVectorUniform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionVectorConstant
// 0x42829400
class DistributionVectorConstant
{
public:
	unsigned char                                      UnknownData00[0x42829400];                                // 0x0000(0x42829400) MISSED OFFSET

	static DistributionVectorConstant ReadAsMe(const uintptr_t address)
	{
		DistributionVectorConstant ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionVectorConstant& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DPICustomScalingRule
// 0x40FE0C00
class DPICustomScalingRule
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static DPICustomScalingRule ReadAsMe(const uintptr_t address)
	{
		DPICustomScalingRule ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DPICustomScalingRule& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DocumentationActor
// 0x40FE8000
class DocumentationActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static DocumentationActor ReadAsMe(const uintptr_t address)
	{
		DocumentationActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DocumentationActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionVectorUniformCurve
// 0x42829400
class DistributionVectorUniformCurve
{
public:
	unsigned char                                      UnknownData00[0x42829400];                                // 0x0000(0x42829400) MISSED OFFSET

	static DistributionVectorUniformCurve ReadAsMe(const uintptr_t address)
	{
		DistributionVectorUniformCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionVectorUniformCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DistributionVectorParticleParameter
// 0x42922E00
class DistributionVectorParticleParameter
{
public:
	unsigned char                                      UnknownData00[0x42922E00];                                // 0x0000(0x42922E00) MISSED OFFSET

	static DistributionVectorParticleParameter ReadAsMe(const uintptr_t address)
	{
		DistributionVectorParticleParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DistributionVectorParticleParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EdGraph
// 0x40FE0C00
class EdGraph
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static EdGraph ReadAsMe(const uintptr_t address)
	{
		EdGraph ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EdGraph& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DrawSphereComponent
// 0x42921E00
class DrawSphereComponent
{
public:
	unsigned char                                      UnknownData00[0x42921E00];                                // 0x0000(0x42921E00) MISSED OFFSET

	static DrawSphereComponent ReadAsMe(const uintptr_t address)
	{
		DrawSphereComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DrawSphereComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SphereComponent
// 0x42857A00
class SphereComponent
{
public:
	unsigned char                                      UnknownData00[0x42857A00];                                // 0x0000(0x42857A00) MISSED OFFSET

	static SphereComponent ReadAsMe(const uintptr_t address)
	{
		SphereComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SphereComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EdGraphNode_Documentation
// 0x42921A00
class EdGraphNode_Documentation
{
public:
	unsigned char                                      UnknownData00[0x42921A00];                                // 0x0000(0x42921A00) MISSED OFFSET

	static EdGraphNode_Documentation ReadAsMe(const uintptr_t address)
	{
		EdGraphNode_Documentation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EdGraphNode_Documentation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EdGraphNode
// 0x40FE0C00
class EdGraphNode
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static EdGraphNode ReadAsMe(const uintptr_t address)
	{
		EdGraphNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EdGraphNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EdGraphSchema
// 0x40FE0C00
class EdGraphSchema
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static EdGraphSchema ReadAsMe(const uintptr_t address)
	{
		EdGraphSchema ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EdGraphSchema& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Emitter
// 0x40FE8000
class Emitter
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static Emitter ReadAsMe(const uintptr_t address)
	{
		Emitter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Emitter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DrawFrustumComponent
// 0x427D0A00
class DrawFrustumComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static DrawFrustumComponent ReadAsMe(const uintptr_t address)
	{
		DrawFrustumComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DrawFrustumComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DirectionalLightComponent
// 0x4290C800
class DirectionalLightComponent
{
public:
	unsigned char                                      UnknownData00[0x4290C800];                                // 0x0000(0x4290C800) MISSED OFFSET

	static DirectionalLightComponent ReadAsMe(const uintptr_t address)
	{
		DirectionalLightComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DirectionalLightComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EngineBaseTypes
// 0x40FE0C00
class EngineBaseTypes
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static EngineBaseTypes ReadAsMe(const uintptr_t address)
	{
		EngineBaseTypes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EngineBaseTypes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EngineHandlerComponentFactory
// 0x427E7400
class EngineHandlerComponentFactory
{
public:
	unsigned char                                      UnknownData00[0x427E7400];                                // 0x0000(0x427E7400) MISSED OFFSET

	static EngineHandlerComponentFactory ReadAsMe(const uintptr_t address)
	{
		EngineHandlerComponentFactory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EngineHandlerComponentFactory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LocalMessage
// 0x40FE0C00
class LocalMessage
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static LocalMessage ReadAsMe(const uintptr_t address)
	{
		LocalMessage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LocalMessage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EdGraphPin_Deprecated
// 0x40FE0C00
class EdGraphPin_Deprecated
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static EdGraphPin_Deprecated ReadAsMe(const uintptr_t address)
	{
		EdGraphPin_Deprecated ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EdGraphPin_Deprecated& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EngineMessage
// 0x42920800
class EngineMessage
{
public:
	unsigned char                                      UnknownData00[0x42920800];                                // 0x0000(0x42920800) MISSED OFFSET

	static EngineMessage ReadAsMe(const uintptr_t address)
	{
		EngineMessage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EngineMessage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.DynamicSubsystem
// 0x42920200
class DynamicSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42920200];                                // 0x0000(0x42920200) MISSED OFFSET

	static DynamicSubsystem ReadAsMe(const uintptr_t address)
	{
		DynamicSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, DynamicSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Subsystem
// 0x40FE0C00
class Subsystem
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Subsystem ReadAsMe(const uintptr_t address)
	{
		Subsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Subsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EmitterCameraLensEffectBase
// 0x42921200
class EmitterCameraLensEffectBase
{
public:
	unsigned char                                      UnknownData00[0x42921200];                                // 0x0000(0x42921200) MISSED OFFSET

	static EmitterCameraLensEffectBase ReadAsMe(const uintptr_t address)
	{
		EmitterCameraLensEffectBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EmitterCameraLensEffectBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ExponentialHeightFogComponent
// 0x40FEF800
class ExponentialHeightFogComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static ExponentialHeightFogComponent ReadAsMe(const uintptr_t address)
	{
		ExponentialHeightFogComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ExponentialHeightFogComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EngineTypes
// 0x40FE0C00
class EngineTypes
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static EngineTypes ReadAsMe(const uintptr_t address)
	{
		EngineTypes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EngineTypes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Font
// 0x40FE0C00
class Font
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Font ReadAsMe(const uintptr_t address)
	{
		Font ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Font& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ExponentialHeightFog
// 0x427D5600
class ExponentialHeightFog
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static ExponentialHeightFog ReadAsMe(const uintptr_t address)
	{
		ExponentialHeightFog ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ExponentialHeightFog& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ForceFeedbackAttenuation
// 0x40FE0C00
class ForceFeedbackAttenuation
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ForceFeedbackAttenuation ReadAsMe(const uintptr_t address)
	{
		ForceFeedbackAttenuation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ForceFeedbackAttenuation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.FontFace
// 0x40FE0C00
class FontFace
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FontFace ReadAsMe(const uintptr_t address)
	{
		FontFace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FontFace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ForceFeedbackComponent
// 0x40FEF800
class ForceFeedbackComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static ForceFeedbackComponent ReadAsMe(const uintptr_t address)
	{
		ForceFeedbackComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ForceFeedbackComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.FontImportOptions
// 0x40FE0C00
class FontImportOptions
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static FontImportOptions ReadAsMe(const uintptr_t address)
	{
		FontImportOptions ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FontImportOptions& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ForceFeedbackEffect
// 0x40FE0C00
class ForceFeedbackEffect
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ForceFeedbackEffect ReadAsMe(const uintptr_t address)
	{
		ForceFeedbackEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ForceFeedbackEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Exporter
// 0x40FE0C00
class Exporter
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Exporter ReadAsMe(const uintptr_t address)
	{
		Exporter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Exporter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameNetworkManager
// 0x427D5600
class GameNetworkManager
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static GameNetworkManager ReadAsMe(const uintptr_t address)
	{
		GameNetworkManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameNetworkManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameplayStatics
// 0x40FEE400
class GameplayStatics
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static GameplayStatics ReadAsMe(const uintptr_t address)
	{
		GameplayStatics ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameplayStatics& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SpotLight
// 0x4290CC00
class SpotLight
{
public:
	unsigned char                                      UnknownData00[0x4290CC00];                                // 0x0000(0x4290CC00) MISSED OFFSET

	static SpotLight ReadAsMe(const uintptr_t address)
	{
		SpotLight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SpotLight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GeneratedMeshAreaLight
// 0x42926600
class GeneratedMeshAreaLight
{
public:
	unsigned char                                      UnknownData00[0x42926600];                                // 0x0000(0x42926600) MISSED OFFSET

	static GeneratedMeshAreaLight ReadAsMe(const uintptr_t address)
	{
		GeneratedMeshAreaLight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GeneratedMeshAreaLight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HapticFeedbackEffect_Curve
// 0x42926400
class HapticFeedbackEffect_Curve
{
public:
	unsigned char                                      UnknownData00[0x42926400];                                // 0x0000(0x42926400) MISSED OFFSET

	static HapticFeedbackEffect_Curve ReadAsMe(const uintptr_t address)
	{
		HapticFeedbackEffect_Curve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HapticFeedbackEffect_Curve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.GameInstanceSubsystem
// 0x42920200
class GameInstanceSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42920200];                                // 0x0000(0x42920200) MISSED OFFSET

	static GameInstanceSubsystem ReadAsMe(const uintptr_t address)
	{
		GameInstanceSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, GameInstanceSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HapticFeedbackEffect_SoundWave
// 0x42926400
class HapticFeedbackEffect_SoundWave
{
public:
	unsigned char                                      UnknownData00[0x42926400];                                // 0x0000(0x42926400) MISSED OFFSET

	static HapticFeedbackEffect_SoundWave ReadAsMe(const uintptr_t address)
	{
		HapticFeedbackEffect_SoundWave ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HapticFeedbackEffect_SoundWave& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HapticFeedbackEffect_Buffer
// 0x42926400
class HapticFeedbackEffect_Buffer
{
public:
	unsigned char                                      UnknownData00[0x42926400];                                // 0x0000(0x42926400) MISSED OFFSET

	static HapticFeedbackEffect_Buffer ReadAsMe(const uintptr_t address)
	{
		HapticFeedbackEffect_Buffer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HapticFeedbackEffect_Buffer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.FloatingPawnMovement
// 0x42805E00
class FloatingPawnMovement
{
public:
	unsigned char                                      UnknownData00[0x42805E00];                                // 0x0000(0x42805E00) MISSED OFFSET

	static FloatingPawnMovement ReadAsMe(const uintptr_t address)
	{
		FloatingPawnMovement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FloatingPawnMovement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HLODProxy
// 0x40FE0C00
class HLODProxy
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static HLODProxy ReadAsMe(const uintptr_t address)
	{
		HLODProxy ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HLODProxy& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SpatializationPluginSourceSettingsBase
// 0x40FE0C00
class SpatializationPluginSourceSettingsBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SpatializationPluginSourceSettingsBase ReadAsMe(const uintptr_t address)
	{
		SpatializationPluginSourceSettingsBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SpatializationPluginSourceSettingsBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HealthSnapshotBlueprintLibrary
// 0x40FEE400
class HealthSnapshotBlueprintLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static HealthSnapshotBlueprintLibrary ReadAsMe(const uintptr_t address)
	{
		HealthSnapshotBlueprintLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HealthSnapshotBlueprintLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HapticFeedbackEffect_Base
// 0x40FE0C00
class HapticFeedbackEffect_Base
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static HapticFeedbackEffect_Base ReadAsMe(const uintptr_t address)
	{
		HapticFeedbackEffect_Base ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HapticFeedbackEffect_Base& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.AmbisonicsSubmixSettingsBase
// 0x40FE0C00
class AmbisonicsSubmixSettingsBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static AmbisonicsSubmixSettingsBase ReadAsMe(const uintptr_t address)
	{
		AmbisonicsSubmixSettingsBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, AmbisonicsSubmixSettingsBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.OcclusionPluginSourceSettingsBase
// 0x40FE0C00
class OcclusionPluginSourceSettingsBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static OcclusionPluginSourceSettingsBase ReadAsMe(const uintptr_t address)
	{
		OcclusionPluginSourceSettingsBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, OcclusionPluginSourceSettingsBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ImportantToggleSettingInterface
// 0x427D4600
class ImportantToggleSettingInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static ImportantToggleSettingInterface ReadAsMe(const uintptr_t address)
	{
		ImportantToggleSettingInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ImportantToggleSettingInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ReverbPluginSourceSettingsBase
// 0x40FE0C00
class ReverbPluginSourceSettingsBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ReverbPluginSourceSettingsBase ReadAsMe(const uintptr_t address)
	{
		ReverbPluginSourceSettingsBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ReverbPluginSourceSettingsBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InGameAdManager
// 0x4290AA00
class InGameAdManager
{
public:
	unsigned char                                      UnknownData00[0x4290AA00];                                // 0x0000(0x4290AA00) MISSED OFFSET

	static InGameAdManager ReadAsMe(const uintptr_t address)
	{
		InGameAdManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InGameAdManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InheritableComponentHandler
// 0x40FE0C00
class InheritableComponentHandler
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InheritableComponentHandler ReadAsMe(const uintptr_t address)
	{
		InheritableComponentHandler ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InheritableComponentHandler& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputDelegateBinding
// 0x428B2400
class InputDelegateBinding
{
public:
	unsigned char                                      UnknownData00[0x428B2400];                                // 0x0000(0x428B2400) MISSED OFFSET

	static InputDelegateBinding ReadAsMe(const uintptr_t address)
	{
		InputDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ImportanceSamplingLibrary
// 0x40FEE400
class ImportanceSamplingLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static ImportanceSamplingLibrary ReadAsMe(const uintptr_t address)
	{
		ImportanceSamplingLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ImportanceSamplingLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputActionDelegateBinding
// 0x42924600
class InputActionDelegateBinding
{
public:
	unsigned char                                      UnknownData00[0x42924600];                                // 0x0000(0x42924600) MISSED OFFSET

	static InputActionDelegateBinding ReadAsMe(const uintptr_t address)
	{
		InputActionDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputActionDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputAxisKeyDelegateBinding
// 0x42924600
class InputAxisKeyDelegateBinding
{
public:
	unsigned char                                      UnknownData00[0x42924600];                                // 0x0000(0x42924600) MISSED OFFSET

	static InputAxisKeyDelegateBinding ReadAsMe(const uintptr_t address)
	{
		InputAxisKeyDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputAxisKeyDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputKeyDelegateBinding
// 0x42924600
class InputKeyDelegateBinding
{
public:
	unsigned char                                      UnknownData00[0x42924600];                                // 0x0000(0x42924600) MISSED OFFSET

	static InputKeyDelegateBinding ReadAsMe(const uintptr_t address)
	{
		InputKeyDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputKeyDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputAxisDelegateBinding
// 0x42924600
class InputAxisDelegateBinding
{
public:
	unsigned char                                      UnknownData00[0x42924600];                                // 0x0000(0x42924600) MISSED OFFSET

	static InputAxisDelegateBinding ReadAsMe(const uintptr_t address)
	{
		InputAxisDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputAxisDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputSettings
// 0x40FE0C00
class InputSettings
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InputSettings ReadAsMe(const uintptr_t address)
	{
		InputSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputVectorAxisDelegateBinding
// 0x4292C600
class InputVectorAxisDelegateBinding
{
public:
	unsigned char                                      UnknownData00[0x4292C600];                                // 0x0000(0x4292C600) MISSED OFFSET

	static InputVectorAxisDelegateBinding ReadAsMe(const uintptr_t address)
	{
		InputVectorAxisDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputVectorAxisDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputTouchDelegateBinding
// 0x42924600
class InputTouchDelegateBinding
{
public:
	unsigned char                                      UnknownData00[0x42924600];                                // 0x0000(0x42924600) MISSED OFFSET

	static InputTouchDelegateBinding ReadAsMe(const uintptr_t address)
	{
		InputTouchDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputTouchDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Interface_AssetUserData
// 0x427D4600
class Interface_AssetUserData
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static Interface_AssetUserData ReadAsMe(const uintptr_t address)
	{
		Interface_AssetUserData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Interface_AssetUserData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Interface_PreviewMeshProvider
// 0x427D4600
class Interface_PreviewMeshProvider
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static Interface_PreviewMeshProvider ReadAsMe(const uintptr_t address)
	{
		Interface_PreviewMeshProvider ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Interface_PreviewMeshProvider& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Interface_CollisionDataProvider
// 0x427D4600
class Interface_CollisionDataProvider
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static Interface_CollisionDataProvider ReadAsMe(const uintptr_t address)
	{
		Interface_CollisionDataProvider ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Interface_CollisionDataProvider& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpCurveEdSetup
// 0x40FE0C00
class InterpCurveEdSetup
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InterpCurveEdSetup ReadAsMe(const uintptr_t address)
	{
		InterpCurveEdSetup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpCurveEdSetup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.EngineSubsystem
// 0x42920400
class EngineSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42920400];                                // 0x0000(0x42920400) MISSED OFFSET

	static EngineSubsystem ReadAsMe(const uintptr_t address)
	{
		EngineSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, EngineSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpFilter_Classes
// 0x4292AE00
class InterpFilter_Classes
{
public:
	unsigned char                                      UnknownData00[0x4292AE00];                                // 0x0000(0x4292AE00) MISSED OFFSET

	static InterpFilter_Classes ReadAsMe(const uintptr_t address)
	{
		InterpFilter_Classes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpFilter_Classes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Interface_PostProcessVolume
// 0x427D4600
class Interface_PostProcessVolume
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static Interface_PostProcessVolume ReadAsMe(const uintptr_t address)
	{
		Interface_PostProcessVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Interface_PostProcessVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpFilter_Custom
// 0x4292AE00
class InterpFilter_Custom
{
public:
	unsigned char                                      UnknownData00[0x4292AE00];                                // 0x0000(0x4292AE00) MISSED OFFSET

	static InterpFilter_Custom ReadAsMe(const uintptr_t address)
	{
		InterpFilter_Custom ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpFilter_Custom& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpFilter
// 0x40FE0C00
class InterpFilter
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InterpFilter ReadAsMe(const uintptr_t address)
	{
		InterpFilter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpFilter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpData
// 0x40FE0C00
class InterpData
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InterpData ReadAsMe(const uintptr_t address)
	{
		InterpData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpGroup
// 0x40FE0C00
class InterpGroup
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InterpGroup ReadAsMe(const uintptr_t address)
	{
		InterpGroup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpGroup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpGroupInst
// 0x40FE0C00
class InterpGroupInst
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static InterpGroupInst ReadAsMe(const uintptr_t address)
	{
		InterpGroupInst ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpGroupInst& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpGroupInstCamera
// 0x4292A200
class InterpGroupInstCamera
{
public:
	unsigned char                                      UnknownData00[0x4292A200];                                // 0x0000(0x4292A200) MISSED OFFSET

	static InterpGroupInstCamera ReadAsMe(const uintptr_t address)
	{
		InterpGroupInstCamera ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpGroupInstCamera& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpGroupInstDirector
// 0x4292A200
class InterpGroupInstDirector
{
public:
	unsigned char                                      UnknownData00[0x4292A200];                                // 0x0000(0x4292A200) MISSED OFFSET

	static InterpGroupInstDirector ReadAsMe(const uintptr_t address)
	{
		InterpGroupInstDirector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpGroupInstDirector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackAnimControl
// 0x427D2A00
class InterpTrackAnimControl
{
public:
	unsigned char                                      UnknownData00[0x427D2A00];                                // 0x0000(0x427D2A00) MISSED OFFSET

	static InterpTrackAnimControl ReadAsMe(const uintptr_t address)
	{
		InterpTrackAnimControl ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackAnimControl& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpToMovementComponent
// 0x42805A00
class InterpToMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x42805A00];                                // 0x0000(0x42805A00) MISSED OFFSET

	static InterpToMovementComponent ReadAsMe(const uintptr_t address)
	{
		InterpToMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpToMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpGroupCamera
// 0x4292A800
class InterpGroupCamera
{
public:
	unsigned char                                      UnknownData00[0x4292A800];                                // 0x0000(0x4292A800) MISSED OFFSET

	static InterpGroupCamera ReadAsMe(const uintptr_t address)
	{
		InterpGroupCamera ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpGroupCamera& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackAudioMaster
// 0x427D3000
class InterpTrackAudioMaster
{
public:
	unsigned char                                      UnknownData00[0x427D3000];                                // 0x0000(0x427D3000) MISSED OFFSET

	static InterpTrackAudioMaster ReadAsMe(const uintptr_t address)
	{
		InterpTrackAudioMaster ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackAudioMaster& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackColorProp
// 0x427D3000
class InterpTrackColorProp
{
public:
	unsigned char                                      UnknownData00[0x427D3000];                                // 0x0000(0x427D3000) MISSED OFFSET

	static InterpTrackColorProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackColorProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackColorProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackColorScale
// 0x427D3000
class InterpTrackColorScale
{
public:
	unsigned char                                      UnknownData00[0x427D3000];                                // 0x0000(0x427D3000) MISSED OFFSET

	static InterpTrackColorScale ReadAsMe(const uintptr_t address)
	{
		InterpTrackColorScale ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackColorScale& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackDirector
// 0x427D2E00
class InterpTrackDirector
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackDirector ReadAsMe(const uintptr_t address)
	{
		InterpTrackDirector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackDirector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpGroupDirector
// 0x4292A800
class InterpGroupDirector
{
public:
	unsigned char                                      UnknownData00[0x4292A800];                                // 0x0000(0x4292A800) MISSED OFFSET

	static InterpGroupDirector ReadAsMe(const uintptr_t address)
	{
		InterpGroupDirector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpGroupDirector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackFade
// 0x427D2A00
class InterpTrackFade
{
public:
	unsigned char                                      UnknownData00[0x427D2A00];                                // 0x0000(0x427D2A00) MISSED OFFSET

	static InterpTrackFade ReadAsMe(const uintptr_t address)
	{
		InterpTrackFade ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackFade& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackEvent
// 0x427D2E00
class InterpTrackEvent
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackEvent ReadAsMe(const uintptr_t address)
	{
		InterpTrackEvent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackEvent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackFloatParticleParam
// 0x427D2A00
class InterpTrackFloatParticleParam
{
public:
	unsigned char                                      UnknownData00[0x427D2A00];                                // 0x0000(0x427D2A00) MISSED OFFSET

	static InterpTrackFloatParticleParam ReadAsMe(const uintptr_t address)
	{
		InterpTrackFloatParticleParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackFloatParticleParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackFloatProp
// 0x427D2A00
class InterpTrackFloatProp
{
public:
	unsigned char                                      UnknownData00[0x427D2A00];                                // 0x0000(0x427D2A00) MISSED OFFSET

	static InterpTrackFloatProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackFloatProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackFloatProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstAnimControl
// 0x427D2600
class InterpTrackInstAnimControl
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstAnimControl ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstAnimControl ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstAnimControl& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackFloatAnimBPParam
// 0x427D2A00
class InterpTrackFloatAnimBPParam
{
public:
	unsigned char                                      UnknownData00[0x427D2A00];                                // 0x0000(0x427D2A00) MISSED OFFSET

	static InterpTrackFloatAnimBPParam ReadAsMe(const uintptr_t address)
	{
		InterpTrackFloatAnimBPParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackFloatAnimBPParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackFloatMaterialParam
// 0x427D2A00
class InterpTrackFloatMaterialParam
{
public:
	unsigned char                                      UnknownData00[0x427D2A00];                                // 0x0000(0x427D2A00) MISSED OFFSET

	static InterpTrackFloatMaterialParam ReadAsMe(const uintptr_t address)
	{
		InterpTrackFloatMaterialParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackFloatMaterialParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstBoolProp
// 0x4292F600
class InterpTrackInstBoolProp
{
public:
	unsigned char                                      UnknownData00[0x4292F600];                                // 0x0000(0x4292F600) MISSED OFFSET

	static InterpTrackInstBoolProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstBoolProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstBoolProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackBoolProp
// 0x427D2E00
class InterpTrackBoolProp
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackBoolProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackBoolProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackBoolProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstColorProp
// 0x4292F600
class InterpTrackInstColorProp
{
public:
	unsigned char                                      UnknownData00[0x4292F600];                                // 0x0000(0x4292F600) MISSED OFFSET

	static InterpTrackInstColorProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstColorProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstColorProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstProperty
// 0x427D2600
class InterpTrackInstProperty
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstProperty ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstAudioMaster
// 0x427D2600
class InterpTrackInstAudioMaster
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstAudioMaster ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstAudioMaster ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstAudioMaster& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstEvent
// 0x427D2600
class InterpTrackInstEvent
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstEvent ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstEvent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstEvent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstFloatAnimBPParam
// 0x427D2600
class InterpTrackInstFloatAnimBPParam
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstFloatAnimBPParam ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstFloatAnimBPParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstFloatAnimBPParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstFloatMaterialParam
// 0x427D2600
class InterpTrackInstFloatMaterialParam
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstFloatMaterialParam ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstFloatMaterialParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstFloatMaterialParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstFloatParticleParam
// 0x427D2600
class InterpTrackInstFloatParticleParam
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstFloatParticleParam ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstFloatParticleParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstFloatParticleParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstFloatProp
// 0x4292F600
class InterpTrackInstFloatProp
{
public:
	unsigned char                                      UnknownData00[0x4292F600];                                // 0x0000(0x4292F600) MISSED OFFSET

	static InterpTrackInstFloatProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstFloatProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstFloatProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstLinearColorProp
// 0x4292F600
class InterpTrackInstLinearColorProp
{
public:
	unsigned char                                      UnknownData00[0x4292F600];                                // 0x0000(0x4292F600) MISSED OFFSET

	static InterpTrackInstLinearColorProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstLinearColorProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstLinearColorProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstMove
// 0x427D2600
class InterpTrackInstMove
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstMove ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstMove ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstMove& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstParticleReplay
// 0x427D2600
class InterpTrackInstParticleReplay
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstParticleReplay ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstParticleReplay ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstParticleReplay& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstFade
// 0x427D2600
class InterpTrackInstFade
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstFade ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstFade ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstFade& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InputComponent
// 0x40FEF600
class InputComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static InputComponent ReadAsMe(const uintptr_t address)
	{
		InputComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InputComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstSound
// 0x427D2600
class InterpTrackInstSound
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstSound ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstSound ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstSound& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstSlomo
// 0x427D2600
class InterpTrackInstSlomo
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstSlomo ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstSlomo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstSlomo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstToggle
// 0x427D2600
class InterpTrackInstToggle
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstToggle ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstToggle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstToggle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstVectorMaterialParam
// 0x427D2600
class InterpTrackInstVectorMaterialParam
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstVectorMaterialParam ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstVectorMaterialParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstVectorMaterialParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstVectorProp
// 0x4292F600
class InterpTrackInstVectorProp
{
public:
	unsigned char                                      UnknownData00[0x4292F600];                                // 0x0000(0x4292F600) MISSED OFFSET

	static InterpTrackInstVectorProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstVectorProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstVectorProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstVisibility
// 0x427D2600
class InterpTrackInstVisibility
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstVisibility ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstVisibility ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstVisibility& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackLinearColorProp
// 0x4292D000
class InterpTrackLinearColorProp
{
public:
	unsigned char                                      UnknownData00[0x4292D000];                                // 0x0000(0x4292D000) MISSED OFFSET

	static InterpTrackLinearColorProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackLinearColorProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackLinearColorProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackMove
// 0x427D2E00
class InterpTrackMove
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackMove ReadAsMe(const uintptr_t address)
	{
		InterpTrackMove ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackMove& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackMoveAxis
// 0x427D2A00
class InterpTrackMoveAxis
{
public:
	unsigned char                                      UnknownData00[0x427D2A00];                                // 0x0000(0x427D2A00) MISSED OFFSET

	static InterpTrackMoveAxis ReadAsMe(const uintptr_t address)
	{
		InterpTrackMoveAxis ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackMoveAxis& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackLinearColorBase
// 0x427D2E00
class InterpTrackLinearColorBase
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackLinearColorBase ReadAsMe(const uintptr_t address)
	{
		InterpTrackLinearColorBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackLinearColorBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstColorScale
// 0x427D2600
class InterpTrackInstColorScale
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstColorScale ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstColorScale ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstColorScale& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackInstDirector
// 0x427D2600
class InterpTrackInstDirector
{
public:
	unsigned char                                      UnknownData00[0x427D2600];                                // 0x0000(0x427D2600) MISSED OFFSET

	static InterpTrackInstDirector ReadAsMe(const uintptr_t address)
	{
		InterpTrackInstDirector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackInstDirector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackSound
// 0x427D3000
class InterpTrackSound
{
public:
	unsigned char                                      UnknownData00[0x427D3000];                                // 0x0000(0x427D3000) MISSED OFFSET

	static InterpTrackSound ReadAsMe(const uintptr_t address)
	{
		InterpTrackSound ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackSound& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackVectorMaterialParam
// 0x427D3000
class InterpTrackVectorMaterialParam
{
public:
	unsigned char                                      UnknownData00[0x427D3000];                                // 0x0000(0x427D3000) MISSED OFFSET

	static InterpTrackVectorMaterialParam ReadAsMe(const uintptr_t address)
	{
		InterpTrackVectorMaterialParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackVectorMaterialParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackVectorProp
// 0x427D3000
class InterpTrackVectorProp
{
public:
	unsigned char                                      UnknownData00[0x427D3000];                                // 0x0000(0x427D3000) MISSED OFFSET

	static InterpTrackVectorProp ReadAsMe(const uintptr_t address)
	{
		InterpTrackVectorProp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackVectorProp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackParticleReplay
// 0x427D2E00
class InterpTrackParticleReplay
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackParticleReplay ReadAsMe(const uintptr_t address)
	{
		InterpTrackParticleReplay ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackParticleReplay& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.IntSerialization
// 0x40FE0C00
class IntSerialization
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static IntSerialization ReadAsMe(const uintptr_t address)
	{
		IntSerialization ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, IntSerialization& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackToggle
// 0x427D2E00
class InterpTrackToggle
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackToggle ReadAsMe(const uintptr_t address)
	{
		InterpTrackToggle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackToggle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackSlomo
// 0x427D2A00
class InterpTrackSlomo
{
public:
	unsigned char                                      UnknownData00[0x427D2A00];                                // 0x0000(0x427D2A00) MISSED OFFSET

	static InterpTrackSlomo ReadAsMe(const uintptr_t address)
	{
		InterpTrackSlomo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackSlomo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.InterpTrackVisibility
// 0x427D2E00
class InterpTrackVisibility
{
public:
	unsigned char                                      UnknownData00[0x427D2E00];                                // 0x0000(0x427D2E00) MISSED OFFSET

	static InterpTrackVisibility ReadAsMe(const uintptr_t address)
	{
		InterpTrackVisibility ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, InterpTrackVisibility& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetArrayLibrary
// 0x40FEE400
class KismetArrayLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetArrayLibrary ReadAsMe(const uintptr_t address)
	{
		KismetArrayLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetArrayLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetInternationalizationLibrary
// 0x40FEE400
class KismetInternationalizationLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetInternationalizationLibrary ReadAsMe(const uintptr_t address)
	{
		KismetInternationalizationLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetInternationalizationLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetInputLibrary
// 0x40FEE400
class KismetInputLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetInputLibrary ReadAsMe(const uintptr_t address)
	{
		KismetInputLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetInputLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetMathLibrary
// 0x40FEE400
class KismetMathLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetMathLibrary ReadAsMe(const uintptr_t address)
	{
		KismetMathLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetMathLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetMaterialLibrary
// 0x40FEE400
class KismetMaterialLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetMaterialLibrary ReadAsMe(const uintptr_t address)
	{
		KismetMaterialLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetMaterialLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetNodeHelperLibrary
// 0x40FEE400
class KismetNodeHelperLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetNodeHelperLibrary ReadAsMe(const uintptr_t address)
	{
		KismetNodeHelperLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetNodeHelperLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetRenderingLibrary
// 0x40FEE400
class KismetRenderingLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetRenderingLibrary ReadAsMe(const uintptr_t address)
	{
		KismetRenderingLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetRenderingLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetStringLibrary
// 0x40FEE400
class KismetStringLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetStringLibrary ReadAsMe(const uintptr_t address)
	{
		KismetStringLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetStringLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetStringTableLibrary
// 0x40FEE400
class KismetStringTableLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetStringTableLibrary ReadAsMe(const uintptr_t address)
	{
		KismetStringTableLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetStringTableLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KillZVolume
// 0x42877400
class KillZVolume
{
public:
	unsigned char                                      UnknownData00[0x42877400];                                // 0x0000(0x42877400) MISSED OFFSET

	static KillZVolume ReadAsMe(const uintptr_t address)
	{
		KillZVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KillZVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetTextLibrary
// 0x40FEE400
class KismetTextLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetTextLibrary ReadAsMe(const uintptr_t address)
	{
		KismetTextLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetTextLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelActorContainer
// 0x40FE0C00
class LevelActorContainer
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static LevelActorContainer ReadAsMe(const uintptr_t address)
	{
		LevelActorContainer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelActorContainer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetGuidLibrary
// 0x40FEE400
class KismetGuidLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetGuidLibrary ReadAsMe(const uintptr_t address)
	{
		KismetGuidLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetGuidLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelScriptActor
// 0x40FE8000
class LevelScriptActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static LevelScriptActor ReadAsMe(const uintptr_t address)
	{
		LevelScriptActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelScriptActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Level
// 0x40FE0C00
class Level
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Level ReadAsMe(const uintptr_t address)
	{
		Level ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Level& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelBounds
// 0x40FE8000
class LevelBounds
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static LevelBounds ReadAsMe(const uintptr_t address)
	{
		LevelBounds ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelBounds& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.KismetSystemLibrary
// 0x40FEE400
class KismetSystemLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static KismetSystemLibrary ReadAsMe(const uintptr_t address)
	{
		KismetSystemLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, KismetSystemLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelStreaming
// 0x40FE0C00
class LevelStreaming
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static LevelStreaming ReadAsMe(const uintptr_t address)
	{
		LevelStreaming ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelStreaming& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelStreamingAlwaysLoaded
// 0x42940E00
class LevelStreamingAlwaysLoaded
{
public:
	unsigned char                                      UnknownData00[0x42940E00];                                // 0x0000(0x42940E00) MISSED OFFSET

	static LevelStreamingAlwaysLoaded ReadAsMe(const uintptr_t address)
	{
		LevelStreamingAlwaysLoaded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelStreamingAlwaysLoaded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelStreamingDynamic
// 0x42940E00
class LevelStreamingDynamic
{
public:
	unsigned char                                      UnknownData00[0x42940E00];                                // 0x0000(0x42940E00) MISSED OFFSET

	static LevelStreamingDynamic ReadAsMe(const uintptr_t address)
	{
		LevelStreamingDynamic ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelStreamingDynamic& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelStreamingPersistent
// 0x42940E00
class LevelStreamingPersistent
{
public:
	unsigned char                                      UnknownData00[0x42940E00];                                // 0x0000(0x42940E00) MISSED OFFSET

	static LevelStreamingPersistent ReadAsMe(const uintptr_t address)
	{
		LevelStreamingPersistent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelStreamingPersistent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelScriptBlueprint
// 0x428BA800
class LevelScriptBlueprint
{
public:
	unsigned char                                      UnknownData00[0x428BA800];                                // 0x0000(0x428BA800) MISSED OFFSET

	static LevelScriptBlueprint ReadAsMe(const uintptr_t address)
	{
		LevelScriptBlueprint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelScriptBlueprint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LevelStreamingVolume
// 0x40FEC600
class LevelStreamingVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static LevelStreamingVolume ReadAsMe(const uintptr_t address)
	{
		LevelStreamingVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LevelStreamingVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightmappedSurfaceCollection
// 0x40FE0C00
class LightmappedSurfaceCollection
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static LightmappedSurfaceCollection ReadAsMe(const uintptr_t address)
	{
		LightmappedSurfaceCollection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightmappedSurfaceCollection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightmassCharacterIndirectDetailVolume
// 0x40FEC600
class LightmassCharacterIndirectDetailVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static LightmassCharacterIndirectDetailVolume ReadAsMe(const uintptr_t address)
	{
		LightmassCharacterIndirectDetailVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightmassCharacterIndirectDetailVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightMapTexture2D
// 0x4290FE00
class LightMapTexture2D
{
public:
	unsigned char                                      UnknownData00[0x4290FE00];                                // 0x0000(0x4290FE00) MISSED OFFSET

	static LightMapTexture2D ReadAsMe(const uintptr_t address)
	{
		LightMapTexture2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightMapTexture2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightmassPortal
// 0x40FE8000
class LightmassPortal
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static LightmassPortal ReadAsMe(const uintptr_t address)
	{
		LightmassPortal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightmassPortal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightmassPrimitiveSettingsObject
// 0x40FE0C00
class LightmassPrimitiveSettingsObject
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static LightmassPrimitiveSettingsObject ReadAsMe(const uintptr_t address)
	{
		LightmassPrimitiveSettingsObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightmassPrimitiveSettingsObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LineBatchComponent
// 0x427D0A00
class LineBatchComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static LineBatchComponent ReadAsMe(const uintptr_t address)
	{
		LineBatchComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LineBatchComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightmassPortalComponent
// 0x40FEF800
class LightmassPortalComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static LightmassPortalComponent ReadAsMe(const uintptr_t address)
	{
		LightmassPortalComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightmassPortalComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LocalPlayerSubsystem
// 0x42920200
class LocalPlayerSubsystem
{
public:
	unsigned char                                      UnknownData00[0x42920200];                                // 0x0000(0x42920200) MISSED OFFSET

	static LocalPlayerSubsystem ReadAsMe(const uintptr_t address)
	{
		LocalPlayerSubsystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LocalPlayerSubsystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LocalLightComponent
// 0x4290C800
class LocalLightComponent
{
public:
	unsigned char                                      UnknownData00[0x4290C800];                                // 0x0000(0x4290C800) MISSED OFFSET

	static LocalLightComponent ReadAsMe(const uintptr_t address)
	{
		LocalLightComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LocalLightComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LODActor
// 0x40FE8000
class LODActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static LODActor ReadAsMe(const uintptr_t address)
	{
		LODActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LODActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Material
// 0x4288AC00
class Material
{
public:
	unsigned char                                      UnknownData00[0x4288AC00];                                // 0x0000(0x4288AC00) MISSED OFFSET

	static Material ReadAsMe(const uintptr_t address)
	{
		Material ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Material& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialBillboardComponent
// 0x427D0A00
class MaterialBillboardComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static MaterialBillboardComponent ReadAsMe(const uintptr_t address)
	{
		MaterialBillboardComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialBillboardComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionAbs
// 0x427E5E00
class MaterialExpressionAbs
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionAbs ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionAbs ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionAbs& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionAdd
// 0x427E5E00
class MaterialExpressionAdd
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionAdd ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionAdd ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionAdd& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MapBuildDataRegistry
// 0x40FE0C00
class MapBuildDataRegistry
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static MapBuildDataRegistry ReadAsMe(const uintptr_t address)
	{
		MapBuildDataRegistry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MapBuildDataRegistry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Layer
// 0x40FE0C00
class Layer
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Layer ReadAsMe(const uintptr_t address)
	{
		Layer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Layer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionAppendVector
// 0x427E5E00
class MaterialExpressionAppendVector
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionAppendVector ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionAppendVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionAppendVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightmassImportanceVolume
// 0x40FEC600
class LightmassImportanceVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static LightmassImportanceVolume ReadAsMe(const uintptr_t address)
	{
		LightmassImportanceVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightmassImportanceVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionArccosine
// 0x427E5E00
class MaterialExpressionArccosine
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionArccosine ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionArccosine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionArccosine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionActorPositionWS
// 0x427E5E00
class MaterialExpressionActorPositionWS
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionActorPositionWS ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionActorPositionWS ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionActorPositionWS& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionArcsine
// 0x427E5E00
class MaterialExpressionArcsine
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionArcsine ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionArcsine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionArcsine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionArccosineFast
// 0x427E5E00
class MaterialExpressionArccosineFast
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionArccosineFast ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionArccosineFast ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionArccosineFast& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionArctangent
// 0x427E5E00
class MaterialExpressionArctangent
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionArctangent ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionArctangent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionArctangent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionAntialiasedTextureMask
// 0x427E6600
class MaterialExpressionAntialiasedTextureMask
{
public:
	unsigned char                                      UnknownData00[0x427E6600];                                // 0x0000(0x427E6600) MISSED OFFSET

	static MaterialExpressionAntialiasedTextureMask ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionAntialiasedTextureMask ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionAntialiasedTextureMask& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionArcsineFast
// 0x427E5E00
class MaterialExpressionArcsineFast
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionArcsineFast ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionArcsineFast ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionArcsineFast& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionArctangent2
// 0x427E5E00
class MaterialExpressionArctangent2
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionArctangent2 ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionArctangent2 ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionArctangent2& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionArctangentFast
// 0x427E5E00
class MaterialExpressionArctangentFast
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionArctangentFast ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionArctangentFast ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionArctangentFast& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionAtmosphericLightVector
// 0x427E5E00
class MaterialExpressionAtmosphericLightVector
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionAtmosphericLightVector ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionAtmosphericLightVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionAtmosphericLightVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionBentNormalCustomOutput
// 0x42889A00
class MaterialExpressionBentNormalCustomOutput
{
public:
	unsigned char                                      UnknownData00[0x42889A00];                                // 0x0000(0x42889A00) MISSED OFFSET

	static MaterialExpressionBentNormalCustomOutput ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionBentNormalCustomOutput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionBentNormalCustomOutput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionAtmosphericLightColor
// 0x427E5E00
class MaterialExpressionAtmosphericLightColor
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionAtmosphericLightColor ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionAtmosphericLightColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionAtmosphericLightColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionArctangent2Fast
// 0x427E5E00
class MaterialExpressionArctangent2Fast
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionArctangent2Fast ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionArctangent2Fast ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionArctangent2Fast& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionBlackBody
// 0x427E5E00
class MaterialExpressionBlackBody
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionBlackBody ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionBlackBody ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionBlackBody& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionBlendMaterialAttributes
// 0x427E5E00
class MaterialExpressionBlendMaterialAttributes
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionBlendMaterialAttributes ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionBlendMaterialAttributes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionBlendMaterialAttributes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionBumpOffset
// 0x427E5E00
class MaterialExpressionBumpOffset
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionBumpOffset ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionBumpOffset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionBumpOffset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCameraPositionWS
// 0x427E5E00
class MaterialExpressionCameraPositionWS
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionCameraPositionWS ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCameraPositionWS ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCameraPositionWS& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionBreakMaterialAttributes
// 0x427E5E00
class MaterialExpressionBreakMaterialAttributes
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionBreakMaterialAttributes ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionBreakMaterialAttributes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionBreakMaterialAttributes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCameraVectorWS
// 0x427E5E00
class MaterialExpressionCameraVectorWS
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionCameraVectorWS ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCameraVectorWS ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCameraVectorWS& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCeil
// 0x427E5E00
class MaterialExpressionCeil
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionCeil ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCeil ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCeil& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionVectorParameter
// 0x4294BC00
class MaterialExpressionVectorParameter
{
public:
	unsigned char                                      UnknownData00[0x4294BC00];                                // 0x0000(0x4294BC00) MISSED OFFSET

	static MaterialExpressionVectorParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionVectorParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionVectorParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParameter
// 0x427E5E00
class MaterialExpressionParameter
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionChannelMaskParameter
// 0x4294BE00
class MaterialExpressionChannelMaskParameter
{
public:
	unsigned char                                      UnknownData00[0x4294BE00];                                // 0x0000(0x4294BE00) MISSED OFFSET

	static MaterialExpressionChannelMaskParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionChannelMaskParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionChannelMaskParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionClamp
// 0x427E5E00
class MaterialExpressionClamp
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionClamp ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionClamp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionClamp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionAtmosphericFogColor
// 0x427E5E00
class MaterialExpressionAtmosphericFogColor
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionAtmosphericFogColor ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionAtmosphericFogColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionAtmosphericFogColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionComment
// 0x427E5E00
class MaterialExpressionComment
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionComment ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionComment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionComment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionClearCoatNormalCustomOutput
// 0x42889A00
class MaterialExpressionClearCoatNormalCustomOutput
{
public:
	unsigned char                                      UnknownData00[0x42889A00];                                // 0x0000(0x42889A00) MISSED OFFSET

	static MaterialExpressionClearCoatNormalCustomOutput ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionClearCoatNormalCustomOutput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionClearCoatNormalCustomOutput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionConstant
// 0x427E5E00
class MaterialExpressionConstant
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionConstant ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionConstant ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionConstant& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCollectionParameter
// 0x427E5E00
class MaterialExpressionCollectionParameter
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionCollectionParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCollectionParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCollectionParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionConstant2Vector
// 0x427E5E00
class MaterialExpressionConstant2Vector
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionConstant2Vector ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionConstant2Vector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionConstant2Vector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionConstant3Vector
// 0x427E5E00
class MaterialExpressionConstant3Vector
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionConstant3Vector ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionConstant3Vector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionConstant3Vector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionComponentMask
// 0x427E5E00
class MaterialExpressionComponentMask
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionComponentMask ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionComponentMask ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionComponentMask& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionConstantBiasScale
// 0x427E5E00
class MaterialExpressionConstantBiasScale
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionConstantBiasScale ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionConstantBiasScale ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionConstantBiasScale& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCosine
// 0x427E5E00
class MaterialExpressionCosine
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionCosine ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCosine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCosine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionScalarParameter
// 0x4294BC00
class MaterialExpressionScalarParameter
{
public:
	unsigned char                                      UnknownData00[0x4294BC00];                                // 0x0000(0x4294BC00) MISSED OFFSET

	static MaterialExpressionScalarParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionScalarParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionScalarParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCurveAtlasRowParameter
// 0x4294A000
class MaterialExpressionCurveAtlasRowParameter
{
public:
	unsigned char                                      UnknownData00[0x4294A000];                                // 0x0000(0x4294A000) MISSED OFFSET

	static MaterialExpressionCurveAtlasRowParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCurveAtlasRowParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCurveAtlasRowParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDDX
// 0x427E5E00
class MaterialExpressionDDX
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDDX ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDDX ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDDX& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDDY
// 0x427E5E00
class MaterialExpressionDDY
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDDY ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDDY ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDDY& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCustom
// 0x427E5E00
class MaterialExpressionCustom
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionCustom ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCustom ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCustom& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDecalLifetimeOpacity
// 0x427E5E00
class MaterialExpressionDecalLifetimeOpacity
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDecalLifetimeOpacity ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDecalLifetimeOpacity ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDecalLifetimeOpacity& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDeltaTime
// 0x427E5E00
class MaterialExpressionDeltaTime
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDeltaTime ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDeltaTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDeltaTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDecalMipmapLevel
// 0x427E5E00
class MaterialExpressionDecalMipmapLevel
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDecalMipmapLevel ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDecalMipmapLevel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDecalMipmapLevel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDepthFade
// 0x427E5E00
class MaterialExpressionDepthFade
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDepthFade ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDepthFade ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDepthFade& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDepthOfFieldFunction
// 0x427E5E00
class MaterialExpressionDepthOfFieldFunction
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDepthOfFieldFunction ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDepthOfFieldFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDepthOfFieldFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDecalDerivative
// 0x427E5E00
class MaterialExpressionDecalDerivative
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDecalDerivative ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDecalDerivative ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDecalDerivative& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDeriveNormalZ
// 0x427E5E00
class MaterialExpressionDeriveNormalZ
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDeriveNormalZ ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDeriveNormalZ ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDeriveNormalZ& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionConstant4Vector
// 0x427E5E00
class MaterialExpressionConstant4Vector
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionConstant4Vector ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionConstant4Vector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionConstant4Vector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDistanceCullFade
// 0x427E5E00
class MaterialExpressionDistanceCullFade
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDistanceCullFade ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDistanceCullFade ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDistanceCullFade& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDesaturation
// 0x427E5E00
class MaterialExpressionDesaturation
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDesaturation ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDesaturation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDesaturation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDistance
// 0x427E5E00
class MaterialExpressionDistance
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDistance ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDistance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDistance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDistanceFieldGradient
// 0x427E5E00
class MaterialExpressionDistanceFieldGradient
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDistanceFieldGradient ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDistanceFieldGradient ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDistanceFieldGradient& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDistanceToNearestSurface
// 0x427E5E00
class MaterialExpressionDistanceToNearestSurface
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDistanceToNearestSurface ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDistanceToNearestSurface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDistanceToNearestSurface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDotProduct
// 0x427E5E00
class MaterialExpressionDotProduct
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDotProduct ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDotProduct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDotProduct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionCrossProduct
// 0x427E5E00
class MaterialExpressionCrossProduct
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionCrossProduct ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionCrossProduct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionCrossProduct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionEyeAdaptation
// 0x427E5E00
class MaterialExpressionEyeAdaptation
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionEyeAdaptation ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionEyeAdaptation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionEyeAdaptation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDynamicParameter
// 0x427E5E00
class MaterialExpressionDynamicParameter
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDynamicParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDynamicParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDynamicParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFloor
// 0x427E5E00
class MaterialExpressionFloor
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionFloor ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFloor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFloor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFeatureLevelSwitch
// 0x427E5E00
class MaterialExpressionFeatureLevelSwitch
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionFeatureLevelSwitch ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFeatureLevelSwitch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFeatureLevelSwitch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFontSample
// 0x427E5E00
class MaterialExpressionFontSample
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionFontSample ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFontSample ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFontSample& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFontSampleParameter
// 0x4294EC00
class MaterialExpressionFontSampleParameter
{
public:
	unsigned char                                      UnknownData00[0x4294EC00];                                // 0x0000(0x4294EC00) MISSED OFFSET

	static MaterialExpressionFontSampleParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFontSampleParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFontSampleParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFrac
// 0x427E5E00
class MaterialExpressionFrac
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionFrac ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFrac ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFrac& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionDivide
// 0x427E5E00
class MaterialExpressionDivide
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionDivide ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionDivide ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionDivide& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFunctionInput
// 0x427E5E00
class MaterialExpressionFunctionInput
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionFunctionInput ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFunctionInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFunctionInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFresnel
// 0x427E5E00
class MaterialExpressionFresnel
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionFresnel ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFresnel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFresnel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFmod
// 0x427E5E00
class MaterialExpressionFmod
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionFmod ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFmod ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFmod& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionFunctionOutput
// 0x427E5E00
class MaterialExpressionFunctionOutput
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionFunctionOutput ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionFunctionOutput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionFunctionOutput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionIf
// 0x427E5E00
class MaterialExpressionIf
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionIf ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionIf ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionIf& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionLightmapUVs
// 0x427E5E00
class MaterialExpressionLightmapUVs
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionLightmapUVs ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionLightmapUVs ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionLightmapUVs& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionLightmassReplace
// 0x427E5E00
class MaterialExpressionLightmassReplace
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionLightmassReplace ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionLightmassReplace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionLightmassReplace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionGIReplace
// 0x427E5E00
class MaterialExpressionGIReplace
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionGIReplace ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionGIReplace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionGIReplace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionLogarithm10
// 0x427E5E00
class MaterialExpressionLogarithm10
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionLogarithm10 ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionLogarithm10 ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionLogarithm10& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionLinearInterpolate
// 0x427E5E00
class MaterialExpressionLinearInterpolate
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionLinearInterpolate ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionLinearInterpolate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionLinearInterpolate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionMakeMaterialAttributes
// 0x427E5E00
class MaterialExpressionMakeMaterialAttributes
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionMakeMaterialAttributes ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionMakeMaterialAttributes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionMakeMaterialAttributes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionLightVector
// 0x427E5E00
class MaterialExpressionLightVector
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionLightVector ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionLightVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionLightVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionGetMaterialAttributes
// 0x427E5E00
class MaterialExpressionGetMaterialAttributes
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionGetMaterialAttributes ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionGetMaterialAttributes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionGetMaterialAttributes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionLogarithm2
// 0x427E5E00
class MaterialExpressionLogarithm2
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionLogarithm2 ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionLogarithm2 ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionLogarithm2& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionMaterialLayerOutput
// 0x4294E200
class MaterialExpressionMaterialLayerOutput
{
public:
	unsigned char                                      UnknownData00[0x4294E200];                                // 0x0000(0x4294E200) MISSED OFFSET

	static MaterialExpressionMaterialLayerOutput ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionMaterialLayerOutput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionMaterialLayerOutput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionMaterialAttributeLayers
// 0x427E5E00
class MaterialExpressionMaterialAttributeLayers
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionMaterialAttributeLayers ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionMaterialAttributeLayers ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionMaterialAttributeLayers& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionMaterialProxyReplace
// 0x427E5E00
class MaterialExpressionMaterialProxyReplace
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionMaterialProxyReplace ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionMaterialProxyReplace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionMaterialProxyReplace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionMax
// 0x427E5E00
class MaterialExpressionMax
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionMax ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionMax ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionMax& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionMin
// 0x427E5E00
class MaterialExpressionMin
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionMin ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionMin ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionMin& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionMaterialFunctionCall
// 0x427E5E00
class MaterialExpressionMaterialFunctionCall
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionMaterialFunctionCall ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionMaterialFunctionCall ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionMaterialFunctionCall& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionObjectBounds
// 0x427E5E00
class MaterialExpressionObjectBounds
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionObjectBounds ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionObjectBounds ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionObjectBounds& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionNormalize
// 0x427E5E00
class MaterialExpressionNormalize
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionNormalize ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionNormalize ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionNormalize& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionNoise
// 0x427E5E00
class MaterialExpressionNoise
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionNoise ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionNoise ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionNoise& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionObjectPositionWS
// 0x427E5E00
class MaterialExpressionObjectPositionWS
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionObjectPositionWS ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionObjectPositionWS ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionObjectPositionWS& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionObjectRadius
// 0x427E5E00
class MaterialExpressionObjectRadius
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionObjectRadius ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionObjectRadius ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionObjectRadius& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPanner
// 0x427E5E00
class MaterialExpressionPanner
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPanner ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPanner ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPanner& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionMultiply
// 0x427E5E00
class MaterialExpressionMultiply
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionMultiply ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionMultiply ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionMultiply& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleDirection
// 0x427E5E00
class MaterialExpressionParticleDirection
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleDirection ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleDirection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleDirection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleColor
// 0x427E5E00
class MaterialExpressionParticleColor
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleColor ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionOneMinus
// 0x427E5E00
class MaterialExpressionOneMinus
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionOneMinus ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionOneMinus ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionOneMinus& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleRadius
// 0x427E5E00
class MaterialExpressionParticleRadius
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleRadius ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleRadius ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleRadius& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleRandom
// 0x427E5E00
class MaterialExpressionParticleRandom
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleRandom ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleRandom ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleRandom& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleRelativeTime
// 0x427E5E00
class MaterialExpressionParticleRelativeTime
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleRelativeTime ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleRelativeTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleRelativeTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionObjectOrientation
// 0x427E5E00
class MaterialExpressionObjectOrientation
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionObjectOrientation ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionObjectOrientation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionObjectOrientation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleMacroUV
// 0x427E5E00
class MaterialExpressionParticleMacroUV
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleMacroUV ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleMacroUV ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleMacroUV& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleSize
// 0x427E5E00
class MaterialExpressionParticleSize
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleSize ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleSize ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleSize& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticlePositionWS
// 0x427E5E00
class MaterialExpressionParticlePositionWS
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticlePositionWS ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticlePositionWS ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticlePositionWS& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleMotionBlurFade
// 0x427E5E00
class MaterialExpressionParticleMotionBlurFade
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleMotionBlurFade ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleMotionBlurFade ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleMotionBlurFade& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleSpeed
// 0x427E5E00
class MaterialExpressionParticleSpeed
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionParticleSpeed ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleSpeed ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleSpeed& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionParticleSubUV
// 0x427E6200
class MaterialExpressionParticleSubUV
{
public:
	unsigned char                                      UnknownData00[0x427E6200];                                // 0x0000(0x427E6200) MISSED OFFSET

	static MaterialExpressionParticleSubUV ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionParticleSubUV ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionParticleSubUV& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPerInstanceRandom
// 0x427E5E00
class MaterialExpressionPerInstanceRandom
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPerInstanceRandom ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPerInstanceRandom ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPerInstanceRandom& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPixelDepth
// 0x427E5E00
class MaterialExpressionPixelDepth
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPixelDepth ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPixelDepth ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPixelDepth& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPrecomputedAOMask
// 0x427E5E00
class MaterialExpressionPrecomputedAOMask
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPrecomputedAOMask ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPrecomputedAOMask ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPrecomputedAOMask& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPixelNormalWS
// 0x427E5E00
class MaterialExpressionPixelNormalWS
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPixelNormalWS ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPixelNormalWS ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPixelNormalWS& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPerInstanceFadeAmount
// 0x427E5E00
class MaterialExpressionPerInstanceFadeAmount
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPerInstanceFadeAmount ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPerInstanceFadeAmount ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPerInstanceFadeAmount& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionQualitySwitch
// 0x427E5E00
class MaterialExpressionQualitySwitch
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionQualitySwitch ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionQualitySwitch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionQualitySwitch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPower
// 0x427E5E00
class MaterialExpressionPower
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPower ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPower ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPower& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPreSkinnedNormal
// 0x427E5E00
class MaterialExpressionPreSkinnedNormal
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPreSkinnedNormal ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPreSkinnedNormal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPreSkinnedNormal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPreviousFrameSwitch
// 0x427E5E00
class MaterialExpressionPreviousFrameSwitch
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPreviousFrameSwitch ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPreviousFrameSwitch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPreviousFrameSwitch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionReroute
// 0x427E5E00
class MaterialExpressionReroute
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionReroute ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionReroute ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionReroute& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionPreSkinnedPosition
// 0x427E5E00
class MaterialExpressionPreSkinnedPosition
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionPreSkinnedPosition ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionPreSkinnedPosition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionPreSkinnedPosition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionRotateAboutAxis
// 0x427E5E00
class MaterialExpressionRotateAboutAxis
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionRotateAboutAxis ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionRotateAboutAxis ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionRotateAboutAxis& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSaturate
// 0x427E5E00
class MaterialExpressionSaturate
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSaturate ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSaturate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSaturate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionRayTracingQualitySwitch
// 0x427E5E00
class MaterialExpressionRayTracingQualitySwitch
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionRayTracingQualitySwitch ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionRayTracingQualitySwitch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionRayTracingQualitySwitch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionReflectionVectorWS
// 0x427E5E00
class MaterialExpressionReflectionVectorWS
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionReflectionVectorWS ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionReflectionVectorWS ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionReflectionVectorWS& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSceneDepth
// 0x427E5E00
class MaterialExpressionSceneDepth
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSceneDepth ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSceneDepth ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSceneDepth& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSceneColor
// 0x427E5E00
class MaterialExpressionSceneColor
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSceneColor ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSceneColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSceneColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSceneTexelSize
// 0x427E5E00
class MaterialExpressionSceneTexelSize
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSceneTexelSize ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSceneTexelSize ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSceneTexelSize& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionRound
// 0x427E5E00
class MaterialExpressionRound
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionRound ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionRound ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionRound& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionRotator
// 0x427E5E00
class MaterialExpressionRotator
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionRotator ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionRotator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionRotator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionScreenPosition
// 0x427E5E00
class MaterialExpressionScreenPosition
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionScreenPosition ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionScreenPosition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionScreenPosition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSceneTexture
// 0x427E5E00
class MaterialExpressionSceneTexture
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSceneTexture ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSceneTexture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSceneTexture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSetMaterialAttributes
// 0x427E5E00
class MaterialExpressionSetMaterialAttributes
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSetMaterialAttributes ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSetMaterialAttributes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSetMaterialAttributes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSine
// 0x427E5E00
class MaterialExpressionSine
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSine ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionShadowReplace
// 0x427E5E00
class MaterialExpressionShadowReplace
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionShadowReplace ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionShadowReplace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionShadowReplace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSpeedTree
// 0x427E5E00
class MaterialExpressionSpeedTree
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSpeedTree ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSpeedTree ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSpeedTree& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSobol
// 0x427E5E00
class MaterialExpressionSobol
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSobol ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSobol ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSobol& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSphereMask
// 0x427E5E00
class MaterialExpressionSphereMask
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSphereMask ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSphereMask ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSphereMask& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionStaticBoolParameter
// 0x4294BC00
class MaterialExpressionStaticBoolParameter
{
public:
	unsigned char                                      UnknownData00[0x4294BC00];                                // 0x0000(0x4294BC00) MISSED OFFSET

	static MaterialExpressionStaticBoolParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionStaticBoolParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionStaticBoolParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSphericalParticleOpacity
// 0x427E5E00
class MaterialExpressionSphericalParticleOpacity
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSphericalParticleOpacity ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSphericalParticleOpacity ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSphericalParticleOpacity& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSquareRoot
// 0x427E5E00
class MaterialExpressionSquareRoot
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSquareRoot ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSquareRoot ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSquareRoot& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionStaticComponentMaskParameter
// 0x4294BC00
class MaterialExpressionStaticComponentMaskParameter
{
public:
	unsigned char                                      UnknownData00[0x4294BC00];                                // 0x0000(0x4294BC00) MISSED OFFSET

	static MaterialExpressionStaticComponentMaskParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionStaticComponentMaskParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionStaticComponentMaskParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSign
// 0x427E5E00
class MaterialExpressionSign
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSign ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSign ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSign& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionShadingPathSwitch
// 0x427E5E00
class MaterialExpressionShadingPathSwitch
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionShadingPathSwitch ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionShadingPathSwitch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionShadingPathSwitch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionSubtract
// 0x427E5E00
class MaterialExpressionSubtract
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionSubtract ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionSubtract ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionSubtract& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTangent
// 0x427E5E00
class MaterialExpressionTangent
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTangent ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTangent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTangent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionStaticSwitchParameter
// 0x42965600
class MaterialExpressionStaticSwitchParameter
{
public:
	unsigned char                                      UnknownData00[0x42965600];                                // 0x0000(0x42965600) MISSED OFFSET

	static MaterialExpressionStaticSwitchParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionStaticSwitchParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionStaticSwitchParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureCoordinate
// 0x427E5E00
class MaterialExpressionTextureCoordinate
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTextureCoordinate ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureCoordinate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureCoordinate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTemporalSobol
// 0x427E5E00
class MaterialExpressionTemporalSobol
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTemporalSobol ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTemporalSobol ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTemporalSobol& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureObjectParameter
// 0x427E6400
class MaterialExpressionTextureObjectParameter
{
public:
	unsigned char                                      UnknownData00[0x427E6400];                                // 0x0000(0x427E6400) MISSED OFFSET

	static MaterialExpressionTextureObjectParameter ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureObjectParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureObjectParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTangentOutput
// 0x42889A00
class MaterialExpressionTangentOutput
{
public:
	unsigned char                                      UnknownData00[0x42889A00];                                // 0x0000(0x42889A00) MISSED OFFSET

	static MaterialExpressionTangentOutput ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTangentOutput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTangentOutput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureProperty
// 0x427E5E00
class MaterialExpressionTextureProperty
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTextureProperty ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionStaticBool
// 0x427E5E00
class MaterialExpressionStaticBool
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionStaticBool ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionStaticBool ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionStaticBool& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureObject
// 0x427E6000
class MaterialExpressionTextureObject
{
public:
	unsigned char                                      UnknownData00[0x427E6000];                                // 0x0000(0x427E6000) MISSED OFFSET

	static MaterialExpressionTextureObject ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureSampleParameterSubUV
// 0x427E6600
class MaterialExpressionTextureSampleParameterSubUV
{
public:
	unsigned char                                      UnknownData00[0x427E6600];                                // 0x0000(0x427E6600) MISSED OFFSET

	static MaterialExpressionTextureSampleParameterSubUV ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureSampleParameterSubUV ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureSampleParameterSubUV& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureSampleParameterCube
// 0x427E6400
class MaterialExpressionTextureSampleParameterCube
{
public:
	unsigned char                                      UnknownData00[0x427E6400];                                // 0x0000(0x427E6400) MISSED OFFSET

	static MaterialExpressionTextureSampleParameterCube ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureSampleParameterCube ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureSampleParameterCube& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTime
// 0x427E5E00
class MaterialExpressionTime
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTime ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTransform
// 0x427E5E00
class MaterialExpressionTransform
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTransform ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTransform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTransform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTransformPosition
// 0x427E5E00
class MaterialExpressionTransformPosition
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTransformPosition ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTransformPosition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTransformPosition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTruncate
// 0x427E5E00
class MaterialExpressionTruncate
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTruncate ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTruncate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTruncate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTwoSidedSign
// 0x427E5E00
class MaterialExpressionTwoSidedSign
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionTwoSidedSign ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTwoSidedSign ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTwoSidedSign& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionVectorNoise
// 0x427E5E00
class MaterialExpressionVectorNoise
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionVectorNoise ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionVectorNoise ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionVectorNoise& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionVertexColor
// 0x427E5E00
class MaterialExpressionVertexColor
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionVertexColor ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionVertexColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionVertexColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionVertexInterpolator
// 0x42889A00
class MaterialExpressionVertexInterpolator
{
public:
	unsigned char                                      UnknownData00[0x42889A00];                                // 0x0000(0x42889A00) MISSED OFFSET

	static MaterialExpressionVertexInterpolator ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionVertexInterpolator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionVertexInterpolator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionViewSize
// 0x427E5E00
class MaterialExpressionViewSize
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionViewSize ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionViewSize ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionViewSize& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionVertexNormalWS
// 0x427E5E00
class MaterialExpressionVertexNormalWS
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionVertexNormalWS ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionVertexNormalWS ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionVertexNormalWS& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialFunction
// 0x4296A200
class MaterialFunction
{
public:
	unsigned char                                      UnknownData00[0x4296A200];                                // 0x0000(0x4296A200) MISSED OFFSET

	static MaterialFunction ReadAsMe(const uintptr_t address)
	{
		MaterialFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionViewProperty
// 0x427E5E00
class MaterialExpressionViewProperty
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionViewProperty ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionViewProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionViewProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionWorldPosition
// 0x427E5E00
class MaterialExpressionWorldPosition
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionWorldPosition ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionWorldPosition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionWorldPosition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialFunctionInterface
// 0x40FE0C00
class MaterialFunctionInterface
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static MaterialFunctionInterface ReadAsMe(const uintptr_t address)
	{
		MaterialFunctionInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialFunctionInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialFunctionMaterialLayer
// 0x4296A400
class MaterialFunctionMaterialLayer
{
public:
	unsigned char                                      UnknownData00[0x4296A400];                                // 0x0000(0x4296A400) MISSED OFFSET

	static MaterialFunctionMaterialLayer ReadAsMe(const uintptr_t address)
	{
		MaterialFunctionMaterialLayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialFunctionMaterialLayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialFunctionMaterialLayerInstance
// 0x4296A000
class MaterialFunctionMaterialLayerInstance
{
public:
	unsigned char                                      UnknownData00[0x4296A000];                                // 0x0000(0x4296A000) MISSED OFFSET

	static MaterialFunctionMaterialLayerInstance ReadAsMe(const uintptr_t address)
	{
		MaterialFunctionMaterialLayerInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialFunctionMaterialLayerInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialFunctionMaterialLayerBlend
// 0x4296A400
class MaterialFunctionMaterialLayerBlend
{
public:
	unsigned char                                      UnknownData00[0x4296A400];                                // 0x0000(0x4296A400) MISSED OFFSET

	static MaterialFunctionMaterialLayerBlend ReadAsMe(const uintptr_t address)
	{
		MaterialFunctionMaterialLayerBlend ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialFunctionMaterialLayerBlend& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialFunctionMaterialLayerBlendInstance
// 0x4296A000
class MaterialFunctionMaterialLayerBlendInstance
{
public:
	unsigned char                                      UnknownData00[0x4296A000];                                // 0x0000(0x4296A000) MISSED OFFSET

	static MaterialFunctionMaterialLayerBlendInstance ReadAsMe(const uintptr_t address)
	{
		MaterialFunctionMaterialLayerBlendInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialFunctionMaterialLayerBlendInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionTextureSampleParameterVolume
// 0x427E6400
class MaterialExpressionTextureSampleParameterVolume
{
public:
	unsigned char                                      UnknownData00[0x427E6400];                                // 0x0000(0x427E6400) MISSED OFFSET

	static MaterialExpressionTextureSampleParameterVolume ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionTextureSampleParameterVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionTextureSampleParameterVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialFunctionInstance
// 0x4296A200
class MaterialFunctionInstance
{
public:
	unsigned char                                      UnknownData00[0x4296A200];                                // 0x0000(0x4296A200) MISSED OFFSET

	static MaterialFunctionInstance ReadAsMe(const uintptr_t address)
	{
		MaterialFunctionInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialFunctionInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialInstanceDynamic
// 0x4288AE00
class MaterialInstanceDynamic
{
public:
	unsigned char                                      UnknownData00[0x4288AE00];                                // 0x0000(0x4288AE00) MISSED OFFSET

	static MaterialInstanceDynamic ReadAsMe(const uintptr_t address)
	{
		MaterialInstanceDynamic ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialInstanceDynamic& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialInstanceActor
// 0x40FE8000
class MaterialInstanceActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static MaterialInstanceActor ReadAsMe(const uintptr_t address)
	{
		MaterialInstanceActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialInstanceActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialParameterCollectionInstance
// 0x40FE0C00
class MaterialParameterCollectionInstance
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static MaterialParameterCollectionInstance ReadAsMe(const uintptr_t address)
	{
		MaterialParameterCollectionInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialParameterCollectionInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialParameterCollection
// 0x40FE0C00
class MaterialParameterCollection
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static MaterialParameterCollection ReadAsMe(const uintptr_t address)
	{
		MaterialParameterCollection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialParameterCollection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MaterialExpressionStaticSwitch
// 0x427E5E00
class MaterialExpressionStaticSwitch
{
public:
	unsigned char                                      UnknownData00[0x427E5E00];                                // 0x0000(0x427E5E00) MISSED OFFSET

	static MaterialExpressionStaticSwitch ReadAsMe(const uintptr_t address)
	{
		MaterialExpressionStaticSwitch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MaterialExpressionStaticSwitch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MatineeActor
// 0x40FE8000
class MatineeActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static MatineeActor ReadAsMe(const uintptr_t address)
	{
		MatineeActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MatineeActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MatineeActorCameraAnim
// 0x42968E00
class MatineeActorCameraAnim
{
public:
	unsigned char                                      UnknownData00[0x42968E00];                                // 0x0000(0x42968E00) MISSED OFFSET

	static MatineeActorCameraAnim ReadAsMe(const uintptr_t address)
	{
		MatineeActorCameraAnim ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MatineeActorCameraAnim& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MatineeInterface
// 0x427D4600
class MatineeInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static MatineeInterface ReadAsMe(const uintptr_t address)
	{
		MatineeInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MatineeInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MeshVertexPainterKismetLibrary
// 0x40FEE400
class MeshVertexPainterKismetLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static MeshVertexPainterKismetLibrary ReadAsMe(const uintptr_t address)
	{
		MeshVertexPainterKismetLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MeshVertexPainterKismetLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MicroTransactionBase
// 0x4290AA00
class MicroTransactionBase
{
public:
	unsigned char                                      UnknownData00[0x4290AA00];                                // 0x0000(0x4290AA00) MISSED OFFSET

	static MicroTransactionBase ReadAsMe(const uintptr_t address)
	{
		MicroTransactionBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MicroTransactionBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ModelComponent
// 0x427D0A00
class ModelComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static ModelComponent ReadAsMe(const uintptr_t address)
	{
		ModelComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ModelComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MorphTarget
// 0x40FE0C00
class MorphTarget
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static MorphTarget ReadAsMe(const uintptr_t address)
	{
		MorphTarget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MorphTarget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MeshSimplificationSettings
// 0x427D8E00
class MeshSimplificationSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static MeshSimplificationSettings ReadAsMe(const uintptr_t address)
	{
		MeshSimplificationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MeshSimplificationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MeshMergeCullingVolume
// 0x40FEC600
class MeshMergeCullingVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static MeshMergeCullingVolume ReadAsMe(const uintptr_t address)
	{
		MeshMergeCullingVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MeshMergeCullingVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavCollisionBase
// 0x40FE0C00
class NavCollisionBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NavCollisionBase ReadAsMe(const uintptr_t address)
	{
		NavCollisionBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavCollisionBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavigationDataChunk
// 0x40FE0C00
class NavigationDataChunk
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NavigationDataChunk ReadAsMe(const uintptr_t address)
	{
		NavigationDataChunk ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavigationDataChunk& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavigationDataInterface
// 0x427D4600
class NavigationDataInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static NavigationDataInterface ReadAsMe(const uintptr_t address)
	{
		NavigationDataInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavigationDataInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavAgentInterface
// 0x427D4600
class NavAgentInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static NavAgentInterface ReadAsMe(const uintptr_t address)
	{
		NavAgentInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavAgentInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavigationSystemBase
// 0x40FE0C00
class NavigationSystemBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NavigationSystemBase ReadAsMe(const uintptr_t address)
	{
		NavigationSystemBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavigationSystemBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavigationSystemConfig
// 0x40FE0C00
class NavigationSystemConfig
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NavigationSystemConfig ReadAsMe(const uintptr_t address)
	{
		NavigationSystemConfig ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavigationSystemConfig& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavEdgeProviderInterface
// 0x427D4600
class NavEdgeProviderInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static NavEdgeProviderInterface ReadAsMe(const uintptr_t address)
	{
		NavEdgeProviderInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavEdgeProviderInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NullNavSysConfig
// 0x4296E600
class NullNavSysConfig
{
public:
	unsigned char                                      UnknownData00[0x4296E600];                                // 0x0000(0x4296E600) MISSED OFFSET

	static NullNavSysConfig ReadAsMe(const uintptr_t address)
	{
		NullNavSysConfig ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NullNavSysConfig& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavPathObserverInterface
// 0x427D4600
class NavPathObserverInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static NavPathObserverInterface ReadAsMe(const uintptr_t address)
	{
		NavPathObserverInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavPathObserverInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SimulatedClientNetConnection
// 0x427DAA00
class SimulatedClientNetConnection
{
public:
	unsigned char                                      UnknownData00[0x427DAA00];                                // 0x0000(0x427DAA00) MISSED OFFSET

	static SimulatedClientNetConnection ReadAsMe(const uintptr_t address)
	{
		SimulatedClientNetConnection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SimulatedClientNetConnection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavRelevantInterface
// 0x427D4600
class NavRelevantInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static NavRelevantInterface ReadAsMe(const uintptr_t address)
	{
		NavRelevantInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavRelevantInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavLinkDefinition
// 0x40FE0C00
class NavLinkDefinition
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NavLinkDefinition ReadAsMe(const uintptr_t address)
	{
		NavLinkDefinition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavLinkDefinition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NetworkSettings
// 0x427D8E00
class NetworkSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static NetworkSettings ReadAsMe(const uintptr_t address)
	{
		NetworkSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NetworkSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NodeMappingContainer
// 0x40FE0C00
class NodeMappingContainer
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NodeMappingContainer ReadAsMe(const uintptr_t address)
	{
		NodeMappingContainer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NodeMappingContainer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NodeMappingProviderInterface
// 0x427D4600
class NodeMappingProviderInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static NodeMappingProviderInterface ReadAsMe(const uintptr_t address)
	{
		NodeMappingProviderInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NodeMappingProviderInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Note
// 0x40FE8000
class Note
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static Note ReadAsMe(const uintptr_t address)
	{
		Note ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Note& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ObjectLibrary
// 0x40FE0C00
class ObjectLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ObjectLibrary ReadAsMe(const uintptr_t address)
	{
		ObjectLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ObjectLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PackageMapClient
// 0x4287C000
class PackageMapClient
{
public:
	unsigned char                                      UnknownData00[0x4287C000];                                // 0x0000(0x4287C000) MISSED OFFSET

	static PackageMapClient ReadAsMe(const uintptr_t address)
	{
		PackageMapClient ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PackageMapClient& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NetworkPredictionInterface
// 0x427D4600
class NetworkPredictionInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static NetworkPredictionInterface ReadAsMe(const uintptr_t address)
	{
		NetworkPredictionInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NetworkPredictionInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleEventManager
// 0x40FE8000
class ParticleEventManager
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static ParticleEventManager ReadAsMe(const uintptr_t address)
	{
		ParticleEventManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleEventManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.NavigationSystem
// 0x40FE0C00
class NavigationSystem
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static NavigationSystem ReadAsMe(const uintptr_t address)
	{
		NavigationSystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, NavigationSystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.MatineeAnimInterface
// 0x427D4600
class MatineeAnimInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static MatineeAnimInterface ReadAsMe(const uintptr_t address)
	{
		MatineeAnimInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, MatineeAnimInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleEmitter
// 0x40FE0C00
class ParticleEmitter
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ParticleEmitter ReadAsMe(const uintptr_t address)
	{
		ParticleEmitter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleEmitter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModule
// 0x40FE0C00
class ParticleModule
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ParticleModule ReadAsMe(const uintptr_t address)
	{
		ParticleModule ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModule& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAccelerationBase
// 0x42973E00
class ParticleModuleAccelerationBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleAccelerationBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAccelerationBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAccelerationBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PainCausingVolume
// 0x42877400
class PainCausingVolume
{
public:
	unsigned char                                      UnknownData00[0x42877400];                                // 0x0000(0x42877400) MISSED OFFSET

	static PainCausingVolume ReadAsMe(const uintptr_t address)
	{
		PainCausingVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PainCausingVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleLODLevel
// 0x40FE0C00
class ParticleLODLevel
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ParticleLODLevel ReadAsMe(const uintptr_t address)
	{
		ParticleLODLevel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleLODLevel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAccelerationConstant
// 0x42973A00
class ParticleModuleAccelerationConstant
{
public:
	unsigned char                                      UnknownData00[0x42973A00];                                // 0x0000(0x42973A00) MISSED OFFSET

	static ParticleModuleAccelerationConstant ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAccelerationConstant ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAccelerationConstant& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ObjectReferencer
// 0x40FE0C00
class ObjectReferencer
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ObjectReferencer ReadAsMe(const uintptr_t address)
	{
		ObjectReferencer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ObjectReferencer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAccelerationDrag
// 0x42973A00
class ParticleModuleAccelerationDrag
{
public:
	unsigned char                                      UnknownData00[0x42973A00];                                // 0x0000(0x42973A00) MISSED OFFSET

	static ParticleModuleAccelerationDrag ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAccelerationDrag ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAccelerationDrag& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAccelerationDragScaleOverLife
// 0x42973A00
class ParticleModuleAccelerationDragScaleOverLife
{
public:
	unsigned char                                      UnknownData00[0x42973A00];                                // 0x0000(0x42973A00) MISSED OFFSET

	static ParticleModuleAccelerationDragScaleOverLife ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAccelerationDragScaleOverLife ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAccelerationDragScaleOverLife& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAccelerationOverLifetime
// 0x42973A00
class ParticleModuleAccelerationOverLifetime
{
public:
	unsigned char                                      UnknownData00[0x42973A00];                                // 0x0000(0x42973A00) MISSED OFFSET

	static ParticleModuleAccelerationOverLifetime ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAccelerationOverLifetime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAccelerationOverLifetime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAttractorBase
// 0x42973E00
class ParticleModuleAttractorBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleAttractorBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAttractorBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAttractorBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAcceleration
// 0x42973A00
class ParticleModuleAcceleration
{
public:
	unsigned char                                      UnknownData00[0x42973A00];                                // 0x0000(0x42973A00) MISSED OFFSET

	static ParticleModuleAcceleration ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAcceleration ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAcceleration& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAttractorPointGravity
// 0x42973000
class ParticleModuleAttractorPointGravity
{
public:
	unsigned char                                      UnknownData00[0x42973000];                                // 0x0000(0x42973000) MISSED OFFSET

	static ParticleModuleAttractorPointGravity ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAttractorPointGravity ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAttractorPointGravity& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleBeamBase
// 0x42973E00
class ParticleModuleBeamBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleBeamBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleBeamBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleBeamBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAttractorLine
// 0x42973000
class ParticleModuleAttractorLine
{
public:
	unsigned char                                      UnknownData00[0x42973000];                                // 0x0000(0x42973000) MISSED OFFSET

	static ParticleModuleAttractorLine ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAttractorLine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAttractorLine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAttractorPoint
// 0x42973000
class ParticleModuleAttractorPoint
{
public:
	unsigned char                                      UnknownData00[0x42973000];                                // 0x0000(0x42973000) MISSED OFFSET

	static ParticleModuleAttractorPoint ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAttractorPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAttractorPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleBeamModifier
// 0x42972600
class ParticleModuleBeamModifier
{
public:
	unsigned char                                      UnknownData00[0x42972600];                                // 0x0000(0x42972600) MISSED OFFSET

	static ParticleModuleBeamModifier ReadAsMe(const uintptr_t address)
	{
		ParticleModuleBeamModifier ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleBeamModifier& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleBeamNoise
// 0x42972600
class ParticleModuleBeamNoise
{
public:
	unsigned char                                      UnknownData00[0x42972600];                                // 0x0000(0x42972600) MISSED OFFSET

	static ParticleModuleBeamNoise ReadAsMe(const uintptr_t address)
	{
		ParticleModuleBeamNoise ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleBeamNoise& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleAttractorParticle
// 0x42973000
class ParticleModuleAttractorParticle
{
public:
	unsigned char                                      UnknownData00[0x42973000];                                // 0x0000(0x42973000) MISSED OFFSET

	static ParticleModuleAttractorParticle ReadAsMe(const uintptr_t address)
	{
		ParticleModuleAttractorParticle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleAttractorParticle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleCameraBase
// 0x42973E00
class ParticleModuleCameraBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleCameraBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleCameraBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleCameraBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleBeamTarget
// 0x42972600
class ParticleModuleBeamTarget
{
public:
	unsigned char                                      UnknownData00[0x42972600];                                // 0x0000(0x42972600) MISSED OFFSET

	static ParticleModuleBeamTarget ReadAsMe(const uintptr_t address)
	{
		ParticleModuleBeamTarget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleBeamTarget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleCollisionBase
// 0x42973E00
class ParticleModuleCollisionBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleCollisionBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleCollisionBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleCollisionBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleCollision
// 0x42971600
class ParticleModuleCollision
{
public:
	unsigned char                                      UnknownData00[0x42971600];                                // 0x0000(0x42971600) MISSED OFFSET

	static ParticleModuleCollision ReadAsMe(const uintptr_t address)
	{
		ParticleModuleCollision ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleCollision& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleCameraOffset
// 0x42971C00
class ParticleModuleCameraOffset
{
public:
	unsigned char                                      UnknownData00[0x42971C00];                                // 0x0000(0x42971C00) MISSED OFFSET

	static ParticleModuleCameraOffset ReadAsMe(const uintptr_t address)
	{
		ParticleModuleCameraOffset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleCameraOffset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleColor
// 0x42971000
class ParticleModuleColor
{
public:
	unsigned char                                      UnknownData00[0x42971000];                                // 0x0000(0x42971000) MISSED OFFSET

	static ParticleModuleColor ReadAsMe(const uintptr_t address)
	{
		ParticleModuleColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleColorOverLife
// 0x42971000
class ParticleModuleColorOverLife
{
public:
	unsigned char                                      UnknownData00[0x42971000];                                // 0x0000(0x42971000) MISSED OFFSET

	static ParticleModuleColorOverLife ReadAsMe(const uintptr_t address)
	{
		ParticleModuleColorOverLife ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleColorOverLife& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleColorBase
// 0x42973E00
class ParticleModuleColorBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleColorBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleColorBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleColorBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleColorScaleOverLife
// 0x42971000
class ParticleModuleColorScaleOverLife
{
public:
	unsigned char                                      UnknownData00[0x42971000];                                // 0x0000(0x42971000) MISSED OFFSET

	static ParticleModuleColorScaleOverLife ReadAsMe(const uintptr_t address)
	{
		ParticleModuleColorScaleOverLife ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleColorScaleOverLife& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleEventBase
// 0x42973E00
class ParticleModuleEventBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleEventBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleEventBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleEventBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleColor_Seeded
// 0x42971200
class ParticleModuleColor_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42971200];                                // 0x0000(0x42971200) MISSED OFFSET

	static ParticleModuleColor_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleColor_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleColor_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleEventGenerator
// 0x42970800
class ParticleModuleEventGenerator
{
public:
	unsigned char                                      UnknownData00[0x42970800];                                // 0x0000(0x42970800) MISSED OFFSET

	static ParticleModuleEventGenerator ReadAsMe(const uintptr_t address)
	{
		ParticleModuleEventGenerator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleEventGenerator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleBeamSource
// 0x42972600
class ParticleModuleBeamSource
{
public:
	unsigned char                                      UnknownData00[0x42972600];                                // 0x0000(0x42972600) MISSED OFFSET

	static ParticleModuleBeamSource ReadAsMe(const uintptr_t address)
	{
		ParticleModuleBeamSource ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleBeamSource& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleEventSendToGame
// 0x40FE0C00
class ParticleModuleEventSendToGame
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ParticleModuleEventSendToGame ReadAsMe(const uintptr_t address)
	{
		ParticleModuleEventSendToGame ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleEventSendToGame& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleKillBase
// 0x42973E00
class ParticleModuleKillBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleKillBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleKillBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleKillBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleEventReceiverBase
// 0x42970800
class ParticleModuleEventReceiverBase
{
public:
	unsigned char                                      UnknownData00[0x42970800];                                // 0x0000(0x42970800) MISSED OFFSET

	static ParticleModuleEventReceiverBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleEventReceiverBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleEventReceiverBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleCollisionGPU
// 0x42971600
class ParticleModuleCollisionGPU
{
public:
	unsigned char                                      UnknownData00[0x42971600];                                // 0x0000(0x42971600) MISSED OFFSET

	static ParticleModuleCollisionGPU ReadAsMe(const uintptr_t address)
	{
		ParticleModuleCollisionGPU ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleCollisionGPU& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleKillHeight
// 0x42978000
class ParticleModuleKillHeight
{
public:
	unsigned char                                      UnknownData00[0x42978000];                                // 0x0000(0x42978000) MISSED OFFSET

	static ParticleModuleKillHeight ReadAsMe(const uintptr_t address)
	{
		ParticleModuleKillHeight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleKillHeight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLifetime
// 0x42977800
class ParticleModuleLifetime
{
public:
	unsigned char                                      UnknownData00[0x42977800];                                // 0x0000(0x42977800) MISSED OFFSET

	static ParticleModuleLifetime ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLifetime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLifetime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleEventReceiverSpawn
// 0x42970400
class ParticleModuleEventReceiverSpawn
{
public:
	unsigned char                                      UnknownData00[0x42970400];                                // 0x0000(0x42970400) MISSED OFFSET

	static ParticleModuleEventReceiverSpawn ReadAsMe(const uintptr_t address)
	{
		ParticleModuleEventReceiverSpawn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleEventReceiverSpawn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleEventReceiverKillParticles
// 0x42970400
class ParticleModuleEventReceiverKillParticles
{
public:
	unsigned char                                      UnknownData00[0x42970400];                                // 0x0000(0x42970400) MISSED OFFSET

	static ParticleModuleEventReceiverKillParticles ReadAsMe(const uintptr_t address)
	{
		ParticleModuleEventReceiverKillParticles ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleEventReceiverKillParticles& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLightBase
// 0x42973E00
class ParticleModuleLightBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleLightBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLightBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLightBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLifetime_Seeded
// 0x42977A00
class ParticleModuleLifetime_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42977A00];                                // 0x0000(0x42977A00) MISSED OFFSET

	static ParticleModuleLifetime_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLifetime_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLifetime_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLifetimeBase
// 0x42973E00
class ParticleModuleLifetimeBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleLifetimeBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLifetimeBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLifetimeBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationBase
// 0x42973E00
class ParticleModuleLocationBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleLocationBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleKillBox
// 0x42978000
class ParticleModuleKillBox
{
public:
	unsigned char                                      UnknownData00[0x42978000];                                // 0x0000(0x42978000) MISSED OFFSET

	static ParticleModuleKillBox ReadAsMe(const uintptr_t address)
	{
		ParticleModuleKillBox ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleKillBox& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocation
// 0x42976C00
class ParticleModuleLocation
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleLocation ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocation_Seeded
// 0x42976E00
class ParticleModuleLocation_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42976E00];                                // 0x0000(0x42976E00) MISSED OFFSET

	static ParticleModuleLocation_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocation_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocation_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLight
// 0x42977200
class ParticleModuleLight
{
public:
	unsigned char                                      UnknownData00[0x42977200];                                // 0x0000(0x42977200) MISSED OFFSET

	static ParticleModuleLight ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLight_Seeded
// 0x42977400
class ParticleModuleLight_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42977400];                                // 0x0000(0x42977400) MISSED OFFSET

	static ParticleModuleLight_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLight_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLight_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationEmitter
// 0x42976C00
class ParticleModuleLocationEmitter
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleLocationEmitter ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationEmitter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationEmitter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationDirect
// 0x42976C00
class ParticleModuleLocationDirect
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleLocationDirect ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationDirect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationDirect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationEmitterDirect
// 0x42976C00
class ParticleModuleLocationEmitterDirect
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleLocationEmitterDirect ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationEmitterDirect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationEmitterDirect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationPrimitiveCylinder
// 0x42976000
class ParticleModuleLocationPrimitiveCylinder
{
public:
	unsigned char                                      UnknownData00[0x42976000];                                // 0x0000(0x42976000) MISSED OFFSET

	static ParticleModuleLocationPrimitiveCylinder ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationPrimitiveCylinder ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationPrimitiveCylinder& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationPrimitiveSphere
// 0x42976000
class ParticleModuleLocationPrimitiveSphere
{
public:
	unsigned char                                      UnknownData00[0x42976000];                                // 0x0000(0x42976000) MISSED OFFSET

	static ParticleModuleLocationPrimitiveSphere ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationPrimitiveSphere ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationPrimitiveSphere& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationPrimitiveCylinder_Seeded
// 0x42975E00
class ParticleModuleLocationPrimitiveCylinder_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42975E00];                                // 0x0000(0x42975E00) MISSED OFFSET

	static ParticleModuleLocationPrimitiveCylinder_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationPrimitiveCylinder_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationPrimitiveCylinder_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationPrimitiveSphere_Seeded
// 0x42975A00
class ParticleModuleLocationPrimitiveSphere_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42975A00];                                // 0x0000(0x42975A00) MISSED OFFSET

	static ParticleModuleLocationPrimitiveSphere_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationPrimitiveSphere_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationPrimitiveSphere_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationPrimitiveTriangle
// 0x42976C00
class ParticleModuleLocationPrimitiveTriangle
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleLocationPrimitiveTriangle ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationPrimitiveTriangle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationPrimitiveTriangle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationSkelVertSurface
// 0x42976C00
class ParticleModuleLocationSkelVertSurface
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleLocationSkelVertSurface ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationSkelVertSurface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationSkelVertSurface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationBoneSocket
// 0x42976C00
class ParticleModuleLocationBoneSocket
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleLocationBoneSocket ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationBoneSocket ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationBoneSocket& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationWorldOffset_Seeded
// 0x42975200
class ParticleModuleLocationWorldOffset_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42975200];                                // 0x0000(0x42975200) MISSED OFFSET

	static ParticleModuleLocationWorldOffset_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationWorldOffset_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationWorldOffset_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleMeshMaterial
// 0x42974E00
class ParticleModuleMeshMaterial
{
public:
	unsigned char                                      UnknownData00[0x42974E00];                                // 0x0000(0x42974E00) MISSED OFFSET

	static ParticleModuleMeshMaterial ReadAsMe(const uintptr_t address)
	{
		ParticleModuleMeshMaterial ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleMeshMaterial& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationPrimitiveBase
// 0x42976C00
class ParticleModuleLocationPrimitiveBase
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleLocationPrimitiveBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationPrimitiveBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationPrimitiveBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleMeshRotation
// 0x42974800
class ParticleModuleMeshRotation
{
public:
	unsigned char                                      UnknownData00[0x42974800];                                // 0x0000(0x42974800) MISSED OFFSET

	static ParticleModuleMeshRotation ReadAsMe(const uintptr_t address)
	{
		ParticleModuleMeshRotation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleMeshRotation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleMaterialBase
// 0x42973E00
class ParticleModuleMaterialBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleMaterialBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleMaterialBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleMaterialBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleLocationWorldOffset
// 0x42976E00
class ParticleModuleLocationWorldOffset
{
public:
	unsigned char                                      UnknownData00[0x42976E00];                                // 0x0000(0x42976E00) MISSED OFFSET

	static ParticleModuleLocationWorldOffset ReadAsMe(const uintptr_t address)
	{
		ParticleModuleLocationWorldOffset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleLocationWorldOffset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRotationBase
// 0x42973E00
class ParticleModuleRotationBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleRotationBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRotationBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRotationBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleMeshRotation_Seeded
// 0x42974A00
class ParticleModuleMeshRotation_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42974A00];                                // 0x0000(0x42974A00) MISSED OFFSET

	static ParticleModuleMeshRotation_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleMeshRotation_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleMeshRotation_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRotationRateBase
// 0x42973E00
class ParticleModuleRotationRateBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleRotationRateBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRotationRateBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRotationRateBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleMeshRotationRate_Seeded
// 0x42974400
class ParticleModuleMeshRotationRate_Seeded
{
public:
	unsigned char                                      UnknownData00[0x42974400];                                // 0x0000(0x42974400) MISSED OFFSET

	static ParticleModuleMeshRotationRate_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleMeshRotationRate_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleMeshRotationRate_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleMeshRotationRateOverLife
// 0x4297C600
class ParticleModuleMeshRotationRateOverLife
{
public:
	unsigned char                                      UnknownData00[0x4297C600];                                // 0x0000(0x4297C600) MISSED OFFSET

	static ParticleModuleMeshRotationRateOverLife ReadAsMe(const uintptr_t address)
	{
		ParticleModuleMeshRotationRateOverLife ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleMeshRotationRateOverLife& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleOrbitBase
// 0x42973E00
class ParticleModuleOrbitBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleOrbitBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleOrbitBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleOrbitBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleOrbit
// 0x4297BC00
class ParticleModuleOrbit
{
public:
	unsigned char                                      UnknownData00[0x4297BC00];                                // 0x0000(0x4297BC00) MISSED OFFSET

	static ParticleModuleOrbit ReadAsMe(const uintptr_t address)
	{
		ParticleModuleOrbit ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleOrbit& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleMeshRotationRateMultiplyLife
// 0x4297C600
class ParticleModuleMeshRotationRateMultiplyLife
{
public:
	unsigned char                                      UnknownData00[0x4297C600];                                // 0x0000(0x4297C600) MISSED OFFSET

	static ParticleModuleMeshRotationRateMultiplyLife ReadAsMe(const uintptr_t address)
	{
		ParticleModuleMeshRotationRateMultiplyLife ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleMeshRotationRateMultiplyLife& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleOrientationAxisLock
// 0x4297B800
class ParticleModuleOrientationAxisLock
{
public:
	unsigned char                                      UnknownData00[0x4297B800];                                // 0x0000(0x4297B800) MISSED OFFSET

	static ParticleModuleOrientationAxisLock ReadAsMe(const uintptr_t address)
	{
		ParticleModuleOrientationAxisLock ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleOrientationAxisLock& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleParameterBase
// 0x42973E00
class ParticleModuleParameterBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleParameterBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleParameterBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleParameterBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleParameterDynamic_Seeded
// 0x4297B400
class ParticleModuleParameterDynamic_Seeded
{
public:
	unsigned char                                      UnknownData00[0x4297B400];                                // 0x0000(0x4297B400) MISSED OFFSET

	static ParticleModuleParameterDynamic_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleParameterDynamic_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleParameterDynamic_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRequired
// 0x42973E00
class ParticleModuleRequired
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleRequired ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRequired ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRequired& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleOrientationBase
// 0x42973E00
class ParticleModuleOrientationBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleOrientationBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleOrientationBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleOrientationBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleParameterDynamic
// 0x4297B600
class ParticleModuleParameterDynamic
{
public:
	unsigned char                                      UnknownData00[0x4297B600];                                // 0x0000(0x4297B600) MISSED OFFSET

	static ParticleModuleParameterDynamic ReadAsMe(const uintptr_t address)
	{
		ParticleModuleParameterDynamic ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleParameterDynamic& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModulePivotOffset
// 0x42976C00
class ParticleModulePivotOffset
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModulePivotOffset ReadAsMe(const uintptr_t address)
	{
		ParticleModulePivotOffset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModulePivotOffset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRotationOverLifetime
// 0x42974800
class ParticleModuleRotationOverLifetime
{
public:
	unsigned char                                      UnknownData00[0x42974800];                                // 0x0000(0x42974800) MISSED OFFSET

	static ParticleModuleRotationOverLifetime ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRotationOverLifetime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRotationOverLifetime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRotationRate
// 0x4297C600
class ParticleModuleRotationRate
{
public:
	unsigned char                                      UnknownData00[0x4297C600];                                // 0x0000(0x4297C600) MISSED OFFSET

	static ParticleModuleRotationRate ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRotationRate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRotationRate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRotationRate_Seeded
// 0x4297A600
class ParticleModuleRotationRate_Seeded
{
public:
	unsigned char                                      UnknownData00[0x4297A600];                                // 0x0000(0x4297A600) MISSED OFFSET

	static ParticleModuleRotationRate_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRotationRate_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRotationRate_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRotationRateMultiplyLife
// 0x4297C600
class ParticleModuleRotationRateMultiplyLife
{
public:
	unsigned char                                      UnknownData00[0x4297C600];                                // 0x0000(0x4297C600) MISSED OFFSET

	static ParticleModuleRotationRateMultiplyLife ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRotationRateMultiplyLife ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRotationRateMultiplyLife& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSizeBase
// 0x42973E00
class ParticleModuleSizeBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleSizeBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSizeBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSizeBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRotation_Seeded
// 0x4297AC00
class ParticleModuleRotation_Seeded
{
public:
	unsigned char                                      UnknownData00[0x4297AC00];                                // 0x0000(0x4297AC00) MISSED OFFSET

	static ParticleModuleRotation_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRotation_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRotation_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleRotation
// 0x42974800
class ParticleModuleRotation
{
public:
	unsigned char                                      UnknownData00[0x42974800];                                // 0x0000(0x42974800) MISSED OFFSET

	static ParticleModuleRotation ReadAsMe(const uintptr_t address)
	{
		ParticleModuleRotation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleRotation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSizeMultiplyLife
// 0x42979E00
class ParticleModuleSizeMultiplyLife
{
public:
	unsigned char                                      UnknownData00[0x42979E00];                                // 0x0000(0x42979E00) MISSED OFFSET

	static ParticleModuleSizeMultiplyLife ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSizeMultiplyLife ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSizeMultiplyLife& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSize
// 0x42979E00
class ParticleModuleSize
{
public:
	unsigned char                                      UnknownData00[0x42979E00];                                // 0x0000(0x42979E00) MISSED OFFSET

	static ParticleModuleSize ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSize ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSize& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSize_Seeded
// 0x4297A000
class ParticleModuleSize_Seeded
{
public:
	unsigned char                                      UnknownData00[0x4297A000];                                // 0x0000(0x4297A000) MISSED OFFSET

	static ParticleModuleSize_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSize_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSize_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSourceMovement
// 0x42976C00
class ParticleModuleSourceMovement
{
public:
	unsigned char                                      UnknownData00[0x42976C00];                                // 0x0000(0x42976C00) MISSED OFFSET

	static ParticleModuleSourceMovement ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSourceMovement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSourceMovement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSizeScaleBySpeed
// 0x42979E00
class ParticleModuleSizeScaleBySpeed
{
public:
	unsigned char                                      UnknownData00[0x42979E00];                                // 0x0000(0x42979E00) MISSED OFFSET

	static ParticleModuleSizeScaleBySpeed ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSizeScaleBySpeed ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSizeScaleBySpeed& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSpawn
// 0x42979000
class ParticleModuleSpawn
{
public:
	unsigned char                                      UnknownData00[0x42979000];                                // 0x0000(0x42979000) MISSED OFFSET

	static ParticleModuleSpawn ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSpawn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSpawn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSpawnBase
// 0x42973E00
class ParticleModuleSpawnBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleSpawnBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSpawnBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSpawnBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSizeScale
// 0x42979E00
class ParticleModuleSizeScale
{
public:
	unsigned char                                      UnknownData00[0x42979E00];                                // 0x0000(0x42979E00) MISSED OFFSET

	static ParticleModuleSizeScale ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSizeScale ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSizeScale& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSubUVBase
// 0x42973E00
class ParticleModuleSubUVBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleSubUVBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSubUVBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSubUVBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSubUV
// 0x42978A00
class ParticleModuleSubUV
{
public:
	unsigned char                                      UnknownData00[0x42978A00];                                // 0x0000(0x42978A00) MISSED OFFSET

	static ParticleModuleSubUV ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSubUV ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSubUV& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleTrailSource
// 0x42978600
class ParticleModuleTrailSource
{
public:
	unsigned char                                      UnknownData00[0x42978600];                                // 0x0000(0x42978600) MISSED OFFSET

	static ParticleModuleTrailSource ReadAsMe(const uintptr_t address)
	{
		ParticleModuleTrailSource ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleTrailSource& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSubUVMovie
// 0x42978C00
class ParticleModuleSubUVMovie
{
public:
	unsigned char                                      UnknownData00[0x42978C00];                                // 0x0000(0x42978C00) MISSED OFFSET

	static ParticleModuleSubUVMovie ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSubUVMovie ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSubUVMovie& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleTrailBase
// 0x42973E00
class ParticleModuleTrailBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleTrailBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleTrailBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleTrailBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleTypeDataAnimTrail
// 0x4297FA00
class ParticleModuleTypeDataAnimTrail
{
public:
	unsigned char                                      UnknownData00[0x4297FA00];                                // 0x0000(0x4297FA00) MISSED OFFSET

	static ParticleModuleTypeDataAnimTrail ReadAsMe(const uintptr_t address)
	{
		ParticleModuleTypeDataAnimTrail ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleTypeDataAnimTrail& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleTypeDataBase
// 0x42973E00
class ParticleModuleTypeDataBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleTypeDataBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleTypeDataBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleTypeDataBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleTypeDataBeam2
// 0x4297FA00
class ParticleModuleTypeDataBeam2
{
public:
	unsigned char                                      UnknownData00[0x4297FA00];                                // 0x0000(0x4297FA00) MISSED OFFSET

	static ParticleModuleTypeDataBeam2 ReadAsMe(const uintptr_t address)
	{
		ParticleModuleTypeDataBeam2 ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleTypeDataBeam2& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleSpawnPerUnit
// 0x42979000
class ParticleModuleSpawnPerUnit
{
public:
	unsigned char                                      UnknownData00[0x42979000];                                // 0x0000(0x42979000) MISSED OFFSET

	static ParticleModuleSpawnPerUnit ReadAsMe(const uintptr_t address)
	{
		ParticleModuleSpawnPerUnit ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleSpawnPerUnit& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleTypeDataGpu
// 0x4297FA00
class ParticleModuleTypeDataGpu
{
public:
	unsigned char                                      UnknownData00[0x4297FA00];                                // 0x0000(0x4297FA00) MISSED OFFSET

	static ParticleModuleTypeDataGpu ReadAsMe(const uintptr_t address)
	{
		ParticleModuleTypeDataGpu ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleTypeDataGpu& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVectorFieldBase
// 0x42973E00
class ParticleModuleVectorFieldBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleVectorFieldBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVectorFieldBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVectorFieldBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleTypeDataMesh
// 0x4297FA00
class ParticleModuleTypeDataMesh
{
public:
	unsigned char                                      UnknownData00[0x4297FA00];                                // 0x0000(0x4297FA00) MISSED OFFSET

	static ParticleModuleTypeDataMesh ReadAsMe(const uintptr_t address)
	{
		ParticleModuleTypeDataMesh ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleTypeDataMesh& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleTypeDataRibbon
// 0x4297FA00
class ParticleModuleTypeDataRibbon
{
public:
	unsigned char                                      UnknownData00[0x4297FA00];                                // 0x0000(0x4297FA00) MISSED OFFSET

	static ParticleModuleTypeDataRibbon ReadAsMe(const uintptr_t address)
	{
		ParticleModuleTypeDataRibbon ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleTypeDataRibbon& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVectorFieldLocal
// 0x4297F000
class ParticleModuleVectorFieldLocal
{
public:
	unsigned char                                      UnknownData00[0x4297F000];                                // 0x0000(0x4297F000) MISSED OFFSET

	static ParticleModuleVectorFieldLocal ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVectorFieldLocal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVectorFieldLocal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVectorFieldGlobal
// 0x4297F000
class ParticleModuleVectorFieldGlobal
{
public:
	unsigned char                                      UnknownData00[0x4297F000];                                // 0x0000(0x4297F000) MISSED OFFSET

	static ParticleModuleVectorFieldGlobal ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVectorFieldGlobal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVectorFieldGlobal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVectorFieldRotation
// 0x4297F000
class ParticleModuleVectorFieldRotation
{
public:
	unsigned char                                      UnknownData00[0x4297F000];                                // 0x0000(0x4297F000) MISSED OFFSET

	static ParticleModuleVectorFieldRotation ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVectorFieldRotation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVectorFieldRotation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVectorFieldRotationRate
// 0x4297F000
class ParticleModuleVectorFieldRotationRate
{
public:
	unsigned char                                      UnknownData00[0x4297F000];                                // 0x0000(0x4297F000) MISSED OFFSET

	static ParticleModuleVectorFieldRotationRate ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVectorFieldRotationRate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVectorFieldRotationRate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVectorFieldScaleOverLife
// 0x4297F000
class ParticleModuleVectorFieldScaleOverLife
{
public:
	unsigned char                                      UnknownData00[0x4297F000];                                // 0x0000(0x4297F000) MISSED OFFSET

	static ParticleModuleVectorFieldScaleOverLife ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVectorFieldScaleOverLife ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVectorFieldScaleOverLife& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVelocityBase
// 0x42973E00
class ParticleModuleVelocityBase
{
public:
	unsigned char                                      UnknownData00[0x42973E00];                                // 0x0000(0x42973E00) MISSED OFFSET

	static ParticleModuleVelocityBase ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVelocityBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVelocityBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVectorFieldScale
// 0x4297F000
class ParticleModuleVectorFieldScale
{
public:
	unsigned char                                      UnknownData00[0x4297F000];                                // 0x0000(0x4297F000) MISSED OFFSET

	static ParticleModuleVectorFieldScale ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVectorFieldScale ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVectorFieldScale& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVelocity
// 0x4297E000
class ParticleModuleVelocity
{
public:
	unsigned char                                      UnknownData00[0x4297E000];                                // 0x0000(0x4297E000) MISSED OFFSET

	static ParticleModuleVelocity ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVelocity ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVelocity& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVelocityInheritParent
// 0x4297E000
class ParticleModuleVelocityInheritParent
{
public:
	unsigned char                                      UnknownData00[0x4297E000];                                // 0x0000(0x4297E000) MISSED OFFSET

	static ParticleModuleVelocityInheritParent ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVelocityInheritParent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVelocityInheritParent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleMeshRotationRate
// 0x4297C600
class ParticleModuleMeshRotationRate
{
public:
	unsigned char                                      UnknownData00[0x4297C600];                                // 0x0000(0x4297C600) MISSED OFFSET

	static ParticleModuleMeshRotationRate ReadAsMe(const uintptr_t address)
	{
		ParticleModuleMeshRotationRate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleMeshRotationRate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleSpriteEmitter
// 0x4296C800
class ParticleSpriteEmitter
{
public:
	unsigned char                                      UnknownData00[0x4296C800];                                // 0x0000(0x4296C800) MISSED OFFSET

	static ParticleSpriteEmitter ReadAsMe(const uintptr_t address)
	{
		ParticleSpriteEmitter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleSpriteEmitter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleSystemComponent
// 0x427D0A00
class ParticleSystemComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static ParticleSystemComponent ReadAsMe(const uintptr_t address)
	{
		ParticleSystemComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleSystemComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVelocityCone
// 0x4297E000
class ParticleModuleVelocityCone
{
public:
	unsigned char                                      UnknownData00[0x4297E000];                                // 0x0000(0x4297E000) MISSED OFFSET

	static ParticleModuleVelocityCone ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVelocityCone ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVelocityCone& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVelocity_Seeded
// 0x4297E200
class ParticleModuleVelocity_Seeded
{
public:
	unsigned char                                      UnknownData00[0x4297E200];                                // 0x0000(0x4297E200) MISSED OFFSET

	static ParticleModuleVelocity_Seeded ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVelocity_Seeded ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVelocity_Seeded& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PawnNoiseEmitterComponent
// 0x40FEF600
class PawnNoiseEmitterComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static PawnNoiseEmitterComponent ReadAsMe(const uintptr_t address)
	{
		PawnNoiseEmitterComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PawnNoiseEmitterComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleSystemReplay
// 0x40FE0C00
class ParticleSystemReplay
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ParticleSystemReplay ReadAsMe(const uintptr_t address)
	{
		ParticleSystemReplay ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleSystemReplay& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleSystem
// 0x40FE0C00
class ParticleSystem
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ParticleSystem ReadAsMe(const uintptr_t address)
	{
		ParticleSystem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleSystem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicalMaterial
// 0x40FE0C00
class PhysicalMaterial
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PhysicalMaterial ReadAsMe(const uintptr_t address)
	{
		PhysicalMaterial ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicalMaterial& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicalMaterialPropertyBase
// 0x40FE0C00
class PhysicalMaterialPropertyBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PhysicalMaterialPropertyBase ReadAsMe(const uintptr_t address)
	{
		PhysicalMaterialPropertyBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicalMaterialPropertyBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsAsset
// 0x40FE0C00
class PhysicsAsset
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PhysicsAsset ReadAsMe(const uintptr_t address)
	{
		PhysicsAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkeletalBodySetup
// 0x42905C00
class SkeletalBodySetup
{
public:
	unsigned char                                      UnknownData00[0x42905C00];                                // 0x0000(0x42905C00) MISSED OFFSET

	static SkeletalBodySetup ReadAsMe(const uintptr_t address)
	{
		SkeletalBodySetup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkeletalBodySetup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ParticleModuleVelocityOverLifetime
// 0x4297E000
class ParticleModuleVelocityOverLifetime
{
public:
	unsigned char                                      UnknownData00[0x4297E000];                                // 0x0000(0x4297E000) MISSED OFFSET

	static ParticleModuleVelocityOverLifetime ReadAsMe(const uintptr_t address)
	{
		ParticleModuleVelocityOverLifetime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ParticleModuleVelocityOverLifetime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PathFollowingAgentInterface
// 0x427D4600
class PathFollowingAgentInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static PathFollowingAgentInterface ReadAsMe(const uintptr_t address)
	{
		PathFollowingAgentInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PathFollowingAgentInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicalAnimationComponent
// 0x40FEF600
class PhysicalAnimationComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static PhysicalAnimationComponent ReadAsMe(const uintptr_t address)
	{
		PhysicalAnimationComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicalAnimationComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsConstraintComponent
// 0x40FEF800
class PhysicsConstraintComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static PhysicsConstraintComponent ReadAsMe(const uintptr_t address)
	{
		PhysicsConstraintComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsConstraintComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsHandleComponent
// 0x40FEF600
class PhysicsHandleComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static PhysicsHandleComponent ReadAsMe(const uintptr_t address)
	{
		PhysicsHandleComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsHandleComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RigidBodyBase
// 0x40FE8000
class RigidBodyBase
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static RigidBodyBase ReadAsMe(const uintptr_t address)
	{
		RigidBodyBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RigidBodyBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsSettings
// 0x427D8E00
class PhysicsSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static PhysicsSettings ReadAsMe(const uintptr_t address)
	{
		PhysicsSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsConstraintTemplate
// 0x40FE0C00
class PhysicsConstraintTemplate
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PhysicsConstraintTemplate ReadAsMe(const uintptr_t address)
	{
		PhysicsConstraintTemplate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsConstraintTemplate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsSpringComponent
// 0x40FEF800
class PhysicsSpringComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static PhysicsSpringComponent ReadAsMe(const uintptr_t address)
	{
		PhysicsSpringComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsSpringComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsThruster
// 0x42993800
class PhysicsThruster
{
public:
	unsigned char                                      UnknownData00[0x42993800];                                // 0x0000(0x42993800) MISSED OFFSET

	static PhysicsThruster ReadAsMe(const uintptr_t address)
	{
		PhysicsThruster ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsThruster& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsThrusterComponent
// 0x40FEF800
class PhysicsThrusterComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static PhysicsThrusterComponent ReadAsMe(const uintptr_t address)
	{
		PhysicsThrusterComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsThrusterComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsConstraintActor
// 0x42993800
class PhysicsConstraintActor
{
public:
	unsigned char                                      UnknownData00[0x42993800];                                // 0x0000(0x42993800) MISSED OFFSET

	static PhysicsConstraintActor ReadAsMe(const uintptr_t address)
	{
		PhysicsConstraintActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsConstraintActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PhysicsCollisionHandler
// 0x40FE0C00
class PhysicsCollisionHandler
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PhysicsCollisionHandler ReadAsMe(const uintptr_t address)
	{
		PhysicsCollisionHandler ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PhysicsCollisionHandler& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlanarReflectionComponent
// 0x42992400
class PlanarReflectionComponent
{
public:
	unsigned char                                      UnknownData00[0x42992400];                                // 0x0000(0x42992400) MISSED OFFSET

	static PlanarReflectionComponent ReadAsMe(const uintptr_t address)
	{
		PlanarReflectionComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlanarReflectionComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlanarReflection
// 0x4285B800
class PlanarReflection
{
public:
	unsigned char                                      UnknownData00[0x4285B800];                                // 0x0000(0x4285B800) MISSED OFFSET

	static PlanarReflection ReadAsMe(const uintptr_t address)
	{
		PlanarReflection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlanarReflection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlaneReflectionCaptureComponent
// 0x42904C00
class PlaneReflectionCaptureComponent
{
public:
	unsigned char                                      UnknownData00[0x42904C00];                                // 0x0000(0x42904C00) MISSED OFFSET

	static PlaneReflectionCaptureComponent ReadAsMe(const uintptr_t address)
	{
		PlaneReflectionCaptureComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlaneReflectionCaptureComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SceneCaptureComponent
// 0x40FEF800
class SceneCaptureComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static SceneCaptureComponent ReadAsMe(const uintptr_t address)
	{
		SceneCaptureComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SceneCaptureComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlaneReflectionCapture
// 0x42905000
class PlaneReflectionCapture
{
public:
	unsigned char                                      UnknownData00[0x42905000];                                // 0x0000(0x42905000) MISSED OFFSET

	static PlaneReflectionCapture ReadAsMe(const uintptr_t address)
	{
		PlaneReflectionCapture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlaneReflectionCapture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlayerCameraManager
// 0x40FE8000
class PlayerCameraManager
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static PlayerCameraManager ReadAsMe(const uintptr_t address)
	{
		PlayerCameraManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlayerCameraManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlayerInput
// 0x40FE0C00
class PlayerInput
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PlayerInput ReadAsMe(const uintptr_t address)
	{
		PlayerInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlayerInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlayerStartPIE
// 0x42863E00
class PlayerStartPIE
{
public:
	unsigned char                                      UnknownData00[0x42863E00];                                // 0x0000(0x42863E00) MISSED OFFSET

	static PlayerStartPIE ReadAsMe(const uintptr_t address)
	{
		PlayerStartPIE ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlayerStartPIE& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PluginCommandlet
// 0x4290A600
class PluginCommandlet
{
public:
	unsigned char                                      UnknownData00[0x4290A600];                                // 0x0000(0x4290A600) MISSED OFFSET

	static PluginCommandlet ReadAsMe(const uintptr_t address)
	{
		PluginCommandlet ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PluginCommandlet& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlatformInterfaceWebResponse
// 0x40FE0C00
class PlatformInterfaceWebResponse
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PlatformInterfaceWebResponse ReadAsMe(const uintptr_t address)
	{
		PlatformInterfaceWebResponse ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlatformInterfaceWebResponse& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PointLightComponent
// 0x42947800
class PointLightComponent
{
public:
	unsigned char                                      UnknownData00[0x42947800];                                // 0x0000(0x42947800) MISSED OFFSET

	static PointLightComponent ReadAsMe(const uintptr_t address)
	{
		PointLightComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PointLightComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PoseableMeshComponent
// 0x427D0E00
class PoseableMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0E00];                                // 0x0000(0x427D0E00) MISSED OFFSET

	static PoseableMeshComponent ReadAsMe(const uintptr_t address)
	{
		PoseableMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PoseableMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PoseAsset
// 0x428BE000
class PoseAsset
{
public:
	unsigned char                                      UnknownData00[0x428BE000];                                // 0x0000(0x428BE000) MISSED OFFSET

	static PoseAsset ReadAsMe(const uintptr_t address)
	{
		PoseAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PoseAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PlatformEventsComponent
// 0x40FEF600
class PlatformEventsComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static PlatformEventsComponent ReadAsMe(const uintptr_t address)
	{
		PlatformEventsComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PlatformEventsComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Polys
// 0x40FE0C00
class Polys
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Polys ReadAsMe(const uintptr_t address)
	{
		Polys ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Polys& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PointLight
// 0x4290CC00
class PointLight
{
public:
	unsigned char                                      UnknownData00[0x4290CC00];                                // 0x0000(0x4290CC00) MISSED OFFSET

	static PointLight ReadAsMe(const uintptr_t address)
	{
		PointLight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PointLight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PostProcessComponent
// 0x40FEF800
class PostProcessComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static PostProcessComponent ReadAsMe(const uintptr_t address)
	{
		PostProcessComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PostProcessComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PostProcessVolume
// 0x40FEC600
class PostProcessVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static PostProcessVolume ReadAsMe(const uintptr_t address)
	{
		PostProcessVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PostProcessVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PoseWatch
// 0x40FE0C00
class PoseWatch
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static PoseWatch ReadAsMe(const uintptr_t address)
	{
		PoseWatch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PoseWatch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PreviewCollectionInterface
// 0x427D4600
class PreviewCollectionInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static PreviewCollectionInterface ReadAsMe(const uintptr_t address)
	{
		PreviewCollectionInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PreviewCollectionInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PrecomputedVisibilityOverrideVolume
// 0x40FEC600
class PrecomputedVisibilityOverrideVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static PrecomputedVisibilityOverrideVolume ReadAsMe(const uintptr_t address)
	{
		PrecomputedVisibilityOverrideVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PrecomputedVisibilityOverrideVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PreviewMeshCollection
// 0x427EC000
class PreviewMeshCollection
{
public:
	unsigned char                                      UnknownData00[0x427EC000];                                // 0x0000(0x427EC000) MISSED OFFSET

	static PreviewMeshCollection ReadAsMe(const uintptr_t address)
	{
		PreviewMeshCollection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PreviewMeshCollection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PrecomputedVisibilityVolume
// 0x40FEC600
class PrecomputedVisibilityVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static PrecomputedVisibilityVolume ReadAsMe(const uintptr_t address)
	{
		PrecomputedVisibilityVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PrecomputedVisibilityVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ProxyInstancedStaticMeshComponent
// 0x42828C00
class ProxyInstancedStaticMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x42828C00];                                // 0x0000(0x42828C00) MISSED OFFSET

	static ProxyInstancedStaticMeshComponent ReadAsMe(const uintptr_t address)
	{
		ProxyInstancedStaticMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ProxyInstancedStaticMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.PrimaryAssetLabel
// 0x4290F600
class PrimaryAssetLabel
{
public:
	unsigned char                                      UnknownData00[0x4290F600];                                // 0x0000(0x4290F600) MISSED OFFSET

	static PrimaryAssetLabel ReadAsMe(const uintptr_t address)
	{
		PrimaryAssetLabel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, PrimaryAssetLabel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ProxyHierarchicalInstancedStaticMeshComponent
// 0x42886400
class ProxyHierarchicalInstancedStaticMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x42886400];                                // 0x0000(0x42886400) MISSED OFFSET

	static ProxyHierarchicalInstancedStaticMeshComponent ReadAsMe(const uintptr_t address)
	{
		ProxyHierarchicalInstancedStaticMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ProxyHierarchicalInstancedStaticMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RectLight
// 0x4290CC00
class RectLight
{
public:
	unsigned char                                      UnknownData00[0x4290CC00];                                // 0x0000(0x4290CC00) MISSED OFFSET

	static RectLight ReadAsMe(const uintptr_t address)
	{
		RectLight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RectLight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RectLightComponent
// 0x42947800
class RectLightComponent
{
public:
	unsigned char                                      UnknownData00[0x42947800];                                // 0x0000(0x42947800) MISSED OFFSET

	static RectLightComponent ReadAsMe(const uintptr_t address)
	{
		RectLightComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RectLightComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ProjectileMovementComponent
// 0x42805A00
class ProjectileMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x42805A00];                                // 0x0000(0x42805A00) MISSED OFFSET

	static ProjectileMovementComponent ReadAsMe(const uintptr_t address)
	{
		ProjectileMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ProjectileMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ProxyLODMeshSimplificationSettings
// 0x427D8E00
class ProxyLODMeshSimplificationSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static ProxyLODMeshSimplificationSettings ReadAsMe(const uintptr_t address)
	{
		ProxyLODMeshSimplificationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ProxyLODMeshSimplificationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RadialForceActor
// 0x42993800
class RadialForceActor
{
public:
	unsigned char                                      UnknownData00[0x42993800];                                // 0x0000(0x42993800) MISSED OFFSET

	static RadialForceActor ReadAsMe(const uintptr_t address)
	{
		RadialForceActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RadialForceActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RadialForceComponent
// 0x40FEF800
class RadialForceComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static RadialForceComponent ReadAsMe(const uintptr_t address)
	{
		RadialForceComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RadialForceComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ReporterBase
// 0x40FE0C00
class ReporterBase
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ReporterBase ReadAsMe(const uintptr_t address)
	{
		ReporterBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ReporterBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ReporterGraph
// 0x42996800
class ReporterGraph
{
public:
	unsigned char                                      UnknownData00[0x42996800];                                // 0x0000(0x42996800) MISSED OFFSET

	static ReporterGraph ReadAsMe(const uintptr_t address)
	{
		ReporterGraph ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ReporterGraph& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ReverbEffect
// 0x40FE0C00
class ReverbEffect
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ReverbEffect ReadAsMe(const uintptr_t address)
	{
		ReverbEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ReverbEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Rig
// 0x40FE0C00
class Rig
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Rig ReadAsMe(const uintptr_t address)
	{
		Rig ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Rig& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RendererSettings
// 0x427D8E00
class RendererSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static RendererSettings ReadAsMe(const uintptr_t address)
	{
		RendererSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RendererSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RotatingMovementComponent
// 0x42805A00
class RotatingMovementComponent
{
public:
	unsigned char                                      UnknownData00[0x42805A00];                                // 0x0000(0x42805A00) MISSED OFFSET

	static RotatingMovementComponent ReadAsMe(const uintptr_t address)
	{
		RotatingMovementComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RotatingMovementComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RVOAvoidanceInterface
// 0x427D4600
class RVOAvoidanceInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static RVOAvoidanceInterface ReadAsMe(const uintptr_t address)
	{
		RVOAvoidanceInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RVOAvoidanceInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.RendererOverrideSettings
// 0x427D8E00
class RendererOverrideSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static RendererOverrideSettings ReadAsMe(const uintptr_t address)
	{
		RendererOverrideSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, RendererOverrideSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SceneCaptureComponent2D
// 0x42992400
class SceneCaptureComponent2D
{
public:
	unsigned char                                      UnknownData00[0x42992400];                                // 0x0000(0x42992400) MISSED OFFSET

	static SceneCaptureComponent2D ReadAsMe(const uintptr_t address)
	{
		SceneCaptureComponent2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SceneCaptureComponent2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SCS_Node
// 0x40FE0C00
class SCS_Node
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SCS_Node ReadAsMe(const uintptr_t address)
	{
		SCS_Node ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SCS_Node& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Scene
// 0x40FE0C00
class Scene
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Scene ReadAsMe(const uintptr_t address)
	{
		Scene ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Scene& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SaveGame
// 0x40FE0C00
class SaveGame
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SaveGame ReadAsMe(const uintptr_t address)
	{
		SaveGame ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SaveGame& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SceneCaptureComponentCube
// 0x42992400
class SceneCaptureComponentCube
{
public:
	unsigned char                                      UnknownData00[0x42992400];                                // 0x0000(0x42992400) MISSED OFFSET

	static SceneCaptureComponentCube ReadAsMe(const uintptr_t address)
	{
		SceneCaptureComponentCube ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SceneCaptureComponentCube& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ShadowMapTexture2D
// 0x4290FE00
class ShadowMapTexture2D
{
public:
	unsigned char                                      UnknownData00[0x4290FE00];                                // 0x0000(0x4290FE00) MISSED OFFSET

	static ShadowMapTexture2D ReadAsMe(const uintptr_t address)
	{
		ShadowMapTexture2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ShadowMapTexture2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SimpleConstructionScript
// 0x40FE0C00
class SimpleConstructionScript
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SimpleConstructionScript ReadAsMe(const uintptr_t address)
	{
		SimpleConstructionScript ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SimpleConstructionScript& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkeletalMeshActor
// 0x40FE8000
class SkeletalMeshActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static SkeletalMeshActor ReadAsMe(const uintptr_t address)
	{
		SkeletalMeshActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkeletalMeshActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SceneCaptureCube
// 0x4285B800
class SceneCaptureCube
{
public:
	unsigned char                                      UnknownData00[0x4285B800];                                // 0x0000(0x4285B800) MISSED OFFSET

	static SceneCaptureCube ReadAsMe(const uintptr_t address)
	{
		SceneCaptureCube ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SceneCaptureCube& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkyLightComponent
// 0x42924200
class SkyLightComponent
{
public:
	unsigned char                                      UnknownData00[0x42924200];                                // 0x0000(0x42924200) MISSED OFFSET

	static SkyLightComponent ReadAsMe(const uintptr_t address)
	{
		SkyLightComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkyLightComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkeletalMeshLODSettings
// 0x427EC000
class SkeletalMeshLODSettings
{
public:
	unsigned char                                      UnknownData00[0x427EC000];                                // 0x0000(0x427EC000) MISSED OFFSET

	static SkeletalMeshLODSettings ReadAsMe(const uintptr_t address)
	{
		SkeletalMeshLODSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkeletalMeshLODSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkeletalMeshSimplificationSettings
// 0x427D8E00
class SkeletalMeshSimplificationSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static SkeletalMeshSimplificationSettings ReadAsMe(const uintptr_t address)
	{
		SkeletalMeshSimplificationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkeletalMeshSimplificationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ServerStatReplicator
// 0x427D5600
class ServerStatReplicator
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static ServerStatReplicator ReadAsMe(const uintptr_t address)
	{
		ServerStatReplicator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ServerStatReplicator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Selection
// 0x40FE0C00
class Selection
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static Selection ReadAsMe(const uintptr_t address)
	{
		Selection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Selection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SkeletalMeshSocket
// 0x40FE0C00
class SkeletalMeshSocket
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SkeletalMeshSocket ReadAsMe(const uintptr_t address)
	{
		SkeletalMeshSocket ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SkeletalMeshSocket& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SlateTextureAtlasInterface
// 0x427D4600
class SlateTextureAtlasInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static SlateTextureAtlasInterface ReadAsMe(const uintptr_t address)
	{
		SlateTextureAtlasInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SlateTextureAtlasInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SmokeTestCommandlet
// 0x4290A600
class SmokeTestCommandlet
{
public:
	unsigned char                                      UnknownData00[0x4290A600];                                // 0x0000(0x4290A600) MISSED OFFSET

	static SmokeTestCommandlet ReadAsMe(const uintptr_t address)
	{
		SmokeTestCommandlet ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SmokeTestCommandlet& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundClass
// 0x40FE0C00
class SoundClass
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundClass ReadAsMe(const uintptr_t address)
	{
		SoundClass ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundClass& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SlateBrushAsset
// 0x40FE0C00
class SlateBrushAsset
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SlateBrushAsset ReadAsMe(const uintptr_t address)
	{
		SlateBrushAsset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SlateBrushAsset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundAttenuation
// 0x40FE0C00
class SoundAttenuation
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundAttenuation ReadAsMe(const uintptr_t address)
	{
		SoundAttenuation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundAttenuation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundEffectSourcePresetChain
// 0x40FE0C00
class SoundEffectSourcePresetChain
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundEffectSourcePresetChain ReadAsMe(const uintptr_t address)
	{
		SoundEffectSourcePresetChain ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundEffectSourcePresetChain& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundEffectSourcePreset
// 0x428B7800
class SoundEffectSourcePreset
{
public:
	unsigned char                                      UnknownData00[0x428B7800];                                // 0x0000(0x428B7800) MISSED OFFSET

	static SoundEffectSourcePreset ReadAsMe(const uintptr_t address)
	{
		SoundEffectSourcePreset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundEffectSourcePreset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundGroups
// 0x40FE0C00
class SoundGroups
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundGroups ReadAsMe(const uintptr_t address)
	{
		SoundGroups ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundGroups& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundMix
// 0x40FE0C00
class SoundMix
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundMix ReadAsMe(const uintptr_t address)
	{
		SoundMix ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundMix& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNode
// 0x40FE0C00
class SoundNode
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundNode ReadAsMe(const uintptr_t address)
	{
		SoundNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeBranch
// 0x4299AC00
class SoundNodeBranch
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeBranch ReadAsMe(const uintptr_t address)
	{
		SoundNodeBranch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeBranch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeAssetReferencer
// 0x4299AC00
class SoundNodeAssetReferencer
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeAssetReferencer ReadAsMe(const uintptr_t address)
	{
		SoundNodeAssetReferencer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeAssetReferencer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeConcatenator
// 0x4299AC00
class SoundNodeConcatenator
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeConcatenator ReadAsMe(const uintptr_t address)
	{
		SoundNodeConcatenator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeConcatenator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeDelay
// 0x4299AC00
class SoundNodeDelay
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeDelay ReadAsMe(const uintptr_t address)
	{
		SoundNodeDelay ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeDelay& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeAttenuation
// 0x4299AC00
class SoundNodeAttenuation
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeAttenuation ReadAsMe(const uintptr_t address)
	{
		SoundNodeAttenuation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeAttenuation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeDistanceCrossFade
// 0x4299AC00
class SoundNodeDistanceCrossFade
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeDistanceCrossFade ReadAsMe(const uintptr_t address)
	{
		SoundNodeDistanceCrossFade ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeDistanceCrossFade& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundConcurrency
// 0x40FE0C00
class SoundConcurrency
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundConcurrency ReadAsMe(const uintptr_t address)
	{
		SoundConcurrency ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundConcurrency& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeDialoguePlayer
// 0x4299AC00
class SoundNodeDialoguePlayer
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeDialoguePlayer ReadAsMe(const uintptr_t address)
	{
		SoundNodeDialoguePlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeDialoguePlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeDoppler
// 0x4299AC00
class SoundNodeDoppler
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeDoppler ReadAsMe(const uintptr_t address)
	{
		SoundNodeDoppler ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeDoppler& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeMixer
// 0x4299AC00
class SoundNodeMixer
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeMixer ReadAsMe(const uintptr_t address)
	{
		SoundNodeMixer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeMixer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeEnveloper
// 0x4299AC00
class SoundNodeEnveloper
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeEnveloper ReadAsMe(const uintptr_t address)
	{
		SoundNodeEnveloper ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeEnveloper& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeLooping
// 0x4299AC00
class SoundNodeLooping
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeLooping ReadAsMe(const uintptr_t address)
	{
		SoundNodeLooping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeLooping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeMature
// 0x4299AC00
class SoundNodeMature
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeMature ReadAsMe(const uintptr_t address)
	{
		SoundNodeMature ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeMature& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeOscillator
// 0x4299AC00
class SoundNodeOscillator
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeOscillator ReadAsMe(const uintptr_t address)
	{
		SoundNodeOscillator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeOscillator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeModulator
// 0x4299AC00
class SoundNodeModulator
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeModulator ReadAsMe(const uintptr_t address)
	{
		SoundNodeModulator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeModulator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeModulatorContinuous
// 0x4299AC00
class SoundNodeModulatorContinuous
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeModulatorContinuous ReadAsMe(const uintptr_t address)
	{
		SoundNodeModulatorContinuous ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeModulatorContinuous& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeQualityLevel
// 0x4299AC00
class SoundNodeQualityLevel
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeQualityLevel ReadAsMe(const uintptr_t address)
	{
		SoundNodeQualityLevel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeQualityLevel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeSoundClass
// 0x4299AC00
class SoundNodeSoundClass
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeSoundClass ReadAsMe(const uintptr_t address)
	{
		SoundNodeSoundClass ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeSoundClass& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeRandom
// 0x4299AC00
class SoundNodeRandom
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeRandom ReadAsMe(const uintptr_t address)
	{
		SoundNodeRandom ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeRandom& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeParamCrossFade
// 0x42999E00
class SoundNodeParamCrossFade
{
public:
	unsigned char                                      UnknownData00[0x42999E00];                                // 0x0000(0x42999E00) MISSED OFFSET

	static SoundNodeParamCrossFade ReadAsMe(const uintptr_t address)
	{
		SoundNodeParamCrossFade ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeParamCrossFade& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeSwitch
// 0x4299AC00
class SoundNodeSwitch
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeSwitch ReadAsMe(const uintptr_t address)
	{
		SoundNodeSwitch ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeSwitch& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundSubmix
// 0x40FE0C00
class SoundSubmix
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SoundSubmix ReadAsMe(const uintptr_t address)
	{
		SoundSubmix ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundSubmix& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeGroupControl
// 0x4299AC00
class SoundNodeGroupControl
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeGroupControl ReadAsMe(const uintptr_t address)
	{
		SoundNodeGroupControl ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeGroupControl& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeWavePlayer
// 0x4299AA00
class SoundNodeWavePlayer
{
public:
	unsigned char                                      UnknownData00[0x4299AA00];                                // 0x0000(0x4299AA00) MISSED OFFSET

	static SoundNodeWavePlayer ReadAsMe(const uintptr_t address)
	{
		SoundNodeWavePlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeWavePlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SpectatorPawn
// 0x4290E800
class SpectatorPawn
{
public:
	unsigned char                                      UnknownData00[0x4290E800];                                // 0x0000(0x4290E800) MISSED OFFSET

	static SpectatorPawn ReadAsMe(const uintptr_t address)
	{
		SpectatorPawn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SpectatorPawn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundSourceBus
// 0x428B6E00
class SoundSourceBus
{
public:
	unsigned char                                      UnknownData00[0x428B6E00];                                // 0x0000(0x428B6E00) MISSED OFFSET

	static SoundSourceBus ReadAsMe(const uintptr_t address)
	{
		SoundSourceBus ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundSourceBus& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SphereReflectionCapture
// 0x42905000
class SphereReflectionCapture
{
public:
	unsigned char                                      UnknownData00[0x42905000];                                // 0x0000(0x42905000) MISSED OFFSET

	static SphereReflectionCapture ReadAsMe(const uintptr_t address)
	{
		SphereReflectionCapture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SphereReflectionCapture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SphereReflectionCaptureComponent
// 0x42904C00
class SphereReflectionCaptureComponent
{
public:
	unsigned char                                      UnknownData00[0x42904C00];                                // 0x0000(0x42904C00) MISSED OFFSET

	static SphereReflectionCaptureComponent ReadAsMe(const uintptr_t address)
	{
		SphereReflectionCaptureComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SphereReflectionCaptureComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundNodeWaveParam
// 0x4299AC00
class SoundNodeWaveParam
{
public:
	unsigned char                                      UnknownData00[0x4299AC00];                                // 0x0000(0x4299AC00) MISSED OFFSET

	static SoundNodeWaveParam ReadAsMe(const uintptr_t address)
	{
		SoundNodeWaveParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundNodeWaveParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SplineMeshComponent
// 0x42828C00
class SplineMeshComponent
{
public:
	unsigned char                                      UnknownData00[0x42828C00];                                // 0x0000(0x42828C00) MISSED OFFSET

	static SplineMeshComponent ReadAsMe(const uintptr_t address)
	{
		SplineMeshComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SplineMeshComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SpotLightComponent
// 0x42991000
class SpotLightComponent
{
public:
	unsigned char                                      UnknownData00[0x42991000];                                // 0x0000(0x42991000) MISSED OFFSET

	static SpotLightComponent ReadAsMe(const uintptr_t address)
	{
		SpotLightComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SpotLightComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.StaticMesh
// 0x40FE0C00
class StaticMesh
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static StaticMesh ReadAsMe(const uintptr_t address)
	{
		StaticMesh ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StaticMesh& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SoundCue
// 0x428B6C00
class SoundCue
{
public:
	unsigned char                                      UnknownData00[0x428B6C00];                                // 0x0000(0x428B6C00) MISSED OFFSET

	static SoundCue ReadAsMe(const uintptr_t address)
	{
		SoundCue ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SoundCue& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SpectatorPawnMovement
// 0x42927C00
class SpectatorPawnMovement
{
public:
	unsigned char                                      UnknownData00[0x42927C00];                                // 0x0000(0x42927C00) MISSED OFFSET

	static SpectatorPawnMovement ReadAsMe(const uintptr_t address)
	{
		SpectatorPawnMovement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SpectatorPawnMovement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SpringArmComponent
// 0x40FEF800
class SpringArmComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static SpringArmComponent ReadAsMe(const uintptr_t address)
	{
		SpringArmComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SpringArmComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.StaticMeshSocket
// 0x40FE0C00
class StaticMeshSocket
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static StaticMeshSocket ReadAsMe(const uintptr_t address)
	{
		StaticMeshSocket ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StaticMeshSocket& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.StereoLayerComponent
// 0x40FEF800
class StereoLayerComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static StereoLayerComponent ReadAsMe(const uintptr_t address)
	{
		StereoLayerComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StereoLayerComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.StringTable
// 0x40FE0C00
class StringTable
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static StringTable ReadAsMe(const uintptr_t address)
	{
		StringTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StringTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SplineMeshActor
// 0x40FE8000
class SplineMeshActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static SplineMeshActor ReadAsMe(const uintptr_t address)
	{
		SplineMeshActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SplineMeshActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SubsystemBlueprintLibrary
// 0x40FEE400
class SubsystemBlueprintLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static SubsystemBlueprintLibrary ReadAsMe(const uintptr_t address)
	{
		SubsystemBlueprintLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SubsystemBlueprintLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.StereoLayerFunctionLibrary
// 0x40FEE400
class StereoLayerFunctionLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static StereoLayerFunctionLibrary ReadAsMe(const uintptr_t address)
	{
		StereoLayerFunctionLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, StereoLayerFunctionLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SubsurfaceProfile
// 0x40FE0C00
class SubsurfaceProfile
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SubsurfaceProfile ReadAsMe(const uintptr_t address)
	{
		SubsurfaceProfile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SubsurfaceProfile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TargetPoint
// 0x40FE8000
class TargetPoint
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static TargetPoint ReadAsMe(const uintptr_t address)
	{
		TargetPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TargetPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TimecodeProvider
// 0x40FE0C00
class TimecodeProvider
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static TimecodeProvider ReadAsMe(const uintptr_t address)
	{
		TimecodeProvider ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TimecodeProvider& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SystemTimeTimecodeProvider
// 0x4299D000
class SystemTimeTimecodeProvider
{
public:
	unsigned char                                      UnknownData00[0x4299D000];                                // 0x0000(0x4299D000) MISSED OFFSET

	static SystemTimeTimecodeProvider ReadAsMe(const uintptr_t address)
	{
		SystemTimeTimecodeProvider ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SystemTimeTimecodeProvider& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.SubUVAnimation
// 0x40FE0C00
class SubUVAnimation
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static SubUVAnimation ReadAsMe(const uintptr_t address)
	{
		SubUVAnimation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, SubUVAnimation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextPropertyTestObject
// 0x40FE0C00
class TextPropertyTestObject
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static TextPropertyTestObject ReadAsMe(const uintptr_t address)
	{
		TextPropertyTestObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextPropertyTestObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextureRenderTargetCube
// 0x4290B600
class TextureRenderTargetCube
{
public:
	unsigned char                                      UnknownData00[0x4290B600];                                // 0x0000(0x4290B600) MISSED OFFSET

	static TextureRenderTargetCube ReadAsMe(const uintptr_t address)
	{
		TextureRenderTargetCube ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextureRenderTargetCube& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.Texture2DDynamic
// 0x42881000
class Texture2DDynamic
{
public:
	unsigned char                                      UnknownData00[0x42881000];                                // 0x0000(0x42881000) MISSED OFFSET

	static Texture2DDynamic ReadAsMe(const uintptr_t address)
	{
		Texture2DDynamic ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, Texture2DDynamic& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextRenderActor
// 0x40FE8000
class TextRenderActor
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static TextRenderActor ReadAsMe(const uintptr_t address)
	{
		TextRenderActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextRenderActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.ThumbnailInfo
// 0x40FE0C00
class ThumbnailInfo
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static ThumbnailInfo ReadAsMe(const uintptr_t address)
	{
		ThumbnailInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, ThumbnailInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TextureLightProfile
// 0x4290FE00
class TextureLightProfile
{
public:
	unsigned char                                      UnknownData00[0x4290FE00];                                // 0x0000(0x4290FE00) MISSED OFFSET

	static TextureLightProfile ReadAsMe(const uintptr_t address)
	{
		TextureLightProfile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TextureLightProfile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TimelineComponent
// 0x40FEF600
class TimelineComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static TimelineComponent ReadAsMe(const uintptr_t address)
	{
		TimelineComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TimelineComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TimelineTemplate
// 0x40FE0C00
class TimelineTemplate
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static TimelineTemplate ReadAsMe(const uintptr_t address)
	{
		TimelineTemplate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TimelineTemplate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TireType
// 0x427EC000
class TireType
{
public:
	unsigned char                                      UnknownData00[0x427EC000];                                // 0x0000(0x427EC000) MISSED OFFSET

	static TireType ReadAsMe(const uintptr_t address)
	{
		TireType ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TireType& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TriggerBox
// 0x429A3400
class TriggerBox
{
public:
	unsigned char                                      UnknownData00[0x429A3400];                                // 0x0000(0x429A3400) MISSED OFFSET

	static TriggerBox ReadAsMe(const uintptr_t address)
	{
		TriggerBox ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TriggerBox& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TouchInterface
// 0x40FE0C00
class TouchInterface
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static TouchInterface ReadAsMe(const uintptr_t address)
	{
		TouchInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TouchInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TriggerSphere
// 0x429A3400
class TriggerSphere
{
public:
	unsigned char                                      UnknownData00[0x429A3400];                                // 0x0000(0x429A3400) MISSED OFFSET

	static TriggerSphere ReadAsMe(const uintptr_t address)
	{
		TriggerSphere ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TriggerSphere& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TriggerVolume
// 0x40FEC600
class TriggerVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static TriggerVolume ReadAsMe(const uintptr_t address)
	{
		TriggerVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TriggerVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TriggerCapsule
// 0x429A3400
class TriggerCapsule
{
public:
	unsigned char                                      UnknownData00[0x429A3400];                                // 0x0000(0x429A3400) MISSED OFFSET

	static TriggerCapsule ReadAsMe(const uintptr_t address)
	{
		TriggerCapsule ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TriggerCapsule& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TriggerBase
// 0x40FE8000
class TriggerBase
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static TriggerBase ReadAsMe(const uintptr_t address)
	{
		TriggerBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TriggerBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.TwitterIntegrationBase
// 0x4290AA00
class TwitterIntegrationBase
{
public:
	unsigned char                                      UnknownData00[0x4290AA00];                                // 0x0000(0x4290AA00) MISSED OFFSET

	static TwitterIntegrationBase ReadAsMe(const uintptr_t address)
	{
		TwitterIntegrationBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, TwitterIntegrationBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.UserDefinedEnum
// 0x4287BE00
class UserDefinedEnum
{
public:
	unsigned char                                      UnknownData00[0x4287BE00];                                // 0x0000(0x4287BE00) MISSED OFFSET

	static UserDefinedEnum ReadAsMe(const uintptr_t address)
	{
		UserDefinedEnum ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, UserDefinedEnum& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VectorField
// 0x40FE0C00
class VectorField
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static VectorField ReadAsMe(const uintptr_t address)
	{
		VectorField ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VectorField& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VectorFieldAnimated
// 0x429A2200
class VectorFieldAnimated
{
public:
	unsigned char                                      UnknownData00[0x429A2200];                                // 0x0000(0x429A2200) MISSED OFFSET

	static VectorFieldAnimated ReadAsMe(const uintptr_t address)
	{
		VectorFieldAnimated ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VectorFieldAnimated& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.UserInterfaceSettings
// 0x427D8E00
class UserInterfaceSettings
{
public:
	unsigned char                                      UnknownData00[0x427D8E00];                                // 0x0000(0x427D8E00) MISSED OFFSET

	static UserInterfaceSettings ReadAsMe(const uintptr_t address)
	{
		UserInterfaceSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, UserInterfaceSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VectorFieldStatic
// 0x429A2200
class VectorFieldStatic
{
public:
	unsigned char                                      UnknownData00[0x429A2200];                                // 0x0000(0x429A2200) MISSED OFFSET

	static VectorFieldStatic ReadAsMe(const uintptr_t address)
	{
		VectorFieldStatic ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VectorFieldStatic& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VirtualTexture
// 0x40FE0C00
class VirtualTexture
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static VirtualTexture ReadAsMe(const uintptr_t address)
	{
		VirtualTexture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VirtualTexture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VectorFieldVolume
// 0x40FE8000
class VectorFieldVolume
{
public:
	unsigned char                                      UnknownData00[0x40FE8000];                                // 0x0000(0x40FE8000) MISSED OFFSET

	static VectorFieldVolume ReadAsMe(const uintptr_t address)
	{
		VectorFieldVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VectorFieldVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightMapVirtualTexture
// 0x429A1800
class LightMapVirtualTexture
{
public:
	unsigned char                                      UnknownData00[0x429A1800];                                // 0x0000(0x429A1800) MISSED OFFSET

	static LightMapVirtualTexture ReadAsMe(const uintptr_t address)
	{
		LightMapVirtualTexture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightMapVirtualTexture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VirtualTextureSpace
// 0x40FE0C00
class VirtualTextureSpace
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static VirtualTextureSpace ReadAsMe(const uintptr_t address)
	{
		VirtualTextureSpace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VirtualTextureSpace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.LightMapVirtualTextureSpace
// 0x429A1400
class LightMapVirtualTextureSpace
{
public:
	unsigned char                                      UnknownData00[0x429A1400];                                // 0x0000(0x429A1400) MISSED OFFSET

	static LightMapVirtualTextureSpace ReadAsMe(const uintptr_t address)
	{
		LightMapVirtualTextureSpace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, LightMapVirtualTextureSpace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VisualLoggerDebugSnapshotInterface
// 0x427D4600
class VisualLoggerDebugSnapshotInterface
{
public:
	unsigned char                                      UnknownData00[0x427D4600];                                // 0x0000(0x427D4600) MISSED OFFSET

	static VisualLoggerDebugSnapshotInterface ReadAsMe(const uintptr_t address)
	{
		VisualLoggerDebugSnapshotInterface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VisualLoggerDebugSnapshotInterface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VisualLoggerAutomationTests
// 0x40FE0C00
class VisualLoggerAutomationTests
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static VisualLoggerAutomationTests ReadAsMe(const uintptr_t address)
	{
		VisualLoggerAutomationTests ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VisualLoggerAutomationTests& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VisualLoggerKismetLibrary
// 0x40FEE400
class VisualLoggerKismetLibrary
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static VisualLoggerKismetLibrary ReadAsMe(const uintptr_t address)
	{
		VisualLoggerKismetLibrary ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VisualLoggerKismetLibrary& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VoiceChannel
// 0x428BE800
class VoiceChannel
{
public:
	unsigned char                                      UnknownData00[0x428BE800];                                // 0x0000(0x428BE800) MISSED OFFSET

	static VoiceChannel ReadAsMe(const uintptr_t address)
	{
		VoiceChannel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VoiceChannel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VOIPTalker
// 0x40FEF600
class VOIPTalker
{
public:
	unsigned char                                      UnknownData00[0x40FEF600];                                // 0x0000(0x40FEF600) MISSED OFFSET

	static VOIPTalker ReadAsMe(const uintptr_t address)
	{
		VOIPTalker ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VOIPTalker& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VectorFieldComponent
// 0x427D0A00
class VectorFieldComponent
{
public:
	unsigned char                                      UnknownData00[0x427D0A00];                                // 0x0000(0x427D0A00) MISSED OFFSET

	static VectorFieldComponent ReadAsMe(const uintptr_t address)
	{
		VectorFieldComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VectorFieldComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VolumeTexture
// 0x42881000
class VolumeTexture
{
public:
	unsigned char                                      UnknownData00[0x42881000];                                // 0x0000(0x42881000) MISSED OFFSET

	static VolumeTexture ReadAsMe(const uintptr_t address)
	{
		VolumeTexture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VolumeTexture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VolumetricLightmapDensityVolume
// 0x40FEC600
class VolumetricLightmapDensityVolume
{
public:
	unsigned char                                      UnknownData00[0x40FEC600];                                // 0x0000(0x40FEC600) MISSED OFFSET

	static VolumetricLightmapDensityVolume ReadAsMe(const uintptr_t address)
	{
		VolumetricLightmapDensityVolume ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VolumetricLightmapDensityVolume& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.UserDefinedStruct
// 0x42874800
class UserDefinedStruct
{
public:
	unsigned char                                      UnknownData00[0x42874800];                                // 0x0000(0x42874800) MISSED OFFSET

	static UserDefinedStruct ReadAsMe(const uintptr_t address)
	{
		UserDefinedStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, UserDefinedStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.WindDirectionalSourceComponent
// 0x40FEF800
class WindDirectionalSourceComponent
{
public:
	unsigned char                                      UnknownData00[0x40FEF800];                                // 0x0000(0x40FEF800) MISSED OFFSET

	static WindDirectionalSourceComponent ReadAsMe(const uintptr_t address)
	{
		WindDirectionalSourceComponent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, WindDirectionalSourceComponent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.WorldComposition
// 0x40FE0C00
class WorldComposition
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static WorldComposition ReadAsMe(const uintptr_t address)
	{
		WorldComposition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, WorldComposition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.WindDirectionalSource
// 0x427D5600
class WindDirectionalSource
{
public:
	unsigned char                                      UnknownData00[0x427D5600];                                // 0x0000(0x427D5600) MISSED OFFSET

	static WindDirectionalSource ReadAsMe(const uintptr_t address)
	{
		WindDirectionalSource ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, WindDirectionalSource& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.HierarchicalLODSetup
// 0x40FE0C00
class HierarchicalLODSetup
{
public:
	unsigned char                                      UnknownData00[0x40FE0C00];                                // 0x0000(0x40FE0C00) MISSED OFFSET

	static HierarchicalLODSetup ReadAsMe(const uintptr_t address)
	{
		HierarchicalLODSetup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, HierarchicalLODSetup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


// Class Engine.VOIPStatics
// 0x40FEE400
class VOIPStatics
{
public:
	unsigned char                                      UnknownData00[0x40FEE400];                                // 0x0000(0x40FEE400) MISSED OFFSET

	static VOIPStatics ReadAsMe(const uintptr_t address)
	{
		VOIPStatics ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, VOIPStatics& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};


}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
