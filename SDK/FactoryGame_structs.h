#pragma once

#include "../Utils.h"
// Name: Satisfactory, Version: 1.0.0

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace SDK
{
//---------------------------------------------------------------------------
// Enums
//---------------------------------------------------------------------------

// Enum FactoryGame.EGamePhase
enum class EGamePhase : uint8_t
{
	EGP_EarlyGame                  = 0,
	EGP_MidGame                    = 1,
	EGP_LateGame                   = 2,
	EGP_EndGame                    = 3,
	EGP_FoodCourt                  = 4,
	EGP_Victory                    = 5,
	EGP_MAX                        = 6
};


// Enum FactoryGame.EHologramSplinePathMode
enum class EHologramSplinePathMode : uint8_t
{
	EHologramSplinePathMode__HSPM_AUTO = 0,
	EHologramSplinePathMode__HSPM_STRAIGHT_HORZ = 1,
	EHologramSplinePathMode__HSPM_STRAIGHT_VERT = 2,
	EHologramSplinePathMode__HSPM_HORZ_TO_VERT = 3,
	EHologramSplinePathMode__HSPM_VERT_TO_HORZ = 4,
	EHologramSplinePathMode__HSPM_PATH_FIND = 5,
	EHologramSplinePathMode__HSPM_AUTO_2D = 6,
	EHologramSplinePathMode__HSPM_NOODLE = 7,
	EHologramSplinePathMode__HSPM_MAX = 8
};


// Enum FactoryGame.ERepresentationType
enum class ERepresentationType : uint8_t
{
	ERepresentationType__RT_Default = 0,
	ERepresentationType__RT_Beacon = 1,
	ERepresentationType__RT_Crate  = 2,
	ERepresentationType__RT_Hub    = 3,
	ERepresentationType__RT_Ping   = 4,
	ERepresentationType__RT_Player = 5,
	ERepresentationType__RT_RadarTower = 6,
	ERepresentationType__RT_Resource = 7,
	ERepresentationType__RT_SpaceElevator = 8,
	ERepresentationType__RT_StartingPod = 9,
	ERepresentationType__RT_Train  = 10,
	ERepresentationType__RT_TrainStation = 11,
	ERepresentationType__RT_Vehicle = 12,
	ERepresentationType__RT_VehicleDockingStation = 13,
	ERepresentationType__RT_MAX    = 14
};


// Enum FactoryGame.EJoinSessionState
enum class EJoinSessionState : uint8_t
{
	EJoinSessionState__JSS_NotJoiningSession = 0,
	EJoinSessionState__JSS_WaitingForLoginToComplete = 1,
	EJoinSessionState__JSS_QueryingHostsId = 2,
	EJoinSessionState__JSS_DestroyingOldSession = 3,
	EJoinSessionState__JSS_ConnectingToHost = 4,
	EJoinSessionState__JSS_MAX     = 5
};


// Enum FactoryGame.EBuildGunState
enum class EBuildGunState : uint8_t
{
	EBuildGunState__BGS_NONE       = 0,
	EBuildGunState__BGS_MENU       = 1,
	EBuildGunState__BGS_BUILD      = 2,
	EBuildGunState__BGS_DISMANTLE  = 3,
	EBuildGunState__BGS_MAX        = 4
};


// Enum FactoryGame.EResearchState
enum class EResearchState : uint8_t
{
	EResearchState__ERS_NotResearching = 0,
	EResearchState__ERS_Researching = 1,
	EResearchState__ERS_MAX        = 2
};


// Enum FactoryGame.ECreateSessionState
enum class ECreateSessionState : uint8_t
{
	ECreateSessionState__CSS_NotCreateingSession = 0,
	ECreateSessionState__CSS_CreatingSession = 1,
	ECreateSessionState__CSS_DestroyingOldSession = 2,
	ECreateSessionState__CSS_UpdatingPresence = 3,
	ECreateSessionState__CSS_WaitingForPresenceToUpdate = 4,
	ECreateSessionState__CSS_WaitingForLogin = 5,
	ECreateSessionState__CSS_MAX   = 6
};


// Enum FactoryGame.ELoginState
enum class ELoginState : uint8_t
{
	LS_NotLoggedIn                 = 0,
	LS_FailedToLogin               = 1,
	LS_LoggingIn                   = 2,
	LS_LoggedIn                    = 3,
	LS_MAX                         = 4
};


// Enum FactoryGame.ETrainDockingState
enum class ETrainDockingState : uint8_t
{
	ETrainDockingState__TDS_None   = 0,
	ETrainDockingState__TDS_ReadyToDock = 1,
	ETrainDockingState__TDS_Docked = 2,
	ETrainDockingState__TDS_MAX    = 3
};


// Enum FactoryGame.EBackEquipment
enum class EBackEquipment : uint8_t
{
	EBackEquipment__BE_None        = 0,
	EBackEquipment__BE_Jetpack     = 1,
	EBackEquipment__BE_MAX         = 2
};


// Enum FactoryGame.EArmEquipment
enum class EArmEquipment : uint8_t
{
	EArmEquipment__AE_None         = 0,
	EArmEquipment__AE_ChainSaw     = 1,
	EArmEquipment__AE_RebarGun     = 2,
	EArmEquipment__AE_BuildGun     = 3,
	EArmEquipment__AE_Nobelisk     = 4,
	EArmEquipment__AE_ResourceScanner = 5,
	EArmEquipment__AE_Rifle        = 6,
	EArmEquipment__AE_ColorGun     = 7,
	EArmEquipment__AE_OneHandEquipment = 8,
	EArmEquipment__AE_Consumables  = 9,
	EArmEquipment__AE_ObjectScanner = 10,
	EArmEquipment__AE_PortableMiner = 11,
	EArmEquipment__AE_StunSpear    = 12,
	EArmEquipment__AE_ShockShank   = 13,
	EArmEquipment__AE_ResourceCollector = 14,
	EArmEquipment__AE_Generic1Hand = 15,
	EArmEquipment__AE_Generic2Hand = 16,
	EArmEquipment__AE_MAX          = 17
};


// Enum FactoryGame.EOutlineColor
enum class EOutlineColor : uint8_t
{
	EOutlineColor__OC_NONE         = 0,
	EOutlineColor__OC_USABLE       = 1,
	EOutlineColor__OC_HOLOGRAM     = 2,
	EOutlineColor__OC_RED          = 3,
	EOutlineColor__OC_MAX          = 4
};


// Enum FactoryGame.ECompassViewDistance
enum class ECompassViewDistance : uint8_t
{
	ECompassViewDistance__CVD_Off  = 0,
	ECompassViewDistance__CVD_Near = 1,
	ECompassViewDistance__CVD_Mid  = 2,
	ECompassViewDistance__CVD_Far  = 3,
	ECompassViewDistance__CVD_Always = 4,
	ECompassViewDistance__CVD_MAX  = 5
};


// Enum FactoryGame.EFogOfWarRevealType
enum class EFogOfWarRevealType : uint8_t
{
	EFogOfWarRevealType__FOWRT_None = 0,
	EFogOfWarRevealType__FOWRT_Static = 1,
	EFogOfWarRevealType__FOWRT_Intermittent = 2,
	EFogOfWarRevealType__FOWRT_Dynamic = 3,
	EFogOfWarRevealType__FOWRT_MAX = 4
};


// Enum FactoryGame.ETrainPlatformDockingStatus
enum class ETrainPlatformDockingStatus : uint8_t
{
	ETrainPlatformDockingStatus__ETPDS_None = 0,
	ETrainPlatformDockingStatus__ETPDS_WaitingToStart = 1,
	ETrainPlatformDockingStatus__ETPDS_Loading = 2,
	ETrainPlatformDockingStatus__ETPDS_Unloading = 3,
	ETrainPlatformDockingStatus__ETPDS_WaitingForTransfer = 4,
	ETrainPlatformDockingStatus__ETPDS_Complete = 5,
	ETrainPlatformDockingStatus__ETPDS_MAX = 6
};


// Enum FactoryGame.EGuideLineType
enum class EGuideLineType : uint8_t
{
	EGuideLineType__GLT_Default    = 0,
	EGuideLineType__GLT_ConveyorBelt = 1,
	EGuideLineType__GLT_MAX        = 2
};


// Enum FactoryGame.EDeferredCollisionChange
enum class EDeferredCollisionChange : uint8_t
{
	EDeferredCollisionChange__DCC_None = 0,
	EDeferredCollisionChange__DCC_TURN_ON = 1,
	EDeferredCollisionChange__DCC_TURN_OFF = 2,
	EDeferredCollisionChange__DCC_MAX = 3
};


// Enum FactoryGame.EProductionStatus
enum class EProductionStatus : uint8_t
{
	EProductionStatus__IS_NONE     = 0,
	EProductionStatus__IS_PRODUCING = 1,
	EProductionStatus__IS_PRODUCING_WITH_CRYSTAL = 2,
	EProductionStatus__IS_STANDBY  = 3,
	EProductionStatus__IS_ERROR    = 4,
	EProductionStatus__IS_MAX      = 5
};


// Enum FactoryGame.ECustomMovementMode
enum class ECustomMovementMode : uint8_t
{
	ECustomMovementMode__CMM_None  = 0,
	ECustomMovementMode__CMM_Ladder = 1,
	ECustomMovementMode__CMM_PipeHyper = 2,
	ECustomMovementMode__CMM_MAX   = 3
};


// Enum FactoryGame.ECameraMode
enum class ECameraMode : uint8_t
{
	ECameraMode__ECM_None          = 0,
	ECameraMode__ECM_FirstPerson   = 1,
	ECameraMode__ECM_ThirdPerson   = 2,
	ECameraMode__ECM_MAX           = 3
};


// Enum FactoryGame.EFGCrateIconType
enum class EFGCrateIconType : uint8_t
{
	EFGCrateIconType__CIT_DeathIcon = 0,
	EFGCrateIconType__CIT_MAX      = 1
};


// Enum FactoryGame.EFGColorGunTargetType
enum class EFGColorGunTargetType : uint8_t
{
	EFGColorGunTargetType__ECGT_noTarget = 0,
	EFGColorGunTargetType__ECGT_nonColorable = 1,
	EFGColorGunTargetType__ECGT_validTarget = 2,
	EFGColorGunTargetType__ECGT_sameColorSlot = 3,
	EFGColorGunTargetType__ECGT_MAX = 4
};


// Enum FactoryGame.EMoveSpeed
enum class EMoveSpeed : uint8_t
{
	EMoveSpeed__MS_Undefined       = 0,
	EMoveSpeed__MS_Walk            = 1,
	EMoveSpeed__MS_Run             = 2,
	EMoveSpeed__MS_Sprint          = 3,
	EMoveSpeed__MS_MAX             = 4
};


// Enum FactoryGame.EEnabled
enum class EEnabled : uint8_t
{
	E_Enabled                      = 0,
	E_Disabled                     = 1,
	E_Unknown                      = 2,
	E_MAX                          = 3
};


// Enum FactoryGame.EIgnore
enum class EIgnore : uint8_t
{
	EIgnore__I_NONE                = 0,
	EIgnore__I_FALSE               = 1,
	EIgnore__I_NOT_VALID           = 2,
	EIgnore__I_NO_PATH             = 3,
	EIgnore__I_LastEnum            = 4,
	EIgnore__I_MAX                 = 5
};


// Enum FactoryGame.EFGChatMessageType
enum class EFGChatMessageType : uint8_t
{
	EFGChatMessageType__CMT_PlayerMessage = 0,
	EFGChatMessageType__CMT_SystemMessage = 1,
	EFGChatMessageType__CMT_AdaMessage = 2,
	EFGChatMessageType__CMT_MAX    = 3
};


// Enum FactoryGame.EOverrideSetting
enum class EOverrideSetting : uint8_t
{
	OS_Additive                    = 0,
	OS_Override                    = 1,
	OS_MAX                         = 2
};


// Enum FactoryGame.EPlayOnDamageEvent
enum class EPlayOnDamageEvent : uint8_t
{
	PODE_OnTakeAnyDamage           = 0,
	PODE_OnTakePointDamage         = 1,
	PODE_OnTakeRadialDamage        = 2,
	PODE_MAX                       = 3
};


// Enum FactoryGame.EDockStationType
enum class EDockStationType : uint8_t
{
	EDockStationType__DST_NONE     = 0,
	EDockStationType__DST_TRUCK    = 1,
	EDockStationType__DST_TRAIN    = 2,
	EDockStationType__DST_LAST_ENUM = 3,
	EDockStationType__DST_MAX      = 4
};


// Enum FactoryGame.EFactoryConnectionDirection
enum class EFactoryConnectionDirection : uint8_t
{
	EFactoryConnectionDirection__FCD_INPUT = 0,
	EFactoryConnectionDirection__FCD_OUTPUT = 1,
	EFactoryConnectionDirection__FCD_ANY = 2,
	EFactoryConnectionDirection__FCD_SNAP_ONLY = 3,
	EFactoryConnectionDirection__FCD_MAX = 4
};


// Enum FactoryGame.EFactoryConnectionConnector
enum class EFactoryConnectionConnector : uint8_t
{
	EFactoryConnectionConnector__FCC_CONVEYOR = 0,
	EFactoryConnectionConnector__FCC_PIPE = 1,
	EFactoryConnectionConnector__FCC_MAX = 2
};


// Enum FactoryGame.EEquipmentSlot
enum class EEquipmentSlot : uint8_t
{
	EEquipmentSlot__ES_NONE        = 0,
	EEquipmentSlot__ES_ARMS        = 1,
	EEquipmentSlot__ES_BACK        = 2,
	EEquipmentSlot__ES_MAX         = 3
};


// Enum FactoryGame.EFreightCargoType
enum class EFreightCargoType : uint8_t
{
	EFreightCargoType__FCT_NONE    = 0,
	EFreightCargoType__FCT_Standard = 1,
	EFreightCargoType__FCT_Liquid  = 2,
	EFreightCargoType__FCT_MAX     = 3
};


// Enum FactoryGame.EHologramScrollMode
enum class EHologramScrollMode : uint8_t
{
	EHologramScrollMode__HSM_NONE  = 0,
	EHologramScrollMode__HSM_ROTATE = 1,
	EHologramScrollMode__HSM_RAISE_LOWER = 2,
	EHologramScrollMode__HSM_SPLINE_PATH_MODE = 3,
	EHologramScrollMode__HSM_MAX   = 4
};


// Enum FactoryGame.EStackSize
enum class EStackSize : uint8_t
{
	EStackSize__SS_ONE             = 0,
	EStackSize__SS_SMALL           = 1,
	EStackSize__SS_MEDIUM          = 2,
	EStackSize__SS_BIG             = 3,
	EStackSize__SS_HUGE            = 4,
	EStackSize__SS_FLUID           = 5,
	EStackSize__SS_LAST_ENUM       = 6,
	EStackSize__SS_MAX             = 7
};


// Enum FactoryGame.EResourceForm
enum class EResourceForm : uint8_t
{
	EResourceForm__RF_INVALID      = 0,
	EResourceForm__RF_SOLID        = 1,
	EResourceForm__RF_LIQUID       = 2,
	EResourceForm__RF_GAS          = 3,
	EResourceForm__RF_HEAT         = 4,
	EResourceForm__RF_LAST_ENUM    = 5,
	EResourceForm__RF_MAX          = 6
};


// Enum FactoryGame.EErrorResponse
enum class EErrorResponse : uint8_t
{
	ER_NonIntrusivePopup           = 0,
	ER_SendToMainMenu              = 1,
	ER_Quit                        = 2,
	ER_MAX                         = 3
};


// Enum FactoryGame.EMultipleUnitControl
enum class EMultipleUnitControl : uint8_t
{
	EMultipleUnitControl__MUC_Disabled = 0,
	EMultipleUnitControl__MUC_Master = 1,
	EMultipleUnitControl__MUC_Slave = 2,
	EMultipleUnitControl__MUC_MAX  = 3
};


// Enum FactoryGame.EMessageType
enum class EMessageType : uint8_t
{
	EMessageType__MT_PIMARY        = 0,
	EMessageType__MT_TUTORIAL      = 1,
	EMessageType__MT_SPAM          = 2,
	EMessageType__MT_UNDEFINED     = 3,
	EMessageType__MT_MAX           = 4
};


// Enum FactoryGame.EHeightDataType
enum class EHeightDataType : uint8_t
{
	EHeightDataType__HDT_NONE      = 0,
	EHeightDataType__HDT_TERRAIN   = 1,
	EHeightDataType__HDT_WATER     = 2,
	EHeightDataType__HDT_FOLIAGE   = 3,
	EHeightDataType__HDT_MAX       = 4
};


// Enum FactoryGame.ECantJoinReason
enum class ECantJoinReason : uint8_t
{
	CJR_InvalidGame                = 0,
	CJR_NotInAGame                 = 1,
	CJR_PlayingOtherGame           = 2,
	CJR_PrivateGame                = 3,
	CJR_DifferentVersion           = 4,
	CJR_FullGame                   = 5,
	CJR_Ok                         = 6,
	CJR_MAX                        = 7
};


// Enum FactoryGame.ENetIdType
enum class ENetIdType : uint8_t
{
	ENetIdType__NIT_EPIC           = 0,
	ENetIdType__NIT_STEAM          = 1,
	ENetIdType__NIT_OTHER          = 2,
	ENetIdType__NIT_MAX            = 3
};


// Enum FactoryGame.ESessionVisibility
enum class ESessionVisibility : uint8_t
{
	SV_Private                     = 0,
	SV_FriendsOnly                 = 1,
	SV_Invalid                     = 2,
	SV_MAX                         = 3
};


// Enum FactoryGame.ECycleDirection
enum class ECycleDirection : uint8_t
{
	ECycleDirection__CD_Forward    = 0,
	ECycleDirection__CD_Backward   = 1,
	ECycleDirection__CD_MAX        = 2
};


// Enum FactoryGame.ENetmodeAvailability
enum class ENetmodeAvailability : uint8_t
{
	ENetmodeAvailability__NA_ServerAndClient = 0,
	ENetmodeAvailability__NA_OnlyServer = 1,
	ENetmodeAvailability__NA_OnlyClient = 2,
	ENetmodeAvailability__NA_MAX   = 3
};


// Enum FactoryGame.EOptionType
enum class EOptionType : uint8_t
{
	EOptionType__OT_Checkbox       = 0,
	EOptionType__OT_IntegerSelection = 1,
	EOptionType__OT_FloatSelection = 2,
	EOptionType__OT_Slider         = 3,
	EOptionType__OT_MAX            = 4
};


// Enum FactoryGame.ECrosshairState
enum class ECrosshairState : uint8_t
{
	ECrosshairState__ECS_Default   = 0,
	ECrosshairState__ECS_GeneralUse = 1,
	ECrosshairState__ECS_PickUp    = 2,
	ECrosshairState__ECS_Vehicle   = 3,
	ECrosshairState__ECS_Weapon    = 4,
	ECrosshairState__ECS_Workbench = 5,
	ECrosshairState__ECS_Dismantle = 6,
	ECrosshairState__ECS_Build     = 7,
	ECrosshairState__ECS_Custom    = 8,
	ECrosshairState__ECS_MAX       = 9
};


// Enum FactoryGame.EPipeConnectionType
enum class EPipeConnectionType : uint8_t
{
	EPipeConnectionType__PCT_ANY   = 0,
	EPipeConnectionType__PCT_CONSUMER = 1,
	EPipeConnectionType__PCT_PRODUCER = 2,
	EPipeConnectionType__PCT_SNAP_ONLY = 3,
	EPipeConnectionType__PCT_MAX   = 4
};


// Enum FactoryGame.EOptionCategory
enum class EOptionCategory : uint8_t
{
	EOptionCategory__OC_Gameplay   = 0,
	EOptionCategory__OC_Audio      = 1,
	EOptionCategory__OC_Video      = 2,
	EOptionCategory__OC_Controls   = 3,
	EOptionCategory__OC_UserInterface = 4,
	EOptionCategory__OC_MAX        = 5
};


// Enum FactoryGame.EPopupId
enum class EPopupId : uint8_t
{
	PID_OK                         = 0,
	PID_OK_CANCEL                  = 1,
	PID_NONE                       = 2,
	PID_MAX                        = 3
};


// Enum FactoryGame.ERailroadVehicleCoupler
enum class ERailroadVehicleCoupler : uint8_t
{
	ERailroadVehicleCoupler__RVC_FRONT = 0,
	ERailroadVehicleCoupler__RVC_BACK = 1,
	ERailroadVehicleCoupler__RVC_MAX = 2
};


// Enum FactoryGame.EResourceAmount
enum class EResourceAmount : uint8_t
{
	RA_Poor                        = 0,
	RA_Normal                      = 1,
	RA_Rich                        = 2,
	RA_Infinite                    = 3,
	RA_MAX                         = 4
};


// Enum FactoryGame.ESaveSortDirection
enum class ESaveSortDirection : uint8_t
{
	ESaveSortDirection__SSD_Ascending = 0,
	ESaveSortDirection__SSD_Descending = 1,
	ESaveSortDirection__SSD_MAX    = 2
};


// Enum FactoryGame.EResourcePurity
enum class EResourcePurity : uint8_t
{
	RP_Inpure                      = 0,
	RP_Normal                      = 1,
	RP_Pure                        = 2,
	RP_MAX                         = 3
};


// Enum FactoryGame.ESaveState
enum class ESaveState : uint8_t
{
	ESaveState__SS_Unsupported     = 0,
	ESaveState__SS_Volatile        = 1,
	ESaveState__SS_Supported       = 2,
	ESaveState__SS_Newer           = 3,
	ESaveState__SS_MAX             = 4
};


// Enum FactoryGame.ESaveSortMode
enum class ESaveSortMode : uint8_t
{
	ESaveSortMode__SSM_Name        = 0,
	ESaveSortMode__SSM_Time        = 1,
	ESaveSortMode__SSM_MAX         = 2
};


// Enum FactoryGame.ESchematicType
enum class ESchematicType : uint8_t
{
	ESchematicType__EST_Custom     = 0,
	ESchematicType__EST_Cheat      = 1,
	ESchematicType__EST_Tutorial   = 2,
	ESchematicType__EST_Milestone  = 3,
	ESchematicType__EST_Alternate  = 4,
	ESchematicType__EST_Story      = 5,
	ESchematicType__EST_MAM        = 6,
	ESchematicType__EST_ResourceSink = 7,
	ESchematicType__EST_HardDrive  = 8,
	ESchematicType__EST_Prototype  = 9,
	ESchematicType__EST_MAX        = 10
};


// Enum FactoryGame.EHorizontalSignTextAlignment
enum class EHorizontalSignTextAlignment : uint8_t
{
	EHorizontalSignTextAlignment__EHSTA_Left = 0,
	EHorizontalSignTextAlignment__EHSTA_Center = 1,
	EHorizontalSignTextAlignment__EHSTA_Right = 2,
	EHorizontalSignTextAlignment__EHSTA_MAX = 3
};


// Enum FactoryGame.ESaveExists
enum class ESaveExists : uint8_t
{
	ESaveExists__SE_DoesntExist    = 0,
	ESaveExists__SE_ExistsInSameSession = 1,
	ESaveExists__SE_ExistsInOtherSession = 2,
	ESaveExists__SE_MAX            = 3
};


// Enum FactoryGame.ESelfDrivingLocomotiveState
enum class ESelfDrivingLocomotiveState : uint8_t
{
	ESelfDrivingLocomotiveState__SDLS_Idle = 0,
	ESelfDrivingLocomotiveState__SDLS_FollowPath = 1,
	ESelfDrivingLocomotiveState__SDLS_Docking = 2,
	ESelfDrivingLocomotiveState__SDLS_DockingCompleted = 3,
	ESelfDrivingLocomotiveState__SDLS_MAX = 4
};


// Enum FactoryGame.ESchematicCategory
enum class ESchematicCategory : uint8_t
{
	ESchematicCategory__ESC_LOGISTICS = 0,
	ESchematicCategory__ESC_PRODUCTION = 1,
	ESchematicCategory__ESC_EQUIPMENT = 2,
	ESchematicCategory__ESC_ORGANISATION = 3,
	ESchematicCategory__ESC_MAX    = 4
};


// Enum FactoryGame.EItemState
enum class EItemState : uint8_t
{
	EItemState__ES_SEED            = 0,
	EItemState__ES_NORMAL          = 1,
	EItemState__ES_MAX             = 2
};


// Enum FactoryGame.ESignElementType
enum class ESignElementType : uint8_t
{
	ESignElementType__ESET_Scene   = 0,
	ESignElementType__ESET_Text    = 1,
	ESignElementType__ESET_SpriteIcon = 2,
	ESignElementType__ESET_MAX     = 3
};


// Enum FactoryGame.ERailroadSignalAspect
enum class ERailroadSignalAspect : uint8_t
{
	ERailroadSignalAspect__RSA_None = 0,
	ERailroadSignalAspect__RSA_Clear = 1,
	ERailroadSignalAspect__RSA_ClearThenStop = 2,
	ERailroadSignalAspect__RSA_Stop = 3,
	ERailroadSignalAspect__RSA_Dock = 4,
	ERailroadSignalAspect__RSA_MAX = 5
};


// Enum FactoryGame.ETrainPlatformConnectionType
enum class ETrainPlatformConnectionType : uint8_t
{
	ETrainPlatformConnectionType__ETPC_Out = 0,
	ETrainPlatformConnectionType__ETPC_In = 1,
	ETrainPlatformConnectionType__ETPC_Neutral = 2,
	ETrainPlatformConnectionType__ETPC_MAX = 3
};


// Enum FactoryGame.ESelfDrivingLocomotiveError
enum class ESelfDrivingLocomotiveError : uint8_t
{
	ESelfDrivingLocomotiveError__SDLE_NoError = 0,
	ESelfDrivingLocomotiveError__SDLE_NoPower = 1,
	ESelfDrivingLocomotiveError__SDLE_NoTimeTable = 2,
	ESelfDrivingLocomotiveError__SDLE_InvalidNextStop = 3,
	ESelfDrivingLocomotiveError__SDLE_InvalidLocomotivePlacement = 4,
	ESelfDrivingLocomotiveError__SDLE_NoPath = 5,
	ESelfDrivingLocomotiveError__SDLE_MAX = 6
};


// Enum FactoryGame.EGameVersion
enum class EGameVersion : uint8_t
{
	EGameVersion__GV_Main          = 0,
	EGameVersion__GV_Experimental  = 1,
	EGameVersion__GV_Other         = 2,
	EGameVersion__GV_MAX           = 3
};


// Enum FactoryGame.EWheelOrder6W
enum class EWheelOrder6W : uint8_t
{
	eFRONT_LEFT                    = 0,
	eFRONT_RIGHT                   = 1,
	eMID_LEFT                      = 2,
	eMID_RIGHT                     = 3,
	eREAR_LEFT                     = 4,
	eREAR_RIGHT                    = 5,
	EWheelOrder6W_MAX              = 6
};


// Enum FactoryGame.EMultiplayerButtonType
enum class EMultiplayerButtonType : uint8_t
{
	EMultiplayerButtonType__MBT_Join = 0,
	EMultiplayerButtonType__MBT_SendInvite = 1,
	EMultiplayerButtonType__MBT_JoinInvite = 2,
	EMultiplayerButtonType__MBT_ManagePlayers = 3,
	EMultiplayerButtonType__MBT_MAX = 4
};


// Enum FactoryGame.EIncludeInBuilds
enum class EIncludeInBuilds : uint8_t
{
	EIncludeInBuilds__IIB_Never    = 0,
	EIncludeInBuilds__IIB_Development = 1,
	EIncludeInBuilds__IIB_PrivateBuilds = 2,
	EIncludeInBuilds__IIB_PublicBuilds = 3,
	EIncludeInBuilds__IIB_MAX      = 4
};


// Enum FactoryGame.ERailroadPathFindingResult
enum class ERailroadPathFindingResult : uint8_t
{
	ERailroadPathFindingResult__RNQR_Error = 0,
	ERailroadPathFindingResult__RNQR_Unreachable = 1,
	ERailroadPathFindingResult__RNQR_Success = 2,
	ERailroadPathFindingResult__RNQR_MAX = 3
};


// Enum FactoryGame.EUndefinedBool
enum class EUndefinedBool : uint8_t
{
	EUndefinedBool__UB_Undefined   = 0,
	EUndefinedBool__UB_False       = 1,
	EUndefinedBool__UB_True        = 2,
	EUndefinedBool__UB_MAX         = 3
};


// Enum FactoryGame.ESplineHologramBuildStep
enum class ESplineHologramBuildStep : uint8_t
{
	ESplineHologramBuildStep__SHBS_FindStart = 0,
	ESplineHologramBuildStep__SHBS_PlacePoleOrSnapEnding = 1,
	ESplineHologramBuildStep__SHBS_AdjustPole = 2,
	ESplineHologramBuildStep__SHBS_MAX = 3
};


// Enum FactoryGame.EIntroTutorialSteps
enum class EIntroTutorialSteps : uint8_t
{
	EIntroTutorialSteps__ITS_NONE  = 0,
	EIntroTutorialSteps__ITS_INTRO = 1,
	EIntroTutorialSteps__ITS_DISMANTLE_POD = 2,
	EIntroTutorialSteps__ITS_OPEN_CODEX = 3,
	EIntroTutorialSteps__ITS_STUN_SPEAR = 4,
	EIntroTutorialSteps__ITS_IRON_ORE = 5,
	EIntroTutorialSteps__ITS_HUB   = 6,
	EIntroTutorialSteps__ITS_HUB_LVL1 = 7,
	EIntroTutorialSteps__ITS_HUB_LVL101 = 8,
	EIntroTutorialSteps__ITS_HUB_LVL2 = 9,
	EIntroTutorialSteps__ITS_HUB_LVL3 = 10,
	EIntroTutorialSteps__ITS_HUB_LVL4 = 11,
	EIntroTutorialSteps__ITS_HUB_LVL5 = 12,
	EIntroTutorialSteps__ITS_DONE  = 13,
	EIntroTutorialSteps__ITS_MAX   = 14
};



//---------------------------------------------------------------------------
// Script Structs
//---------------------------------------------------------------------------

// ScriptStruct FactoryGame.InventoryStack
// 0x0000
struct FInventoryStack
{


	static FInventoryStack ReadAsMe(uintptr_t address)
	{
		FInventoryStack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInventoryStack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.InventoryItem
// 0x0000
struct FInventoryItem
{


	static FInventoryItem ReadAsMe(uintptr_t address)
	{
		FInventoryItem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInventoryItem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SharedInventoryStatePtr
// 0x0000
struct FSharedInventoryStatePtr
{


	static FSharedInventoryStatePtr ReadAsMe(uintptr_t address)
	{
		FSharedInventoryStatePtr ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSharedInventoryStatePtr& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SaveHeader
// 0x0000
struct FSaveHeader
{


	static FSaveHeader ReadAsMe(uintptr_t address)
	{
		FSaveHeader ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSaveHeader& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.UpdatedFriends
// 0x0000
struct FUpdatedFriends
{


	static FUpdatedFriends ReadAsMe(uintptr_t address)
	{
		FUpdatedFriends ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FUpdatedFriends& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGOnlineFriend
// 0x0000
struct FFGOnlineFriend
{


	static FFGOnlineFriend ReadAsMe(uintptr_t address)
	{
		FFGOnlineFriend ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGOnlineFriend& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PendingInvite
// 0x0000
struct FPendingInvite
{


	static FPendingInvite ReadAsMe(uintptr_t address)
	{
		FPendingInvite ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPendingInvite& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DisabledInputGate
// 0x0000
struct FDisabledInputGate
{


	static FDisabledInputGate ReadAsMe(uintptr_t address)
	{
		FDisabledInputGate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDisabledInputGate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FactoryTickFunction
// 0x5A123C40
struct FFactoryTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FFactoryTickFunction ReadAsMe(uintptr_t address)
	{
		FFactoryTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFactoryTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SpawnerInfo
// 0x0000
struct FSpawnerInfo
{


	static FSpawnerInfo ReadAsMe(uintptr_t address)
	{
		FSpawnerInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSpawnerInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.AnimInstanceProxyTrainDocking
// 0x5A12FD00
struct FAnimInstanceProxyTrainDocking
{
	unsigned char                                      UnknownData00[0x5A12FD00];                                // 0x0000(0x5A12FD00) MISSED OFFSET

	static FAnimInstanceProxyTrainDocking ReadAsMe(uintptr_t address)
	{
		FAnimInstanceProxyTrainDocking ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimInstanceProxyTrainDocking& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.AnimInstanceProxyTruckStation
// 0x5A12FD00
struct FAnimInstanceProxyTruckStation
{
	unsigned char                                      UnknownData00[0x5A12FD00];                                // 0x0000(0x5A12FD00) MISSED OFFSET

	static FAnimInstanceProxyTruckStation ReadAsMe(uintptr_t address)
	{
		FAnimInstanceProxyTruckStation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimInstanceProxyTruckStation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGAnimNode_RandomPlayer
// 0x5A310DC0
struct FFGAnimNode_RandomPlayer
{
	unsigned char                                      UnknownData00[0x5A310DC0];                                // 0x0000(0x5A310DC0) MISSED OFFSET

	static FFGAnimNode_RandomPlayer ReadAsMe(uintptr_t address)
	{
		FFGAnimNode_RandomPlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGAnimNode_RandomPlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ExponentialFogSettings
// 0x0000
struct FExponentialFogSettings
{


	static FExponentialFogSettings ReadAsMe(uintptr_t address)
	{
		FExponentialFogSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FExponentialFogSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.AudioSubtitlePair
// 0x0000
struct FAudioSubtitlePair
{


	static FAudioSubtitlePair ReadAsMe(uintptr_t address)
	{
		FAudioSubtitlePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAudioSubtitlePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ConnectionItemStruct
// 0x0000
struct FConnectionItemStruct
{


	static FConnectionItemStruct ReadAsMe(uintptr_t address)
	{
		FConnectionItemStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConnectionItemStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ConveyorSpaceData
// 0x0000
struct FConveyorSpaceData
{


	static FConveyorSpaceData ReadAsMe(uintptr_t address)
	{
		FConveyorSpaceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConveyorSpaceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ConveyorBeltItems
// 0x0000
struct FConveyorBeltItems
{


	static FConveyorBeltItems ReadAsMe(uintptr_t address)
	{
		FConveyorBeltItems ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConveyorBeltItems& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ConveyorBeltItem
// 0x0000
struct FConveyorBeltItem
{


	static FConveyorBeltItem ReadAsMe(uintptr_t address)
	{
		FConveyorBeltItem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConveyorBeltItem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FoundationSideSelectionFlags
// 0x0000
struct FFoundationSideSelectionFlags
{


	static FFoundationSideSelectionFlags ReadAsMe(uintptr_t address)
	{
		FFoundationSideSelectionFlags ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFoundationSideSelectionFlags& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.StringPair
// 0x0000
struct FStringPair
{


	static FStringPair ReadAsMe(uintptr_t address)
	{
		FStringPair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStringPair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.QuantizedPipelineIndicatorData
// 0x0000
struct FQuantizedPipelineIndicatorData
{


	static FQuantizedPipelineIndicatorData ReadAsMe(uintptr_t address)
	{
		FQuantizedPipelineIndicatorData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FQuantizedPipelineIndicatorData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.QuantizedPumpIndicatorData
// 0x0000
struct FQuantizedPumpIndicatorData
{


	static FQuantizedPumpIndicatorData ReadAsMe(uintptr_t address)
	{
		FQuantizedPumpIndicatorData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FQuantizedPumpIndicatorData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.QuantizedReservoirIndicatorData
// 0x0000
struct FQuantizedReservoirIndicatorData
{


	static FQuantizedReservoirIndicatorData ReadAsMe(uintptr_t address)
	{
		FQuantizedReservoirIndicatorData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FQuantizedReservoirIndicatorData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.RailroadTrackPosition
// 0x0000
struct FRailroadTrackPosition
{


	static FRailroadTrackPosition ReadAsMe(uintptr_t address)
	{
		FRailroadTrackPosition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRailroadTrackPosition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SignWallData
// 0x0000
struct FSignWallData
{


	static FSignWallData ReadAsMe(uintptr_t address)
	{
		FSignWallData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSignWallData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.BuildableGroupTimeData
// 0x0000
struct FBuildableGroupTimeData
{


	static FBuildableGroupTimeData ReadAsMe(uintptr_t address)
	{
		FBuildableGroupTimeData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBuildableGroupTimeData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ConveyorBucket
// 0x0000
struct FConveyorBucket
{


	static FConveyorBucket ReadAsMe(uintptr_t address)
	{
		FConveyorBucket ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConveyorBucket& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SplitterSortRule
// 0x0000
struct FSplitterSortRule
{


	static FSplitterSortRule ReadAsMe(uintptr_t address)
	{
		FSplitterSortRule ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSplitterSortRule& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.BuildableBucket
// 0x0000
struct FBuildableBucket
{


	static FBuildableBucket ReadAsMe(uintptr_t address)
	{
		FBuildableBucket ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBuildableBucket& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DistanceBasedTickRate
// 0x0000
struct FDistanceBasedTickRate
{


	static FDistanceBasedTickRate ReadAsMe(uintptr_t address)
	{
		FDistanceBasedTickRate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDistanceBasedTickRate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.NetConstructionID
// 0x0000
struct FNetConstructionID
{


	static FNetConstructionID ReadAsMe(uintptr_t address)
	{
		FNetConstructionID ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNetConstructionID& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FactoryClearanceData
// 0x0000
struct FFactoryClearanceData
{


	static FFactoryClearanceData ReadAsMe(uintptr_t address)
	{
		FFactoryClearanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFactoryClearanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ConnectionRepresentation
// 0x0000
struct FConnectionRepresentation
{


	static FConnectionRepresentation ReadAsMe(uintptr_t address)
	{
		FConnectionRepresentation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConnectionRepresentation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DismantleRefunds
// 0x0000
struct FDismantleRefunds
{


	static FDismantleRefunds ReadAsMe(uintptr_t address)
	{
		FDismantleRefunds ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDismantleRefunds& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGBuildingColorSlotStruct
// 0x0000
struct FFGBuildingColorSlotStruct
{


	static FFGBuildingColorSlotStruct ReadAsMe(uintptr_t address)
	{
		FFGBuildingColorSlotStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGBuildingColorSlotStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PickedUpInstance
// 0x0000
struct FPickedUpInstance
{


	static FPickedUpInstance ReadAsMe(uintptr_t address)
	{
		FPickedUpInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPickedUpInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FootstepEffectWater
// 0x0000
struct FFootstepEffectWater
{


	static FFootstepEffectWater ReadAsMe(uintptr_t address)
	{
		FFootstepEffectWater ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFootstepEffectWater& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FootstepEffect
// 0x0000
struct FFootstepEffect
{


	static FFootstepEffect ReadAsMe(uintptr_t address)
	{
		FFootstepEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFootstepEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PlayerPipeHyperData
// 0x0000
struct FPlayerPipeHyperData
{


	static FPlayerPipeHyperData ReadAsMe(uintptr_t address)
	{
		FPlayerPipeHyperData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPlayerPipeHyperData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FootstepEffectSurface
// 0x0000
struct FFootstepEffectSurface
{


	static FFootstepEffectSurface ReadAsMe(uintptr_t address)
	{
		FFootstepEffectSurface ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFootstepEffectSurface& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ChatMessageStruct
// 0x0000
struct FChatMessageStruct
{


	static FChatMessageStruct ReadAsMe(uintptr_t address)
	{
		FChatMessageStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FChatMessageStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ConstructHologramMessage
// 0x0000
struct FConstructHologramMessage
{


	static FConstructHologramMessage ReadAsMe(uintptr_t address)
	{
		FConstructHologramMessage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConstructHologramMessage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SimulatedActorTransform
// 0x0000
struct FSimulatedActorTransform
{


	static FSimulatedActorTransform ReadAsMe(uintptr_t address)
	{
		FSimulatedActorTransform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSimulatedActorTransform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SimulatedItemDropTransform
// 0x0000
struct FSimulatedItemDropTransform
{


	static FSimulatedItemDropTransform ReadAsMe(uintptr_t address)
	{
		FSimulatedItemDropTransform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSimulatedItemDropTransform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SimulatedMeshTransform
// 0x0000
struct FSimulatedMeshTransform
{


	static FSimulatedMeshTransform ReadAsMe(uintptr_t address)
	{
		FSimulatedMeshTransform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSimulatedMeshTransform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DebrisItemDrop
// 0x0000
struct FDebrisItemDrop
{


	static FDebrisItemDrop ReadAsMe(uintptr_t address)
	{
		FDebrisItemDrop ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDebrisItemDrop& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DebrisActor
// 0x0000
struct FDebrisActor
{


	static FDebrisActor ReadAsMe(uintptr_t address)
	{
		FDebrisActor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDebrisActor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DebrisMesh
// 0x0000
struct FDebrisMesh
{


	static FDebrisMesh ReadAsMe(uintptr_t address)
	{
		FDebrisMesh ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDebrisMesh& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MoveSpeedPair
// 0x0000
struct FMoveSpeedPair
{


	static FMoveSpeedPair ReadAsMe(uintptr_t address)
	{
		FMoveSpeedPair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMoveSpeedPair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DestroyedFoliageEffectData
// 0x0000
struct FDestroyedFoliageEffectData
{


	static FDestroyedFoliageEffectData ReadAsMe(uintptr_t address)
	{
		FDestroyedFoliageEffectData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDestroyedFoliageEffectData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DropPackage
// 0x0000
struct FDropPackage
{


	static FDropPackage ReadAsMe(uintptr_t address)
	{
		FDropPackage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDropPackage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ItemDrop
// 0x0000
struct FItemDrop
{


	static FItemDrop ReadAsMe(uintptr_t address)
	{
		FItemDrop ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FItemDrop& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.AggroEntry
// 0x0000
struct FAggroEntry
{


	static FAggroEntry ReadAsMe(uintptr_t address)
	{
		FAggroEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAggroEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGEngineCommon
// 0x0000
struct FFGEngineCommon
{


	static FFGEngineCommon ReadAsMe(uintptr_t address)
	{
		FFGEngineCommon ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGEngineCommon& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FeetOffset
// 0x0000
struct FFeetOffset
{


	static FFeetOffset ReadAsMe(uintptr_t address)
	{
		FFeetOffset ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFeetOffset& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ViscosityToPuddlePair
// 0x0000
struct FViscosityToPuddlePair
{


	static FViscosityToPuddlePair ReadAsMe(uintptr_t address)
	{
		FViscosityToPuddlePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FViscosityToPuddlePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.Category
// 0x0000
struct FCategory
{


	static FCategory ReadAsMe(uintptr_t address)
	{
		FCategory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCategory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SpawnData
// 0x0000
struct FSpawnData
{


	static FSpawnData ReadAsMe(uintptr_t address)
	{
		FSpawnData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSpawnData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.AnimInstanceProxyFactory
// 0x5A12FD00
struct FAnimInstanceProxyFactory
{
	unsigned char                                      UnknownData00[0x5A12FD00];                                // 0x0000(0x5A12FD00) MISSED OFFSET

	static FAnimInstanceProxyFactory ReadAsMe(uintptr_t address)
	{
		FAnimInstanceProxyFactory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimInstanceProxyFactory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FluidBox
// 0x0000
struct FFluidBox
{


	static FFluidBox ReadAsMe(uintptr_t address)
	{
		FFluidBox ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFluidBox& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.RemovedInstance
// 0x5A127FC0
struct FRemovedInstance
{
	unsigned char                                      UnknownData00[0x5A127FC0];                                // 0x0000(0x5A127FC0) MISSED OFFSET

	static FRemovedInstance ReadAsMe(uintptr_t address)
	{
		FRemovedInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRemovedInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.Building
// 0x0000
struct FBuilding
{


	static FBuilding ReadAsMe(uintptr_t address)
	{
		FBuilding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBuilding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.RemovedInstanceArray
// 0x5A128140
struct FRemovedInstanceArray
{
	unsigned char                                      UnknownData00[0x5A128140];                                // 0x0000(0x5A128140) MISSED OFFSET

	static FRemovedInstanceArray ReadAsMe(uintptr_t address)
	{
		FRemovedInstanceArray ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRemovedInstanceArray& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGGameNetworkErrorMsg
// 0x0000
struct FFGGameNetworkErrorMsg
{


	static FFGGameNetworkErrorMsg ReadAsMe(uintptr_t address)
	{
		FFGGameNetworkErrorMsg ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGGameNetworkErrorMsg& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGModPackage
// 0x0000
struct FFGModPackage
{


	static FFGModPackage ReadAsMe(uintptr_t address)
	{
		FFGModPackage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGModPackage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.OnJoinSessionData
// 0x0000
struct FOnJoinSessionData
{


	static FOnJoinSessionData ReadAsMe(uintptr_t address)
	{
		FOnJoinSessionData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FOnJoinSessionData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PhaseCost
// 0x0000
struct FPhaseCost
{


	static FPhaseCost ReadAsMe(uintptr_t address)
	{
		FPhaseCost ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPhaseCost& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ItemAmount
// 0x0000
struct FItemAmount
{


	static FItemAmount ReadAsMe(uintptr_t address)
	{
		FItemAmount ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FItemAmount& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PhaseTierInfo
// 0x0000
struct FPhaseTierInfo
{


	static FPhaseTierInfo ReadAsMe(uintptr_t address)
	{
		FPhaseTierInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPhaseTierInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.OptionUpdateDelegateData
// 0x0000
struct FOptionUpdateDelegateData
{


	static FOptionUpdateDelegateData ReadAsMe(uintptr_t address)
	{
		FOptionUpdateDelegateData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FOptionUpdateDelegateData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.AudioVolumeMap
// 0x0000
struct FAudioVolumeMap
{


	static FAudioVolumeMap ReadAsMe(uintptr_t address)
	{
		FAudioVolumeMap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAudioVolumeMap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGKeyMapping
// 0x0000
struct FFGKeyMapping
{


	static FFGKeyMapping ReadAsMe(uintptr_t address)
	{
		FFGKeyMapping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGKeyMapping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.RailroadVehicleInputRate
// 0x0000
struct FRailroadVehicleInputRate
{


	static FRailroadVehicleInputRate ReadAsMe(uintptr_t address)
	{
		FRailroadVehicleInputRate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRailroadVehicleInputRate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ReplicatedRailroadVehicleState
// 0x0000
struct FReplicatedRailroadVehicleState
{


	static FReplicatedRailroadVehicleState ReadAsMe(uintptr_t address)
	{
		FReplicatedRailroadVehicleState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FReplicatedRailroadVehicleState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ItemView
// 0x0000
struct FItemView
{


	static FItemView ReadAsMe(uintptr_t address)
	{
		FItemView ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FItemView& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FogOfWarQueuePair
// 0x0000
struct FFogOfWarQueuePair
{


	static FFogOfWarQueuePair ReadAsMe(uintptr_t address)
	{
		FFogOfWarQueuePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFogOfWarQueuePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.CachedMaterialArray
// 0x0000
struct FCachedMaterialArray
{


	static FCachedMaterialArray ReadAsMe(uintptr_t address)
	{
		FCachedMaterialArray ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedMaterialArray& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ColorMapAreaPair
// 0x0000
struct FColorMapAreaPair
{


	static FColorMapAreaPair ReadAsMe(uintptr_t address)
	{
		FColorMapAreaPair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FColorMapAreaPair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MaterialFlowConnection
// 0x0000
struct FMaterialFlowConnection
{


	static FMaterialFlowConnection ReadAsMe(uintptr_t address)
	{
		FMaterialFlowConnection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialFlowConnection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MaterialFlowGraph
// 0x0000
struct FMaterialFlowGraph
{


	static FMaterialFlowGraph ReadAsMe(uintptr_t address)
	{
		FMaterialFlowGraph ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialFlowGraph& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.CustomFastArraySerializer
// 0x0000
struct FCustomFastArraySerializer
{


	static FCustomFastArraySerializer ReadAsMe(uintptr_t address)
	{
		FCustomFastArraySerializer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCustomFastArraySerializer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.CustomFastArraySerializerItem
// 0x0000
struct FCustomFastArraySerializerItem
{


	static FCustomFastArraySerializerItem ReadAsMe(uintptr_t address)
	{
		FCustomFastArraySerializerItem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCustomFastArraySerializerItem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGHeightData
// 0x0000
struct FFGHeightData
{


	static FFGHeightData ReadAsMe(uintptr_t address)
	{
		FFGHeightData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGHeightData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MaterialFlowNode
// 0x0000
struct FMaterialFlowNode
{


	static FMaterialFlowNode ReadAsMe(uintptr_t address)
	{
		FMaterialFlowNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialFlowNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.UserFeedbackFrontEndData
// 0x0000
struct FUserFeedbackFrontEndData
{


	static FUserFeedbackFrontEndData ReadAsMe(uintptr_t address)
	{
		FUserFeedbackFrontEndData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FUserFeedbackFrontEndData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.OnlinePresence
// 0x0000
struct FOnlinePresence
{


	static FOnlinePresence ReadAsMe(uintptr_t address)
	{
		FOnlinePresence ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FOnlinePresence& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.NewsFeedMap
// 0x0000
struct FNewsFeedMap
{


	static FNewsFeedMap ReadAsMe(uintptr_t address)
	{
		FNewsFeedMap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNewsFeedMap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ScannableDetails
// 0x0000
struct FScannableDetails
{


	static FScannableDetails ReadAsMe(uintptr_t address)
	{
		FScannableDetails ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FScannableDetails& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.CostIngredientEffectActorInfo
// 0x0000
struct FCostIngredientEffectActorInfo
{


	static FCostIngredientEffectActorInfo ReadAsMe(uintptr_t address)
	{
		FCostIngredientEffectActorInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCostIngredientEffectActorInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.FGOnlineSessionSettings
// 0x0000
struct FFGOnlineSessionSettings
{


	static FFGOnlineSessionSettings ReadAsMe(uintptr_t address)
	{
		FFGOnlineSessionSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFGOnlineSessionSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.AxisMappingDisplayName
// 0x0000
struct FAxisMappingDisplayName
{


	static FAxisMappingDisplayName ReadAsMe(uintptr_t address)
	{
		FAxisMappingDisplayName ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAxisMappingDisplayName& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ActionMappingDisplayName
// 0x0000
struct FActionMappingDisplayName
{


	static FActionMappingDisplayName ReadAsMe(uintptr_t address)
	{
		FActionMappingDisplayName ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FActionMappingDisplayName& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.OptionRowData
// 0x0000
struct FOptionRowData
{


	static FOptionRowData ReadAsMe(uintptr_t address)
	{
		FOptionRowData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FOptionRowData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.CachedSplineMeshToMaterialObject
// 0x0000
struct FCachedSplineMeshToMaterialObject
{


	static FCachedSplineMeshToMaterialObject ReadAsMe(uintptr_t address)
	{
		FCachedSplineMeshToMaterialObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedSplineMeshToMaterialObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.CachedMaterialInterfaceArray
// 0x0000
struct FCachedMaterialInterfaceArray
{


	static FCachedMaterialInterfaceArray ReadAsMe(uintptr_t address)
	{
		FCachedMaterialInterfaceArray ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedMaterialInterfaceArray& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.CachedMeshToMaterialObject
// 0x0000
struct FCachedMeshToMaterialObject
{


	static FCachedMeshToMaterialObject ReadAsMe(uintptr_t address)
	{
		FCachedMeshToMaterialObject ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedMeshToMaterialObject& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SplineSupportPair
// 0x0000
struct FSplineSupportPair
{


	static FSplineSupportPair ReadAsMe(uintptr_t address)
	{
		FSplineSupportPair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSplineSupportPair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PresetHotbar
// 0x0000
struct FPresetHotbar
{


	static FPresetHotbar ReadAsMe(uintptr_t address)
	{
		FPresetHotbar ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPresetHotbar& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MessageData
// 0x0000
struct FMessageData
{


	static FMessageData ReadAsMe(uintptr_t address)
	{
		FMessageData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMessageData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SlotData
// 0x0000
struct FSlotData
{


	static FSlotData ReadAsMe(uintptr_t address)
	{
		FSlotData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSlotData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PoleHeightMesh
// 0x0000
struct FPoleHeightMesh
{


	static FPoleHeightMesh ReadAsMe(uintptr_t address)
	{
		FPoleHeightMesh ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPoleHeightMesh& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PopupData
// 0x0000
struct FPopupData
{


	static FPopupData ReadAsMe(uintptr_t address)
	{
		FPopupData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPopupData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.Hotbar
// 0x0000
struct FHotbar
{


	static FHotbar ReadAsMe(uintptr_t address)
	{
		FHotbar ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FHotbar& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PowerCircuitStats
// 0x0000
struct FPowerCircuitStats
{


	static FPowerCircuitStats ReadAsMe(uintptr_t address)
	{
		FPowerCircuitStats ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPowerCircuitStats& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PowerGraphPoint
// 0x0000
struct FPowerGraphPoint
{


	static FPowerGraphPoint ReadAsMe(uintptr_t address)
	{
		FPowerGraphPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPowerGraphPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.RadiationVisualization
// 0x0000
struct FRadiationVisualization
{


	static FRadiationVisualization ReadAsMe(uintptr_t address)
	{
		FRadiationVisualization ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRadiationVisualization& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MapAreaParticleCollection
// 0x0000
struct FMapAreaParticleCollection
{


	static FMapAreaParticleCollection ReadAsMe(uintptr_t address)
	{
		FMapAreaParticleCollection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMapAreaParticleCollection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SetEmitterID
// 0x0000
struct FSetEmitterID
{


	static FSetEmitterID ReadAsMe(uintptr_t address)
	{
		FSetEmitterID ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSetEmitterID& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.RemoveEmitterID
// 0x0000
struct FRemoveEmitterID
{


	static FRemoveEmitterID ReadAsMe(uintptr_t address)
	{
		FRemoveEmitterID ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRemoveEmitterID& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.RadioactiveSource
// 0x0000
struct FRadioactiveSource
{


	static FRadioactiveSource ReadAsMe(uintptr_t address)
	{
		FRadioactiveSource ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRadioactiveSource& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TrackGraph
// 0x0000
struct FTrackGraph
{


	static FTrackGraph ReadAsMe(uintptr_t address)
	{
		FTrackGraph ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTrackGraph& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TimeTableStop
// 0x0000
struct FTimeTableStop
{


	static FTimeTableStop ReadAsMe(uintptr_t address)
	{
		FTimeTableStop ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimeTableStop& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.AnimInstanceProxyRailRoadVehicle
// 0x5A12FD00
struct FAnimInstanceProxyRailRoadVehicle
{
	unsigned char                                      UnknownData00[0x5A12FD00];                                // 0x0000(0x5A12FD00) MISSED OFFSET

	static FAnimInstanceProxyRailRoadVehicle ReadAsMe(uintptr_t address)
	{
		FAnimInstanceProxyRailRoadVehicle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimInstanceProxyRailRoadVehicle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.CouplerSetup
// 0x0000
struct FCouplerSetup
{


	static FCouplerSetup ReadAsMe(uintptr_t address)
	{
		FCouplerSetup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCouplerSetup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.WheelsetSetup
// 0x0000
struct FWheelsetSetup
{


	static FWheelsetSetup ReadAsMe(uintptr_t address)
	{
		FWheelsetSetup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FWheelsetSetup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ConnectionAlwaysRelevant_NodePair
// 0x0000
struct FConnectionAlwaysRelevant_NodePair
{


	static FConnectionAlwaysRelevant_NodePair ReadAsMe(uintptr_t address)
	{
		FConnectionAlwaysRelevant_NodePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConnectionAlwaysRelevant_NodePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ResearchTime
// 0x0000
struct FResearchTime
{


	static FResearchTime ReadAsMe(uintptr_t address)
	{
		FResearchTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FResearchTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ResearchData
// 0x0000
struct FResearchData
{


	static FResearchData ReadAsMe(uintptr_t address)
	{
		FResearchData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FResearchData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ResearchRecipeReward
// 0x0000
struct FResearchRecipeReward
{


	static FResearchRecipeReward ReadAsMe(uintptr_t address)
	{
		FResearchRecipeReward ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FResearchRecipeReward& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PurityTextPair
// 0x0000
struct FPurityTextPair
{


	static FPurityTextPair ReadAsMe(uintptr_t address)
	{
		FPurityTextPair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPurityTextPair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.NodeClusterData
// 0x0000
struct FNodeClusterData
{


	static FNodeClusterData ReadAsMe(uintptr_t address)
	{
		FNodeClusterData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNodeClusterData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ResourceDepositPackage
// 0x0000
struct FResourceDepositPackage
{


	static FResourceDepositPackage ReadAsMe(uintptr_t address)
	{
		FResourceDepositPackage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FResourceDepositPackage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ItemSettings
// 0x0000
struct FItemSettings
{


	static FItemSettings ReadAsMe(uintptr_t address)
	{
		FItemSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FItemSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ResourceSinkPointsData
// 0x5A314A80
struct FResourceSinkPointsData
{
	unsigned char                                      UnknownData00[0x5A314A80];                                // 0x0000(0x5A314A80) MISSED OFFSET

	static FResourceSinkPointsData ReadAsMe(uintptr_t address)
	{
		FResourceSinkPointsData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FResourceSinkPointsData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ResourceSinkRewardLevelsData
// 0x5A314A80
struct FResourceSinkRewardLevelsData
{
	unsigned char                                      UnknownData00[0x5A314A80];                                // 0x0000(0x5A314A80) MISSED OFFSET

	static FResourceSinkRewardLevelsData ReadAsMe(uintptr_t address)
	{
		FResourceSinkRewardLevelsData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FResourceSinkRewardLevelsData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MapRedirector
// 0x0000
struct FMapRedirector
{


	static FMapRedirector ReadAsMe(uintptr_t address)
	{
		FMapRedirector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMapRedirector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SessionSaveStruct
// 0x0000
struct FSessionSaveStruct
{


	static FSessionSaveStruct ReadAsMe(uintptr_t address)
	{
		FSessionSaveStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSessionSaveStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MultipleItemStruct
// 0x0000
struct FMultipleItemStruct
{


	static FMultipleItemStruct ReadAsMe(uintptr_t address)
	{
		FMultipleItemStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMultipleItemStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SchematicCost
// 0x0000
struct FSchematicCost
{


	static FSchematicCost ReadAsMe(uintptr_t address)
	{
		FSchematicCost ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSchematicCost& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.GainSignificanceData
// 0x0000
struct FGainSignificanceData
{


	static FGainSignificanceData ReadAsMe(uintptr_t address)
	{
		FGainSignificanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGainSignificanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SignData
// 0x0000
struct FSignData
{


	static FSignData ReadAsMe(uintptr_t address)
	{
		FSignData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSignData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SignPixelColumn
// 0x0000
struct FSignPixelColumn
{


	static FSignPixelColumn ReadAsMe(uintptr_t address)
	{
		FSignPixelColumn ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSignPixelColumn& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SignElementConstraints
// 0x0000
struct FSignElementConstraints
{


	static FSignElementConstraints ReadAsMe(uintptr_t address)
	{
		FSignElementConstraints ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSignElementConstraints& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SignColorData
// 0x0000
struct FSignColorData
{


	static FSignColorData ReadAsMe(uintptr_t address)
	{
		FSignColorData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSignColorData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SkySphereSettings
// 0x0000
struct FSkySphereSettings
{


	static FSkySphereSettings ReadAsMe(uintptr_t address)
	{
		FSkySphereSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkySphereSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ResearchTreeMessageData
// 0x0000
struct FResearchTreeMessageData
{


	static FResearchTreeMessageData ReadAsMe(uintptr_t address)
	{
		FResearchTreeMessageData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FResearchTreeMessageData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SchematicMessagePair
// 0x0000
struct FSchematicMessagePair
{


	static FSchematicMessagePair ReadAsMe(uintptr_t address)
	{
		FSchematicMessagePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSchematicMessagePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ItemFoundData
// 0x0000
struct FItemFoundData
{


	static FItemFoundData ReadAsMe(uintptr_t address)
	{
		FItemFoundData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FItemFoundData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MapAreaVisitedData
// 0x0000
struct FMapAreaVisitedData
{


	static FMapAreaVisitedData ReadAsMe(uintptr_t address)
	{
		FMapAreaVisitedData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMapAreaVisitedData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.MaterialAndSlotName
// 0x0000
struct FMaterialAndSlotName
{


	static FMaterialAndSlotName ReadAsMe(uintptr_t address)
	{
		FMaterialAndSlotName ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialAndSlotName& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TrainSimulationData
// 0x0000
struct FTrainSimulationData
{


	static FTrainSimulationData ReadAsMe(uintptr_t address)
	{
		FTrainSimulationData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTrainSimulationData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TrainSelfDrivingData
// 0x0000
struct FTrainSelfDrivingData
{


	static FTrainSelfDrivingData ReadAsMe(uintptr_t address)
	{
		FTrainSelfDrivingData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTrainSelfDrivingData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TrainConsist
// 0x0000
struct FTrainConsist
{


	static FTrainConsist ReadAsMe(uintptr_t address)
	{
		FTrainConsist ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTrainConsist& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TutorialHintData
// 0x0000
struct FTutorialHintData
{


	static FTutorialHintData ReadAsMe(uintptr_t address)
	{
		FTutorialHintData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTutorialHintData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.RecipeAmountPair
// 0x0000
struct FRecipeAmountPair
{


	static FRecipeAmountPair ReadAsMe(uintptr_t address)
	{
		FRecipeAmountPair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRecipeAmountPair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TutorialData
// 0x0000
struct FTutorialData
{


	static FTutorialData ReadAsMe(uintptr_t address)
	{
		FTutorialData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTutorialData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.UseState
// 0x0000
struct FUseState
{


	static FUseState ReadAsMe(uintptr_t address)
	{
		FUseState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FUseState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TrainAtcData
// 0x0000
struct FTrainAtcData
{


	static FTrainAtcData ReadAsMe(uintptr_t address)
	{
		FTrainAtcData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTrainAtcData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TrainAtcPoint
// 0x0000
struct FTrainAtcPoint
{


	static FTrainAtcPoint ReadAsMe(uintptr_t address)
	{
		FTrainAtcPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTrainAtcPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.VehicleSeat
// 0x0000
struct FVehicleSeat
{


	static FVehicleSeat ReadAsMe(uintptr_t address)
	{
		FVehicleSeat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVehicleSeat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PawnImpactAudio
// 0x0000
struct FPawnImpactAudio
{


	static FPawnImpactAudio ReadAsMe(uintptr_t address)
	{
		FPawnImpactAudio ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPawnImpactAudio& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.VehiclePhysicsData
// 0x0000
struct FVehiclePhysicsData
{


	static FVehiclePhysicsData ReadAsMe(uintptr_t address)
	{
		FVehiclePhysicsData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVehiclePhysicsData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ProjectileData
// 0x0000
struct FProjectileData
{


	static FProjectileData ReadAsMe(uintptr_t address)
	{
		FProjectileData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FProjectileData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TireParticleCollection
// 0x0000
struct FTireParticleCollection
{


	static FTireParticleCollection ReadAsMe(uintptr_t address)
	{
		FTireParticleCollection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTireParticleCollection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ParticleTemplatePair
// 0x0000
struct FParticleTemplatePair
{


	static FParticleTemplatePair ReadAsMe(uintptr_t address)
	{
		FParticleTemplatePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleTemplatePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ReplicatedAddedVelocitiesState
// 0x0000
struct FReplicatedAddedVelocitiesState
{


	static FReplicatedAddedVelocitiesState ReadAsMe(uintptr_t address)
	{
		FReplicatedAddedVelocitiesState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FReplicatedAddedVelocitiesState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.SurfaceParticlePair
// 0x0000
struct FSurfaceParticlePair
{


	static FSurfaceParticlePair ReadAsMe(uintptr_t address)
	{
		FSurfaceParticlePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSurfaceParticlePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TireTrackDecalDetails
// 0x0000
struct FTireTrackDecalDetails
{


	static FTireTrackDecalDetails ReadAsMe(uintptr_t address)
	{
		FTireTrackDecalDetails ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTireTrackDecalDetails& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.TireData
// 0x0000
struct FTireData
{


	static FTireData ReadAsMe(uintptr_t address)
	{
		FTireData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTireData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.VehicleTransmissionData6W
// 0x0000
struct FVehicleTransmissionData6W
{


	static FVehicleTransmissionData6W ReadAsMe(uintptr_t address)
	{
		FVehicleTransmissionData6W ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVehicleTransmissionData6W& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.VehicleGearData6W
// 0x0000
struct FVehicleGearData6W
{


	static FVehicleGearData6W ReadAsMe(uintptr_t address)
	{
		FVehicleGearData6W ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVehicleGearData6W& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.VehicleEngineData6W
// 0x0000
struct FVehicleEngineData6W
{


	static FVehicleEngineData6W ReadAsMe(uintptr_t address)
	{
		FVehicleEngineData6W ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVehicleEngineData6W& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.VehicleDifferential6WData
// 0x0000
struct FVehicleDifferential6WData
{


	static FVehicleDifferential6WData ReadAsMe(uintptr_t address)
	{
		FVehicleDifferential6WData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVehicleDifferential6WData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.DifferentialSetup6W
// 0x0000
struct FDifferentialSetup6W
{


	static FDifferentialSetup6W ReadAsMe(uintptr_t address)
	{
		FDifferentialSetup6W ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDifferentialSetup6W& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.ItemDropWithChance
// 0x0000
struct FItemDropWithChance
{


	static FItemDropWithChance ReadAsMe(uintptr_t address)
	{
		FItemDropWithChance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FItemDropWithChance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.PlayerPresenceState
// 0x0000
struct FPlayerPresenceState
{


	static FPlayerPresenceState ReadAsMe(uintptr_t address)
	{
		FPlayerPresenceState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPlayerPresenceState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct FactoryGame.Errors
// 0x0000
struct FErrors
{


	static FErrors ReadAsMe(uintptr_t address)
	{
		FErrors ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FErrors& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
