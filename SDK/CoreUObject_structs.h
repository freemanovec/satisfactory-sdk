#pragma once

#include "../Utils.h"
// Name: Satisfactory, Version: 1.0.0

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace SDK
{
//---------------------------------------------------------------------------
// Enums
//---------------------------------------------------------------------------

// Enum CoreUObject.EInterpCurveMode
enum class EInterpCurveMode : uint8_t
{
	CIM_Linear                     = 0,
	CIM_CurveAuto                  = 1,
	CIM_Constant                   = 2,
	CIM_CurveUser                  = 3,
	CIM_CurveBreak                 = 4,
	CIM_CurveAutoClamped           = 5,
	CIM_MAX                        = 6
};


// Enum CoreUObject.ERangeBoundTypes
enum class ERangeBoundTypes : uint8_t
{
	ERangeBoundTypes__Exclusive    = 0,
	ERangeBoundTypes__Inclusive    = 1,
	ERangeBoundTypes__Open         = 2,
	ERangeBoundTypes__ERangeBoundTypes_MAX = 3
};


// Enum CoreUObject.ELocalizedTextSourceCategory
enum class ELocalizedTextSourceCategory : uint8_t
{
	ELocalizedTextSourceCategory__Game = 0,
	ELocalizedTextSourceCategory__Engine = 1,
	ELocalizedTextSourceCategory__Editor = 2,
	ELocalizedTextSourceCategory__ELocalizedTextSourceCategory_MAX = 3
};


// Enum CoreUObject.EAutomationEventType
enum class EAutomationEventType : uint8_t
{
	EAutomationEventType__Info     = 0,
	EAutomationEventType__Warning  = 1,
	EAutomationEventType__Error    = 2,
	EAutomationEventType__EAutomationEventType_MAX = 3
};


// Enum CoreUObject.EMouseCursor
enum class EMouseCursor : uint8_t
{
	EMouseCursor__None             = 0,
	EMouseCursor__Default          = 1,
	EMouseCursor__TextEditBeam     = 2,
	EMouseCursor__ResizeLeftRight  = 3,
	EMouseCursor__ResizeUpDown     = 4,
	EMouseCursor__ResizeSouthEast  = 5,
	EMouseCursor__ResizeSouthWest  = 6,
	EMouseCursor__CardinalCross    = 7,
	EMouseCursor__Crosshairs       = 8,
	EMouseCursor__Hand             = 9,
	EMouseCursor__GrabHand         = 10,
	EMouseCursor__GrabHandClosed   = 11,
	EMouseCursor__SlashedCircle    = 12,
	EMouseCursor__EyeDropper       = 13,
	EMouseCursor__EMouseCursor_MAX = 14
};


// Enum CoreUObject.ELifetimeCondition
enum class ELifetimeCondition : uint8_t
{
	COND_None                      = 0,
	COND_InitialOnly               = 1,
	COND_OwnerOnly                 = 2,
	COND_SkipOwner                 = 3,
	COND_SimulatedOnly             = 4,
	COND_AutonomousOnly            = 5,
	COND_SimulatedOrPhysics        = 6,
	COND_InitialOrOwner            = 7,
	COND_Custom                    = 8,
	COND_ReplayOrOwner             = 9,
	COND_ReplayOnly                = 10,
	COND_SimulatedOnlyNoReplay     = 11,
	COND_SimulatedOrPhysicsNoReplay = 12,
	COND_SkipReplay                = 13,
	COND_Max                       = 14
};


// Enum CoreUObject.EUnit
enum class EUnit : uint8_t
{
	EUnit__Micrometers             = 0,
	EUnit__Millimeters             = 1,
	EUnit__Centimeters             = 2,
	EUnit__Meters                  = 3,
	EUnit__Kilometers              = 4,
	EUnit__Inches                  = 5,
	EUnit__Feet                    = 6,
	EUnit__Yards                   = 7,
	EUnit__Miles                   = 8,
	EUnit__Lightyears              = 9,
	EUnit__Degrees                 = 10,
	EUnit__Radians                 = 11,
	EUnit__MetersPerSecond         = 12,
	EUnit__KilometersPerHour       = 13,
	EUnit__MilesPerHour            = 14,
	EUnit__Celsius                 = 15,
	EUnit__Farenheit               = 16,
	EUnit__Kelvin                  = 17,
	EUnit__Micrograms              = 18,
	EUnit__Milligrams              = 19,
	EUnit__Grams                   = 20,
	EUnit__Kilograms               = 21,
	EUnit__MetricTons              = 22,
	EUnit__Ounces                  = 23,
	EUnit__Pounds                  = 24,
	EUnit__Stones                  = 25,
	EUnit__Newtons                 = 26,
	EUnit__PoundsForce             = 27,
	EUnit__KilogramsForce          = 28,
	EUnit__Hertz                   = 29,
	EUnit__Kilohertz               = 30,
	EUnit__Megahertz               = 31,
	EUnit__Gigahertz               = 32,
	EUnit__RevolutionsPerMinute    = 33,
	EUnit__Bytes                   = 34,
	EUnit__Kilobytes               = 35,
	EUnit__Megabytes               = 36,
	EUnit__Gigabytes               = 37,
	EUnit__Terabytes               = 38,
	EUnit__Lumens                  = 39,
	EUnit__Milliseconds            = 40,
	EUnit__Seconds                 = 41,
	EUnit__Minutes                 = 42,
	EUnit__Hours                   = 43,
	EUnit__Days                    = 44,
	EUnit__Months                  = 45,
	EUnit__Years                   = 46,
	EUnit__Multiplier              = 47,
	EUnit__Percentage              = 48,
	EUnit__Unspecified             = 49,
	EUnit__EUnit_MAX               = 50
};


// Enum CoreUObject.ELogTimes
enum class ELogTimes : uint8_t
{
	ELogTimes__None                = 0,
	ELogTimes__UTC                 = 1,
	ELogTimes__SinceGStartTime     = 2,
	ELogTimes__Local               = 3,
	ELogTimes__ELogTimes_MAX       = 4
};


// Enum CoreUObject.EPixelFormat
enum class EPixelFormat : uint8_t
{
	PF_Unknown                     = 0,
	PF_A32B32G32R32F               = 1,
	PF_B8G8R8A8                    = 2,
	PF_G8                          = 3,
	PF_G16                         = 4,
	PF_DXT1                        = 5,
	PF_DXT3                        = 6,
	PF_DXT5                        = 7,
	PF_UYVY                        = 8,
	PF_FloatRGB                    = 9,
	PF_FloatRGBA                   = 10,
	PF_DepthStencil                = 11,
	PF_ShadowDepth                 = 12,
	PF_R32_FLOAT                   = 13,
	PF_G16R16                      = 14,
	PF_G16R16F                     = 15,
	PF_G16R16F_FILTER              = 16,
	PF_G32R32F                     = 17,
	PF_A2B10G10R10                 = 18,
	PF_A16B16G16R16                = 19,
	PF_D24                         = 20,
	PF_R16F                        = 21,
	PF_R16F_FILTER                 = 22,
	PF_BC5                         = 23,
	PF_V8U8                        = 24,
	PF_A1                          = 25,
	PF_FloatR11G11B10              = 26,
	PF_A8                          = 27,
	PF_R32_UINT                    = 28,
	PF_R32_SINT                    = 29,
	PF_PVRTC2                      = 30,
	PF_PVRTC4                      = 31,
	PF_R16_UINT                    = 32,
	PF_R16_SINT                    = 33,
	PF_R16G16B16A16_UINT           = 34,
	PF_R16G16B16A16_SINT           = 35,
	PF_R5G6B5_UNORM                = 36,
	PF_R8G8B8A8                    = 37,
	PF_A8R8G8B8                    = 38,
	PF_BC4                         = 39,
	PF_R8G8                        = 40,
	PF_ATC_RGB                     = 41,
	PF_ATC_RGBA_E                  = 42,
	PF_ATC_RGBA_I                  = 43,
	PF_X24_G8                      = 44,
	PF_ETC1                        = 45,
	PF_ETC2_RGB                    = 46,
	PF_ETC2_RGBA                   = 47,
	PF_R32G32B32A32_UINT           = 48,
	PF_R16G16_UINT                 = 49,
	PF_ASTC_4x4                    = 50,
	PF_ASTC_6x6                    = 51,
	PF_ASTC_8x8                    = 52,
	PF_ASTC_10x10                  = 53,
	PF_ASTC_12x12                  = 54,
	PF_BC6H                        = 55,
	PF_BC7                         = 56,
	PF_R8_UINT                     = 57,
	PF_L8                          = 58,
	PF_XGXR8                       = 59,
	PF_R8G8B8A8_UINT               = 60,
	PF_R8G8B8A8_SNORM              = 61,
	PF_R16G16B16A16_UNORM          = 62,
	PF_R16G16B16A16_SNORM          = 63,
	PF_PLATFORM_HDR                = 64,
	PF_PLATFORM_HDR01              = 65,
	PF_PLATFORM_HDR02              = 66,
	PF_NV12                        = 67,
	PF_MAX                         = 68
};


// Enum CoreUObject.EAxis
enum class EAxis : uint8_t
{
	EAxis__None                    = 0,
	EAxis__X                       = 1,
	EAxis__Y                       = 2,
	EAxis__Z                       = 3,
	EAxis__EAxis_MAX               = 4
};


// Enum CoreUObject.ESearchDir
enum class ESearchDir : uint8_t
{
	ESearchDir__FromStart          = 0,
	ESearchDir__FromEnd            = 1,
	ESearchDir__ESearchDir_MAX     = 2
};


// Enum CoreUObject.ESearchCase
enum class ESearchCase : uint8_t
{
	ESearchCase__CaseSensitive     = 0,
	ESearchCase__IgnoreCase        = 1,
	ESearchCase__ESearchCase_MAX   = 2
};



//---------------------------------------------------------------------------
// Script Structs
//---------------------------------------------------------------------------

// ScriptStruct CoreUObject.JoinabilitySettings
// 0x0000
struct FJoinabilitySettings
{


	static FJoinabilitySettings ReadAsMe(uintptr_t address)
	{
		FJoinabilitySettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FJoinabilitySettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Guid
// 0x0000
struct FGuid
{


	static FGuid ReadAsMe(uintptr_t address)
	{
		FGuid ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGuid& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Vector
// 0x0000
struct FVector
{


	static FVector ReadAsMe(uintptr_t address)
	{
		FVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.UniqueNetIdWrapper
// 0x0000
struct FUniqueNetIdWrapper
{


	static FUniqueNetIdWrapper ReadAsMe(uintptr_t address)
	{
		FUniqueNetIdWrapper ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FUniqueNetIdWrapper& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Vector4
// 0x0000
struct alignas(16) FVector4
{


	static FVector4 ReadAsMe(uintptr_t address)
	{
		FVector4 ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector4& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Vector2D
// 0x0000
struct FVector2D
{


	static FVector2D ReadAsMe(uintptr_t address)
	{
		FVector2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

	inline FVector2D()
		: X(0), Y(0)
	{ }

	inline FVector2D(float x, float y)
		: X(x),
		  Y(y)
	{ }

};

// ScriptStruct CoreUObject.TwoVectors
// 0x0000
struct FTwoVectors
{


	static FTwoVectors ReadAsMe(uintptr_t address)
	{
		FTwoVectors ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTwoVectors& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Plane
// 0x59ECFDC0
struct alignas(16) FPlane
{
	unsigned char                                      UnknownData00[0x59ECFDC0];                                // 0x0000(0x59ECFDC0) MISSED OFFSET

	static FPlane ReadAsMe(uintptr_t address)
	{
		FPlane ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPlane& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Rotator
// 0x0000
struct FRotator
{


	static FRotator ReadAsMe(uintptr_t address)
	{
		FRotator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRotator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Quat
// 0x0000
struct alignas(16) FQuat
{


	static FQuat ReadAsMe(uintptr_t address)
	{
		FQuat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FQuat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.PackedNormal
// 0x0000
struct FPackedNormal
{


	static FPackedNormal ReadAsMe(uintptr_t address)
	{
		FPackedNormal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPackedNormal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.PackedRGB10A2N
// 0x0000
struct FPackedRGB10A2N
{


	static FPackedRGB10A2N ReadAsMe(uintptr_t address)
	{
		FPackedRGB10A2N ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPackedRGB10A2N& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.IntPoint
// 0x0000
struct FIntPoint
{


	static FIntPoint ReadAsMe(uintptr_t address)
	{
		FIntPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FIntPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.IntVector
// 0x0000
struct FIntVector
{


	static FIntVector ReadAsMe(uintptr_t address)
	{
		FIntVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FIntVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.PackedRGBA16N
// 0x0000
struct FPackedRGBA16N
{


	static FPackedRGBA16N ReadAsMe(uintptr_t address)
	{
		FPackedRGBA16N ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPackedRGBA16N& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Color
// 0x0000
struct FColor
{


	static FColor ReadAsMe(uintptr_t address)
	{
		FColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.LinearColor
// 0x0000
struct FLinearColor
{


	static FLinearColor ReadAsMe(uintptr_t address)
	{
		FLinearColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLinearColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

	inline FLinearColor()
		: R(0), G(0), B(0), A(0)
	{ }

	inline FLinearColor(float r, float g, float b, float a)
		: R(r),
		  G(g),
		  B(b),
		  A(a)
	{ }

};

// ScriptStruct CoreUObject.Box
// 0x0000
struct FBox
{


	static FBox ReadAsMe(uintptr_t address)
	{
		FBox ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBox& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Box2D
// 0x0000
struct FBox2D
{


	static FBox2D ReadAsMe(uintptr_t address)
	{
		FBox2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBox2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.BoxSphereBounds
// 0x0000
struct FBoxSphereBounds
{


	static FBoxSphereBounds ReadAsMe(uintptr_t address)
	{
		FBoxSphereBounds ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBoxSphereBounds& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.OrientedBox
// 0x0000
struct FOrientedBox
{


	static FOrientedBox ReadAsMe(uintptr_t address)
	{
		FOrientedBox ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FOrientedBox& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurvePointFloat
// 0x0000
struct FInterpCurvePointFloat
{


	static FInterpCurvePointFloat ReadAsMe(uintptr_t address)
	{
		FInterpCurvePointFloat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurvePointFloat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurveFloat
// 0x0000
struct FInterpCurveFloat
{


	static FInterpCurveFloat ReadAsMe(uintptr_t address)
	{
		FInterpCurveFloat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurveFloat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurvePointVector2D
// 0x0000
struct FInterpCurvePointVector2D
{


	static FInterpCurvePointVector2D ReadAsMe(uintptr_t address)
	{
		FInterpCurvePointVector2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurvePointVector2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurveVector2D
// 0x0000
struct FInterpCurveVector2D
{


	static FInterpCurveVector2D ReadAsMe(uintptr_t address)
	{
		FInterpCurveVector2D ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurveVector2D& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurvePointVector
// 0x0000
struct FInterpCurvePointVector
{


	static FInterpCurvePointVector ReadAsMe(uintptr_t address)
	{
		FInterpCurvePointVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurvePointVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurveVector
// 0x0000
struct FInterpCurveVector
{


	static FInterpCurveVector ReadAsMe(uintptr_t address)
	{
		FInterpCurveVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurveVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Matrix
// 0x0000
struct FMatrix
{


	static FMatrix ReadAsMe(uintptr_t address)
	{
		FMatrix ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMatrix& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurveQuat
// 0x0000
struct FInterpCurveQuat
{


	static FInterpCurveQuat ReadAsMe(uintptr_t address)
	{
		FInterpCurveQuat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurveQuat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurvePointTwoVectors
// 0x0000
struct FInterpCurvePointTwoVectors
{


	static FInterpCurvePointTwoVectors ReadAsMe(uintptr_t address)
	{
		FInterpCurvePointTwoVectors ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurvePointTwoVectors& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurveTwoVectors
// 0x0000
struct FInterpCurveTwoVectors
{


	static FInterpCurveTwoVectors ReadAsMe(uintptr_t address)
	{
		FInterpCurveTwoVectors ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurveTwoVectors& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurvePointLinearColor
// 0x0000
struct FInterpCurvePointLinearColor
{


	static FInterpCurvePointLinearColor ReadAsMe(uintptr_t address)
	{
		FInterpCurvePointLinearColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurvePointLinearColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurveLinearColor
// 0x0000
struct FInterpCurveLinearColor
{


	static FInterpCurveLinearColor ReadAsMe(uintptr_t address)
	{
		FInterpCurveLinearColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurveLinearColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Transform
// 0x0000
struct alignas(16) FTransform
{


	static FTransform ReadAsMe(uintptr_t address)
	{
		FTransform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTransform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.InterpCurvePointQuat
// 0x0000
struct FInterpCurvePointQuat
{


	static FInterpCurvePointQuat ReadAsMe(uintptr_t address)
	{
		FInterpCurvePointQuat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpCurvePointQuat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.DateTime
// 0x0000
struct FDateTime
{


	static FDateTime ReadAsMe(uintptr_t address)
	{
		FDateTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDateTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.FrameNumber
// 0x0000
struct FFrameNumber
{


	static FFrameNumber ReadAsMe(uintptr_t address)
	{
		FFrameNumber ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFrameNumber& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.RandomStream
// 0x0000
struct FRandomStream
{


	static FRandomStream ReadAsMe(uintptr_t address)
	{
		FRandomStream ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRandomStream& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.FrameRate
// 0x0000
struct FFrameRate
{


	static FFrameRate ReadAsMe(uintptr_t address)
	{
		FFrameRate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFrameRate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.FrameTime
// 0x0000
struct FFrameTime
{


	static FFrameTime ReadAsMe(uintptr_t address)
	{
		FFrameTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFrameTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.QualifiedFrameTime
// 0x0000
struct FQualifiedFrameTime
{


	static FQualifiedFrameTime ReadAsMe(uintptr_t address)
	{
		FQualifiedFrameTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FQualifiedFrameTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Timecode
// 0x0000
struct FTimecode
{


	static FTimecode ReadAsMe(uintptr_t address)
	{
		FTimecode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimecode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.SoftClassPath
// 0x5A120100
struct FSoftClassPath
{
	unsigned char                                      UnknownData00[0x5A120100];                                // 0x0000(0x5A120100) MISSED OFFSET

	static FSoftClassPath ReadAsMe(uintptr_t address)
	{
		FSoftClassPath ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoftClassPath& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.SoftObjectPath
// 0x0000
struct FSoftObjectPath
{


	static FSoftObjectPath ReadAsMe(uintptr_t address)
	{
		FSoftObjectPath ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoftObjectPath& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.PrimaryAssetType
// 0x0000
struct FPrimaryAssetType
{


	static FPrimaryAssetType ReadAsMe(uintptr_t address)
	{
		FPrimaryAssetType ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimaryAssetType& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.PrimaryAssetId
// 0x0000
struct FPrimaryAssetId
{


	static FPrimaryAssetId ReadAsMe(uintptr_t address)
	{
		FPrimaryAssetId ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimaryAssetId& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.FallbackStruct
// 0x0000
struct FFallbackStruct
{


	static FFallbackStruct ReadAsMe(uintptr_t address)
	{
		FFallbackStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFallbackStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.FloatRangeBound
// 0x0000
struct FFloatRangeBound
{


	static FFloatRangeBound ReadAsMe(uintptr_t address)
	{
		FFloatRangeBound ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFloatRangeBound& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Timespan
// 0x0000
struct FTimespan
{


	static FTimespan ReadAsMe(uintptr_t address)
	{
		FTimespan ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimespan& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.FloatRange
// 0x0000
struct FFloatRange
{


	static FFloatRange ReadAsMe(uintptr_t address)
	{
		FFloatRange ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFloatRange& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Int32Range
// 0x0000
struct FInt32Range
{


	static FInt32Range ReadAsMe(uintptr_t address)
	{
		FInt32Range ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInt32Range& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Int32RangeBound
// 0x0000
struct FInt32RangeBound
{


	static FInt32RangeBound ReadAsMe(uintptr_t address)
	{
		FInt32RangeBound ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInt32RangeBound& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.FloatInterval
// 0x0000
struct FFloatInterval
{


	static FFloatInterval ReadAsMe(uintptr_t address)
	{
		FFloatInterval ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFloatInterval& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.Int32Interval
// 0x0000
struct FInt32Interval
{


	static FInt32Interval ReadAsMe(uintptr_t address)
	{
		FInt32Interval ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInt32Interval& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.PolyglotTextData
// 0x0000
struct FPolyglotTextData
{


	static FPolyglotTextData ReadAsMe(uintptr_t address)
	{
		FPolyglotTextData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPolyglotTextData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.AutomationEvent
// 0x0000
struct FAutomationEvent
{


	static FAutomationEvent ReadAsMe(uintptr_t address)
	{
		FAutomationEvent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAutomationEvent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct CoreUObject.AutomationExecutionEntry
// 0x0000
struct FAutomationExecutionEntry
{


	static FAutomationExecutionEntry ReadAsMe(uintptr_t address)
	{
		FAutomationExecutionEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAutomationExecutionEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
