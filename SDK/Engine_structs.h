#pragma once

#include "../Utils.h"
// Name: Satisfactory, Version: 1.0.0

#ifdef _MSC_VER
	#pragma pack(push, 0x8)
#endif

namespace SDK
{
//---------------------------------------------------------------------------
// Enums
//---------------------------------------------------------------------------

// Enum Engine.ETextGender
enum class ETextGender : uint8_t
{
	ETextGender__Masculine         = 0,
	ETextGender__Feminine          = 1,
	ETextGender__Neuter            = 2,
	ETextGender__ETextGender_MAX   = 3
};


// Enum Engine.EFormatArgumentType
enum class EFormatArgumentType : uint8_t
{
	EFormatArgumentType__Int       = 0,
	EFormatArgumentType__UInt      = 1,
	EFormatArgumentType__Float     = 2,
	EFormatArgumentType__Double    = 3,
	EFormatArgumentType__Text      = 4,
	EFormatArgumentType__Gender    = 5,
	EFormatArgumentType__EFormatArgumentType_MAX = 6
};


// Enum Engine.EEndPlayReason
enum class EEndPlayReason : uint8_t
{
	EEndPlayReason__Destroyed      = 0,
	EEndPlayReason__LevelTransition = 1,
	EEndPlayReason__EndPlayInEditor = 2,
	EEndPlayReason__RemovedFromWorld = 3,
	EEndPlayReason__Quit           = 4,
	EEndPlayReason__EEndPlayReason_MAX = 5
};


// Enum Engine.ETickingGroup
enum class ETickingGroup : uint8_t
{
	TG_PrePhysics                  = 0,
	TG_StartPhysics                = 1,
	TG_DuringPhysics               = 2,
	TG_EndPhysics                  = 3,
	TG_PostPhysics                 = 4,
	TG_PostUpdateWork              = 5,
	TG_LastDemotable               = 6,
	TG_NewlySpawned                = 7,
	TG_MAX                         = 8
};


// Enum Engine.EComponentCreationMethod
enum class EComponentCreationMethod : uint8_t
{
	EComponentCreationMethod__Native = 0,
	EComponentCreationMethod__SimpleConstructionScript = 1,
	EComponentCreationMethod__UserConstructionScript = 2,
	EComponentCreationMethod__Instance = 3,
	EComponentCreationMethod__EComponentCreationMethod_MAX = 4
};


// Enum Engine.ETemperatureSeverityType
enum class ETemperatureSeverityType : uint8_t
{
	ETemperatureSeverityType__Unknown = 0,
	ETemperatureSeverityType__Good = 1,
	ETemperatureSeverityType__Bad  = 2,
	ETemperatureSeverityType__Serious = 3,
	ETemperatureSeverityType__Critical = 4,
	ETemperatureSeverityType__NumSeverities = 5,
	ETemperatureSeverityType__ETemperatureSeverityType_MAX = 6
};


// Enum Engine.EPlaneConstraintAxisSetting
enum class EPlaneConstraintAxisSetting : uint8_t
{
	EPlaneConstraintAxisSetting__Custom = 0,
	EPlaneConstraintAxisSetting__X = 1,
	EPlaneConstraintAxisSetting__Y = 2,
	EPlaneConstraintAxisSetting__Z = 3,
	EPlaneConstraintAxisSetting__UseGlobalPhysicsSetting = 4,
	EPlaneConstraintAxisSetting__EPlaneConstraintAxisSetting_MAX = 5
};


// Enum Engine.EInterpToBehaviourType
enum class EInterpToBehaviourType : uint8_t
{
	EInterpToBehaviourType__OneShot = 0,
	EInterpToBehaviourType__OneShot_Reverse = 1,
	EInterpToBehaviourType__Loop_Reset = 2,
	EInterpToBehaviourType__PingPong = 3,
	EInterpToBehaviourType__EInterpToBehaviourType_MAX = 4
};


// Enum Engine.EPlatformInterfaceDataType
enum class EPlatformInterfaceDataType : uint8_t
{
	PIDT_None                      = 0,
	PIDT_Int                       = 1,
	PIDT_Float                     = 2,
	PIDT_String                    = 3,
	PIDT_Object                    = 4,
	PIDT_Custom                    = 5,
	PIDT_MAX                       = 6
};


// Enum Engine.EMovementMode
enum class EMovementMode : uint8_t
{
	MOVE_None                      = 0,
	MOVE_Walking                   = 1,
	MOVE_NavWalking                = 2,
	MOVE_Falling                   = 3,
	MOVE_Swimming                  = 4,
	MOVE_Flying                    = 5,
	MOVE_Custom                    = 6,
	MOVE_MAX                       = 7
};


// Enum Engine.ENetworkFailure
enum class ENetworkFailure : uint8_t
{
	ENetworkFailure__NetDriverAlreadyExists = 0,
	ENetworkFailure__NetDriverCreateFailure = 1,
	ENetworkFailure__NetDriverListenFailure = 2,
	ENetworkFailure__ConnectionLost = 3,
	ENetworkFailure__ConnectionTimeout = 4,
	ENetworkFailure__FailureReceived = 5,
	ENetworkFailure__OutdatedClient = 6,
	ENetworkFailure__OutdatedServer = 7,
	ENetworkFailure__PendingConnectionFailure = 8,
	ENetworkFailure__NetGuidMismatch = 9,
	ENetworkFailure__NetChecksumMismatch = 10,
	ENetworkFailure__ENetworkFailure_MAX = 11
};


// Enum Engine.ETravelFailure
enum class ETravelFailure : uint8_t
{
	ETravelFailure__NoLevel        = 0,
	ETravelFailure__LoadMapFailure = 1,
	ETravelFailure__InvalidURL     = 2,
	ETravelFailure__PackageMissing = 3,
	ETravelFailure__PackageVersion = 4,
	ETravelFailure__NoDownload     = 5,
	ETravelFailure__TravelFailure  = 6,
	ETravelFailure__CheatCommands  = 7,
	ETravelFailure__PendingNetGameCreateFailure = 8,
	ETravelFailure__CloudSaveFailure = 9,
	ETravelFailure__ServerTravelFailure = 10,
	ETravelFailure__ClientTravelFailure = 11,
	ETravelFailure__ETravelFailure_MAX = 12
};


// Enum Engine.EScreenOrientation
enum class EScreenOrientation : uint8_t
{
	EScreenOrientation__Unknown    = 0,
	EScreenOrientation__Portrait   = 1,
	EScreenOrientation__PortraitUpsideDown = 2,
	EScreenOrientation__LandscapeLeft = 3,
	EScreenOrientation__LandscapeRight = 4,
	EScreenOrientation__FaceUp     = 5,
	EScreenOrientation__FaceDown   = 6,
	EScreenOrientation__EScreenOrientation_MAX = 7
};


// Enum Engine.EApplicationState
enum class EApplicationState : uint8_t
{
	EApplicationState__Unknown     = 0,
	EApplicationState__Inactive    = 1,
	EApplicationState__Background  = 2,
	EApplicationState__Active      = 3,
	EApplicationState__EApplicationState_MAX = 4
};


// Enum Engine.EObjectTypeQuery
enum class EObjectTypeQuery : uint8_t
{
	ObjectTypeQuery1               = 0,
	ObjectTypeQuery2               = 1,
	ObjectTypeQuery3               = 2,
	ObjectTypeQuery4               = 3,
	ObjectTypeQuery5               = 4,
	ObjectTypeQuery6               = 5,
	ObjectTypeQuery7               = 6,
	ObjectTypeQuery8               = 7,
	ObjectTypeQuery9               = 8,
	ObjectTypeQuery10              = 9,
	ObjectTypeQuery11              = 10,
	ObjectTypeQuery12              = 11,
	ObjectTypeQuery13              = 12,
	ObjectTypeQuery14              = 13,
	ObjectTypeQuery15              = 14,
	ObjectTypeQuery16              = 15,
	ObjectTypeQuery17              = 16,
	ObjectTypeQuery18              = 17,
	ObjectTypeQuery19              = 18,
	ObjectTypeQuery20              = 19,
	ObjectTypeQuery21              = 20,
	ObjectTypeQuery22              = 21,
	ObjectTypeQuery23              = 22,
	ObjectTypeQuery24              = 23,
	ObjectTypeQuery25              = 24,
	ObjectTypeQuery26              = 25,
	ObjectTypeQuery27              = 26,
	ObjectTypeQuery28              = 27,
	ObjectTypeQuery29              = 28,
	ObjectTypeQuery30              = 29,
	ObjectTypeQuery31              = 30,
	ObjectTypeQuery32              = 31,
	ObjectTypeQuery_MAX            = 32,
	EObjectTypeQuery_MAX           = 33
};


// Enum Engine.EDrawDebugTrace
enum class EDrawDebugTrace : uint8_t
{
	EDrawDebugTrace__None          = 0,
	EDrawDebugTrace__ForOneFrame   = 1,
	EDrawDebugTrace__ForDuration   = 2,
	EDrawDebugTrace__Persistent    = 3,
	EDrawDebugTrace__EDrawDebugTrace_MAX = 4
};


// Enum Engine.ETraceTypeQuery
enum class ETraceTypeQuery : uint8_t
{
	TraceTypeQuery1                = 0,
	TraceTypeQuery2                = 1,
	TraceTypeQuery3                = 2,
	TraceTypeQuery4                = 3,
	TraceTypeQuery5                = 4,
	TraceTypeQuery6                = 5,
	TraceTypeQuery7                = 6,
	TraceTypeQuery8                = 7,
	TraceTypeQuery9                = 8,
	TraceTypeQuery10               = 9,
	TraceTypeQuery11               = 10,
	TraceTypeQuery12               = 11,
	TraceTypeQuery13               = 12,
	TraceTypeQuery14               = 13,
	TraceTypeQuery15               = 14,
	TraceTypeQuery16               = 15,
	TraceTypeQuery17               = 16,
	TraceTypeQuery18               = 17,
	TraceTypeQuery19               = 18,
	TraceTypeQuery20               = 19,
	TraceTypeQuery21               = 20,
	TraceTypeQuery22               = 21,
	TraceTypeQuery23               = 22,
	TraceTypeQuery24               = 23,
	TraceTypeQuery25               = 24,
	TraceTypeQuery26               = 25,
	TraceTypeQuery27               = 26,
	TraceTypeQuery28               = 27,
	TraceTypeQuery29               = 28,
	TraceTypeQuery30               = 29,
	TraceTypeQuery31               = 30,
	TraceTypeQuery32               = 31,
	TraceTypeQuery_MAX             = 32,
	ETraceTypeQuery_MAX            = 33
};


// Enum Engine.EMoveComponentAction
enum class EMoveComponentAction : uint8_t
{
	EMoveComponentAction__Move     = 0,
	EMoveComponentAction__Stop     = 1,
	EMoveComponentAction__Return   = 2,
	EMoveComponentAction__EMoveComponentAction_MAX = 3
};


// Enum Engine.EQuitPreference
enum class EQuitPreference : uint8_t
{
	EQuitPreference__Quit          = 0,
	EQuitPreference__Background    = 1,
	EQuitPreference__EQuitPreference_MAX = 2
};


// Enum Engine.EMouseLockMode
enum class EMouseLockMode : uint8_t
{
	EMouseLockMode__DoNotLock      = 0,
	EMouseLockMode__LockOnCapture  = 1,
	EMouseLockMode__LockAlways     = 2,
	EMouseLockMode__LockInFullscreen = 3,
	EMouseLockMode__EMouseLockMode_MAX = 4
};


// Enum Engine.EWindowTitleBarMode
enum class EWindowTitleBarMode : uint8_t
{
	EWindowTitleBarMode__Overlay   = 0,
	EWindowTitleBarMode__VerticalBox = 1,
	EWindowTitleBarMode__EWindowTitleBarMode_MAX = 2
};


// Enum Engine.EInputEvent
enum class EInputEvent : uint8_t
{
	IE_Pressed                     = 0,
	IE_Released                    = 1,
	IE_Repeat                      = 2,
	IE_DoubleClick                 = 3,
	IE_Axis                        = 4,
	IE_MAX                         = 5
};


// Enum Engine.ERelativeTransformSpace
enum class ERelativeTransformSpace : uint8_t
{
	RTS_World                      = 0,
	RTS_Actor                      = 1,
	RTS_Component                  = 2,
	RTS_ParentBoneSpace            = 3,
	RTS_MAX                        = 4
};


// Enum Engine.EAttachLocation
enum class EAttachLocation : uint8_t
{
	EAttachLocation__KeepRelativeOffset = 0,
	EAttachLocation__KeepWorldPosition = 1,
	EAttachLocation__SnapToTarget  = 2,
	EAttachLocation__SnapToTargetIncludingScale = 3,
	EAttachLocation__EAttachLocation_MAX = 4
};


// Enum Engine.EAttachmentRule
enum class EAttachmentRule : uint8_t
{
	EAttachmentRule__KeepRelative  = 0,
	EAttachmentRule__KeepWorld     = 1,
	EAttachmentRule__SnapToTarget  = 2,
	EAttachmentRule__EAttachmentRule_MAX = 3
};


// Enum Engine.EDetachmentRule
enum class EDetachmentRule : uint8_t
{
	EDetachmentRule__KeepRelative  = 0,
	EDetachmentRule__KeepWorld     = 1,
	EDetachmentRule__EDetachmentRule_MAX = 2
};


// Enum Engine.EComponentMobility
enum class EComponentMobility : uint8_t
{
	EComponentMobility__Static     = 0,
	EComponentMobility__Stationary = 1,
	EComponentMobility__Movable    = 2,
	EComponentMobility__EComponentMobility_MAX = 3
};


// Enum Engine.EDetailMode
enum class EDetailMode : uint8_t
{
	DM_Low                         = 0,
	DM_Medium                      = 1,
	DM_High                        = 2,
	DM_MAX                         = 3
};


// Enum Engine.EAlphaBlendOption
enum class EAlphaBlendOption : uint8_t
{
	EAlphaBlendOption__Linear      = 0,
	EAlphaBlendOption__Cubic       = 1,
	EAlphaBlendOption__HermiteCubic = 2,
	EAlphaBlendOption__Sinusoidal  = 3,
	EAlphaBlendOption__QuadraticInOut = 4,
	EAlphaBlendOption__CubicInOut  = 5,
	EAlphaBlendOption__QuarticInOut = 6,
	EAlphaBlendOption__QuinticInOut = 7,
	EAlphaBlendOption__CircularIn  = 8,
	EAlphaBlendOption__CircularOut = 9,
	EAlphaBlendOption__CircularInOut = 10,
	EAlphaBlendOption__ExpIn       = 11,
	EAlphaBlendOption__ExpOut      = 12,
	EAlphaBlendOption__ExpInOut    = 13,
	EAlphaBlendOption__Custom      = 14,
	EAlphaBlendOption__EAlphaBlendOption_MAX = 15
};


// Enum Engine.ERawCurveTrackTypes
enum class ERawCurveTrackTypes : uint8_t
{
	ERawCurveTrackTypes__RCT_Float = 0,
	ERawCurveTrackTypes__RCT_Vector = 1,
	ERawCurveTrackTypes__RCT_Transform = 2,
	ERawCurveTrackTypes__RCT_MAX   = 3
};


// Enum Engine.EAnimGroupRole
enum class EAnimGroupRole : uint8_t
{
	EAnimGroupRole__CanBeLeader    = 0,
	EAnimGroupRole__AlwaysFollower = 1,
	EAnimGroupRole__AlwaysLeader   = 2,
	EAnimGroupRole__TransitionLeader = 3,
	EAnimGroupRole__TransitionFollower = 4,
	EAnimGroupRole__EAnimGroupRole_MAX = 5
};


// Enum Engine.EAnimAssetCurveFlags
enum class EAnimAssetCurveFlags : uint8_t
{
	AACF_DriveMorphTarget_DEPRECATED = 0,
	AACF_DriveAttribute_DEPRECATED = 1,
	AACF_Editable                  = 2,
	AACF_DriveMaterial_DEPRECATED  = 3,
	AACF_Metadata                  = 4,
	AACF_DriveTrack                = 5,
	AACF_Disabled                  = 6,
	AACF_MAX                       = 7
};


// Enum Engine.AnimationCompressionFormat
enum class EAnimationCompressionFormat : uint8_t
{
	ACF_None                       = 0,
	ACF_Float96NoW                 = 1,
	ACF_Fixed48NoW                 = 2,
	ACF_IntervalFixed32NoW         = 3,
	ACF_Fixed32NoW                 = 4,
	ACF_Float32NoW                 = 5,
	ACF_Identity                   = 6,
	ACF_MAX                        = 7
};


// Enum Engine.EAdditiveBasePoseType
enum class EAdditiveBasePoseType : uint8_t
{
	ABPT_None                      = 0,
	ABPT_RefPose                   = 1,
	ABPT_AnimScaled                = 2,
	ABPT_AnimFrame                 = 3,
	ABPT_MAX                       = 4
};


// Enum Engine.ERootMotionMode
enum class ERootMotionMode : uint8_t
{
	ERootMotionMode__NoRootMotionExtraction = 0,
	ERootMotionMode__IgnoreRootMotion = 1,
	ERootMotionMode__RootMotionFromEverything = 2,
	ERootMotionMode__RootMotionFromMontagesOnly = 3,
	ERootMotionMode__ERootMotionMode_MAX = 4
};


// Enum Engine.ERootMotionRootLock
enum class ERootMotionRootLock : uint8_t
{
	ERootMotionRootLock__RefPose   = 0,
	ERootMotionRootLock__AnimFirstFrame = 1,
	ERootMotionRootLock__Zero      = 2,
	ERootMotionRootLock__ERootMotionRootLock_MAX = 3
};


// Enum Engine.EDrawDebugItemType
enum class EDrawDebugItemType : uint8_t
{
	EDrawDebugItemType__DirectionalArrow = 0,
	EDrawDebugItemType__Sphere     = 1,
	EDrawDebugItemType__Line       = 2,
	EDrawDebugItemType__OnScreenMessage = 3,
	EDrawDebugItemType__CoordinateSystem = 4,
	EDrawDebugItemType__EDrawDebugItemType_MAX = 5
};


// Enum Engine.EEvaluatorMode
enum class EEvaluatorMode : uint8_t
{
	EEvaluatorMode__EM_Standard    = 0,
	EEvaluatorMode__EM_Freeze      = 1,
	EEvaluatorMode__EM_DelayedFreeze = 2,
	EEvaluatorMode__EM_MAX         = 3
};


// Enum Engine.EAnimNotifyEventType
enum class EAnimNotifyEventType : uint8_t
{
	EAnimNotifyEventType__Begin    = 0,
	EAnimNotifyEventType__End      = 1,
	EAnimNotifyEventType__EAnimNotifyEventType_MAX = 2
};


// Enum Engine.EMontagePlayReturnType
enum class EMontagePlayReturnType : uint8_t
{
	EMontagePlayReturnType__MontageLength = 0,
	EMontagePlayReturnType__Duration = 1,
	EMontagePlayReturnType__EMontagePlayReturnType_MAX = 2
};


// Enum Engine.ECopyType
enum class ECopyType : uint8_t
{
	ECopyType__MemCopy             = 0,
	ECopyType__BoolProperty        = 1,
	ECopyType__StructProperty      = 2,
	ECopyType__ObjectProperty      = 3,
	ECopyType__ECopyType_MAX       = 4
};


// Enum Engine.EPinHidingMode
enum class EPinHidingMode : uint8_t
{
	EPinHidingMode__NeverAsPin     = 0,
	EPinHidingMode__PinHiddenByDefault = 1,
	EPinHidingMode__PinShownByDefault = 2,
	EPinHidingMode__AlwaysAsPin    = 3,
	EPinHidingMode__EPinHidingMode_MAX = 4
};


// Enum Engine.EPostCopyOperation
enum class EPostCopyOperation : uint8_t
{
	EPostCopyOperation__None       = 0,
	EPostCopyOperation__LogicalNegateBool = 1,
	EPostCopyOperation__EPostCopyOperation_MAX = 2
};


// Enum Engine.EMontageSubStepResult
enum class EMontageSubStepResult : uint8_t
{
	EMontageSubStepResult__Moved   = 0,
	EMontageSubStepResult__NotMoved = 1,
	EMontageSubStepResult__InvalidSection = 2,
	EMontageSubStepResult__InvalidMontage = 3,
	EMontageSubStepResult__EMontageSubStepResult_MAX = 4
};


// Enum Engine.AnimPhysCollisionType
enum class EAnimPhysCollisionType : uint8_t
{
	AnimPhysCollisionType__CoM     = 0,
	AnimPhysCollisionType__CustomSphere = 1,
	AnimPhysCollisionType__InnerSphere = 2,
	AnimPhysCollisionType__OuterSphere = 3,
	AnimPhysCollisionType__AnimPhysCollisionType_MAX = 4
};


// Enum Engine.AnimationKeyFormat
enum class EAnimationKeyFormat : uint8_t
{
	AKF_ConstantKeyLerp            = 0,
	AKF_VariableKeyLerp            = 1,
	AKF_PerTrackCompression        = 2,
	AKF_MAX                        = 3
};


// Enum Engine.AnimPhysTwistAxis
enum class EAnimPhysTwistAxis : uint8_t
{
	AnimPhysTwistAxis__AxisX       = 0,
	AnimPhysTwistAxis__AxisY       = 1,
	AnimPhysTwistAxis__AxisZ       = 2,
	AnimPhysTwistAxis__AnimPhysTwistAxis_MAX = 3
};


// Enum Engine.EEvaluatorDataSource
enum class EEvaluatorDataSource : uint8_t
{
	EEvaluatorDataSource__EDS_SourcePose = 0,
	EEvaluatorDataSource__EDS_DestinationPose = 1,
	EEvaluatorDataSource__EDS_MAX  = 2
};


// Enum Engine.ETypeAdvanceAnim
enum class ETypeAdvanceAnim : uint8_t
{
	ETAA_Default                   = 0,
	ETAA_Finished                  = 1,
	ETAA_Looped                    = 2,
	ETAA_MAX                       = 3
};


// Enum Engine.EAnimInterpolationType
enum class EAnimInterpolationType : uint8_t
{
	EAnimInterpolationType__Linear = 0,
	EAnimInterpolationType__Step   = 1,
	EAnimInterpolationType__EAnimInterpolationType_MAX = 2
};


// Enum Engine.ETransitionBlendMode
enum class ETransitionBlendMode : uint8_t
{
	ETransitionBlendMode__TBM_Linear = 0,
	ETransitionBlendMode__TBM_Cubic = 1,
	ETransitionBlendMode__TBM_MAX  = 2
};


// Enum Engine.EComponentType
enum class EComponentType : uint8_t
{
	EComponentType__None           = 0,
	EComponentType__TranslationX   = 1,
	EComponentType__TranslationY   = 2,
	EComponentType__TranslationZ   = 3,
	EComponentType__RotationX      = 4,
	EComponentType__RotationY      = 5,
	EComponentType__RotationZ      = 6,
	EComponentType__Scale          = 7,
	EComponentType__ScaleX         = 8,
	EComponentType__ScaleY         = 9,
	EComponentType__ScaleZ         = 10,
	EComponentType__EComponentType_MAX = 11
};


// Enum Engine.ETransitionLogicType
enum class ETransitionLogicType : uint8_t
{
	ETransitionLogicType__TLT_StandardBlend = 0,
	ETransitionLogicType__TLT_Custom = 1,
	ETransitionLogicType__TLT_MAX  = 2
};


// Enum Engine.ECurveBlendOption
enum class ECurveBlendOption : uint8_t
{
	ECurveBlendOption__MaxWeight   = 0,
	ECurveBlendOption__NormalizeByWeight = 1,
	ECurveBlendOption__BlendByWeight = 2,
	ECurveBlendOption__ECurveBlendOption_MAX = 3
};


// Enum Engine.ENotifyFilterType
enum class ENotifyFilterType : uint8_t
{
	ENotifyFilterType__NoFiltering = 0,
	ENotifyFilterType__LOD         = 1,
	ENotifyFilterType__ENotifyFilterType_MAX = 2
};


// Enum Engine.EMontageNotifyTickType
enum class EMontageNotifyTickType : uint8_t
{
	EMontageNotifyTickType__Queued = 0,
	EMontageNotifyTickType__BranchingPoint = 1,
	EMontageNotifyTickType__EMontageNotifyTickType_MAX = 2
};


// Enum Engine.EBoneControlSpace
enum class EBoneControlSpace : uint8_t
{
	BCS_WorldSpace                 = 0,
	BCS_ComponentSpace             = 1,
	BCS_ParentBoneSpace            = 2,
	BCS_BoneSpace                  = 3,
	BCS_MAX                        = 4
};


// Enum Engine.EAxisOption
enum class EAxisOption : uint8_t
{
	EAxisOption__X                 = 0,
	EAxisOption__Y                 = 1,
	EAxisOption__Z                 = 2,
	EAxisOption__X_Neg             = 3,
	EAxisOption__Y_Neg             = 4,
	EAxisOption__Z_Neg             = 5,
	EAxisOption__Custom            = 6,
	EAxisOption__EAxisOption_MAX   = 7
};


// Enum Engine.EAdditiveAnimationType
enum class EAdditiveAnimationType : uint8_t
{
	AAT_None                       = 0,
	AAT_LocalSpaceBase             = 1,
	AAT_RotationOffsetMeshSpace    = 2,
	AAT_MAX                        = 3
};


// Enum Engine.EPrimaryAssetCookRule
enum class EPrimaryAssetCookRule : uint8_t
{
	EPrimaryAssetCookRule__Unknown = 0,
	EPrimaryAssetCookRule__NeverCook = 1,
	EPrimaryAssetCookRule__DevelopmentCook = 2,
	EPrimaryAssetCookRule__DevelopmentAlwaysCook = 3,
	EPrimaryAssetCookRule__AlwaysCook = 4,
	EPrimaryAssetCookRule__EPrimaryAssetCookRule_MAX = 5
};


// Enum Engine.EBoneAxis
enum class EBoneAxis : uint8_t
{
	BA_X                           = 0,
	BA_Y                           = 1,
	BA_Z                           = 2,
	BA_MAX                         = 3
};


// Enum Engine.EMonoChannelUpmixMethod
enum class EMonoChannelUpmixMethod : uint8_t
{
	EMonoChannelUpmixMethod__Linear = 0,
	EMonoChannelUpmixMethod__EqualPower = 1,
	EMonoChannelUpmixMethod__FullVolume = 2,
	EMonoChannelUpmixMethod__EMonoChannelUpmixMethod_MAX = 3
};


// Enum Engine.EBoneRotationSource
enum class EBoneRotationSource : uint8_t
{
	BRS_KeepComponentSpaceRotation = 0,
	BRS_KeepLocalSpaceRotation     = 1,
	BRS_CopyFromTarget             = 2,
	BRS_MAX                        = 3
};


// Enum Engine.EPanningMethod
enum class EPanningMethod : uint8_t
{
	EPanningMethod__Linear         = 0,
	EPanningMethod__EqualPower     = 1,
	EPanningMethod__EPanningMethod_MAX = 2
};


// Enum Engine.EAttenuationShape
enum class EAttenuationShape : uint8_t
{
	EAttenuationShape__Sphere      = 0,
	EAttenuationShape__Capsule     = 1,
	EAttenuationShape__Box         = 2,
	EAttenuationShape__Cone        = 3,
	EAttenuationShape__EAttenuationShape_MAX = 4
};


// Enum Engine.EAttenuationDistanceModel
enum class EAttenuationDistanceModel : uint8_t
{
	EAttenuationDistanceModel__Linear = 0,
	EAttenuationDistanceModel__Logarithmic = 1,
	EAttenuationDistanceModel__Inverse = 2,
	EAttenuationDistanceModel__LogReverse = 3,
	EAttenuationDistanceModel__NaturalSound = 4,
	EAttenuationDistanceModel__Custom = 5,
	EAttenuationDistanceModel__EAttenuationDistanceModel_MAX = 6
};


// Enum Engine.ReverbPreset
enum class EReverbPreset : uint8_t
{
	REVERB_Default                 = 0,
	REVERB_Bathroom                = 1,
	REVERB_StoneRoom               = 2,
	REVERB_Auditorium              = 3,
	REVERB_ConcertHall             = 4,
	REVERB_Cave                    = 5,
	REVERB_Hallway                 = 6,
	REVERB_StoneCorridor           = 7,
	REVERB_Alley                   = 8,
	REVERB_Forest                  = 9,
	REVERB_City                    = 10,
	REVERB_Mountains               = 11,
	REVERB_Quarry                  = 12,
	REVERB_Plain                   = 13,
	REVERB_ParkingLot              = 14,
	REVERB_SewerPipe               = 15,
	REVERB_Underwater              = 16,
	REVERB_SmallRoom               = 17,
	REVERB_MediumRoom              = 18,
	REVERB_LargeRoom               = 19,
	REVERB_MediumHall              = 20,
	REVERB_LargeHall               = 21,
	REVERB_Plate                   = 22,
	REVERB_MAX                     = 23
};


// Enum Engine.EVoiceSampleRate
enum class EVoiceSampleRate : uint8_t
{
	EVoiceSampleRate__Low16000Hz   = 0,
	EVoiceSampleRate__Normal24000Hz = 1,
	EVoiceSampleRate__EVoiceSampleRate_MAX = 2
};


// Enum Engine.EBlendableLocation
enum class EBlendableLocation : uint8_t
{
	BL_AfterTonemapping            = 0,
	BL_BeforeTonemapping           = 1,
	BL_BeforeTranslucency          = 2,
	BL_ReplacingTonemapper         = 3,
	BL_SSRInput                    = 4,
	BL_MAX                         = 5
};


// Enum Engine.ENotifyTriggerMode
enum class ENotifyTriggerMode : uint8_t
{
	ENotifyTriggerMode__AllAnimations = 0,
	ENotifyTriggerMode__HighestWeightedAnimation = 1,
	ENotifyTriggerMode__None       = 2,
	ENotifyTriggerMode__ENotifyTriggerMode_MAX = 3
};


// Enum Engine.EBlendSpaceAxis
enum class EBlendSpaceAxis : uint8_t
{
	BSA_None                       = 0,
	BSA_X                          = 1,
	BSA_Y                          = 2,
	BSA_Max                        = 3
};


// Enum Engine.EBlueprintCompileMode
enum class EBlueprintCompileMode : uint8_t
{
	EBlueprintCompileMode__Default = 0,
	EBlueprintCompileMode__Development = 1,
	EBlueprintCompileMode__FinalRelease = 2,
	EBlueprintCompileMode__EBlueprintCompileMode_MAX = 3
};


// Enum Engine.EBlueprintNativizationFlag
enum class EBlueprintNativizationFlag : uint8_t
{
	EBlueprintNativizationFlag__Disabled = 0,
	EBlueprintNativizationFlag__Dependency = 1,
	EBlueprintNativizationFlag__ExplicitlyEnabled = 2,
	EBlueprintNativizationFlag__EBlueprintNativizationFlag_MAX = 3
};


// Enum Engine.EBlueprintType
enum class EBlueprintType : uint8_t
{
	BPTYPE_Normal                  = 0,
	BPTYPE_Const                   = 1,
	BPTYPE_MacroLibrary            = 2,
	BPTYPE_Interface               = 3,
	BPTYPE_LevelScript             = 4,
	BPTYPE_FunctionLibrary         = 5,
	BPTYPE_MAX                     = 6
};


// Enum Engine.EBodyCollisionResponse
enum class EBodyCollisionResponse : uint8_t
{
	EBodyCollisionResponse__BodyCollision_Enabled = 0,
	EBodyCollisionResponse__BodyCollision_Disabled = 1,
	EBodyCollisionResponse__BodyCollision_MAX = 2
};


// Enum Engine.EAnimLinkMethod
enum class EAnimLinkMethod : uint8_t
{
	EAnimLinkMethod__Absolute      = 0,
	EAnimLinkMethod__Relative      = 1,
	EAnimLinkMethod__Proportional  = 2,
	EAnimLinkMethod__EAnimLinkMethod_MAX = 3
};


// Enum Engine.EPhysicsType
enum class EPhysicsType : uint8_t
{
	PhysType_Default               = 0,
	PhysType_Kinematic             = 1,
	PhysType_Simulated             = 2,
	PhysType_MAX                   = 3
};


// Enum Engine.EDOFMode
enum class EDOFMode : uint8_t
{
	EDOFMode__Default              = 0,
	EDOFMode__SixDOF               = 1,
	EDOFMode__YZPlane              = 2,
	EDOFMode__XZPlane              = 3,
	EDOFMode__XYPlane              = 4,
	EDOFMode__CustomPlane          = 5,
	EDOFMode__None                 = 6,
	EDOFMode__EDOFMode_MAX         = 7
};


// Enum Engine.ECsgOper
enum class ECsgOper : uint8_t
{
	CSG_Active                     = 0,
	CSG_Add                        = 1,
	CSG_Subtract                   = 2,
	CSG_Intersect                  = 3,
	CSG_Deintersect                = 4,
	CSG_None                       = 5,
	CSG_MAX                        = 6
};


// Enum Engine.EBrushType
enum class EBrushType : uint8_t
{
	Brush_Default                  = 0,
	Brush_Add                      = 1,
	Brush_Subtract                 = 2,
	Brush_MAX                      = 3
};


// Enum Engine.EOscillatorWaveform
enum class EOscillatorWaveform : uint8_t
{
	EOscillatorWaveform__SineWave  = 0,
	EOscillatorWaveform__PerlinNoise = 1,
	EOscillatorWaveform__EOscillatorWaveform_MAX = 2
};


// Enum Engine.EInitialOscillatorOffset
enum class EInitialOscillatorOffset : uint8_t
{
	EOO_OffsetRandom               = 0,
	EOO_OffsetZero                 = 1,
	EOO_MAX                        = 2
};


// Enum Engine.ECameraProjectionMode
enum class ECameraProjectionMode : uint8_t
{
	ECameraProjectionMode__Perspective = 0,
	ECameraProjectionMode__Orthographic = 1,
	ECameraProjectionMode__ECameraProjectionMode_MAX = 2
};


// Enum Engine.ECollisionTraceFlag
enum class ECollisionTraceFlag : uint8_t
{
	CTF_UseDefault                 = 0,
	CTF_UseSimpleAndComplex        = 1,
	CTF_UseSimpleAsComplex         = 2,
	CTF_UseComplexAsSimple         = 3,
	CTF_MAX                        = 4
};


// Enum Engine.ECloudStorageDelegate
enum class ECloudStorageDelegate : uint8_t
{
	CSD_KeyValueReadComplete       = 0,
	CSD_KeyValueWriteComplete      = 1,
	CSD_ValueChanged               = 2,
	CSD_DocumentQueryComplete      = 3,
	CSD_DocumentReadComplete       = 4,
	CSD_DocumentWriteComplete      = 5,
	CSD_DocumentConflictDetected   = 6,
	CSD_MAX                        = 7
};


// Enum Engine.ELinearConstraintMotion
enum class ELinearConstraintMotion : uint8_t
{
	LCM_Free                       = 0,
	LCM_Limited                    = 1,
	LCM_Locked                     = 2,
	LCM_MAX                        = 3
};


// Enum Engine.ECameraAlphaBlendMode
enum class ECameraAlphaBlendMode : uint8_t
{
	ECameraAlphaBlendMode__CABM_Linear = 0,
	ECameraAlphaBlendMode__CABM_Cubic = 1,
	ECameraAlphaBlendMode__CABM_MAX = 2
};


// Enum Engine.EAngularDriveMode
enum class EAngularDriveMode : uint8_t
{
	EAngularDriveMode__SLERP       = 0,
	EAngularDriveMode__TwistAndSwing = 1,
	EAngularDriveMode__EAngularDriveMode_MAX = 2
};


// Enum Engine.EBlueprintStatus
enum class EBlueprintStatus : uint8_t
{
	BS_Unknown                     = 0,
	BS_Dirty                       = 1,
	BS_Error                       = 2,
	BS_UpToDate                    = 3,
	BS_BeingCreated                = 4,
	BS_UpToDateWithWarnings        = 5,
	BS_MAX                         = 6
};


// Enum Engine.ECameraAnimPlaySpace
enum class ECameraAnimPlaySpace : uint8_t
{
	ECameraAnimPlaySpace__CameraLocal = 0,
	ECameraAnimPlaySpace__World    = 1,
	ECameraAnimPlaySpace__UserDefined = 2,
	ECameraAnimPlaySpace__ECameraAnimPlaySpace_MAX = 3
};


// Enum Engine.EEvaluateCurveTableResult
enum class EEvaluateCurveTableResult : uint8_t
{
	EEvaluateCurveTableResult__RowFound = 0,
	EEvaluateCurveTableResult__RowNotFound = 1,
	EEvaluateCurveTableResult__EEvaluateCurveTableResult_MAX = 2
};


// Enum Engine.EGrammaticalNumber
enum class EGrammaticalNumber : uint8_t
{
	EGrammaticalNumber__Singular   = 0,
	EGrammaticalNumber__Plural     = 1,
	EGrammaticalNumber__EGrammaticalNumber_MAX = 2
};


// Enum Engine.DistributionParamMode
enum class EDistributionParamMode : uint8_t
{
	DPM_Normal                     = 0,
	DPM_Abs                        = 1,
	DPM_Direct                     = 2,
	DPM_MAX                        = 3
};


// Enum Engine.EGrammaticalGender
enum class EGrammaticalGender : uint8_t
{
	EGrammaticalGender__Neuter     = 0,
	EGrammaticalGender__Masculine  = 1,
	EGrammaticalGender__Feminine   = 2,
	EGrammaticalGender__Mixed      = 3,
	EGrammaticalGender__EGrammaticalGender_MAX = 4
};


// Enum Engine.EDistributionVectorMirrorFlags
enum class EDistributionVectorMirrorFlags : uint8_t
{
	EDVMF_Same                     = 0,
	EDVMF_Different                = 1,
	EDVMF_Mirror                   = 2,
	EDVMF_MAX                      = 3
};


// Enum Engine.EDistributionVectorLockFlags
enum class EDistributionVectorLockFlags : uint8_t
{
	EDVLF_None                     = 0,
	EDVLF_XY                       = 1,
	EDVLF_XZ                       = 2,
	EDVLF_YZ                       = 3,
	EDVLF_XYZ                      = 4,
	EDVLF_MAX                      = 5
};


// Enum Engine.ENodeTitleType
enum class ENodeTitleType : uint8_t
{
	ENodeTitleType__FullTitle      = 0,
	ENodeTitleType__ListView       = 1,
	ENodeTitleType__EditableTitle  = 2,
	ENodeTitleType__MenuTitle      = 3,
	ENodeTitleType__MAX_TitleTypes = 4,
	ENodeTitleType__ENodeTitleType_MAX = 5
};


// Enum Engine.EEdGraphPinDirection
enum class EEdGraphPinDirection : uint8_t
{
	EGPD_Input                     = 0,
	EGPD_Output                    = 1,
	EGPD_MAX                       = 2
};


// Enum Engine.EPinContainerType
enum class EPinContainerType : uint8_t
{
	EPinContainerType__None        = 0,
	EPinContainerType__Array       = 1,
	EPinContainerType__Set         = 2,
	EPinContainerType__Map         = 3,
	EPinContainerType__EPinContainerType_MAX = 4
};


// Enum Engine.ENodeAdvancedPins
enum class ENodeAdvancedPins : uint8_t
{
	ENodeAdvancedPins__NoPins      = 0,
	ENodeAdvancedPins__Shown       = 1,
	ENodeAdvancedPins__Hidden      = 2,
	ENodeAdvancedPins__ENodeAdvancedPins_MAX = 3
};


// Enum Engine.ENodeEnabledState
enum class ENodeEnabledState : uint8_t
{
	ENodeEnabledState__Enabled     = 0,
	ENodeEnabledState__Disabled    = 1,
	ENodeEnabledState__DevelopmentOnly = 2,
	ENodeEnabledState__ENodeEnabledState_MAX = 3
};


// Enum Engine.ECanCreateConnectionResponse
enum class ECanCreateConnectionResponse : uint8_t
{
	CONNECT_RESPONSE_MAKE          = 0,
	CONNECT_RESPONSE_DISALLOW      = 1,
	CONNECT_RESPONSE_BREAK_OTHERS_A = 2,
	CONNECT_RESPONSE_BREAK_OTHERS_B = 3,
	CONNECT_RESPONSE_BREAK_OTHERS_AB = 4,
	CONNECT_RESPONSE_MAKE_WITH_CONVERSION_NODE = 5,
	CONNECT_RESPONSE_MAX           = 6
};


// Enum Engine.EBlueprintPinStyleType
enum class EBlueprintPinStyleType : uint8_t
{
	BPST_Original                  = 0,
	BPST_VariantA                  = 1,
	BPST_MAX                       = 2
};


// Enum Engine.EGraphType
enum class EGraphType : uint8_t
{
	GT_Function                    = 0,
	GT_Ubergraph                   = 1,
	GT_Macro                       = 2,
	GT_Animation                   = 3,
	GT_StateMachine                = 4,
	GT_MAX                         = 5
};


// Enum Engine.EConsoleType
enum class EConsoleType : uint8_t
{
	CONSOLE_Any                    = 0,
	CONSOLE_Mobile                 = 1,
	CONSOLE_MAX                    = 2
};


// Enum Engine.ETransitionType
enum class ETransitionType : uint8_t
{
	TT_None                        = 0,
	TT_Paused                      = 1,
	TT_Loading                     = 2,
	TT_Saving                      = 3,
	TT_Connecting                  = 4,
	TT_Precaching                  = 5,
	TT_WaitingToConnect            = 6,
	TT_MAX                         = 7
};


// Enum Engine.EFullyLoadPackageType
enum class EFullyLoadPackageType : uint8_t
{
	FULLYLOAD_Map                  = 0,
	FULLYLOAD_Game_PreLoadClass    = 1,
	FULLYLOAD_Game_PostLoadClass   = 2,
	FULLYLOAD_Always               = 3,
	FULLYLOAD_Mutator              = 4,
	FULLYLOAD_MAX                  = 5
};


// Enum Engine.ENetworkLagState
enum class ENetworkLagState : uint8_t
{
	ENetworkLagState__NotLagging   = 0,
	ENetworkLagState__Lagging      = 1,
	ENetworkLagState__ENetworkLagState_MAX = 2
};


// Enum Engine.EMouseCaptureMode
enum class EMouseCaptureMode : uint8_t
{
	EMouseCaptureMode__NoCapture   = 0,
	EMouseCaptureMode__CapturePermanently = 1,
	EMouseCaptureMode__CapturePermanently_IncludingInitialMouseDown = 2,
	EMouseCaptureMode__CaptureDuringMouseDown = 3,
	EMouseCaptureMode__CaptureDuringRightMouseDown = 4,
	EMouseCaptureMode__EMouseCaptureMode_MAX = 5
};


// Enum Engine.ETravelType
enum class ETravelType : uint8_t
{
	TRAVEL_Absolute                = 0,
	TRAVEL_Partial                 = 1,
	TRAVEL_Relative                = 2,
	TRAVEL_MAX                     = 3
};


// Enum Engine.ECustomTimeStepSynchronizationState
enum class ECustomTimeStepSynchronizationState : uint8_t
{
	ECustomTimeStepSynchronizationState__Closed = 0,
	ECustomTimeStepSynchronizationState__Error = 1,
	ECustomTimeStepSynchronizationState__Synchronized = 2,
	ECustomTimeStepSynchronizationState__Synchronizing = 3,
	ECustomTimeStepSynchronizationState__ECustomTimeStepSynchronizationState_MAX = 4
};


// Enum Engine.EDemoPlayFailure
enum class EDemoPlayFailure : uint8_t
{
	EDemoPlayFailure__Generic      = 0,
	EDemoPlayFailure__DemoNotFound = 1,
	EDemoPlayFailure__Corrupt      = 2,
	EDemoPlayFailure__InvalidVersion = 3,
	EDemoPlayFailure__InitBase     = 4,
	EDemoPlayFailure__GameSpecificHeader = 5,
	EDemoPlayFailure__ReplayStreamerInternal = 6,
	EDemoPlayFailure__LoadMap      = 7,
	EDemoPlayFailure__Serialization = 8,
	EDemoPlayFailure__EDemoPlayFailure_MAX = 9
};


// Enum Engine.EViewModeIndex
enum class EViewModeIndex : uint8_t
{
	VMI_BrushWireframe             = 0,
	VMI_Wireframe                  = 1,
	VMI_Unlit                      = 2,
	VMI_Lit                        = 3,
	VMI_Lit_DetailLighting         = 4,
	VMI_LightingOnly               = 5,
	VMI_LightComplexity            = 6,
	VMI_ShaderComplexity           = 7,
	VMI_LightmapDensity            = 8,
	VMI_LitLightmapDensity         = 9,
	VMI_ReflectionOverride         = 10,
	VMI_VisualizeBuffer            = 11,
	VMI_StationaryLightOverlap     = 12,
	VMI_CollisionPawn              = 13,
	VMI_CollisionVisibility        = 14,
	VMI_LODColoration              = 15,
	VMI_QuadOverdraw               = 16,
	VMI_PrimitiveDistanceAccuracy  = 17,
	VMI_MeshUVDensityAccuracy      = 18,
	VMI_ShaderComplexityWithQuadOverdraw = 19,
	VMI_HLODColoration             = 20,
	VMI_GroupLODColoration         = 21,
	VMI_MaterialTextureScaleAccuracy = 22,
	VMI_RequiredTextureResolution  = 23,
	VMI_PathTracing                = 24,
	VMI_RayTracingDebug            = 25,
	VMI_Max                        = 26,
	VMI_Unknown                    = 27
};


// Enum Engine.EMeshBufferAccess
enum class EMeshBufferAccess : uint8_t
{
	EMeshBufferAccess__Default     = 0,
	EMeshBufferAccess__ForceCPUAndGPU = 1,
	EMeshBufferAccess__EMeshBufferAccess_MAX = 2
};


// Enum Engine.EAngularConstraintMotion
enum class EAngularConstraintMotion : uint8_t
{
	ACM_Free                       = 0,
	ACM_Limited                    = 1,
	ACM_Locked                     = 2,
	ACM_MAX                        = 3
};


// Enum Engine.EComponentSocketType
enum class EComponentSocketType : uint8_t
{
	EComponentSocketType__Invalid  = 0,
	EComponentSocketType__Bone     = 1,
	EComponentSocketType__Socket   = 2,
	EComponentSocketType__EComponentSocketType_MAX = 3
};


// Enum Engine.ECurveTableMode
enum class ECurveTableMode : uint8_t
{
	ECurveTableMode__Empty         = 0,
	ECurveTableMode__SimpleCurves  = 1,
	ECurveTableMode__RichCurves    = 2,
	ECurveTableMode__ECurveTableMode_MAX = 3
};


// Enum Engine.EPhysicalSurface
enum class EPhysicalSurface : uint8_t
{
	SurfaceType_Default            = 0,
	SurfaceType1                   = 1,
	SurfaceType2                   = 2,
	SurfaceType3                   = 3,
	SurfaceType4                   = 4,
	SurfaceType5                   = 5,
	SurfaceType6                   = 6,
	SurfaceType7                   = 7,
	SurfaceType8                   = 8,
	SurfaceType9                   = 9,
	SurfaceType10                  = 10,
	SurfaceType11                  = 11,
	SurfaceType12                  = 12,
	SurfaceType13                  = 13,
	SurfaceType14                  = 14,
	SurfaceType15                  = 15,
	SurfaceType16                  = 16,
	SurfaceType17                  = 17,
	SurfaceType18                  = 18,
	SurfaceType19                  = 19,
	SurfaceType20                  = 20,
	SurfaceType21                  = 21,
	SurfaceType22                  = 22,
	SurfaceType23                  = 23,
	SurfaceType24                  = 24,
	SurfaceType25                  = 25,
	SurfaceType26                  = 26,
	SurfaceType27                  = 27,
	SurfaceType28                  = 28,
	SurfaceType29                  = 29,
	SurfaceType30                  = 30,
	SurfaceType31                  = 31,
	SurfaceType32                  = 32,
	SurfaceType33                  = 33,
	SurfaceType34                  = 34,
	SurfaceType35                  = 35,
	SurfaceType36                  = 36,
	SurfaceType37                  = 37,
	SurfaceType38                  = 38,
	SurfaceType39                  = 39,
	SurfaceType40                  = 40,
	SurfaceType41                  = 41,
	SurfaceType42                  = 42,
	SurfaceType43                  = 43,
	SurfaceType44                  = 44,
	SurfaceType45                  = 45,
	SurfaceType46                  = 46,
	SurfaceType47                  = 47,
	SurfaceType48                  = 48,
	SurfaceType49                  = 49,
	SurfaceType50                  = 50,
	SurfaceType51                  = 51,
	SurfaceType52                  = 52,
	SurfaceType53                  = 53,
	SurfaceType54                  = 54,
	SurfaceType55                  = 55,
	SurfaceType56                  = 56,
	SurfaceType57                  = 57,
	SurfaceType58                  = 58,
	SurfaceType59                  = 59,
	SurfaceType60                  = 60,
	SurfaceType61                  = 61,
	SurfaceType62                  = 62,
	SurfaceType_Max                = 63,
	EPhysicalSurface_MAX           = 64
};


// Enum Engine.EWalkableSlopeBehavior
enum class EWalkableSlopeBehavior : uint8_t
{
	WalkableSlope_Default          = 0,
	WalkableSlope_Increase         = 1,
	WalkableSlope_Decrease         = 2,
	WalkableSlope_Unwalkable       = 3,
	WalkableSlope_Max              = 4
};


// Enum Engine.EVectorQuantization
enum class EVectorQuantization : uint8_t
{
	EVectorQuantization__RoundWholeNumber = 0,
	EVectorQuantization__RoundOneDecimal = 1,
	EVectorQuantization__RoundTwoDecimals = 2,
	EVectorQuantization__EVectorQuantization_MAX = 3
};


// Enum Engine.EAutoPossessAI
enum class EAutoPossessAI : uint8_t
{
	EAutoPossessAI__Disabled       = 0,
	EAutoPossessAI__PlacedInWorld  = 1,
	EAutoPossessAI__Spawned        = 2,
	EAutoPossessAI__PlacedInWorldOrSpawned = 3,
	EAutoPossessAI__EAutoPossessAI_MAX = 4
};


// Enum Engine.ESpawnActorCollisionHandlingMethod
enum class ESpawnActorCollisionHandlingMethod : uint8_t
{
	ESpawnActorCollisionHandlingMethod__Undefined = 0,
	ESpawnActorCollisionHandlingMethod__AlwaysSpawn = 1,
	ESpawnActorCollisionHandlingMethod__AdjustIfPossibleButAlwaysSpawn = 2,
	ESpawnActorCollisionHandlingMethod__AdjustIfPossibleButDontSpawnIfColliding = 3,
	ESpawnActorCollisionHandlingMethod__DontSpawnIfColliding = 4,
	ESpawnActorCollisionHandlingMethod__ESpawnActorCollisionHandlingMethod_MAX = 5
};


// Enum Engine.EAutoReceiveInput
enum class EAutoReceiveInput : uint8_t
{
	EAutoReceiveInput__Disabled    = 0,
	EAutoReceiveInput__Player0     = 1,
	EAutoReceiveInput__Player1     = 2,
	EAutoReceiveInput__Player2     = 3,
	EAutoReceiveInput__Player3     = 4,
	EAutoReceiveInput__Player4     = 5,
	EAutoReceiveInput__Player5     = 6,
	EAutoReceiveInput__Player6     = 7,
	EAutoReceiveInput__Player7     = 8,
	EAutoReceiveInput__EAutoReceiveInput_MAX = 9
};


// Enum Engine.ENetRole
enum class ENetRole : uint8_t
{
	ROLE_None                      = 0,
	ROLE_SimulatedProxy            = 1,
	ROLE_AutonomousProxy           = 2,
	ROLE_Authority                 = 3,
	ROLE_MAX                       = 4
};


// Enum Engine.ERotatorQuantization
enum class ERotatorQuantization : uint8_t
{
	ERotatorQuantization__ByteComponents = 0,
	ERotatorQuantization__ShortComponents = 1,
	ERotatorQuantization__ERotatorQuantization_MAX = 2
};


// Enum Engine.ENetDormancy
enum class ENetDormancy : uint8_t
{
	DORM_Never                     = 0,
	DORM_Awake                     = 1,
	DORM_DormantAll                = 2,
	DORM_DormantPartial            = 3,
	DORM_Initial                   = 4,
	DORM_MAX                       = 5
};


// Enum Engine.EConstraintFrame
enum class EConstraintFrame : uint8_t
{
	EConstraintFrame__Frame1       = 0,
	EConstraintFrame__Frame2       = 1,
	EConstraintFrame__EConstraintFrame_MAX = 2
};


// Enum Engine.ETeleportType
enum class ETeleportType : uint8_t
{
	ETeleportType__None            = 0,
	ETeleportType__TeleportPhysics = 1,
	ETeleportType__ResetPhysics    = 2,
	ETeleportType__ETeleportType_MAX = 3
};


// Enum Engine.EUpdateRateShiftBucket
enum class EUpdateRateShiftBucket : uint8_t
{
	EUpdateRateShiftBucket__ShiftBucket0 = 0,
	EUpdateRateShiftBucket__ShiftBucket1 = 1,
	EUpdateRateShiftBucket__ShiftBucket2 = 2,
	EUpdateRateShiftBucket__ShiftBucket3 = 3,
	EUpdateRateShiftBucket__ShiftBucket4 = 4,
	EUpdateRateShiftBucket__ShiftBucket5 = 5,
	EUpdateRateShiftBucket__ShiftBucketMax = 6,
	EUpdateRateShiftBucket__EUpdateRateShiftBucket_MAX = 7
};


// Enum Engine.EShadowMapFlags
enum class EShadowMapFlags : uint8_t
{
	SMF_None                       = 0,
	SMF_Streamed                   = 1,
	SMF_MAX                        = 2
};


// Enum Engine.ECollisionEnabled
enum class ECollisionEnabled : uint8_t
{
	ECollisionEnabled__NoCollision = 0,
	ECollisionEnabled__QueryOnly   = 1,
	ECollisionEnabled__PhysicsOnly = 2,
	ECollisionEnabled__QueryAndPhysics = 3,
	ECollisionEnabled__ECollisionEnabled_MAX = 4
};


// Enum Engine.ESleepFamily
enum class ESleepFamily : uint8_t
{
	ESleepFamily__Normal           = 0,
	ESleepFamily__Sensitive        = 1,
	ESleepFamily__Custom           = 2,
	ESleepFamily__ESleepFamily_MAX = 3
};


// Enum Engine.ELightMapPaddingType
enum class ELightMapPaddingType : uint8_t
{
	LMPT_NormalPadding             = 0,
	LMPT_PrePadding                = 1,
	LMPT_NoPadding                 = 2,
	LMPT_MAX                       = 3
};


// Enum Engine.ETimelineSigType
enum class ETimelineSigType : uint8_t
{
	ETS_EventSignature             = 0,
	ETS_FloatSignature             = 1,
	ETS_VectorSignature            = 2,
	ETS_LinearColorSignature       = 3,
	ETS_InvalidSignature           = 4,
	ETS_MAX                        = 5
};


// Enum Engine.EFilterInterpolationType
enum class EFilterInterpolationType : uint8_t
{
	BSIT_Average                   = 0,
	BSIT_Linear                    = 1,
	BSIT_Cubic                     = 2,
	BSIT_MAX                       = 3
};


// Enum Engine.ECollisionResponse
enum class ECollisionResponse : uint8_t
{
	ECR_Ignore                     = 0,
	ECR_Overlap                    = 1,
	ECR_Block                      = 2,
	ECR_MAX                        = 3
};


// Enum Engine.EOverlapFilterOption
enum class EOverlapFilterOption : uint8_t
{
	OverlapFilter_All              = 0,
	OverlapFilter_DynamicOnly      = 1,
	OverlapFilter_StaticOnly       = 2,
	OverlapFilter_MAX              = 3
};


// Enum Engine.ECollisionChannel
enum class ECollisionChannel : uint8_t
{
	ECC_WorldStatic                = 0,
	ECC_WorldDynamic               = 1,
	ECC_Pawn                       = 2,
	ECC_Visibility                 = 3,
	ECC_Camera                     = 4,
	ECC_PhysicsBody                = 5,
	ECC_Vehicle                    = 6,
	ECC_Destructible               = 7,
	ECC_EngineTraceChannel1        = 8,
	ECC_EngineTraceChannel2        = 9,
	ECC_EngineTraceChannel3        = 10,
	ECC_EngineTraceChannel4        = 11,
	ECC_EngineTraceChannel5        = 12,
	ECC_EngineTraceChannel6        = 13,
	ECC_GameTraceChannel1          = 14,
	ECC_GameTraceChannel2          = 15,
	ECC_GameTraceChannel3          = 16,
	ECC_GameTraceChannel4          = 17,
	ECC_GameTraceChannel5          = 18,
	ECC_GameTraceChannel6          = 19,
	ECC_GameTraceChannel7          = 20,
	ECC_GameTraceChannel8          = 21,
	ECC_GameTraceChannel9          = 22,
	ECC_GameTraceChannel10         = 23,
	ECC_GameTraceChannel11         = 24,
	ECC_GameTraceChannel12         = 25,
	ECC_GameTraceChannel13         = 26,
	ECC_GameTraceChannel14         = 27,
	ECC_GameTraceChannel15         = 28,
	ECC_GameTraceChannel16         = 29,
	ECC_GameTraceChannel17         = 30,
	ECC_GameTraceChannel18         = 31,
	ECC_OverlapAll_Deprecated      = 32,
	ECC_MAX                        = 33
};


// Enum Engine.ELightingBuildQuality
enum class ELightingBuildQuality : uint8_t
{
	Quality_Preview                = 0,
	Quality_Medium                 = 1,
	Quality_High                   = 2,
	Quality_Production             = 3,
	Quality_MAX                    = 4
};


// Enum Engine.ENetworkSmoothingMode
enum class ENetworkSmoothingMode : uint8_t
{
	ENetworkSmoothingMode__Disabled = 0,
	ENetworkSmoothingMode__Linear  = 1,
	ENetworkSmoothingMode__Exponential = 2,
	ENetworkSmoothingMode__Replay  = 3,
	ENetworkSmoothingMode__ENetworkSmoothingMode_MAX = 4
};


// Enum Engine.EMaterialTessellationMode
enum class EMaterialTessellationMode : uint8_t
{
	MTM_NoTessellation             = 0,
	MTM_FlatTessellation           = 1,
	MTM_PNTriangles                = 2,
	MTM_MAX                        = 3
};


// Enum Engine.EMaterialSamplerType
enum class EMaterialSamplerType : uint8_t
{
	SAMPLERTYPE_Color              = 0,
	SAMPLERTYPE_Grayscale          = 1,
	SAMPLERTYPE_Alpha              = 2,
	SAMPLERTYPE_Normal             = 3,
	SAMPLERTYPE_Masks              = 4,
	SAMPLERTYPE_DistanceFieldFont  = 5,
	SAMPLERTYPE_LinearColor        = 6,
	SAMPLERTYPE_LinearGrayscale    = 7,
	SAMPLERTYPE_External           = 8,
	SAMPLERTYPE_MAX                = 9
};


// Enum Engine.ETrailWidthMode
enum class ETrailWidthMode : uint8_t
{
	ETrailWidthMode_FromCentre     = 0,
	ETrailWidthMode_FromFirst      = 1,
	ETrailWidthMode_FromSecond     = 2,
	ETrailWidthMode_MAX            = 3
};


// Enum Engine.EGBufferFormat
enum class EGBufferFormat : uint8_t
{
	EGBufferFormat__Force8BitsPerChannel = 0,
	EGBufferFormat__Default        = 1,
	EGBufferFormat__HighPrecisionNormals = 2,
	EGBufferFormat__Force16BitsPerChannel = 3,
	EGBufferFormat__EGBufferFormat_MAX = 4
};


// Enum Engine.ERadialImpulseFalloff
enum class ERadialImpulseFalloff : uint8_t
{
	RIF_Constant                   = 0,
	RIF_Linear                     = 1,
	RIF_MAX                        = 2
};


// Enum Engine.EMaterialShadingModel
enum class EMaterialShadingModel : uint8_t
{
	MSM_Unlit                      = 0,
	MSM_DefaultLit                 = 1,
	MSM_Subsurface                 = 2,
	MSM_PreintegratedSkin          = 3,
	MSM_ClearCoat                  = 4,
	MSM_SubsurfaceProfile          = 5,
	MSM_TwoSidedFoliage            = 6,
	MSM_Hair                       = 7,
	MSM_Cloth                      = 8,
	MSM_Eye                        = 9,
	MSM_MAX                        = 10
};


// Enum Engine.ESceneCaptureSource
enum class ESceneCaptureSource : uint8_t
{
	SCS_SceneColorHDR              = 0,
	SCS_SceneColorHDRNoAlpha       = 1,
	SCS_FinalColorLDR              = 2,
	SCS_SceneColorSceneDepth       = 3,
	SCS_SceneDepth                 = 4,
	SCS_DeviceDepth                = 5,
	SCS_Normal                     = 6,
	SCS_BaseColor                  = 7,
	SCS_MAX                        = 8
};


// Enum Engine.EParticleCollisionMode
enum class EParticleCollisionMode : uint8_t
{
	EParticleCollisionMode__SceneDepth = 0,
	EParticleCollisionMode__DistanceField = 1,
	EParticleCollisionMode__EParticleCollisionMode_MAX = 2
};


// Enum Engine.ERefractionMode
enum class ERefractionMode : uint8_t
{
	RM_IndexOfRefraction           = 0,
	RM_PixelNormalOffset           = 1,
	RM_MAX                         = 2
};


// Enum Engine.EBlendMode
enum class EBlendMode : uint8_t
{
	BLEND_Opaque                   = 0,
	BLEND_Masked                   = 1,
	BLEND_Translucent              = 2,
	BLEND_Additive                 = 3,
	BLEND_Modulate                 = 4,
	BLEND_AlphaComposite           = 5,
	BLEND_MAX                      = 6
};


// Enum Engine.ESceneCaptureCompositeMode
enum class ESceneCaptureCompositeMode : uint8_t
{
	SCCM_Overwrite                 = 0,
	SCCM_Additive                  = 1,
	SCCM_Composite                 = 2,
	SCCM_MAX                       = 3
};


// Enum Engine.ETranslucentSortPolicy
enum class ETranslucentSortPolicy : uint8_t
{
	ETranslucentSortPolicy__SortByDistance = 0,
	ETranslucentSortPolicy__SortByProjectedZ = 1,
	ETranslucentSortPolicy__SortAlongAxis = 2,
	ETranslucentSortPolicy__ETranslucentSortPolicy_MAX = 3
};


// Enum Engine.ESamplerSourceMode
enum class ESamplerSourceMode : uint8_t
{
	SSM_FromTextureAsset           = 0,
	SSM_Wrap_WorldGroupSettings    = 1,
	SSM_Clamp_WorldGroupSettings   = 2,
	SSM_MAX                        = 3
};


// Enum Engine.ETranslucencyLightingMode
enum class ETranslucencyLightingMode : uint8_t
{
	TLM_VolumetricNonDirectional   = 0,
	TLM_VolumetricDirectional      = 1,
	TLM_VolumetricPerVertexNonDirectional = 2,
	TLM_VolumetricPerVertexDirectional = 3,
	TLM_Surface                    = 4,
	TLM_SurfacePerPixelLighting    = 5,
	TLM_MAX                        = 6
};


// Enum Engine.ELightmapType
enum class ELightmapType : uint8_t
{
	ELightmapType__Default         = 0,
	ELightmapType__ForceSurface    = 1,
	ELightmapType__ForceVolumetric = 2,
	ELightmapType__ELightmapType_MAX = 3
};


// Enum Engine.EIndirectLightingCacheQuality
enum class EIndirectLightingCacheQuality : uint8_t
{
	ILCQ_Off                       = 0,
	ILCQ_Point                     = 1,
	ILCQ_Volume                    = 2,
	ILCQ_MAX                       = 3
};


// Enum Engine.EFontCacheType
enum class EFontCacheType : uint8_t
{
	EFontCacheType__Offline        = 0,
	EFontCacheType__Runtime        = 1,
	EFontCacheType__EFontCacheType_MAX = 2
};


// Enum Engine.ESceneDepthPriorityGroup
enum class ESceneDepthPriorityGroup : uint8_t
{
	SDPG_World                     = 0,
	SDPG_Foreground                = 1,
	SDPG_MAX                       = 2
};


// Enum Engine.EOcclusionCombineMode
enum class EOcclusionCombineMode : uint8_t
{
	OCM_Minimum                    = 0,
	OCM_Multiply                   = 1,
	OCM_MAX                        = 2
};


// Enum Engine.EAspectRatioAxisConstraint
enum class EAspectRatioAxisConstraint : uint8_t
{
	AspectRatio_MaintainYFOV       = 0,
	AspectRatio_MaintainXFOV       = 1,
	AspectRatio_MajorAxisFOV       = 2,
	AspectRatio_MAX                = 3
};


// Enum Engine.EStandbyType
enum class EStandbyType : uint8_t
{
	STDBY_Rx                       = 0,
	STDBY_Tx                       = 1,
	STDBY_BadPing                  = 2,
	STDBY_MAX                      = 3
};


// Enum Engine.EAdManagerDelegate
enum class EAdManagerDelegate : uint8_t
{
	AMD_ClickedBanner              = 0,
	AMD_UserClosedAd               = 1,
	AMD_MAX                        = 2
};


// Enum Engine.ESuggestProjVelocityTraceOption
enum class ESuggestProjVelocityTraceOption : uint8_t
{
	ESuggestProjVelocityTraceOption__DoNotTrace = 0,
	ESuggestProjVelocityTraceOption__TraceFullPath = 1,
	ESuggestProjVelocityTraceOption__OnlyTraceWhileAscending = 2,
	ESuggestProjVelocityTraceOption__ESuggestProjVelocityTraceOption_MAX = 3
};


// Enum Engine.EWindowMode
enum class EWindowMode : uint8_t
{
	EWindowMode__Fullscreen        = 0,
	EWindowMode__WindowedFullscreen = 1,
	EWindowMode__Windowed          = 2,
	EWindowMode__EWindowMode_MAX   = 3
};


// Enum Engine.EImportanceWeight
enum class EImportanceWeight : uint8_t
{
	EImportanceWeight__Luminance   = 0,
	EImportanceWeight__Red         = 1,
	EImportanceWeight__Green       = 2,
	EImportanceWeight__Blue        = 3,
	EImportanceWeight__Alpha       = 4,
	EImportanceWeight__EImportanceWeight_MAX = 5
};


// Enum Engine.EAnimAlphaInputType
enum class EAnimAlphaInputType : uint8_t
{
	EAnimAlphaInputType__Float     = 0,
	EAnimAlphaInputType__Bool      = 1,
	EAnimAlphaInputType__Curve     = 2,
	EAnimAlphaInputType__EAnimAlphaInputType_MAX = 3
};


// Enum Engine.EFontImportCharacterSet
enum class EFontImportCharacterSet : uint8_t
{
	FontICS_Default                = 0,
	FontICS_Ansi                   = 1,
	FontICS_Symbol                 = 2,
	FontICS_MAX                    = 3
};


// Enum Engine.ETrackActiveCondition
enum class ETrackActiveCondition : uint8_t
{
	ETAC_Always                    = 0,
	ETAC_GoreEnabled               = 1,
	ETAC_GoreDisabled              = 2,
	ETAC_MAX                       = 3
};


// Enum Engine.EInterpTrackMoveRotMode
enum class EInterpTrackMoveRotMode : uint8_t
{
	IMR_Keyframed                  = 0,
	IMR_LookAtGroup                = 1,
	IMR_Ignore                     = 2,
	IMR_MAX                        = 3
};


// Enum Engine.EInterpMoveAxis
enum class EInterpMoveAxis : uint8_t
{
	AXIS_TranslationX              = 0,
	AXIS_TranslationY              = 1,
	AXIS_TranslationZ              = 2,
	AXIS_RotationX                 = 3,
	AXIS_RotationY                 = 4,
	AXIS_RotationZ                 = 5,
	AXIS_MAX                       = 6
};


// Enum Engine.ETrackToggleAction
enum class ETrackToggleAction : uint8_t
{
	ETTA_Off                       = 0,
	ETTA_On                        = 1,
	ETTA_Toggle                    = 2,
	ETTA_Trigger                   = 3,
	ETTA_MAX                       = 4
};


// Enum Engine.EVisibilityTrackCondition
enum class EVisibilityTrackCondition : uint8_t
{
	EVTC_Always                    = 0,
	EVTC_GoreEnabled               = 1,
	EVTC_GoreDisabled              = 2,
	EVTC_MAX                       = 3
};


// Enum Engine.EControllerAnalogStick
enum class EControllerAnalogStick : uint8_t
{
	EControllerAnalogStick__CAS_LeftStick = 0,
	EControllerAnalogStick__CAS_RightStick = 1,
	EControllerAnalogStick__CAS_MAX = 2
};


// Enum Engine.ESlateGesture
enum class ESlateGesture : uint8_t
{
	ESlateGesture__None            = 0,
	ESlateGesture__Scroll          = 1,
	ESlateGesture__Magnify         = 2,
	ESlateGesture__Swipe           = 3,
	ESlateGesture__Rotate          = 4,
	ESlateGesture__LongPress       = 5,
	ESlateGesture__ESlateGesture_MAX = 6
};


// Enum Engine.EVisibilityTrackAction
enum class EVisibilityTrackAction : uint8_t
{
	EVTA_Hide                      = 0,
	EVTA_Show                      = 1,
	EVTA_Toggle                    = 2,
	EVTA_MAX                       = 3
};


// Enum Engine.EEasingFunc
enum class EEasingFunc : uint8_t
{
	EEasingFunc__Linear            = 0,
	EEasingFunc__Step              = 1,
	EEasingFunc__SinusoidalIn      = 2,
	EEasingFunc__SinusoidalOut     = 3,
	EEasingFunc__SinusoidalInOut   = 4,
	EEasingFunc__EaseIn            = 5,
	EEasingFunc__EaseOut           = 6,
	EEasingFunc__EaseInOut         = 7,
	EEasingFunc__ExpoIn            = 8,
	EEasingFunc__ExpoOut           = 9,
	EEasingFunc__ExpoInOut         = 10,
	EEasingFunc__CircularIn        = 11,
	EEasingFunc__CircularOut       = 12,
	EEasingFunc__CircularInOut     = 13,
	EEasingFunc__EEasingFunc_MAX   = 14
};


// Enum Engine.ELerpInterpolationMode
enum class ELerpInterpolationMode : uint8_t
{
	ELerpInterpolationMode__QuatInterp = 0,
	ELerpInterpolationMode__EulerInterp = 1,
	ELerpInterpolationMode__DualQuatInterp = 2,
	ELerpInterpolationMode__ELerpInterpolationMode_MAX = 3
};


// Enum Engine.ERoundingMode
enum class ERoundingMode : uint8_t
{
	HalfToEven                     = 0,
	HalfFromZero                   = 1,
	HalfToZero                     = 2,
	FromZero                       = 3,
	ToZero                         = 4,
	ToNegativeInfinity             = 5,
	ToPositiveInfinity             = 6,
	ERoundingMode_MAX              = 7
};


// Enum Engine.EDecalBlendMode
enum class EDecalBlendMode : uint8_t
{
	DBM_Translucent                = 0,
	DBM_Stain                      = 1,
	DBM_Normal                     = 2,
	DBM_Emissive                   = 3,
	DBM_DBuffer_ColorNormalRoughness = 4,
	DBM_DBuffer_Color              = 5,
	DBM_DBuffer_ColorNormal        = 6,
	DBM_DBuffer_ColorRoughness     = 7,
	DBM_DBuffer_Normal             = 8,
	DBM_DBuffer_NormalRoughness    = 9,
	DBM_DBuffer_Roughness          = 10,
	DBM_DBuffer_Emissive           = 11,
	DBM_DBuffer_AlphaComposite     = 12,
	DBM_DBuffer_EmissiveAlphaComposite = 13,
	DBM_Volumetric_DistanceFunction = 14,
	DBM_AlphaComposite             = 15,
	DBM_AmbientOcclusion           = 16,
	DBM_MAX                        = 17
};


// Enum Engine.EMaterialDecalResponse
enum class EMaterialDecalResponse : uint8_t
{
	MDR_None                       = 0,
	MDR_ColorNormalRoughness       = 1,
	MDR_Color                      = 2,
	MDR_ColorNormal                = 3,
	MDR_ColorRoughness             = 4,
	MDR_Normal                     = 5,
	MDR_NormalRoughness            = 6,
	MDR_Roughness                  = 7,
	MDR_MAX                        = 8
};


// Enum Engine.EMaterialAttributeBlend
enum class EMaterialAttributeBlend : uint8_t
{
	EMaterialAttributeBlend__Blend = 0,
	EMaterialAttributeBlend__UseA  = 1,
	EMaterialAttributeBlend__UseB  = 2,
	EMaterialAttributeBlend__EMaterialAttributeBlend_MAX = 3
};


// Enum Engine.EChannelMaskParameterColor
enum class EChannelMaskParameterColor : uint8_t
{
	EChannelMaskParameterColor__Red = 0,
	EChannelMaskParameterColor__Green = 1,
	EChannelMaskParameterColor__Blue = 2,
	EChannelMaskParameterColor__Alpha = 3,
	EChannelMaskParameterColor__EChannelMaskParameterColor_MAX = 4
};


// Enum Engine.EStreamingVolumeUsage
enum class EStreamingVolumeUsage : uint8_t
{
	SVB_Loading                    = 0,
	SVB_LoadingAndVisibility       = 1,
	SVB_VisibilityBlockingOnLoad   = 2,
	SVB_BlockingOnLoad             = 3,
	SVB_LoadingNotVisible          = 4,
	SVB_MAX                        = 5
};


// Enum Engine.ETextureColorChannel
enum class ETextureColorChannel : uint8_t
{
	TCC_Red                        = 0,
	TCC_Green                      = 1,
	TCC_Blue                       = 2,
	TCC_Alpha                      = 3,
	TCC_MAX                        = 4
};


// Enum Engine.EDepthOfFieldFunctionValue
enum class EDepthOfFieldFunctionValue : uint8_t
{
	TDOF_NearAndFarMask            = 0,
	TDOF_NearMask                  = 1,
	TDOF_FarMask                   = 2,
	TDOF_CircleOfConfusionRadius   = 3,
	TDOF_MAX                       = 4
};


// Enum Engine.ECustomMaterialOutputType
enum class ECustomMaterialOutputType : uint8_t
{
	CMOT_Float1                    = 0,
	CMOT_Float2                    = 1,
	CMOT_Float3                    = 2,
	CMOT_Float4                    = 3,
	CMOT_MAX                       = 4
};


// Enum Engine.EMaterialSceneAttributeInputMode
enum class EMaterialSceneAttributeInputMode : uint8_t
{
	EMaterialSceneAttributeInputMode__Coordinates = 0,
	EMaterialSceneAttributeInputMode__OffsetFraction = 1,
	EMaterialSceneAttributeInputMode__EMaterialSceneAttributeInputMode_MAX = 2
};


// Enum Engine.ESpeedTreeLODType
enum class ESpeedTreeLODType : uint8_t
{
	STLOD_Pop                      = 0,
	STLOD_Smooth                   = 1,
	STLOD_MAX                      = 2
};


// Enum Engine.EFunctionInputType
enum class EFunctionInputType : uint8_t
{
	FunctionInput_Scalar           = 0,
	FunctionInput_Vector2          = 1,
	FunctionInput_Vector3          = 2,
	FunctionInput_Vector4          = 3,
	FunctionInput_Texture2D        = 4,
	FunctionInput_TextureCube      = 5,
	FunctionInput_VolumeTexture    = 6,
	FunctionInput_StaticBool       = 7,
	FunctionInput_MaterialAttributes = 8,
	FunctionInput_TextureExternal  = 9,
	FunctionInput_MAX              = 10
};


// Enum Engine.ESceneTextureId
enum class ESceneTextureId : uint8_t
{
	PPI_SceneColor                 = 0,
	PPI_SceneDepth                 = 1,
	PPI_DiffuseColor               = 2,
	PPI_SpecularColor              = 3,
	PPI_SubsurfaceColor            = 4,
	PPI_BaseColor                  = 5,
	PPI_Specular                   = 6,
	PPI_Metallic                   = 7,
	PPI_WorldNormal                = 8,
	PPI_SeparateTranslucency       = 9,
	PPI_Opacity                    = 10,
	PPI_Roughness                  = 11,
	PPI_MaterialAO                 = 12,
	PPI_CustomDepth                = 13,
	PPI_PostProcessInput0          = 14,
	PPI_PostProcessInput1          = 15,
	PPI_PostProcessInput2          = 16,
	PPI_PostProcessInput3          = 17,
	PPI_PostProcessInput4          = 18,
	PPI_PostProcessInput5          = 19,
	PPI_PostProcessInput6          = 20,
	PPI_DecalMask                  = 21,
	PPI_ShadingModelColor          = 22,
	PPI_ShadingModelID             = 23,
	PPI_AmbientOcclusion           = 24,
	PPI_CustomStencil              = 25,
	PPI_StoredBaseColor            = 26,
	PPI_StoredSpecular             = 27,
	PPI_Velocity                   = 28,
	PPI_MAX                        = 29
};


// Enum Engine.ESpeedTreeWindType
enum class ESpeedTreeWindType : uint8_t
{
	STW_None                       = 0,
	STW_Fastest                    = 1,
	STW_Fast                       = 2,
	STW_Better                     = 3,
	STW_Best                       = 4,
	STW_Palm                       = 5,
	STW_BestPlus                   = 6,
	STW_MAX                        = 7
};


// Enum Engine.ESpeedTreeGeometryType
enum class ESpeedTreeGeometryType : uint8_t
{
	STG_Branch                     = 0,
	STG_Frond                      = 1,
	STG_Leaf                       = 2,
	STG_FacingLeaf                 = 3,
	STG_Billboard                  = 4,
	STG_MAX                        = 5
};


// Enum Engine.ETextureMipValueMode
enum class ETextureMipValueMode : uint8_t
{
	TMVM_None                      = 0,
	TMVM_MipLevel                  = 1,
	TMVM_MipBias                   = 2,
	TMVM_Derivative                = 3,
	TMVM_MAX                       = 4
};


// Enum Engine.EMaterialVectorCoordTransform
enum class EMaterialVectorCoordTransform : uint8_t
{
	TRANSFORM_Tangent              = 0,
	TRANSFORM_Local                = 1,
	TRANSFORM_World                = 2,
	TRANSFORM_View                 = 3,
	TRANSFORM_Camera               = 4,
	TRANSFORM_ParticleWorld        = 5,
	TRANSFORM_MAX                  = 6
};


// Enum Engine.EMaterialVectorCoordTransformSource
enum class EMaterialVectorCoordTransformSource : uint8_t
{
	TRANSFORMSOURCE_Tangent        = 0,
	TRANSFORMSOURCE_Local          = 1,
	TRANSFORMSOURCE_World          = 2,
	TRANSFORMSOURCE_View           = 3,
	TRANSFORMSOURCE_Camera         = 4,
	TRANSFORMSOURCE_ParticleWorld  = 5,
	TRANSFORMSOURCE_MAX            = 6
};


// Enum Engine.EVectorNoiseFunction
enum class EVectorNoiseFunction : uint8_t
{
	VNF_CellnoiseALU               = 0,
	VNF_VectorALU                  = 1,
	VNF_GradientALU                = 2,
	VNF_CurlALU                    = 3,
	VNF_VoronoiALU                 = 4,
	VNF_MAX                        = 5
};


// Enum Engine.EClampMode
enum class EClampMode : uint8_t
{
	CMODE_Clamp                    = 0,
	CMODE_ClampMin                 = 1,
	CMODE_ClampMax                 = 2,
	CMODE_MAX                      = 3
};


// Enum Engine.EWorldPositionIncludedOffsets
enum class EWorldPositionIncludedOffsets : uint8_t
{
	WPT_Default                    = 0,
	WPT_ExcludeAllShaderOffsets    = 1,
	WPT_CameraRelative             = 2,
	WPT_CameraRelativeNoOffsets    = 3,
	WPT_MAX                        = 4
};


// Enum Engine.EMaterialPositionTransformSource
enum class EMaterialPositionTransformSource : uint8_t
{
	TRANSFORMPOSSOURCE_Local       = 0,
	TRANSFORMPOSSOURCE_World       = 1,
	TRANSFORMPOSSOURCE_TranslatedWorld = 2,
	TRANSFORMPOSSOURCE_View        = 3,
	TRANSFORMPOSSOURCE_Camera      = 4,
	TRANSFORMPOSSOURCE_Particle    = 5,
	TRANSFORMPOSSOURCE_MAX         = 6
};


// Enum Engine.EMaterialExposedViewProperty
enum class EMaterialExposedViewProperty : uint8_t
{
	MEVP_BufferSize                = 0,
	MEVP_FieldOfView               = 1,
	MEVP_TanHalfFieldOfView        = 2,
	MEVP_ViewSize                  = 3,
	MEVP_WorldSpaceViewPosition    = 4,
	MEVP_WorldSpaceCameraPosition  = 5,
	MEVP_ViewportOffset            = 6,
	MEVP_MAX                       = 7
};


// Enum Engine.ENoiseFunction
enum class ENoiseFunction : uint8_t
{
	NOISEFUNCTION_SimplexTex       = 0,
	NOISEFUNCTION_GradientTex      = 1,
	NOISEFUNCTION_GradientTex3D    = 2,
	NOISEFUNCTION_GradientALU      = 3,
	NOISEFUNCTION_ValueALU         = 4,
	NOISEFUNCTION_VoronoiALU       = 5,
	NOISEFUNCTION_MAX              = 6
};


// Enum Engine.EMaterialExposedTextureProperty
enum class EMaterialExposedTextureProperty : uint8_t
{
	TMTM_TextureSize               = 0,
	TMTM_TexelSize                 = 1,
	TMTM_MAX                       = 2
};


// Enum Engine.EMaterialFunctionUsage
enum class EMaterialFunctionUsage : uint8_t
{
	Default                        = 0,
	MaterialLayer                  = 1,
	MaterialLayerBlend             = 2,
	EMaterialFunctionUsage_MAX     = 3
};


// Enum Engine.EMaterialUsage
enum class EMaterialUsage : uint8_t
{
	MATUSAGE_SkeletalMesh          = 0,
	MATUSAGE_ParticleSprites       = 1,
	MATUSAGE_BeamTrails            = 2,
	MATUSAGE_MeshParticles         = 3,
	MATUSAGE_StaticLighting        = 4,
	MATUSAGE_MorphTargets          = 5,
	MATUSAGE_SplineMesh            = 6,
	MATUSAGE_InstancedStaticMeshes = 7,
	MATUSAGE_InstancedSplineMeshes = 8,
	MATUSAGE_Clothing              = 9,
	MATUSAGE_NiagaraSprites        = 10,
	MATUSAGE_NiagaraRibbons        = 11,
	MATUSAGE_NiagaraMeshParticles  = 12,
	MATUSAGE_GeometryCache         = 13,
	MATUSAGE_MAX                   = 14
};


// Enum Engine.ETextureSizingType
enum class ETextureSizingType : uint8_t
{
	TextureSizingType_UseSingleTextureSize = 0,
	TextureSizingType_UseAutomaticBiasedSizes = 1,
	TextureSizingType_UseManualOverrideTextureSize = 2,
	TextureSizingType_UseSimplygonAutomaticSizing = 3,
	TextureSizingType_MAX          = 4
};


// Enum Engine.EMaterialMergeType
enum class EMaterialMergeType : uint8_t
{
	MaterialMergeType_Default      = 0,
	MaterialMergeType_Simplygon    = 1,
	MaterialMergeType_MAX          = 2
};


// Enum Engine.EMeshInstancingReplacementMethod
enum class EMeshInstancingReplacementMethod : uint8_t
{
	EMeshInstancingReplacementMethod__RemoveOriginalActors = 0,
	EMeshInstancingReplacementMethod__KeepOriginalActorsAsEditorOnly = 1,
	EMeshInstancingReplacementMethod__EMeshInstancingReplacementMethod_MAX = 2
};


// Enum Engine.EMaterialParameterAssociation
enum class EMaterialParameterAssociation : uint8_t
{
	LayerParameter                 = 0,
	BlendParameter                 = 1,
	GlobalParameter                = 2,
	EMaterialParameterAssociation_MAX = 3
};


// Enum Engine.EMaterialDomain
enum class EMaterialDomain : uint8_t
{
	MD_Surface                     = 0,
	MD_DeferredDecal               = 1,
	MD_LightFunction               = 2,
	MD_Volume                      = 3,
	MD_PostProcess                 = 4,
	MD_UI                          = 5,
	MD_MAX                         = 6
};


// Enum Engine.EMeshMergeType
enum class EMeshMergeType : uint8_t
{
	EMeshMergeType__MeshMergeType_Default = 0,
	EMeshMergeType__MeshMergeType_MergeActor = 1,
	EMeshMergeType__MeshMergeType_MAX = 2
};


// Enum Engine.EProxyNormalComputationMethod
enum class EProxyNormalComputationMethod : uint8_t
{
	EProxyNormalComputationMethod__AngleWeighted = 0,
	EProxyNormalComputationMethod__AreaWeighted = 1,
	EProxyNormalComputationMethod__EqualWeighted = 2,
	EProxyNormalComputationMethod__EProxyNormalComputationMethod_MAX = 3
};


// Enum Engine.EUVOutput
enum class EUVOutput : uint8_t
{
	EUVOutput__DoNotOutputChannel  = 0,
	EUVOutput__OutputChannel       = 1,
	EUVOutput__EUVOutput_MAX       = 2
};


// Enum Engine.EStaticMeshReductionTerimationCriterion
enum class EStaticMeshReductionTerimationCriterion : uint8_t
{
	EStaticMeshReductionTerimationCriterion__Triangles = 0,
	EStaticMeshReductionTerimationCriterion__Vertices = 1,
	EStaticMeshReductionTerimationCriterion__Any = 2,
	EStaticMeshReductionTerimationCriterion__EStaticMeshReductionTerimationCriterion_MAX = 3
};


// Enum Engine.EMeshFeatureImportance
enum class EMeshFeatureImportance : uint8_t
{
	EMeshFeatureImportance__Off    = 0,
	EMeshFeatureImportance__Lowest = 1,
	EMeshFeatureImportance__Low    = 2,
	EMeshFeatureImportance__Normal = 3,
	EMeshFeatureImportance__High   = 4,
	EMeshFeatureImportance__Highest = 5,
	EMeshFeatureImportance__EMeshFeatureImportance_MAX = 6
};


// Enum Engine.EVertexPaintAxis
enum class EVertexPaintAxis : uint8_t
{
	EVertexPaintAxis__X            = 0,
	EVertexPaintAxis__Y            = 1,
	EVertexPaintAxis__Z            = 2,
	EVertexPaintAxis__EVertexPaintAxis_MAX = 3
};


// Enum Engine.ELandscapeCullingPrecision
enum class ELandscapeCullingPrecision : uint8_t
{
	ELandscapeCullingPrecision__High = 0,
	ELandscapeCullingPrecision__Medium = 1,
	ELandscapeCullingPrecision__Low = 2,
	ELandscapeCullingPrecision__ELandscapeCullingPrecision_MAX = 3
};


// Enum Engine.EMicroTransactionResult
enum class EMicroTransactionResult : uint8_t
{
	MTR_Succeeded                  = 0,
	MTR_Failed                     = 1,
	MTR_Canceled                   = 2,
	MTR_RestoredFromServer         = 3,
	MTR_MAX                        = 4
};


// Enum Engine.FNavigationSystemRunMode
enum class EFNavigationSystemRunMode : uint8_t
{
	FNavigationSystemRunMode__InvalidMode = 0,
	FNavigationSystemRunMode__GameMode = 1,
	FNavigationSystemRunMode__EditorMode = 2,
	FNavigationSystemRunMode__SimulationMode = 3,
	FNavigationSystemRunMode__PIEMode = 4,
	FNavigationSystemRunMode__FNavigationSystemRunMode_MAX = 5
};


// Enum Engine.ENavigationQueryResult
enum class ENavigationQueryResult : uint8_t
{
	ENavigationQueryResult__Invalid = 0,
	ENavigationQueryResult__Error  = 1,
	ENavigationQueryResult__Fail   = 2,
	ENavigationQueryResult__Success = 3,
	ENavigationQueryResult__ENavigationQueryResult_MAX = 4
};


// Enum Engine.EMeshLODSelectionType
enum class EMeshLODSelectionType : uint8_t
{
	EMeshLODSelectionType__AllLODs = 0,
	EMeshLODSelectionType__SpecificLOD = 1,
	EMeshLODSelectionType__CalculateLOD = 2,
	EMeshLODSelectionType__LowestDetailLOD = 3,
	EMeshLODSelectionType__EMeshLODSelectionType_MAX = 4
};


// Enum Engine.ENavDataGatheringModeConfig
enum class ENavDataGatheringModeConfig : uint8_t
{
	ENavDataGatheringModeConfig__Invalid = 0,
	ENavDataGatheringModeConfig__Instant = 1,
	ENavDataGatheringModeConfig__Lazy = 2,
	ENavDataGatheringModeConfig__ENavDataGatheringModeConfig_MAX = 3
};


// Enum Engine.ENavigationOptionFlag
enum class ENavigationOptionFlag : uint8_t
{
	ENavigationOptionFlag__Default = 0,
	ENavigationOptionFlag__Enable  = 1,
	ENavigationOptionFlag__Disable = 2,
	ENavigationOptionFlag__MAX     = 3
};


// Enum Engine.ENavPathEvent
enum class ENavPathEvent : uint8_t
{
	ENavPathEvent__Cleared         = 0,
	ENavPathEvent__NewPath         = 1,
	ENavPathEvent__UpdatedDueToGoalMoved = 2,
	ENavPathEvent__UpdatedDueToNavigationChanged = 3,
	ENavPathEvent__Invalidated     = 4,
	ENavPathEvent__RePathFailed    = 5,
	ENavPathEvent__MetaPathUpdate  = 6,
	ENavPathEvent__Custom          = 7,
	ENavPathEvent__ENavPathEvent_MAX = 8
};


// Enum Engine.EParticleSubUVInterpMethod
enum class EParticleSubUVInterpMethod : uint8_t
{
	PSUVIM_None                    = 0,
	PSUVIM_Linear                  = 1,
	PSUVIM_Linear_Blend            = 2,
	PSUVIM_Random                  = 3,
	PSUVIM_Random_Blend            = 4,
	PSUVIM_MAX                     = 5
};


// Enum Engine.EMicroTransactionDelegate
enum class EMicroTransactionDelegate : uint8_t
{
	MTD_PurchaseQueryComplete      = 0,
	MTD_PurchaseComplete           = 1,
	MTD_MAX                        = 2
};


// Enum Engine.EParticleBurstMethod
enum class EParticleBurstMethod : uint8_t
{
	EPBM_Instant                   = 0,
	EPBM_Interpolated              = 1,
	EPBM_MAX                       = 2
};


// Enum Engine.EParticleSystemInsignificanceReaction
enum class EParticleSystemInsignificanceReaction : uint8_t
{
	EParticleSystemInsignificanceReaction__Auto = 0,
	EParticleSystemInsignificanceReaction__Complete = 1,
	EParticleSystemInsignificanceReaction__DisableTick = 2,
	EParticleSystemInsignificanceReaction__DisableTickAndKill = 3,
	EParticleSystemInsignificanceReaction__Num = 4,
	EParticleSystemInsignificanceReaction__EParticleSystemInsignificanceReaction_MAX = 5
};


// Enum Engine.EEmitterRenderMode
enum class EEmitterRenderMode : uint8_t
{
	ERM_Normal                     = 0,
	ERM_Point                      = 1,
	ERM_Cross                      = 2,
	ERM_LightsOnly                 = 3,
	ERM_None                       = 4,
	ERM_MAX                        = 5
};


// Enum Engine.ENavLinkDirection
enum class ENavLinkDirection : uint8_t
{
	ENavLinkDirection__BothWays    = 0,
	ENavLinkDirection__LeftToRight = 1,
	ENavLinkDirection__RightToLeft = 2,
	ENavLinkDirection__ENavLinkDirection_MAX = 3
};


// Enum Engine.ENavDataGatheringMode
enum class ENavDataGatheringMode : uint8_t
{
	ENavDataGatheringMode__Default = 0,
	ENavDataGatheringMode__Instant = 1,
	ENavDataGatheringMode__Lazy    = 2,
	ENavDataGatheringMode__ENavDataGatheringMode_MAX = 3
};


// Enum Engine.EParticleSignificanceLevel
enum class EParticleSignificanceLevel : uint8_t
{
	EParticleSignificanceLevel__Low = 0,
	EParticleSignificanceLevel__Medium = 1,
	EParticleSignificanceLevel__High = 2,
	EParticleSignificanceLevel__Critical = 3,
	EParticleSignificanceLevel__Num = 4,
	EParticleSignificanceLevel__EParticleSignificanceLevel_MAX = 5
};


// Enum Engine.EParticleSourceSelectionMethod
enum class EParticleSourceSelectionMethod : uint8_t
{
	EPSSM_Random                   = 0,
	EPSSM_Sequential               = 1,
	EPSSM_MAX                      = 2
};


// Enum Engine.EAttractorParticleSelectionMethod
enum class EAttractorParticleSelectionMethod : uint8_t
{
	EAPSM_Random                   = 0,
	EAPSM_Sequential               = 1,
	EAPSM_MAX                      = 2
};


// Enum Engine.BeamModifierType
enum class EBeamModifierType : uint8_t
{
	PEB2MT_Source                  = 0,
	PEB2MT_Target                  = 1,
	PEB2MT_MAX                     = 2
};


// Enum Engine.EModuleType
enum class EModuleType : uint8_t
{
	EPMT_General                   = 0,
	EPMT_TypeData                  = 1,
	EPMT_Beam                      = 2,
	EPMT_Trail                     = 3,
	EPMT_Spawn                     = 4,
	EPMT_Required                  = 5,
	EPMT_Event                     = 6,
	EPMT_Light                     = 7,
	EPMT_SubUV                     = 8,
	EPMT_MAX                       = 9
};


// Enum Engine.Beam2SourceTargetTangentMethod
enum class EBeam2SourceTargetTangentMethod : uint8_t
{
	PEB2STTM_Direct                = 0,
	PEB2STTM_UserSet               = 1,
	PEB2STTM_Distribution          = 2,
	PEB2STTM_Emitter               = 3,
	PEB2STTM_MAX                   = 4
};


// Enum Engine.Beam2SourceTargetMethod
enum class EBeam2SourceTargetMethod : uint8_t
{
	PEB2STM_Default                = 0,
	PEB2STM_UserSet                = 1,
	PEB2STM_Emitter                = 2,
	PEB2STM_Particle               = 3,
	PEB2STM_Actor                  = 4,
	PEB2STM_MAX                    = 5
};


// Enum Engine.EParticleCollisionComplete
enum class EParticleCollisionComplete : uint8_t
{
	EPCC_Kill                      = 0,
	EPCC_Freeze                    = 1,
	EPCC_HaltCollisions            = 2,
	EPCC_FreezeTranslation         = 3,
	EPCC_FreezeRotation            = 4,
	EPCC_FreezeMovement            = 5,
	EPCC_MAX                       = 6
};


// Enum Engine.EParticleCameraOffsetUpdateMethod
enum class EParticleCameraOffsetUpdateMethod : uint8_t
{
	EPCOUM_DirectSet               = 0,
	EPCOUM_Additive                = 1,
	EPCOUM_Scalar                  = 2,
	EPCOUM_MAX                     = 3
};


// Enum Engine.EParticleCollisionResponse
enum class EParticleCollisionResponse : uint8_t
{
	EParticleCollisionResponse__Bounce = 0,
	EParticleCollisionResponse__Stop = 1,
	EParticleCollisionResponse__Kill = 2,
	EParticleCollisionResponse__EParticleCollisionResponse_MAX = 3
};


// Enum Engine.ELocationBoneSocketSelectionMethod
enum class ELocationBoneSocketSelectionMethod : uint8_t
{
	BONESOCKETSEL_Sequential       = 0,
	BONESOCKETSEL_Random           = 1,
	BONESOCKETSEL_MAX              = 2
};


// Enum Engine.ELocationEmitterSelectionMethod
enum class ELocationEmitterSelectionMethod : uint8_t
{
	ELESM_Random                   = 0,
	ELESM_Sequential               = 1,
	ELESM_MAX                      = 2
};


// Enum Engine.ELocationBoneSocketSource
enum class ELocationBoneSocketSource : uint8_t
{
	BONESOCKETSOURCE_Bones         = 0,
	BONESOCKETSOURCE_Sockets       = 1,
	BONESOCKETSOURCE_MAX           = 2
};


// Enum Engine.ELocationSkelVertSurfaceSource
enum class ELocationSkelVertSurfaceSource : uint8_t
{
	VERTSURFACESOURCE_Vert         = 0,
	VERTSURFACESOURCE_Surface      = 1,
	VERTSURFACESOURCE_MAX          = 2
};


// Enum Engine.EOrbitChainMode
enum class EOrbitChainMode : uint8_t
{
	EOChainMode_Add                = 0,
	EOChainMode_Scale              = 1,
	EOChainMode_Link               = 2,
	EOChainMode_MAX                = 3
};


// Enum Engine.CylinderHeightAxis
enum class ECylinderHeightAxis : uint8_t
{
	PMLPC_HEIGHTAXIS_X             = 0,
	PMLPC_HEIGHTAXIS_Y             = 1,
	PMLPC_HEIGHTAXIS_Z             = 2,
	PMLPC_HEIGHTAXIS_MAX           = 3
};


// Enum Engine.EEmitterDynamicParameterValue
enum class EEmitterDynamicParameterValue : uint8_t
{
	EDPV_UserSet                   = 0,
	EDPV_AutoSet                   = 1,
	EDPV_VelocityX                 = 2,
	EDPV_VelocityY                 = 3,
	EDPV_VelocityZ                 = 4,
	EDPV_VelocityMag               = 5,
	EDPV_MAX                       = 6
};


// Enum Engine.EParticleAxisLock
enum class EParticleAxisLock : uint8_t
{
	EPAL_NONE                      = 0,
	EPAL_X                         = 1,
	EPAL_Y                         = 2,
	EPAL_Z                         = 3,
	EPAL_NEGATIVE_X                = 4,
	EPAL_NEGATIVE_Y                = 5,
	EPAL_NEGATIVE_Z                = 6,
	EPAL_ROTATE_X                  = 7,
	EPAL_ROTATE_Y                  = 8,
	EPAL_ROTATE_Z                  = 9,
	EPAL_MAX                       = 10
};


// Enum Engine.EParticleSortMode
enum class EParticleSortMode : uint8_t
{
	PSORTMODE_None                 = 0,
	PSORTMODE_ViewProjDepth        = 1,
	PSORTMODE_DistanceToView       = 2,
	PSORTMODE_Age_OldestFirst      = 3,
	PSORTMODE_Age_NewestFirst      = 4,
	PSORTMODE_MAX                  = 5
};


// Enum Engine.ETrail2SourceMethod
enum class ETrail2SourceMethod : uint8_t
{
	PET2SRCM_Default               = 0,
	PET2SRCM_Particle              = 1,
	PET2SRCM_Actor                 = 2,
	PET2SRCM_MAX                   = 3
};


// Enum Engine.EParticleUVFlipMode
enum class EParticleUVFlipMode : uint8_t
{
	EParticleUVFlipMode__None      = 0,
	EParticleUVFlipMode__FlipUV    = 1,
	EParticleUVFlipMode__FlipUOnly = 2,
	EParticleUVFlipMode__FlipVOnly = 3,
	EParticleUVFlipMode__RandomFlipUV = 4,
	EParticleUVFlipMode__RandomFlipUOnly = 5,
	EParticleUVFlipMode__RandomFlipVOnly = 6,
	EParticleUVFlipMode__RandomFlipUVIndependent = 7,
	EParticleUVFlipMode__EParticleUVFlipMode_MAX = 8
};


// Enum Engine.EBeamTaperMethod
enum class EBeamTaperMethod : uint8_t
{
	PEBTM_None                     = 0,
	PEBTM_Full                     = 1,
	PEBTM_Partial                  = 2,
	PEBTM_MAX                      = 3
};


// Enum Engine.EEmitterNormalsMode
enum class EEmitterNormalsMode : uint8_t
{
	ENM_CameraFacing               = 0,
	ENM_Spherical                  = 1,
	ENM_Cylindrical                = 2,
	ENM_MAX                        = 3
};


// Enum Engine.EMeshCameraFacingUpAxis
enum class EMeshCameraFacingUpAxis : uint8_t
{
	CameraFacing_NoneUP            = 0,
	CameraFacing_ZUp               = 1,
	CameraFacing_NegativeZUp       = 2,
	CameraFacing_YUp               = 3,
	CameraFacing_NegativeYUp       = 4,
	CameraFacing_MAX               = 5
};


// Enum Engine.EParticleDetailMode
enum class EParticleDetailMode : uint8_t
{
	PDM_Low                        = 0,
	PDM_Medium                     = 1,
	PDM_High                       = 2,
	PDM_MAX                        = 3
};


// Enum Engine.EMeshCameraFacingOptions
enum class EMeshCameraFacingOptions : uint8_t
{
	XAxisFacing_NoUp               = 0,
	XAxisFacing_ZUp                = 1,
	XAxisFacing_NegativeZUp        = 2,
	XAxisFacing_YUp                = 3,
	XAxisFacing_NegativeYUp        = 4,
	LockedAxis_ZAxisFacing         = 5,
	LockedAxis_NegativeZAxisFacing = 6,
	LockedAxis_YAxisFacing         = 7,
	LockedAxis_NegativeYAxisFacing = 8,
	VelocityAligned_ZAxisFacing    = 9,
	VelocityAligned_NegativeZAxisFacing = 10,
	VelocityAligned_YAxisFacing    = 11,
	VelocityAligned_NegativeYAxisFacing = 12,
	EMeshCameraFacingOptions_MAX   = 13
};


// Enum Engine.ETrailsRenderAxisOption
enum class ETrailsRenderAxisOption : uint8_t
{
	Trails_CameraUp                = 0,
	Trails_SourceUp                = 1,
	Trails_WorldUp                 = 2,
	Trails_MAX                     = 3
};


// Enum Engine.EBeam2Method
enum class EBeam2Method : uint8_t
{
	PEB2M_Distance                 = 0,
	PEB2M_Target                   = 1,
	PEB2M_Branch                   = 2,
	PEB2M_MAX                      = 3
};


// Enum Engine.EParticleScreenAlignment
enum class EParticleScreenAlignment : uint8_t
{
	PSA_FacingCameraPosition       = 0,
	PSA_Square                     = 1,
	PSA_Rectangle                  = 2,
	PSA_Velocity                   = 3,
	PSA_AwayFromCenter             = 4,
	PSA_TypeSpecific               = 5,
	PSA_FacingCameraDistanceBlend  = 6,
	PSA_MAX                        = 7
};


// Enum Engine.EParticleSystemOcclusionBoundsMethod
enum class EParticleSystemOcclusionBoundsMethod : uint8_t
{
	EPSOBM_None                    = 0,
	EPSOBM_ParticleBounds          = 1,
	EPSOBM_CustomBounds            = 2,
	EPSOBM_MAX                     = 3
};


// Enum Engine.EParticleSystemUpdateMode
enum class EParticleSystemUpdateMode : uint8_t
{
	EPSUM_RealTime                 = 0,
	EPSUM_FixedTime                = 1,
	EPSUM_MAX                      = 2
};


// Enum Engine.EMeshScreenAlignment
enum class EMeshScreenAlignment : uint8_t
{
	PSMA_MeshFaceCameraWithRoll    = 0,
	PSMA_MeshFaceCameraWithSpin    = 1,
	PSMA_MeshFaceCameraWithLockedAxis = 2,
	PSMA_MAX                       = 3
};


// Enum Engine.ESettingsLockedAxis
enum class ESettingsLockedAxis : uint8_t
{
	ESettingsLockedAxis__None      = 0,
	ESettingsLockedAxis__X         = 1,
	ESettingsLockedAxis__Y         = 2,
	ESettingsLockedAxis__Z         = 3,
	ESettingsLockedAxis__Invalid   = 4,
	ESettingsLockedAxis__ESettingsLockedAxis_MAX = 5
};


// Enum Engine.ParticleSystemLODMethod
enum class EParticleSystemLODMethod : uint8_t
{
	PARTICLESYSTEMLODMETHOD_Automatic = 0,
	PARTICLESYSTEMLODMETHOD_DirectSet = 1,
	PARTICLESYSTEMLODMETHOD_ActivateAutomatic = 2,
	PARTICLESYSTEMLODMETHOD_MAX    = 3
};


// Enum Engine.EParticleEventType
enum class EParticleEventType : uint8_t
{
	EPET_Any                       = 0,
	EPET_Spawn                     = 1,
	EPET_Death                     = 2,
	EPET_Collision                 = 3,
	EPET_Burst                     = 4,
	EPET_Blueprint                 = 5,
	EPET_MAX                       = 6
};


// Enum Engine.EParticleSysParamType
enum class EParticleSysParamType : uint8_t
{
	PSPT_None                      = 0,
	PSPT_Scalar                    = 1,
	PSPT_ScalarRand                = 2,
	PSPT_Vector                    = 3,
	PSPT_VectorRand                = 4,
	PSPT_Color                     = 5,
	PSPT_Actor                     = 6,
	PSPT_Material                  = 7,
	PSPT_MAX                       = 8
};


// Enum Engine.ParticleReplayState
enum class EParticleReplayState : uint8_t
{
	PRS_Disabled                   = 0,
	PRS_Capturing                  = 1,
	PRS_Replaying                  = 2,
	PRS_MAX                        = 3
};


// Enum Engine.ESettingsDOF
enum class ESettingsDOF : uint8_t
{
	ESettingsDOF__Full3D           = 0,
	ESettingsDOF__YZPlane          = 1,
	ESettingsDOF__XZPlane          = 2,
	ESettingsDOF__XYPlane          = 3,
	ESettingsDOF__ESettingsDOF_MAX = 4
};


// Enum Engine.EFrictionCombineMode
enum class EFrictionCombineMode : uint8_t
{
	EFrictionCombineMode__Average  = 0,
	EFrictionCombineMode__Min      = 1,
	EFrictionCombineMode__Multiply = 2,
	EFrictionCombineMode__Max      = 3
};


// Enum Engine.ERendererStencilMask
enum class ERendererStencilMask : uint8_t
{
	ERendererStencilMask__ERSM_Default = 0,
	ERendererStencilMask__ERSM     = 1,
	ERendererStencilMask__ERSM01   = 2,
	ERendererStencilMask__ERSM02   = 3,
	ERendererStencilMask__ERSM03   = 4,
	ERendererStencilMask__ERSM04   = 5,
	ERendererStencilMask__ERSM05   = 6,
	ERendererStencilMask__ERSM06   = 7,
	ERendererStencilMask__ERSM07   = 8,
	ERendererStencilMask__ERSM08   = 9,
	ERendererStencilMask__ERSM_MAX = 10
};


// Enum Engine.EHasCustomNavigableGeometry
enum class EHasCustomNavigableGeometry : uint8_t
{
	EHasCustomNavigableGeometry__No = 0,
	EHasCustomNavigableGeometry__Yes = 1,
	EHasCustomNavigableGeometry__EvenIfNotCollidable = 2,
	EHasCustomNavigableGeometry__DontExport = 3,
	EHasCustomNavigableGeometry__EHasCustomNavigableGeometry_MAX = 4
};


// Enum Engine.EViewTargetBlendFunction
enum class EViewTargetBlendFunction : uint8_t
{
	VTBlend_Linear                 = 0,
	VTBlend_Cubic                  = 1,
	VTBlend_EaseIn                 = 2,
	VTBlend_EaseOut                = 3,
	VTBlend_EaseInOut              = 4,
	VTBlend_MAX                    = 5
};


// Enum Engine.ERichCurveExtrapolation
enum class ERichCurveExtrapolation : uint8_t
{
	RCCE_Cycle                     = 0,
	RCCE_CycleWithOffset           = 1,
	RCCE_Oscillate                 = 2,
	RCCE_Linear                    = 3,
	RCCE_Constant                  = 4,
	RCCE_None                      = 5,
	RCCE_MAX                       = 6
};


// Enum Engine.ERichCurveInterpMode
enum class ERichCurveInterpMode : uint8_t
{
	RCIM_Linear                    = 0,
	RCIM_Constant                  = 1,
	RCIM_Cubic                     = 2,
	RCIM_None                      = 3,
	RCIM_MAX                       = 4
};


// Enum Engine.ECanBeCharacterBase
enum class ECanBeCharacterBase : uint8_t
{
	ECB_No                         = 0,
	ECB_Yes                        = 1,
	ECB_Owner                      = 2,
	ECB_MAX                        = 3
};


// Enum Engine.EDefaultBackBufferPixelFormat
enum class EDefaultBackBufferPixelFormat : uint8_t
{
	EDefaultBackBufferPixelFormat__DBBPF_B8G8R8A8 = 0,
	EDefaultBackBufferPixelFormat__DBBPF_A16B16G16R16_DEPRECATED = 1,
	EDefaultBackBufferPixelFormat__DBBPF_FloatRGB_DEPRECATED = 2,
	EDefaultBackBufferPixelFormat__DBBPF_FloatRGBA = 3,
	EDefaultBackBufferPixelFormat__DBBPF_A2B10G10R10 = 4,
	EDefaultBackBufferPixelFormat__DBBPF_MAX = 5
};


// Enum Engine.EReflectionSourceType
enum class EReflectionSourceType : uint8_t
{
	EReflectionSourceType__CapturedScene = 0,
	EReflectionSourceType__SpecifiedCubemap = 1,
	EReflectionSourceType__EReflectionSourceType_MAX = 2
};


// Enum Engine.EAlphaChannelMode
enum class EAlphaChannelMode : uint8_t
{
	EAlphaChannelMode__Disabled    = 0,
	EAlphaChannelMode__LinearColorSpaceOnly = 1,
	EAlphaChannelMode__AllowThroughTonemapper = 2,
	EAlphaChannelMode__EAlphaChannelMode_MAX = 3
};


// Enum Engine.ECustomDepthStencil
enum class ECustomDepthStencil : uint8_t
{
	ECustomDepthStencil__Disabled  = 0,
	ECustomDepthStencil__Enabled   = 1,
	ECustomDepthStencil__EnabledOnDemand = 2,
	ECustomDepthStencil__EnabledWithStencil = 3,
	ECustomDepthStencil__ECustomDepthStencil_MAX = 4
};


// Enum Engine.EEarlyZPass
enum class EEarlyZPass : uint8_t
{
	EEarlyZPass__None              = 0,
	EEarlyZPass__OpaqueOnly        = 1,
	EEarlyZPass__OpaqueAndMasked   = 2,
	EEarlyZPass__Auto              = 3,
	EEarlyZPass__EEarlyZPass_MAX   = 4
};


// Enum Engine.ECompositingSampleCount
enum class ECompositingSampleCount : uint8_t
{
	ECompositingSampleCount__One   = 0,
	ECompositingSampleCount__Two   = 1,
	ECompositingSampleCount__Four  = 2,
	ECompositingSampleCount__Eight = 3,
	ECompositingSampleCount__ECompositingSampleCount_MAX = 4
};


// Enum Engine.EMobileMSAASampleCount
enum class EMobileMSAASampleCount : uint8_t
{
	EMobileMSAASampleCount__One    = 0,
	EMobileMSAASampleCount__Two    = 1,
	EMobileMSAASampleCount__Four   = 2,
	EMobileMSAASampleCount__Eight  = 3,
	EMobileMSAASampleCount__EMobileMSAASampleCount_MAX = 4
};


// Enum Engine.ELegendPosition
enum class ELegendPosition : uint8_t
{
	ELegendPosition__Outside       = 0,
	ELegendPosition__Inside        = 1,
	ELegendPosition__ELegendPosition_MAX = 2
};


// Enum Engine.EClearSceneOptions
enum class EClearSceneOptions : uint8_t
{
	EClearSceneOptions__NoClear    = 0,
	EClearSceneOptions__HardwareClear = 1,
	EClearSceneOptions__QuadAtMaxZ = 2,
	EClearSceneOptions__EClearSceneOptions_MAX = 3
};


// Enum Engine.EGraphDataStyle
enum class EGraphDataStyle : uint8_t
{
	EGraphDataStyle__Lines         = 0,
	EGraphDataStyle__Filled        = 1,
	EGraphDataStyle__EGraphDataStyle_MAX = 2
};


// Enum Engine.EDynamicForceFeedbackAction
enum class EDynamicForceFeedbackAction : uint8_t
{
	EDynamicForceFeedbackAction__Start = 0,
	EDynamicForceFeedbackAction__Update = 1,
	EDynamicForceFeedbackAction__Stop = 2,
	EDynamicForceFeedbackAction__EDynamicForceFeedbackAction_MAX = 3
};


// Enum Engine.ERichCurveKeyTimeCompressionFormat
enum class ERichCurveKeyTimeCompressionFormat : uint8_t
{
	RCKTCF_uint16                  = 0,
	RCKTCF_float32                 = 1,
	RCKTCF_MAX                     = 2
};


// Enum Engine.EAutoExposureMethodUI
enum class EAutoExposureMethodUI : uint8_t
{
	EAutoExposureMethodUI__AEM_Histogram = 0,
	EAutoExposureMethodUI__AEM_Basic = 1,
	EAutoExposureMethodUI__AEM_Manual = 2,
	EAutoExposureMethodUI__AEM_MAX = 3
};


// Enum Engine.EGraphAxisStyle
enum class EGraphAxisStyle : uint8_t
{
	EGraphAxisStyle__Lines         = 0,
	EGraphAxisStyle__Notches       = 1,
	EGraphAxisStyle__Grid          = 2,
	EGraphAxisStyle__EGraphAxisStyle_MAX = 3
};


// Enum Engine.ERichCurveCompressionFormat
enum class ERichCurveCompressionFormat : uint8_t
{
	RCCF_Empty                     = 0,
	RCCF_Constant                  = 1,
	RCCF_Linear                    = 2,
	RCCF_Cubic                     = 3,
	RCCF_Mixed                     = 4,
	RCCF_MAX                       = 5
};


// Enum Engine.ERichCurveTangentWeightMode
enum class ERichCurveTangentWeightMode : uint8_t
{
	RCTWM_WeightedNone             = 0,
	RCTWM_WeightedArrive           = 1,
	RCTWM_WeightedLeave            = 2,
	RCTWM_WeightedBoth             = 3,
	RCTWM_MAX                      = 4
};


// Enum Engine.EControlConstraint
enum class EControlConstraint : uint8_t
{
	EControlConstraint__Orientation = 0,
	EControlConstraint__Translation = 1,
	EControlConstraint__MAX        = 2
};


// Enum Engine.ERichCurveTangentMode
enum class ERichCurveTangentMode : uint8_t
{
	RCTM_Auto                      = 0,
	RCTM_User                      = 1,
	RCTM_Break                     = 2,
	RCTM_None                      = 3,
	RCTM_MAX                       = 4
};


// Enum Engine.ERootMotionFinishVelocityMode
enum class ERootMotionFinishVelocityMode : uint8_t
{
	ERootMotionFinishVelocityMode__MaintainLastRootMotionVelocity = 0,
	ERootMotionFinishVelocityMode__SetVelocity = 1,
	ERootMotionFinishVelocityMode__ClampVelocity = 2,
	ERootMotionFinishVelocityMode__ERootMotionFinishVelocityMode_MAX = 3
};


// Enum Engine.ERootMotionSourceSettingsFlags
enum class ERootMotionSourceSettingsFlags : uint8_t
{
	ERootMotionSourceSettingsFlags__UseSensitiveLiftoffCheck = 0,
	ERootMotionSourceSettingsFlags__DisablePartialEndTick = 1,
	ERootMotionSourceSettingsFlags__ERootMotionSourceSettingsFlags_MAX = 2
};


// Enum Engine.EReflectedAndRefractedRayTracedShadows
enum class EReflectedAndRefractedRayTracedShadows : uint8_t
{
	EReflectedAndRefractedRayTracedShadows__Disabled = 0,
	EReflectedAndRefractedRayTracedShadows__Hard_shadows = 1,
	EReflectedAndRefractedRayTracedShadows__Area_shadows = 2,
	EReflectedAndRefractedRayTracedShadows__EReflectedAndRefractedRayTracedShadows_MAX = 3
};


// Enum Engine.ERootMotionAccumulateMode
enum class ERootMotionAccumulateMode : uint8_t
{
	ERootMotionAccumulateMode__Override = 0,
	ERootMotionAccumulateMode__Additive = 1,
	ERootMotionAccumulateMode__ERootMotionAccumulateMode_MAX = 2
};


// Enum Engine.ERootMotionSourceStatusFlags
enum class ERootMotionSourceStatusFlags : uint8_t
{
	ERootMotionSourceStatusFlags__Prepared = 0,
	ERootMotionSourceStatusFlags__Finished = 1,
	ERootMotionSourceStatusFlags__MarkedForRemoval = 2,
	ERootMotionSourceStatusFlags__ERootMotionSourceStatusFlags_MAX = 3
};


// Enum Engine.ELightUnits
enum class ELightUnits : uint8_t
{
	ELightUnits__Unitless          = 0,
	ELightUnits__Candelas          = 1,
	ELightUnits__Lumens            = 2,
	ELightUnits__ELightUnits_MAX   = 3
};


// Enum Engine.EConstraintTransform
enum class EConstraintTransform : uint8_t
{
	EConstraintTransform__Absolute = 0,
	EConstraintTransform__Relative = 1,
	EConstraintTransform__EConstraintTransform_MAX = 2
};


// Enum Engine.EAutoExposureMethod
enum class EAutoExposureMethod : uint8_t
{
	AEM_Histogram                  = 0,
	AEM_Basic                      = 1,
	AEM_Manual                     = 2,
	AEM_MAX                        = 3
};


// Enum Engine.EAntiAliasingMethod
enum class EAntiAliasingMethod : uint8_t
{
	AAM_None                       = 0,
	AAM_FXAA                       = 1,
	AAM_TemporalAA                 = 2,
	AAM_MSAA                       = 3,
	AAM_MAX                        = 4
};


// Enum Engine.ETranslucencyType
enum class ETranslucencyType : uint8_t
{
	ETranslucencyType__Raster      = 0,
	ETranslucencyType__RayTracing  = 1,
	ETranslucencyType__ETranslucencyType_MAX = 2
};


// Enum Engine.ESceneCapturePrimitiveRenderMode
enum class ESceneCapturePrimitiveRenderMode : uint8_t
{
	ESceneCapturePrimitiveRenderMode__PRM_LegacySceneCapture = 0,
	ESceneCapturePrimitiveRenderMode__PRM_RenderScenePrimitives = 1,
	ESceneCapturePrimitiveRenderMode__PRM_UseShowOnlyList = 2,
	ESceneCapturePrimitiveRenderMode__PRM_MAX = 3
};


// Enum Engine.EBloomMethod
enum class EBloomMethod : uint8_t
{
	BM_SOG                         = 0,
	BM_FFT                         = 1,
	BM_MAX                         = 2
};


// Enum Engine.EMaterialProperty
enum class EMaterialProperty : uint8_t
{
	MP_EmissiveColor               = 0,
	MP_Opacity                     = 1,
	MP_OpacityMask                 = 2,
	MP_DiffuseColor                = 3,
	MP_SpecularColor               = 4,
	MP_BaseColor                   = 5,
	MP_Metallic                    = 6,
	MP_Specular                    = 7,
	MP_Roughness                   = 8,
	MP_Normal                      = 9,
	MP_WorldPositionOffset         = 10,
	MP_WorldDisplacement           = 11,
	MP_TessellationMultiplier      = 12,
	MP_SubsurfaceColor             = 13,
	MP_CustomData0                 = 14,
	MP_CustomData1                 = 15,
	MP_AmbientOcclusion            = 16,
	MP_Refraction                  = 17,
	MP_CustomizedUVs0              = 18,
	MP_CustomizedUVs1              = 19,
	MP_CustomizedUVs2              = 20,
	MP_CustomizedUVs3              = 21,
	MP_CustomizedUVs4              = 22,
	MP_CustomizedUVs5              = 23,
	MP_CustomizedUVs6              = 24,
	MP_CustomizedUVs7              = 25,
	MP_PixelDepthOffset            = 26,
	MP_MaterialAttributes          = 27,
	MP_CustomOutput                = 28,
	MP_MAX                         = 29
};


// Enum Engine.EAnimationMode
enum class EAnimationMode : uint8_t
{
	EAnimationMode__AnimationBlueprint = 0,
	EAnimationMode__AnimationSingleNode = 1,
	EAnimationMode__AnimationCustomMode = 2,
	EAnimationMode__EAnimationMode_MAX = 3
};


// Enum Engine.EKinematicBonesUpdateToPhysics
enum class EKinematicBonesUpdateToPhysics : uint8_t
{
	EKinematicBonesUpdateToPhysics__SkipSimulatingBones = 0,
	EKinematicBonesUpdateToPhysics__SkipAllBones = 1,
	EKinematicBonesUpdateToPhysics__EKinematicBonesUpdateToPhysics_MAX = 2
};


// Enum Engine.EPhysicsTransformUpdateMode
enum class EPhysicsTransformUpdateMode : uint8_t
{
	EPhysicsTransformUpdateMode__SimulationUpatesComponentTransform = 0,
	EPhysicsTransformUpdateMode__ComponentTransformIsKinematic = 1,
	EPhysicsTransformUpdateMode__EPhysicsTransformUpdateMode_MAX = 2
};


// Enum Engine.EAnimCurveType
enum class EAnimCurveType : uint8_t
{
	EAnimCurveType__AttributeCurve = 0,
	EAnimCurveType__MaterialCurve  = 1,
	EAnimCurveType__MorphTargetCurve = 2,
	EAnimCurveType__MaxAnimCurveType = 3,
	EAnimCurveType__EAnimCurveType_MAX = 4
};


// Enum Engine.SkeletalMeshOptimizationImportance
enum class ESkeletalMeshOptimizationImportance : uint8_t
{
	SMOI_Off                       = 0,
	SMOI_Lowest                    = 1,
	SMOI_Low                       = 2,
	SMOI_Normal                    = 3,
	SMOI_High                      = 4,
	SMOI_Highest                   = 5,
	SMOI_MAX                       = 6
};


// Enum Engine.EDepthOfFieldMethod
enum class EDepthOfFieldMethod : uint8_t
{
	DOFM_BokehDOF                  = 0,
	DOFM_Gaussian                  = 1,
	DOFM_CircleDOF                 = 2,
	DOFM_MAX                       = 3
};


// Enum Engine.EBoneFilterActionOption
enum class EBoneFilterActionOption : uint8_t
{
	EBoneFilterActionOption__Remove = 0,
	EBoneFilterActionOption__Keep  = 1,
	EBoneFilterActionOption__Invalid = 2,
	EBoneFilterActionOption__EBoneFilterActionOption_MAX = 3
};


// Enum Engine.EReflectionsType
enum class EReflectionsType : uint8_t
{
	EReflectionsType__ScreenSpace  = 0,
	EReflectionsType__RayTracing   = 1,
	EReflectionsType__EReflectionsType_MAX = 2
};


// Enum Engine.SkeletalMeshTerminationCriterion
enum class ESkeletalMeshTerminationCriterion : uint8_t
{
	SMTC_NumOfTriangles            = 0,
	SMTC_NumOfVerts                = 1,
	SMTC_TriangleOrVert            = 2,
	SMTC_AbsNumOfTriangles         = 3,
	SMTC_AbsNumOfVerts             = 4,
	SMTC_AbsTriangleOrVert         = 5,
	SMTC_MAX                       = 6
};


// Enum Engine.EBoneTranslationRetargetingMode
enum class EBoneTranslationRetargetingMode : uint8_t
{
	EBoneTranslationRetargetingMode__Animation = 0,
	EBoneTranslationRetargetingMode__Skeleton = 1,
	EBoneTranslationRetargetingMode__AnimationScaled = 2,
	EBoneTranslationRetargetingMode__AnimationRelative = 3,
	EBoneTranslationRetargetingMode__OrientAndScale = 4,
	EBoneTranslationRetargetingMode__EBoneTranslationRetargetingMode_MAX = 5
};


// Enum Engine.EVisibilityBasedAnimTickOption
enum class EVisibilityBasedAnimTickOption : uint8_t
{
	EVisibilityBasedAnimTickOption__AlwaysTickPoseAndRefreshBones = 0,
	EVisibilityBasedAnimTickOption__AlwaysTickPose = 1,
	EVisibilityBasedAnimTickOption__OnlyTickMontagesWhenNotRendered = 2,
	EVisibilityBasedAnimTickOption__OnlyTickPoseWhenRendered = 3,
	EVisibilityBasedAnimTickOption__EVisibilityBasedAnimTickOption_MAX = 4
};


// Enum Engine.SkeletalMeshOptimizationType
enum class ESkeletalMeshOptimizationType : uint8_t
{
	SMOT_NumOfTriangles            = 0,
	SMOT_MaxDeviation              = 1,
	SMOT_TriangleOrDeviation       = 2,
	SMOT_MAX                       = 3
};


// Enum Engine.EPhysBodyOp
enum class EPhysBodyOp : uint8_t
{
	PBO_None                       = 0,
	PBO_Term                       = 1,
	PBO_MAX                        = 2
};


// Enum Engine.EBoneVisibilityStatus
enum class EBoneVisibilityStatus : uint8_t
{
	BVS_HiddenByParent             = 0,
	BVS_Visible                    = 1,
	BVS_ExplicitlyHidden           = 2,
	BVS_MAX                        = 3
};


// Enum Engine.ESkyLightSourceType
enum class ESkyLightSourceType : uint8_t
{
	SLS_CapturedScene              = 0,
	SLS_SpecifiedCubemap           = 1,
	SLS_MAX                        = 2
};


// Enum Engine.EAirAbsorptionMethod
enum class EAirAbsorptionMethod : uint8_t
{
	EAirAbsorptionMethod__Linear   = 0,
	EAirAbsorptionMethod__CustomCurve = 1,
	EAirAbsorptionMethod__EAirAbsorptionMethod_MAX = 2
};


// Enum Engine.EBoneSpaces
enum class EBoneSpaces : uint8_t
{
	EBoneSpaces__WorldSpace        = 0,
	EBoneSpaces__ComponentSpace    = 1,
	EBoneSpaces__EBoneSpaces_MAX   = 2
};


// Enum Engine.ESoundDistanceCalc
enum class ESoundDistanceCalc : uint8_t
{
	SOUNDDISTANCE_Normal           = 0,
	SOUNDDISTANCE_InfiniteXYPlane  = 1,
	SOUNDDISTANCE_InfiniteXZPlane  = 2,
	SOUNDDISTANCE_InfiniteYZPlane  = 3,
	SOUNDDISTANCE_MAX              = 4
};


// Enum Engine.EAudioOutputTarget
enum class EAudioOutputTarget : uint8_t
{
	EAudioOutputTarget__Speaker    = 0,
	EAudioOutputTarget__Controller = 1,
	EAudioOutputTarget__ControllerFallbackToSpeaker = 2,
	EAudioOutputTarget__EAudioOutputTarget_MAX = 3
};


// Enum Engine.EReverbSendMethod
enum class EReverbSendMethod : uint8_t
{
	EReverbSendMethod__Linear      = 0,
	EReverbSendMethod__CustomCurve = 1,
	EReverbSendMethod__Manual      = 2,
	EReverbSendMethod__EReverbSendMethod_MAX = 3
};


// Enum Engine.ESoundGroup
enum class ESoundGroup : uint8_t
{
	SOUNDGROUP_Default             = 0,
	SOUNDGROUP_Effects             = 1,
	SOUNDGROUP_UI                  = 2,
	SOUNDGROUP_Music               = 3,
	SOUNDGROUP_Voice               = 4,
	SOUNDGROUP_GameSoundGroup1     = 5,
	SOUNDGROUP_GameSoundGroup2     = 6,
	SOUNDGROUP_GameSoundGroup3     = 7,
	SOUNDGROUP_GameSoundGroup4     = 8,
	SOUNDGROUP_GameSoundGroup5     = 9,
	SOUNDGROUP_GameSoundGroup6     = 10,
	SOUNDGROUP_GameSoundGroup7     = 11,
	SOUNDGROUP_GameSoundGroup8     = 12,
	SOUNDGROUP_GameSoundGroup9     = 13,
	SOUNDGROUP_GameSoundGroup10    = 14,
	SOUNDGROUP_GameSoundGroup11    = 15,
	SOUNDGROUP_GameSoundGroup12    = 16,
	SOUNDGROUP_GameSoundGroup13    = 17,
	SOUNDGROUP_GameSoundGroup14    = 18,
	SOUNDGROUP_GameSoundGroup15    = 19,
	SOUNDGROUP_GameSoundGroup16    = 20,
	SOUNDGROUP_GameSoundGroup17    = 21,
	SOUNDGROUP_GameSoundGroup18    = 22,
	SOUNDGROUP_GameSoundGroup19    = 23,
	SOUNDGROUP_GameSoundGroup20    = 24,
	SOUNDGROUP_MAX                 = 25
};


// Enum Engine.ESourceBusChannels
enum class ESourceBusChannels : uint8_t
{
	ESourceBusChannels__Mono       = 0,
	ESourceBusChannels__Stereo     = 1,
	ESourceBusChannels__ESourceBusChannels_MAX = 2
};


// Enum Engine.EAudioRecordingExportType
enum class EAudioRecordingExportType : uint8_t
{
	EAudioRecordingExportType__SoundWave = 0,
	EAudioRecordingExportType__WavFile = 1,
	EAudioRecordingExportType__EAudioRecordingExportType_MAX = 2
};


// Enum Engine.ModulationParamMode
enum class EModulationParamMode : uint8_t
{
	MPM_Normal                     = 0,
	MPM_Abs                        = 1,
	MPM_Direct                     = 2,
	MPM_MAX                        = 3
};


// Enum Engine.ESubmixChannelFormat
enum class ESubmixChannelFormat : uint8_t
{
	ESubmixChannelFormat__Device   = 0,
	ESubmixChannelFormat__Stereo   = 1,
	ESubmixChannelFormat__Quad     = 2,
	ESubmixChannelFormat__FiveDotOne = 3,
	ESubmixChannelFormat__SevenDotOne = 4,
	ESubmixChannelFormat__Ambisonics = 5,
	ESubmixChannelFormat__Count    = 6,
	ESubmixChannelFormat__ESubmixChannelFormat_MAX = 7
};


// Enum Engine.ESoundWaveFFTSize
enum class ESoundWaveFFTSize : uint8_t
{
	ESoundWaveFFTSize__VerySmall   = 0,
	ESoundWaveFFTSize__Small       = 1,
	ESoundWaveFFTSize__Medium      = 2,
	ESoundWaveFFTSize__Large       = 3,
	ESoundWaveFFTSize__VeryLarge   = 4,
	ESoundWaveFFTSize__ESoundWaveFFTSize_MAX = 5
};


// Enum Engine.EMaxConcurrentResolutionRule
enum class EMaxConcurrentResolutionRule : uint8_t
{
	EMaxConcurrentResolutionRule__PreventNew = 0,
	EMaxConcurrentResolutionRule__StopOldest = 1,
	EMaxConcurrentResolutionRule__StopFarthestThenPreventNew = 2,
	EMaxConcurrentResolutionRule__StopFarthestThenOldest = 3,
	EMaxConcurrentResolutionRule__StopLowestPriority = 4,
	EMaxConcurrentResolutionRule__StopQuietest = 5,
	EMaxConcurrentResolutionRule__StopLowestPriorityThenPreventNew = 6,
	EMaxConcurrentResolutionRule__EMaxConcurrentResolutionRule_MAX = 7
};


// Enum Engine.ESplineCoordinateSpace
enum class ESplineCoordinateSpace : uint8_t
{
	ESplineCoordinateSpace__Local  = 0,
	ESplineCoordinateSpace__World  = 1,
	ESplineCoordinateSpace__ESplineCoordinateSpace_MAX = 2
};


// Enum Engine.EDecompressionType
enum class EDecompressionType : uint8_t
{
	DTYPE_Setup                    = 0,
	DTYPE_Invalid                  = 1,
	DTYPE_Preview                  = 2,
	DTYPE_Native                   = 3,
	DTYPE_RealTime                 = 4,
	DTYPE_Procedural               = 5,
	DTYPE_Xenon                    = 6,
	DTYPE_Streaming                = 7,
	DTYPE_MAX                      = 8
};


// Enum Engine.EImportanceLevel
enum class EImportanceLevel : uint8_t
{
	IL_Off                         = 0,
	IL_Lowest                      = 1,
	IL_Low                         = 2,
	IL_Normal                      = 3,
	IL_High                        = 4,
	IL_Highest                     = 5,
	TEMP_BROKEN2                   = 6,
	EImportanceLevel_MAX           = 7
};


// Enum Engine.ESplineMeshAxis
enum class ESplineMeshAxis : uint8_t
{
	ESplineMeshAxis__X             = 0,
	ESplineMeshAxis__Y             = 1,
	ESplineMeshAxis__Z             = 2,
	ESplineMeshAxis__ESplineMeshAxis_MAX = 3
};


// Enum Engine.ENormalMode
enum class ENormalMode : uint8_t
{
	NM_PreserveSmoothingGroups     = 0,
	NM_RecalculateNormals          = 1,
	NM_RecalculateNormalsSmooth    = 2,
	NM_RecalculateNormalsHard      = 3,
	TEMP_BROKEN                    = 4,
	ENormalMode_MAX                = 5
};


// Enum Engine.ESplinePointType
enum class ESplinePointType : uint8_t
{
	ESplinePointType__Linear       = 0,
	ESplinePointType__Curve        = 1,
	ESplinePointType__Constant     = 2,
	ESplinePointType__CurveClamped = 3,
	ESplinePointType__CurveCustomTangent = 4,
	ESplinePointType__ESplinePointType_MAX = 5
};


// Enum Engine.EStereoLayerShape
enum class EStereoLayerShape : uint8_t
{
	SLSH_QuadLayer                 = 0,
	SLSH_CylinderLayer             = 1,
	SLSH_CubemapLayer              = 2,
	SLSH_MAX                       = 3
};


// Enum Engine.EOptimizationType
enum class EOptimizationType : uint8_t
{
	OT_NumOfTriangles              = 0,
	OT_MaxDeviation                = 1,
	OT_MAX                         = 2
};


// Enum Engine.ESoundSpatializationAlgorithm
enum class ESoundSpatializationAlgorithm : uint8_t
{
	SPATIALIZATION_Default         = 0,
	SPATIALIZATION_HRTF            = 1,
	SPATIALIZATION_MAX             = 2
};


// Enum Engine.EStereoLayerType
enum class EStereoLayerType : uint8_t
{
	SLT_WorldLocked                = 0,
	SLT_TrackerLocked              = 1,
	SLT_FaceLocked                 = 2,
	SLT_MAX                        = 3
};


// Enum Engine.EOpacitySourceMode
enum class EOpacitySourceMode : uint8_t
{
	OSM_Alpha                      = 0,
	OSM_ColorBrightness            = 1,
	OSM_RedChannel                 = 2,
	OSM_GreenChannel               = 3,
	OSM_BlueChannel                = 4,
	OSM_MAX                        = 5
};


// Enum Engine.ETextureCompressionQuality
enum class ETextureCompressionQuality : uint8_t
{
	TCQ_Default                    = 0,
	TCQ_Lowest                     = 1,
	TCQ_Low                        = 2,
	TCQ_Medium                     = 3,
	TCQ_High                       = 4,
	TCQ_Highest                    = 5,
	TCQ_MAX                        = 6
};


// Enum Engine.ETextureSourceFormat
enum class ETextureSourceFormat : uint8_t
{
	TSF_Invalid                    = 0,
	TSF_G8                         = 1,
	TSF_BGRA8                      = 2,
	TSF_BGRE8                      = 3,
	TSF_RGBA16                     = 4,
	TSF_RGBA16F                    = 5,
	TSF_RGBA8                      = 6,
	TSF_RGBE8                      = 7,
	TSF_MAX                        = 8
};


// Enum Engine.ESubUVBoundingVertexCount
enum class ESubUVBoundingVertexCount : uint8_t
{
	BVC_FourVertices               = 0,
	BVC_EightVertices              = 1,
	BVC_MAX                        = 2
};


// Enum Engine.EHorizTextAligment
enum class EHorizTextAligment : uint8_t
{
	EHTA_Left                      = 0,
	EHTA_Center                    = 1,
	EHTA_Right                     = 2,
	EHTA_MAX                       = 3
};


// Enum Engine.EVerticalTextAligment
enum class EVerticalTextAligment : uint8_t
{
	EVRTA_TextTop                  = 0,
	EVRTA_TextCenter               = 1,
	EVRTA_TextBottom               = 2,
	EVRTA_QuadTop                  = 3,
	EVRTA_MAX                      = 4
};


// Enum Engine.ECompositeTextureMode
enum class ECompositeTextureMode : uint8_t
{
	CTM_Disabled                   = 0,
	CTM_NormalRoughnessToRed       = 1,
	CTM_NormalRoughnessToGreen     = 2,
	CTM_NormalRoughnessToBlue      = 3,
	CTM_NormalRoughnessToAlpha     = 4,
	CTM_MAX                        = 5
};


// Enum Engine.ETextureSourceArtType
enum class ETextureSourceArtType : uint8_t
{
	TSAT_Uncompressed              = 0,
	TSAT_PNGCompressed             = 1,
	TSAT_DDSFile                   = 2,
	TSAT_MAX                       = 3
};


// Enum Engine.TextureFilter
enum class ETextureFilter : uint8_t
{
	TF_Nearest                     = 0,
	TF_Bilinear                    = 1,
	TF_Trilinear                   = 2,
	TF_Default                     = 3,
	TF_MAX                         = 4
};


// Enum Engine.TextureCompressionSettings
enum class ETextureCompressionSettings : uint8_t
{
	TC_Default                     = 0,
	TC_Normalmap                   = 1,
	TC_Masks                       = 2,
	TC_Grayscale                   = 3,
	TC_Displacementmap             = 4,
	TC_VectorDisplacementmap       = 5,
	TC_HDR                         = 6,
	TC_EditorIcon                  = 7,
	TC_Alpha                       = 8,
	TC_DistanceFieldFont           = 9,
	TC_HDR_Compressed              = 10,
	TC_BC7                         = 11,
	TC_MAX                         = 12
};


// Enum Engine.ETexturePowerOfTwoSetting
enum class ETexturePowerOfTwoSetting : uint8_t
{
	ETexturePowerOfTwoSetting__None = 0,
	ETexturePowerOfTwoSetting__PadToPowerOfTwo = 1,
	ETexturePowerOfTwoSetting__PadToSquarePowerOfTwo = 2,
	ETexturePowerOfTwoSetting__ETexturePowerOfTwoSetting_MAX = 3
};


// Enum Engine.TextureAddress
enum class ETextureAddress : uint8_t
{
	TA_Wrap                        = 0,
	TA_Clamp                       = 1,
	TA_Mirror                      = 2,
	TA_MAX                         = 3
};


// Enum Engine.ETextureSamplerFilter
enum class ETextureSamplerFilter : uint8_t
{
	ETextureSamplerFilter__Point   = 0,
	ETextureSamplerFilter__Bilinear = 1,
	ETextureSamplerFilter__Trilinear = 2,
	ETextureSamplerFilter__AnisotropicPoint = 3,
	ETextureSamplerFilter__AnisotropicLinear = 4,
	ETextureSamplerFilter__ETextureSamplerFilter_MAX = 5
};


// Enum Engine.ETextureRenderTargetFormat
enum class ETextureRenderTargetFormat : uint8_t
{
	RTF_R8                         = 0,
	RTF_RG8                        = 1,
	RTF_RGBA8                      = 2,
	RTF_R16f                       = 3,
	RTF_RG16f                      = 4,
	RTF_RGBA16f                    = 5,
	RTF_R32f                       = 6,
	RTF_RG32f                      = 7,
	RTF_RGBA32f                    = 8,
	RTF_RGB10A2                    = 9,
	RTF_MAX                        = 10
};


// Enum Engine.TextureMipGenSettings
enum class ETextureMipGenSettings : uint8_t
{
	TMGS_FromTextureGroup          = 0,
	TMGS_SimpleAverage             = 1,
	TMGS_Sharpen0                  = 2,
	TMGS_Sharpen1                  = 3,
	TMGS_Sharpen2                  = 4,
	TMGS_Sharpen3                  = 5,
	TMGS_Sharpen4                  = 6,
	TMGS_Sharpen5                  = 7,
	TMGS_Sharpen6                  = 8,
	TMGS_Sharpen7                  = 9,
	TMGS_Sharpen8                  = 10,
	TMGS_Sharpen9                  = 11,
	TMGS_Sharpen10                 = 12,
	TMGS_NoMipmaps                 = 13,
	TMGS_LeaveExistingMips         = 14,
	TMGS_Blur1                     = 15,
	TMGS_Blur2                     = 16,
	TMGS_Blur3                     = 17,
	TMGS_Blur4                     = 18,
	TMGS_Blur5                     = 19,
	TMGS_MAX                       = 20
};


// Enum Engine.ETextureMipCount
enum class ETextureMipCount : uint8_t
{
	TMC_ResidentMips               = 0,
	TMC_AllMips                    = 1,
	TMC_AllMipsBiased              = 2,
	TMC_MAX                        = 3
};


// Enum Engine.ETimelineDirection
enum class ETimelineDirection : uint8_t
{
	ETimelineDirection__Forward    = 0,
	ETimelineDirection__Backward   = 1,
	ETimelineDirection__ETimelineDirection_MAX = 2
};


// Enum Engine.ETimelineLengthMode
enum class ETimelineLengthMode : uint8_t
{
	TL_TimelineLength              = 0,
	TL_LastKeyFrame                = 1,
	TL_MAX                         = 2
};


// Enum Engine.ETimecodeProviderSynchronizationState
enum class ETimecodeProviderSynchronizationState : uint8_t
{
	ETimecodeProviderSynchronizationState__Closed = 0,
	ETimecodeProviderSynchronizationState__Error = 1,
	ETimecodeProviderSynchronizationState__Synchronized = 2,
	ETimecodeProviderSynchronizationState__Synchronizing = 3,
	ETimecodeProviderSynchronizationState__ETimecodeProviderSynchronizationState_MAX = 4
};


// Enum Engine.ETwitterRequestMethod
enum class ETwitterRequestMethod : uint8_t
{
	TRM_Get                        = 0,
	TRM_Post                       = 1,
	TRM_Delete                     = 2,
	TRM_MAX                        = 3
};


// Enum Engine.ETimeStretchCurveMapping
enum class ETimeStretchCurveMapping : uint8_t
{
	ETimeStretchCurveMapping__T_Original = 0,
	ETimeStretchCurveMapping__T_TargetMin = 1,
	ETimeStretchCurveMapping__T_TargetMax = 2,
	ETimeStretchCurveMapping__MAX  = 3
};


// Enum Engine.EUserDefinedStructureStatus
enum class EUserDefinedStructureStatus : uint8_t
{
	UDSS_UpToDate                  = 0,
	UDSS_Dirty                     = 1,
	UDSS_Error                     = 2,
	UDSS_Duplicate                 = 3,
	UDSS_MAX                       = 4
};


// Enum Engine.ETwitterIntegrationDelegate
enum class ETwitterIntegrationDelegate : uint8_t
{
	TID_AuthorizeComplete          = 0,
	TID_TweetUIComplete            = 1,
	TID_RequestComplete            = 2,
	TID_MAX                        = 3
};


// Enum Engine.ERenderFocusRule
enum class ERenderFocusRule : uint8_t
{
	ERenderFocusRule__Always       = 0,
	ERenderFocusRule__NonPointer   = 1,
	ERenderFocusRule__NavigationOnly = 2,
	ERenderFocusRule__Never        = 3,
	ERenderFocusRule__ERenderFocusRule_MAX = 4
};


// Enum Engine.EVectorFieldConstructionOp
enum class EVectorFieldConstructionOp : uint8_t
{
	VFCO_Extrude                   = 0,
	VFCO_Revolve                   = 1,
	VFCO_MAX                       = 2
};


// Enum Engine.EWindSourceType
enum class EWindSourceType : uint8_t
{
	EWindSourceType__Directional   = 0,
	EWindSourceType__Point         = 1,
	EWindSourceType__EWindSourceType_MAX = 2
};


// Enum Engine.EPSCPoolMethod
enum class EPSCPoolMethod : uint8_t
{
	EPSCPoolMethod__None           = 0,
	EPSCPoolMethod__AutoRelease    = 1,
	EPSCPoolMethod__ManualRelease  = 2,
	EPSCPoolMethod__ManualRelease_OnComplete = 3,
	EPSCPoolMethod__FreeInPool     = 4,
	EPSCPoolMethod__EPSCPoolMethod_MAX = 5
};


// Enum Engine.TextureGroup
enum class ETextureGroup : uint8_t
{
	TEXTUREGROUP_World             = 0,
	TEXTUREGROUP_WorldNormalMap    = 1,
	TEXTUREGROUP_WorldSpecular     = 2,
	TEXTUREGROUP_Character         = 3,
	TEXTUREGROUP_CharacterNormalMap = 4,
	TEXTUREGROUP_CharacterSpecular = 5,
	TEXTUREGROUP_Weapon            = 6,
	TEXTUREGROUP_WeaponNormalMap   = 7,
	TEXTUREGROUP_WeaponSpecular    = 8,
	TEXTUREGROUP_Vehicle           = 9,
	TEXTUREGROUP_VehicleNormalMap  = 10,
	TEXTUREGROUP_VehicleSpecular   = 11,
	TEXTUREGROUP_Cinematic         = 12,
	TEXTUREGROUP_Effects           = 13,
	TEXTUREGROUP_EffectsNotFiltered = 14,
	TEXTUREGROUP_Skybox            = 15,
	TEXTUREGROUP_UI                = 16,
	TEXTUREGROUP_Lightmap          = 17,
	TEXTUREGROUP_RenderTarget      = 18,
	TEXTUREGROUP_MobileFlattened   = 19,
	TEXTUREGROUP_ProcBuilding_Face = 20,
	TEXTUREGROUP_ProcBuilding_LightMap = 21,
	TEXTUREGROUP_Shadowmap         = 22,
	TEXTUREGROUP_ColorLookupTable  = 23,
	TEXTUREGROUP_Terrain_Heightmap = 24,
	TEXTUREGROUP_Terrain_Weightmap = 25,
	TEXTUREGROUP_Bokeh             = 26,
	TEXTUREGROUP_IESLightProfile   = 27,
	TEXTUREGROUP_Pixels2D          = 28,
	TEXTUREGROUP_HierarchicalLOD   = 29,
	TEXTUREGROUP_Impostor          = 30,
	TEXTUREGROUP_ImpostorNormalDepth = 31,
	TEXTUREGROUP_Project01         = 32,
	TEXTUREGROUP_Project02         = 33,
	TEXTUREGROUP_Project03         = 34,
	TEXTUREGROUP_Project04         = 35,
	TEXTUREGROUP_Project05         = 36,
	TEXTUREGROUP_Project06         = 37,
	TEXTUREGROUP_Project07         = 38,
	TEXTUREGROUP_Project08         = 39,
	TEXTUREGROUP_Project09         = 40,
	TEXTUREGROUP_Project10         = 41,
	TEXTUREGROUP_MAX               = 42
};


// Enum Engine.PageTableFormat
enum class EPageTableFormat : uint8_t
{
	PTF                            = 0,
	PTF01                          = 1,
	PTF_MAX                        = 2
};


// Enum Engine.EVisibilityAggressiveness
enum class EVisibilityAggressiveness : uint8_t
{
	VIS_LeastAggressive            = 0,
	VIS_ModeratelyAggressive       = 1,
	VIS_MostAggressive             = 2,
	VIS_Max                        = 3
};


// Enum Engine.EVolumeLightingMethod
enum class EVolumeLightingMethod : uint8_t
{
	VLM_VolumetricLightmap         = 0,
	VLM_SparseVolumeLightingSamples = 1,
	VLM_MAX                        = 2
};


// Enum Engine.EReporterLineStyle
enum class EReporterLineStyle : uint8_t
{
	EReporterLineStyle__Line       = 0,
	EReporterLineStyle__Dash       = 1,
	EReporterLineStyle__EReporterLineStyle_MAX = 2
};


// Enum Engine.EUIScalingRule
enum class EUIScalingRule : uint8_t
{
	EUIScalingRule__ShortestSide   = 0,
	EUIScalingRule__LongestSide    = 1,
	EUIScalingRule__Horizontal     = 2,
	EUIScalingRule__Vertical       = 3,
	EUIScalingRule__Custom         = 4,
	EUIScalingRule__EUIScalingRule_MAX = 5
};



//---------------------------------------------------------------------------
// Script Structs
//---------------------------------------------------------------------------

// ScriptStruct Engine.DistributionLookupTable
// 0x0000
struct FDistributionLookupTable
{


	static FDistributionLookupTable ReadAsMe(uintptr_t address)
	{
		FDistributionLookupTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDistributionLookupTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RawDistribution
// 0x0000
struct FRawDistribution
{


	static FRawDistribution ReadAsMe(uintptr_t address)
	{
		FRawDistribution ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRawDistribution& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FloatDistribution
// 0x0000
struct FFloatDistribution
{


	static FFloatDistribution ReadAsMe(uintptr_t address)
	{
		FFloatDistribution ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFloatDistribution& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Vector4Distribution
// 0x0000
struct FVector4Distribution
{


	static FVector4Distribution ReadAsMe(uintptr_t address)
	{
		FVector4Distribution ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector4Distribution& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FloatRK4SpringInterpolator
// 0x0000
struct FFloatRK4SpringInterpolator
{


	static FFloatRK4SpringInterpolator ReadAsMe(uintptr_t address)
	{
		FFloatRK4SpringInterpolator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFloatRK4SpringInterpolator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VectorDistribution
// 0x0000
struct FVectorDistribution
{


	static FVectorDistribution ReadAsMe(uintptr_t address)
	{
		FVectorDistribution ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVectorDistribution& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VectorRK4SpringInterpolator
// 0x0000
struct FVectorRK4SpringInterpolator
{


	static FVectorRK4SpringInterpolator ReadAsMe(uintptr_t address)
	{
		FVectorRK4SpringInterpolator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVectorRK4SpringInterpolator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FormatArgumentData
// 0x0000
struct FFormatArgumentData
{


	static FFormatArgumentData ReadAsMe(uintptr_t address)
	{
		FFormatArgumentData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFormatArgumentData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialAttributesInput
// 0x5A1246C0
struct FMaterialAttributesInput
{
	unsigned char                                      UnknownData00[0x5A1246C0];                                // 0x0000(0x5A1246C0) MISSED OFFSET

	static FMaterialAttributesInput ReadAsMe(uintptr_t address)
	{
		FMaterialAttributesInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialAttributesInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ExpressionInput
// 0x0000
struct FExpressionInput
{


	static FExpressionInput ReadAsMe(uintptr_t address)
	{
		FExpressionInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FExpressionInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialInput
// 0x0000
struct FMaterialInput
{


	static FMaterialInput ReadAsMe(uintptr_t address)
	{
		FMaterialInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ScalarMaterialInput
// 0x5A124480
struct FScalarMaterialInput
{
	unsigned char                                      UnknownData00[0x5A124480];                                // 0x0000(0x5A124480) MISSED OFFSET

	static FScalarMaterialInput ReadAsMe(uintptr_t address)
	{
		FScalarMaterialInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FScalarMaterialInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Vector2MaterialInput
// 0x5A124480
struct FVector2MaterialInput
{
	unsigned char                                      UnknownData00[0x5A124480];                                // 0x0000(0x5A124480) MISSED OFFSET

	static FVector2MaterialInput ReadAsMe(uintptr_t address)
	{
		FVector2MaterialInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector2MaterialInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ColorMaterialInput
// 0x5A124480
struct FColorMaterialInput
{
	unsigned char                                      UnknownData00[0x5A124480];                                // 0x0000(0x5A124480) MISSED OFFSET

	static FColorMaterialInput ReadAsMe(uintptr_t address)
	{
		FColorMaterialInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FColorMaterialInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ExpressionOutput
// 0x0000
struct FExpressionOutput
{


	static FExpressionOutput ReadAsMe(uintptr_t address)
	{
		FExpressionOutput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FExpressionOutput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VectorMaterialInput
// 0x5A124480
struct FVectorMaterialInput
{
	unsigned char                                      UnknownData00[0x5A124480];                                // 0x0000(0x5A124480) MISSED OFFSET

	static FVectorMaterialInput ReadAsMe(uintptr_t address)
	{
		FVectorMaterialInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVectorMaterialInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.HitResult
// 0x0000
struct FHitResult
{


	static FHitResult ReadAsMe(uintptr_t address)
	{
		FHitResult ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FHitResult& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Vector_NetQuantizeNormal
// 0x59ECFDC0
struct FVector_NetQuantizeNormal
{
	unsigned char                                      UnknownData00[0x59ECFDC0];                                // 0x0000(0x59ECFDC0) MISSED OFFSET

	static FVector_NetQuantizeNormal ReadAsMe(uintptr_t address)
	{
		FVector_NetQuantizeNormal ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector_NetQuantizeNormal& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Vector_NetQuantize
// 0x59ECFDC0
struct FVector_NetQuantize
{
	unsigned char                                      UnknownData00[0x59ECFDC0];                                // 0x0000(0x59ECFDC0) MISSED OFFSET

	static FVector_NetQuantize ReadAsMe(uintptr_t address)
	{
		FVector_NetQuantize ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector_NetQuantize& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BranchingPointNotifyPayload
// 0x0000
struct FBranchingPointNotifyPayload
{


	static FBranchingPointNotifyPayload ReadAsMe(uintptr_t address)
	{
		FBranchingPointNotifyPayload ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBranchingPointNotifyPayload& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SimpleMemberReference
// 0x0000
struct FSimpleMemberReference
{


	static FSimpleMemberReference ReadAsMe(uintptr_t address)
	{
		FSimpleMemberReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSimpleMemberReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TickFunction
// 0x0000
struct FTickFunction
{


	static FTickFunction ReadAsMe(uintptr_t address)
	{
		FTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ActorComponentTickFunction
// 0x5A123C40
struct FActorComponentTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FActorComponentTickFunction ReadAsMe(uintptr_t address)
	{
		FActorComponentTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FActorComponentTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SubtitleCue
// 0x0000
struct FSubtitleCue
{


	static FSubtitleCue ReadAsMe(uintptr_t address)
	{
		FSubtitleCue ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSubtitleCue& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InterpControlPoint
// 0x0000
struct FInterpControlPoint
{


	static FInterpControlPoint ReadAsMe(uintptr_t address)
	{
		FInterpControlPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpControlPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PlatformInterfaceDelegateResult
// 0x0000
struct FPlatformInterfaceDelegateResult
{


	static FPlatformInterfaceDelegateResult ReadAsMe(uintptr_t address)
	{
		FPlatformInterfaceDelegateResult ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPlatformInterfaceDelegateResult& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PlatformInterfaceData
// 0x0000
struct FPlatformInterfaceData
{


	static FPlatformInterfaceData ReadAsMe(uintptr_t address)
	{
		FPlatformInterfaceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPlatformInterfaceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DebugFloatHistory
// 0x0000
struct FDebugFloatHistory
{


	static FDebugFloatHistory ReadAsMe(uintptr_t address)
	{
		FDebugFloatHistory ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDebugFloatHistory& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LatentActionInfo
// 0x0000
struct FLatentActionInfo
{


	static FLatentActionInfo ReadAsMe(uintptr_t address)
	{
		FLatentActionInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLatentActionInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TimerHandle
// 0x0000
struct FTimerHandle
{


	static FTimerHandle ReadAsMe(uintptr_t address)
	{
		FTimerHandle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimerHandle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollisionProfileName
// 0x0000
struct FCollisionProfileName
{


	static FCollisionProfileName ReadAsMe(uintptr_t address)
	{
		FCollisionProfileName ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollisionProfileName& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GenericStruct
// 0x0000
struct FGenericStruct
{


	static FGenericStruct ReadAsMe(uintptr_t address)
	{
		FGenericStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGenericStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.UserActivity
// 0x0000
struct FUserActivity
{


	static FUserActivity ReadAsMe(uintptr_t address)
	{
		FUserActivity ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FUserActivity& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.UniqueNetIdRepl
// 0x59ECFF40
struct FUniqueNetIdRepl
{
	unsigned char                                      UnknownData00[0x59ECFF40];                                // 0x0000(0x59ECFF40) MISSED OFFSET

	static FUniqueNetIdRepl ReadAsMe(uintptr_t address)
	{
		FUniqueNetIdRepl ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FUniqueNetIdRepl& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FastArraySerializer
// 0x0000
struct FFastArraySerializer
{


	static FFastArraySerializer ReadAsMe(uintptr_t address)
	{
		FFastArraySerializer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFastArraySerializer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FastArraySerializerItem
// 0x0000
struct FFastArraySerializerItem
{


	static FFastArraySerializerItem ReadAsMe(uintptr_t address)
	{
		FFastArraySerializerItem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFastArraySerializerItem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KeyHandleLookupTable
// 0x0000
struct FKeyHandleLookupTable
{


	static FKeyHandleLookupTable ReadAsMe(uintptr_t address)
	{
		FKeyHandleLookupTable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKeyHandleLookupTable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PerPlatformFloat
// 0x0000
struct FPerPlatformFloat
{


	static FPerPlatformFloat ReadAsMe(uintptr_t address)
	{
		FPerPlatformFloat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPerPlatformFloat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PerPlatformInt
// 0x0000
struct FPerPlatformInt
{


	static FPerPlatformInt ReadAsMe(uintptr_t address)
	{
		FPerPlatformInt ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPerPlatformInt& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PerPlatformBool
// 0x0000
struct FPerPlatformBool
{


	static FPerPlatformBool ReadAsMe(uintptr_t address)
	{
		FPerPlatformBool ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPerPlatformBool& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_Base
// 0x0000
struct FAnimNode_Base
{


	static FAnimNode_Base ReadAsMe(uintptr_t address)
	{
		FAnimNode_Base ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_Base& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputRange
// 0x0000
struct FInputRange
{


	static FInputRange ReadAsMe(uintptr_t address)
	{
		FInputRange ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputRange& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputScaleBiasClamp
// 0x0000
struct FInputScaleBiasClamp
{


	static FInputScaleBiasClamp ReadAsMe(uintptr_t address)
	{
		FInputScaleBiasClamp ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputScaleBiasClamp& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputAlphaBoolBlend
// 0x0000
struct FInputAlphaBoolBlend
{


	static FInputAlphaBoolBlend ReadAsMe(uintptr_t address)
	{
		FInputAlphaBoolBlend ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputAlphaBoolBlend& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AlphaBlend
// 0x0000
struct FAlphaBlend
{


	static FAlphaBlend ReadAsMe(uintptr_t address)
	{
		FAlphaBlend ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAlphaBlend& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputScaleBias
// 0x0000
struct FInputScaleBias
{


	static FInputScaleBias ReadAsMe(uintptr_t address)
	{
		FInputScaleBias ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputScaleBias& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PoseLinkBase
// 0x0000
struct FPoseLinkBase
{


	static FPoseLinkBase ReadAsMe(uintptr_t address)
	{
		FPoseLinkBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPoseLinkBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ComponentSpacePoseLink
// 0x5A12DF00
struct FComponentSpacePoseLink
{
	unsigned char                                      UnknownData00[0x5A12DF00];                                // 0x0000(0x5A12DF00) MISSED OFFSET

	static FComponentSpacePoseLink ReadAsMe(uintptr_t address)
	{
		FComponentSpacePoseLink ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FComponentSpacePoseLink& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimInstanceProxy
// 0x0000
struct FAnimInstanceProxy
{


	static FAnimInstanceProxy ReadAsMe(uintptr_t address)
	{
		FAnimInstanceProxy ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimInstanceProxy& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RuntimeFloatCurve
// 0x0000
struct FRuntimeFloatCurve
{


	static FRuntimeFloatCurve ReadAsMe(uintptr_t address)
	{
		FRuntimeFloatCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRuntimeFloatCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KeyHandleMap
// 0x0000
struct FKeyHandleMap
{


	static FKeyHandleMap ReadAsMe(uintptr_t address)
	{
		FKeyHandleMap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKeyHandleMap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.IndexedCurve
// 0x0000
struct FIndexedCurve
{


	static FIndexedCurve ReadAsMe(uintptr_t address)
	{
		FIndexedCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FIndexedCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RealCurve
// 0x5A311780
struct FRealCurve
{
	unsigned char                                      UnknownData00[0x5A311780];                                // 0x0000(0x5A311780) MISSED OFFSET

	static FRealCurve ReadAsMe(uintptr_t address)
	{
		FRealCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRealCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RichCurve
// 0x5A311600
struct FRichCurve
{
	unsigned char                                      UnknownData00[0x5A311600];                                // 0x0000(0x5A311600) MISSED OFFSET

	static FRichCurve ReadAsMe(uintptr_t address)
	{
		FRichCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRichCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RichCurveKey
// 0x0000
struct FRichCurveKey
{


	static FRichCurveKey ReadAsMe(uintptr_t address)
	{
		FRichCurveKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRichCurveKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Vector_NetQuantize10
// 0x59ECFDC0
struct FVector_NetQuantize10
{
	unsigned char                                      UnknownData00[0x59ECFDC0];                                // 0x0000(0x59ECFDC0) MISSED OFFSET

	static FVector_NetQuantize10 ReadAsMe(uintptr_t address)
	{
		FVector_NetQuantize10 ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector_NetQuantize10& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputAxisKeyMapping
// 0x0000
struct FInputAxisKeyMapping
{


	static FInputAxisKeyMapping ReadAsMe(uintptr_t address)
	{
		FInputAxisKeyMapping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputAxisKeyMapping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputActionKeyMapping
// 0x0000
struct FInputActionKeyMapping
{


	static FInputActionKeyMapping ReadAsMe(uintptr_t address)
	{
		FInputActionKeyMapping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputActionKeyMapping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TableRowBase
// 0x0000
struct FTableRowBase
{


	static FTableRowBase ReadAsMe(uintptr_t address)
	{
		FTableRowBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTableRowBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LightingChannels
// 0x0000
struct FLightingChannels
{


	static FLightingChannels ReadAsMe(uintptr_t address)
	{
		FLightingChannels ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLightingChannels& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_AssetPlayerBase
// 0x5A12E440
struct FAnimNode_AssetPlayerBase
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_AssetPlayerBase ReadAsMe(uintptr_t address)
	{
		FAnimNode_AssetPlayerBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_AssetPlayerBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PoseLink
// 0x5A12DF00
struct FPoseLink
{
	unsigned char                                      UnknownData00[0x5A12DF00];                                // 0x0000(0x5A12DF00) MISSED OFFSET

	static FPoseLink ReadAsMe(uintptr_t address)
	{
		FPoseLink ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPoseLink& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BoneReference
// 0x0000
struct FBoneReference
{


	static FBoneReference ReadAsMe(uintptr_t address)
	{
		FBoneReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBoneReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PerBoneBlendWeight
// 0x0000
struct FPerBoneBlendWeight
{


	static FPerBoneBlendWeight ReadAsMe(uintptr_t address)
	{
		FPerBoneBlendWeight ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPerBoneBlendWeight& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BranchFilter
// 0x0000
struct FBranchFilter
{


	static FBranchFilter ReadAsMe(uintptr_t address)
	{
		FBranchFilter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBranchFilter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputBlendPose
// 0x0000
struct FInputBlendPose
{


	static FInputBlendPose ReadAsMe(uintptr_t address)
	{
		FInputBlendPose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputBlendPose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PoseSnapshot
// 0x0000
struct FPoseSnapshot
{


	static FPoseSnapshot ReadAsMe(uintptr_t address)
	{
		FPoseSnapshot ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPoseSnapshot& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimCurveParam
// 0x0000
struct FAnimCurveParam
{


	static FAnimCurveParam ReadAsMe(uintptr_t address)
	{
		FAnimCurveParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimCurveParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ActorComponentInstanceData
// 0x0000
struct FActorComponentInstanceData
{


	static FActorComponentInstanceData ReadAsMe(uintptr_t address)
	{
		FActorComponentInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FActorComponentInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ActorComponentDuplicatedObjectData
// 0x0000
struct FActorComponentDuplicatedObjectData
{


	static FActorComponentDuplicatedObjectData ReadAsMe(uintptr_t address)
	{
		FActorComponentDuplicatedObjectData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FActorComponentDuplicatedObjectData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SceneComponentInstanceData
// 0x5A367600
struct FSceneComponentInstanceData
{
	unsigned char                                      UnknownData00[0x5A367600];                                // 0x0000(0x5A367600) MISSED OFFSET

	static FSceneComponentInstanceData ReadAsMe(uintptr_t address)
	{
		FSceneComponentInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSceneComponentInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RepAttachment
// 0x0000
struct FRepAttachment
{


	static FRepAttachment ReadAsMe(uintptr_t address)
	{
		FRepAttachment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRepAttachment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Vector_NetQuantize100
// 0x59ECFDC0
struct FVector_NetQuantize100
{
	unsigned char                                      UnknownData00[0x59ECFDC0];                                // 0x0000(0x59ECFDC0) MISSED OFFSET

	static FVector_NetQuantize100 ReadAsMe(uintptr_t address)
	{
		FVector_NetQuantize100 ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVector_NetQuantize100& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RepMovement
// 0x0000
struct FRepMovement
{


	static FRepMovement ReadAsMe(uintptr_t address)
	{
		FRepMovement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRepMovement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ActorTickFunction
// 0x5A123C40
struct FActorTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FActorTickFunction ReadAsMe(uintptr_t address)
	{
		FActorTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FActorTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KAggregateGeom
// 0x0000
struct FKAggregateGeom
{


	static FKAggregateGeom ReadAsMe(uintptr_t address)
	{
		FKAggregateGeom ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKAggregateGeom& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DirectoryPath
// 0x0000
struct FDirectoryPath
{


	static FDirectoryPath ReadAsMe(uintptr_t address)
	{
		FDirectoryPath ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDirectoryPath& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KShapeElem
// 0x0000
struct FKShapeElem
{


	static FKShapeElem ReadAsMe(uintptr_t address)
	{
		FKShapeElem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKShapeElem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KTaperedCapsuleElem
// 0x5A36A300
struct FKTaperedCapsuleElem
{
	unsigned char                                      UnknownData00[0x5A36A300];                                // 0x0000(0x5A36A300) MISSED OFFSET

	static FKTaperedCapsuleElem ReadAsMe(uintptr_t address)
	{
		FKTaperedCapsuleElem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKTaperedCapsuleElem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KConvexElem
// 0x5A36A300
struct FKConvexElem
{
	unsigned char                                      UnknownData00[0x5A36A300];                                // 0x0000(0x5A36A300) MISSED OFFSET

	static FKConvexElem ReadAsMe(uintptr_t address)
	{
		FKConvexElem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKConvexElem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KSphylElem
// 0x5A36A300
struct FKSphylElem
{
	unsigned char                                      UnknownData00[0x5A36A300];                                // 0x0000(0x5A36A300) MISSED OFFSET

	static FKSphylElem ReadAsMe(uintptr_t address)
	{
		FKSphylElem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKSphylElem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KBoxElem
// 0x5A36A300
struct FKBoxElem
{
	unsigned char                                      UnknownData00[0x5A36A300];                                // 0x0000(0x5A36A300) MISSED OFFSET

	static FKBoxElem ReadAsMe(uintptr_t address)
	{
		FKBoxElem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKBoxElem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KSphereElem
// 0x5A36A300
struct FKSphereElem
{
	unsigned char                                      UnknownData00[0x5A36A300];                                // 0x0000(0x5A36A300) MISSED OFFSET

	static FKSphereElem ReadAsMe(uintptr_t address)
	{
		FKSphereElem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKSphereElem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationGroupReference
// 0x0000
struct FAnimationGroupReference
{


	static FAnimationGroupReference ReadAsMe(uintptr_t address)
	{
		FAnimationGroupReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationGroupReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimGroupInstance
// 0x0000
struct FAnimGroupInstance
{


	static FAnimGroupInstance ReadAsMe(uintptr_t address)
	{
		FAnimGroupInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimGroupInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimTickRecord
// 0x0000
struct FAnimTickRecord
{


	static FAnimTickRecord ReadAsMe(uintptr_t address)
	{
		FAnimTickRecord ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimTickRecord& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MarkerSyncAnimPosition
// 0x0000
struct FMarkerSyncAnimPosition
{


	static FMarkerSyncAnimPosition ReadAsMe(uintptr_t address)
	{
		FMarkerSyncAnimPosition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMarkerSyncAnimPosition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionMovementParams
// 0x0000
struct FRootMotionMovementParams
{


	static FRootMotionMovementParams ReadAsMe(uintptr_t address)
	{
		FRootMotionMovementParams ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionMovementParams& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlendFilter
// 0x0000
struct FBlendFilter
{


	static FBlendFilter ReadAsMe(uintptr_t address)
	{
		FBlendFilter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlendFilter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlendSampleData
// 0x0000
struct FBlendSampleData
{


	static FBlendSampleData ReadAsMe(uintptr_t address)
	{
		FBlendSampleData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlendSampleData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationRecordingSettings
// 0x0000
struct FAnimationRecordingSettings
{


	static FAnimationRecordingSettings ReadAsMe(uintptr_t address)
	{
		FAnimationRecordingSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationRecordingSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ComponentSpacePose
// 0x0000
struct FComponentSpacePose
{


	static FComponentSpacePose ReadAsMe(uintptr_t address)
	{
		FComponentSpacePose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FComponentSpacePose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LocalSpacePose
// 0x0000
struct FLocalSpacePose
{


	static FLocalSpacePose ReadAsMe(uintptr_t address)
	{
		FLocalSpacePose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLocalSpacePose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NamedTransform
// 0x0000
struct FNamedTransform
{


	static FNamedTransform ReadAsMe(uintptr_t address)
	{
		FNamedTransform ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNamedTransform& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NamedVector
// 0x0000
struct FNamedVector
{


	static FNamedVector ReadAsMe(uintptr_t address)
	{
		FNamedVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNamedVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NamedColor
// 0x0000
struct FNamedColor
{


	static FNamedColor ReadAsMe(uintptr_t address)
	{
		FNamedColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNamedColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NamedFloat
// 0x0000
struct FNamedFloat
{


	static FNamedFloat ReadAsMe(uintptr_t address)
	{
		FNamedFloat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNamedFloat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimParentNodeAssetOverride
// 0x0000
struct FAnimParentNodeAssetOverride
{


	static FAnimParentNodeAssetOverride ReadAsMe(uintptr_t address)
	{
		FAnimParentNodeAssetOverride ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimParentNodeAssetOverride& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimGroupInfo
// 0x0000
struct FAnimGroupInfo
{


	static FAnimGroupInfo ReadAsMe(uintptr_t address)
	{
		FAnimGroupInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimGroupInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimBlueprintDebugData
// 0x0000
struct FAnimBlueprintDebugData
{


	static FAnimBlueprintDebugData ReadAsMe(uintptr_t address)
	{
		FAnimBlueprintDebugData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimBlueprintDebugData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationFrameSnapshot
// 0x0000
struct FAnimationFrameSnapshot
{


	static FAnimationFrameSnapshot ReadAsMe(uintptr_t address)
	{
		FAnimationFrameSnapshot ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationFrameSnapshot& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StateMachineDebugData
// 0x0000
struct FStateMachineDebugData
{


	static FStateMachineDebugData ReadAsMe(uintptr_t address)
	{
		FStateMachineDebugData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStateMachineDebugData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimTrack
// 0x0000
struct FAnimTrack
{


	static FAnimTrack ReadAsMe(uintptr_t address)
	{
		FAnimTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimSegment
// 0x0000
struct FAnimSegment
{


	static FAnimSegment ReadAsMe(uintptr_t address)
	{
		FAnimSegment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimSegment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionExtractionStep
// 0x0000
struct FRootMotionExtractionStep
{


	static FRootMotionExtractionStep ReadAsMe(uintptr_t address)
	{
		FRootMotionExtractionStep ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionExtractionStep& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RawCurveTracks
// 0x0000
struct FRawCurveTracks
{


	static FRawCurveTracks ReadAsMe(uintptr_t address)
	{
		FRawCurveTracks ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRawCurveTracks& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimCurveBase
// 0x0000
struct FAnimCurveBase
{


	static FAnimCurveBase ReadAsMe(uintptr_t address)
	{
		FAnimCurveBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimCurveBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SmartName
// 0x0000
struct FSmartName
{


	static FSmartName ReadAsMe(uintptr_t address)
	{
		FSmartName ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSmartName& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TransformCurve
// 0x5A36BEC0
struct FTransformCurve
{
	unsigned char                                      UnknownData00[0x5A36BEC0];                                // 0x0000(0x5A36BEC0) MISSED OFFSET

	static FTransformCurve ReadAsMe(uintptr_t address)
	{
		FTransformCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTransformCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FloatCurve
// 0x5A36BEC0
struct FFloatCurve
{
	unsigned char                                      UnknownData00[0x5A36BEC0];                                // 0x0000(0x5A36BEC0) MISSED OFFSET

	static FFloatCurve ReadAsMe(uintptr_t address)
	{
		FFloatCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFloatCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VectorCurve
// 0x5A36BEC0
struct FVectorCurve
{
	unsigned char                                      UnknownData00[0x5A36BEC0];                                // 0x0000(0x5A36BEC0) MISSED OFFSET

	static FVectorCurve ReadAsMe(uintptr_t address)
	{
		FVectorCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVectorCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SlotEvaluationPose
// 0x0000
struct FSlotEvaluationPose
{


	static FSlotEvaluationPose ReadAsMe(uintptr_t address)
	{
		FSlotEvaluationPose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSlotEvaluationPose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.A2Pose
// 0x0000
struct FA2Pose
{


	static FA2Pose ReadAsMe(uintptr_t address)
	{
		FA2Pose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FA2Pose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.A2CSPose
// 0x5A36BA40
struct FA2CSPose
{
	unsigned char                                      UnknownData00[0x5A36BA40];                                // 0x0000(0x5A36BA40) MISSED OFFSET

	static FA2CSPose ReadAsMe(uintptr_t address)
	{
		FA2CSPose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FA2CSPose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.QueuedDrawDebugItem
// 0x0000
struct FQueuedDrawDebugItem
{


	static FQueuedDrawDebugItem ReadAsMe(uintptr_t address)
	{
		FQueuedDrawDebugItem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FQueuedDrawDebugItem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimLinkableElement
// 0x0000
struct FAnimLinkableElement
{


	static FAnimLinkableElement ReadAsMe(uintptr_t address)
	{
		FAnimLinkableElement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimLinkableElement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimMontageInstance
// 0x0000
struct FAnimMontageInstance
{


	static FAnimMontageInstance ReadAsMe(uintptr_t address)
	{
		FAnimMontageInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimMontageInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNotifyEvent
// 0x5A36B800
struct FAnimNotifyEvent
{
	unsigned char                                      UnknownData00[0x5A36B800];                                // 0x0000(0x5A36B800) MISSED OFFSET

	static FAnimNotifyEvent ReadAsMe(uintptr_t address)
	{
		FAnimNotifyEvent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNotifyEvent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BranchingPointMarker
// 0x0000
struct FBranchingPointMarker
{


	static FBranchingPointMarker ReadAsMe(uintptr_t address)
	{
		FBranchingPointMarker ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBranchingPointMarker& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BranchingPoint
// 0x5A36B800
struct FBranchingPoint
{
	unsigned char                                      UnknownData00[0x5A36B800];                                // 0x0000(0x5A36B800) MISSED OFFSET

	static FBranchingPoint ReadAsMe(uintptr_t address)
	{
		FBranchingPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBranchingPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SlotAnimationTrack
// 0x0000
struct FSlotAnimationTrack
{


	static FSlotAnimationTrack ReadAsMe(uintptr_t address)
	{
		FSlotAnimationTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSlotAnimationTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_ApplyMeshSpaceAdditive
// 0x5A12E440
struct FAnimNode_ApplyMeshSpaceAdditive
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_ApplyMeshSpaceAdditive ReadAsMe(uintptr_t address)
	{
		FAnimNode_ApplyMeshSpaceAdditive ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_ApplyMeshSpaceAdditive& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CompositeSection
// 0x5A36B800
struct FCompositeSection
{
	unsigned char                                      UnknownData00[0x5A36B800];                                // 0x0000(0x5A36B800) MISSED OFFSET

	static FCompositeSection ReadAsMe(uintptr_t address)
	{
		FCompositeSection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCompositeSection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_SaveCachedPose
// 0x5A12E440
struct FAnimNode_SaveCachedPose
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_SaveCachedPose ReadAsMe(uintptr_t address)
	{
		FAnimNode_SaveCachedPose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_SaveCachedPose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_SequencePlayer
// 0x5A31E500
struct FAnimNode_SequencePlayer
{
	unsigned char                                      UnknownData00[0x5A31E500];                                // 0x0000(0x5A31E500) MISSED OFFSET

	static FAnimNode_SequencePlayer ReadAsMe(uintptr_t address)
	{
		FAnimNode_SequencePlayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_SequencePlayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationPotentialTransition
// 0x0000
struct FAnimationPotentialTransition
{


	static FAnimationPotentialTransition ReadAsMe(uintptr_t address)
	{
		FAnimationPotentialTransition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationPotentialTransition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationActiveTransitionEntry
// 0x0000
struct FAnimationActiveTransitionEntry
{


	static FAnimationActiveTransitionEntry ReadAsMe(uintptr_t address)
	{
		FAnimationActiveTransitionEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationActiveTransitionEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_StateMachine
// 0x5A12E440
struct FAnimNode_StateMachine
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_StateMachine ReadAsMe(uintptr_t address)
	{
		FAnimNode_StateMachine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_StateMachine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_SubInstance
// 0x5A12E440
struct FAnimNode_SubInstance
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_SubInstance ReadAsMe(uintptr_t address)
	{
		FAnimNode_SubInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_SubInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_SubInput
// 0x5A12E440
struct FAnimNode_SubInput
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_SubInput ReadAsMe(uintptr_t address)
	{
		FAnimNode_SubInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_SubInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_TransitionPoseEvaluator
// 0x5A12E440
struct FAnimNode_TransitionPoseEvaluator
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_TransitionPoseEvaluator ReadAsMe(uintptr_t address)
	{
		FAnimNode_TransitionPoseEvaluator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_TransitionPoseEvaluator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_TransitionResult
// 0x5A12E440
struct FAnimNode_TransitionResult
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_TransitionResult ReadAsMe(uintptr_t address)
	{
		FAnimNode_TransitionResult ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_TransitionResult& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_UseCachedPose
// 0x5A12E440
struct FAnimNode_UseCachedPose
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_UseCachedPose ReadAsMe(uintptr_t address)
	{
		FAnimNode_UseCachedPose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_UseCachedPose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ExposedValueHandler
// 0x0000
struct FExposedValueHandler
{


	static FExposedValueHandler ReadAsMe(uintptr_t address)
	{
		FExposedValueHandler ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FExposedValueHandler& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ExposedValueCopyRecord
// 0x0000
struct FExposedValueCopyRecord
{


	static FExposedValueCopyRecord ReadAsMe(uintptr_t address)
	{
		FExposedValueCopyRecord ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FExposedValueCopyRecord& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_ConvertLocalToComponentSpace
// 0x5A12E440
struct FAnimNode_ConvertLocalToComponentSpace
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_ConvertLocalToComponentSpace ReadAsMe(uintptr_t address)
	{
		FAnimNode_ConvertLocalToComponentSpace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_ConvertLocalToComponentSpace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNotifyQueue
// 0x0000
struct FAnimNotifyQueue
{


	static FAnimNotifyQueue ReadAsMe(uintptr_t address)
	{
		FAnimNotifyQueue ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNotifyQueue& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_ConvertComponentToLocalSpace
// 0x5A12E440
struct FAnimNode_ConvertComponentToLocalSpace
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_ConvertComponentToLocalSpace ReadAsMe(uintptr_t address)
	{
		FAnimNode_ConvertComponentToLocalSpace ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_ConvertComponentToLocalSpace& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNotifyArray
// 0x0000
struct FAnimNotifyArray
{


	static FAnimNotifyArray ReadAsMe(uintptr_t address)
	{
		FAnimNotifyArray ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNotifyArray& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CompressedSegment
// 0x0000
struct FCompressedSegment
{


	static FCompressedSegment ReadAsMe(uintptr_t address)
	{
		FCompressedSegment ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCompressedSegment& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CompressedTrack
// 0x0000
struct FCompressedTrack
{


	static FCompressedTrack ReadAsMe(uintptr_t address)
	{
		FCompressedTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCompressedTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNotifyEventReference
// 0x0000
struct FAnimNotifyEventReference
{


	static FAnimNotifyEventReference ReadAsMe(uintptr_t address)
	{
		FAnimNotifyEventReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNotifyEventReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ScaleTrack
// 0x0000
struct FScaleTrack
{


	static FScaleTrack ReadAsMe(uintptr_t address)
	{
		FScaleTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FScaleTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CurveTrack
// 0x0000
struct FCurveTrack
{


	static FCurveTrack ReadAsMe(uintptr_t address)
	{
		FCurveTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCurveTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RotationTrack
// 0x0000
struct FRotationTrack
{


	static FRotationTrack ReadAsMe(uintptr_t address)
	{
		FRotationTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRotationTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TranslationTrack
// 0x0000
struct FTranslationTrack
{


	static FTranslationTrack ReadAsMe(uintptr_t address)
	{
		FTranslationTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTranslationTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TrackToSkeletonMap
// 0x0000
struct FTrackToSkeletonMap
{


	static FTrackToSkeletonMap ReadAsMe(uintptr_t address)
	{
		FTrackToSkeletonMap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTrackToSkeletonMap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimSequenceTrackContainer
// 0x0000
struct FAnimSequenceTrackContainer
{


	static FAnimSequenceTrackContainer ReadAsMe(uintptr_t address)
	{
		FAnimSequenceTrackContainer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimSequenceTrackContainer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RawAnimSequenceTrack
// 0x0000
struct FRawAnimSequenceTrack
{


	static FRawAnimSequenceTrack ReadAsMe(uintptr_t address)
	{
		FRawAnimSequenceTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRawAnimSequenceTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimSetMeshLinkup
// 0x0000
struct FAnimSetMeshLinkup
{


	static FAnimSetMeshLinkup ReadAsMe(uintptr_t address)
	{
		FAnimSetMeshLinkup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimSetMeshLinkup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BakedAnimationStateMachine
// 0x0000
struct FBakedAnimationStateMachine
{


	static FBakedAnimationStateMachine ReadAsMe(uintptr_t address)
	{
		FBakedAnimationStateMachine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBakedAnimationStateMachine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNode_SingleNode
// 0x5A12E440
struct FAnimNode_SingleNode
{
	unsigned char                                      UnknownData00[0x5A12E440];                                // 0x0000(0x5A12E440) MISSED OFFSET

	static FAnimNode_SingleNode ReadAsMe(uintptr_t address)
	{
		FAnimNode_SingleNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNode_SingleNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationStateBase
// 0x0000
struct FAnimationStateBase
{


	static FAnimationStateBase ReadAsMe(uintptr_t address)
	{
		FAnimationStateBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationStateBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimSingleNodeInstanceProxy
// 0x5A12FD00
struct FAnimSingleNodeInstanceProxy
{
	unsigned char                                      UnknownData00[0x5A12FD00];                                // 0x0000(0x5A12FD00) MISSED OFFSET

	static FAnimSingleNodeInstanceProxy ReadAsMe(uintptr_t address)
	{
		FAnimSingleNodeInstanceProxy ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimSingleNodeInstanceProxy& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationTransitionBetweenStates
// 0x5A36CD00
struct FAnimationTransitionBetweenStates
{
	unsigned char                                      UnknownData00[0x5A36CD00];                                // 0x0000(0x5A36CD00) MISSED OFFSET

	static FAnimationTransitionBetweenStates ReadAsMe(uintptr_t address)
	{
		FAnimationTransitionBetweenStates ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationTransitionBetweenStates& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BakedAnimationState
// 0x0000
struct FBakedAnimationState
{


	static FBakedAnimationState ReadAsMe(uintptr_t address)
	{
		FBakedAnimationState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBakedAnimationState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BakedStateExitTransition
// 0x0000
struct FBakedStateExitTransition
{


	static FBakedStateExitTransition ReadAsMe(uintptr_t address)
	{
		FBakedStateExitTransition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBakedStateExitTransition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationState
// 0x5A36CD00
struct FAnimationState
{
	unsigned char                                      UnknownData00[0x5A36CD00];                                // 0x0000(0x5A36CD00) MISSED OFFSET

	static FAnimationState ReadAsMe(uintptr_t address)
	{
		FAnimationState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimationTransitionRule
// 0x0000
struct FAnimationTransitionRule
{


	static FAnimationTransitionRule ReadAsMe(uintptr_t address)
	{
		FAnimationTransitionRule ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimationTransitionRule& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MarkerSyncData
// 0x0000
struct FMarkerSyncData
{


	static FMarkerSyncData ReadAsMe(uintptr_t address)
	{
		FMarkerSyncData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMarkerSyncData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimSyncMarker
// 0x0000
struct FAnimSyncMarker
{


	static FAnimSyncMarker ReadAsMe(uintptr_t address)
	{
		FAnimSyncMarker ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimSyncMarker& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimNotifyTrack
// 0x0000
struct FAnimNotifyTrack
{


	static FAnimNotifyTrack ReadAsMe(uintptr_t address)
	{
		FAnimNotifyTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimNotifyTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PerBoneBlendWeights
// 0x0000
struct FPerBoneBlendWeights
{


	static FPerBoneBlendWeights ReadAsMe(uintptr_t address)
	{
		FPerBoneBlendWeights ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPerBoneBlendWeights& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AssetImportInfo
// 0x0000
struct FAssetImportInfo
{


	static FAssetImportInfo ReadAsMe(uintptr_t address)
	{
		FAssetImportInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAssetImportInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrimaryAssetRules
// 0x0000
struct FPrimaryAssetRules
{


	static FPrimaryAssetRules ReadAsMe(uintptr_t address)
	{
		FPrimaryAssetRules ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimaryAssetRules& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrimaryAssetRulesCustomOverride
// 0x0000
struct FPrimaryAssetRulesCustomOverride
{


	static FPrimaryAssetRulesCustomOverride ReadAsMe(uintptr_t address)
	{
		FPrimaryAssetRulesCustomOverride ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimaryAssetRulesCustomOverride& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrimaryAssetRulesOverride
// 0x0000
struct FPrimaryAssetRulesOverride
{


	static FPrimaryAssetRulesOverride ReadAsMe(uintptr_t address)
	{
		FPrimaryAssetRulesOverride ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimaryAssetRulesOverride& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AssetManagerRedirect
// 0x0000
struct FAssetManagerRedirect
{


	static FAssetManagerRedirect ReadAsMe(uintptr_t address)
	{
		FAssetManagerRedirect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAssetManagerRedirect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrimaryAssetTypeInfo
// 0x0000
struct FPrimaryAssetTypeInfo
{


	static FPrimaryAssetTypeInfo ReadAsMe(uintptr_t address)
	{
		FPrimaryAssetTypeInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimaryAssetTypeInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AtmospherePrecomputeInstanceData
// 0x5A367480
struct FAtmospherePrecomputeInstanceData
{
	unsigned char                                      UnknownData00[0x5A367480];                                // 0x0000(0x5A367480) MISSED OFFSET

	static FAtmospherePrecomputeInstanceData ReadAsMe(uintptr_t address)
	{
		FAtmospherePrecomputeInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAtmospherePrecomputeInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AssetMapping
// 0x0000
struct FAssetMapping
{


	static FAssetMapping ReadAsMe(uintptr_t address)
	{
		FAssetMapping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAssetMapping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AtmospherePrecomputeParameters
// 0x0000
struct FAtmospherePrecomputeParameters
{


	static FAtmospherePrecomputeParameters ReadAsMe(uintptr_t address)
	{
		FAtmospherePrecomputeParameters ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAtmospherePrecomputeParameters& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BaseAttenuationSettings
// 0x0000
struct FBaseAttenuationSettings
{


	static FBaseAttenuationSettings ReadAsMe(uintptr_t address)
	{
		FBaseAttenuationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBaseAttenuationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AudioComponentParam
// 0x0000
struct FAudioComponentParam
{


	static FAudioComponentParam ReadAsMe(uintptr_t address)
	{
		FAudioComponentParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAudioComponentParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InteriorSettings
// 0x0000
struct FInteriorSettings
{


	static FInteriorSettings ReadAsMe(uintptr_t address)
	{
		FInteriorSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInteriorSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AudioQualitySettings
// 0x0000
struct FAudioQualitySettings
{


	static FAudioQualitySettings ReadAsMe(uintptr_t address)
	{
		FAudioQualitySettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAudioQualitySettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ReverbSettings
// 0x0000
struct FReverbSettings
{


	static FReverbSettings ReadAsMe(uintptr_t address)
	{
		FReverbSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FReverbSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LaunchOnTestSettings
// 0x0000
struct FLaunchOnTestSettings
{


	static FLaunchOnTestSettings ReadAsMe(uintptr_t address)
	{
		FLaunchOnTestSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLaunchOnTestSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FilePath
// 0x0000
struct FFilePath
{


	static FFilePath ReadAsMe(uintptr_t address)
	{
		FFilePath ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFilePath& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EditorMapPerformanceTestDefinition
// 0x0000
struct FEditorMapPerformanceTestDefinition
{


	static FEditorMapPerformanceTestDefinition ReadAsMe(uintptr_t address)
	{
		FEditorMapPerformanceTestDefinition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEditorMapPerformanceTestDefinition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BuildPromotionNewProjectSettings
// 0x0000
struct FBuildPromotionNewProjectSettings
{


	static FBuildPromotionNewProjectSettings ReadAsMe(uintptr_t address)
	{
		FBuildPromotionNewProjectSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBuildPromotionNewProjectSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BuildPromotionOpenAssetSettings
// 0x0000
struct FBuildPromotionOpenAssetSettings
{


	static FBuildPromotionOpenAssetSettings ReadAsMe(uintptr_t address)
	{
		FBuildPromotionOpenAssetSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBuildPromotionOpenAssetSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BuildPromotionImportWorkflowSettings
// 0x0000
struct FBuildPromotionImportWorkflowSettings
{


	static FBuildPromotionImportWorkflowSettings ReadAsMe(uintptr_t address)
	{
		FBuildPromotionImportWorkflowSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBuildPromotionImportWorkflowSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EditorImportWorkflowDefinition
// 0x0000
struct FEditorImportWorkflowDefinition
{


	static FEditorImportWorkflowDefinition ReadAsMe(uintptr_t address)
	{
		FEditorImportWorkflowDefinition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEditorImportWorkflowDefinition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BuildPromotionTestSettings
// 0x0000
struct FBuildPromotionTestSettings
{


	static FBuildPromotionTestSettings ReadAsMe(uintptr_t address)
	{
		FBuildPromotionTestSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBuildPromotionTestSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ImportFactorySettingValues
// 0x0000
struct FImportFactorySettingValues
{


	static FImportFactorySettingValues ReadAsMe(uintptr_t address)
	{
		FImportFactorySettingValues ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FImportFactorySettingValues& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleEditorPromotionSettings
// 0x0000
struct FParticleEditorPromotionSettings
{


	static FParticleEditorPromotionSettings ReadAsMe(uintptr_t address)
	{
		FParticleEditorPromotionSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleEditorPromotionSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialEditorPromotionSettings
// 0x0000
struct FMaterialEditorPromotionSettings
{


	static FMaterialEditorPromotionSettings ReadAsMe(uintptr_t address)
	{
		FMaterialEditorPromotionSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialEditorPromotionSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintEditorPromotionSettings
// 0x0000
struct FBlueprintEditorPromotionSettings
{


	static FBlueprintEditorPromotionSettings ReadAsMe(uintptr_t address)
	{
		FBlueprintEditorPromotionSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintEditorPromotionSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EditorImportExportTestDefinition
// 0x0000
struct FEditorImportExportTestDefinition
{


	static FEditorImportExportTestDefinition ReadAsMe(uintptr_t address)
	{
		FEditorImportExportTestDefinition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEditorImportExportTestDefinition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ExternalToolDefinition
// 0x0000
struct FExternalToolDefinition
{


	static FExternalToolDefinition ReadAsMe(uintptr_t address)
	{
		FExternalToolDefinition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FExternalToolDefinition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NavAvoidanceData
// 0x0000
struct FNavAvoidanceData
{


	static FNavAvoidanceData ReadAsMe(uintptr_t address)
	{
		FNavAvoidanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNavAvoidanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PerBoneInterpolation
// 0x0000
struct FPerBoneInterpolation
{


	static FPerBoneInterpolation ReadAsMe(uintptr_t address)
	{
		FPerBoneInterpolation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPerBoneInterpolation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlendProfileBoneEntry
// 0x0000
struct FBlendProfileBoneEntry
{


	static FBlendProfileBoneEntry ReadAsMe(uintptr_t address)
	{
		FBlendProfileBoneEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlendProfileBoneEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GridBlendSample
// 0x0000
struct FGridBlendSample
{


	static FGridBlendSample ReadAsMe(uintptr_t address)
	{
		FGridBlendSample ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGridBlendSample& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EditorElement
// 0x0000
struct FEditorElement
{


	static FEditorElement ReadAsMe(uintptr_t address)
	{
		FEditorElement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEditorElement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlendParameter
// 0x0000
struct FBlendParameter
{


	static FBlendParameter ReadAsMe(uintptr_t address)
	{
		FBlendParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlendParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlendSample
// 0x0000
struct FBlendSample
{


	static FBlendSample ReadAsMe(uintptr_t address)
	{
		FBlendSample ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlendSample& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InterpolationParameter
// 0x0000
struct FInterpolationParameter
{


	static FInterpolationParameter ReadAsMe(uintptr_t address)
	{
		FInterpolationParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpolationParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BPEditorBookmarkNode
// 0x0000
struct FBPEditorBookmarkNode
{


	static FBPEditorBookmarkNode ReadAsMe(uintptr_t address)
	{
		FBPEditorBookmarkNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBPEditorBookmarkNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EditedDocumentInfo
// 0x0000
struct FEditedDocumentInfo
{


	static FEditedDocumentInfo ReadAsMe(uintptr_t address)
	{
		FEditedDocumentInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEditedDocumentInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BPInterfaceDescription
// 0x0000
struct FBPInterfaceDescription
{


	static FBPInterfaceDescription ReadAsMe(uintptr_t address)
	{
		FBPInterfaceDescription ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBPInterfaceDescription& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BPVariableDescription
// 0x0000
struct FBPVariableDescription
{


	static FBPVariableDescription ReadAsMe(uintptr_t address)
	{
		FBPVariableDescription ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBPVariableDescription& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BPVariableMetaDataEntry
// 0x0000
struct FBPVariableMetaDataEntry
{


	static FBPVariableMetaDataEntry ReadAsMe(uintptr_t address)
	{
		FBPVariableMetaDataEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBPVariableMetaDataEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EdGraphPinType
// 0x0000
struct FEdGraphPinType
{


	static FEdGraphPinType ReadAsMe(uintptr_t address)
	{
		FEdGraphPinType ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEdGraphPinType& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EdGraphTerminalType
// 0x0000
struct FEdGraphTerminalType
{


	static FEdGraphTerminalType ReadAsMe(uintptr_t address)
	{
		FEdGraphTerminalType ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEdGraphTerminalType& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintMacroCosmeticInfo
// 0x0000
struct FBlueprintMacroCosmeticInfo
{


	static FBlueprintMacroCosmeticInfo ReadAsMe(uintptr_t address)
	{
		FBlueprintMacroCosmeticInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintMacroCosmeticInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CompilerNativizationOptions
// 0x0000
struct FCompilerNativizationOptions
{


	static FCompilerNativizationOptions ReadAsMe(uintptr_t address)
	{
		FCompilerNativizationOptions ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCompilerNativizationOptions& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintComponentChangedPropertyInfo
// 0x0000
struct FBlueprintComponentChangedPropertyInfo
{


	static FBlueprintComponentChangedPropertyInfo ReadAsMe(uintptr_t address)
	{
		FBlueprintComponentChangedPropertyInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintComponentChangedPropertyInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintCookedComponentInstancingData
// 0x0000
struct FBlueprintCookedComponentInstancingData
{


	static FBlueprintCookedComponentInstancingData ReadAsMe(uintptr_t address)
	{
		FBlueprintCookedComponentInstancingData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintCookedComponentInstancingData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintDebugData
// 0x0000
struct FBlueprintDebugData
{


	static FBlueprintDebugData ReadAsMe(uintptr_t address)
	{
		FBlueprintDebugData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintDebugData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EventGraphFastCallPair
// 0x0000
struct FEventGraphFastCallPair
{


	static FEventGraphFastCallPair ReadAsMe(uintptr_t address)
	{
		FEventGraphFastCallPair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEventGraphFastCallPair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PointerToUberGraphFrame
// 0x0000
struct FPointerToUberGraphFrame
{


	static FPointerToUberGraphFrame ReadAsMe(uintptr_t address)
	{
		FPointerToUberGraphFrame ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPointerToUberGraphFrame& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BodyInstance
// 0x0000
struct FBodyInstance
{


	static FBodyInstance ReadAsMe(uintptr_t address)
	{
		FBodyInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBodyInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NodeToCodeAssociation
// 0x0000
struct FNodeToCodeAssociation
{


	static FNodeToCodeAssociation ReadAsMe(uintptr_t address)
	{
		FNodeToCodeAssociation ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNodeToCodeAssociation& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DebuggingInfoForSingleFunction
// 0x0000
struct FDebuggingInfoForSingleFunction
{


	static FDebuggingInfoForSingleFunction ReadAsMe(uintptr_t address)
	{
		FDebuggingInfoForSingleFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDebuggingInfoForSingleFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.WalkableSlopeOverride
// 0x0000
struct FWalkableSlopeOverride
{


	static FWalkableSlopeOverride ReadAsMe(uintptr_t address)
	{
		FWalkableSlopeOverride ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FWalkableSlopeOverride& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollisionResponse
// 0x0000
struct FCollisionResponse
{


	static FCollisionResponse ReadAsMe(uintptr_t address)
	{
		FCollisionResponse ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollisionResponse& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollisionResponseContainer
// 0x0000
struct FCollisionResponseContainer
{


	static FCollisionResponseContainer ReadAsMe(uintptr_t address)
	{
		FCollisionResponseContainer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollisionResponseContainer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ResponseChannel
// 0x0000
struct FResponseChannel
{


	static FResponseChannel ReadAsMe(uintptr_t address)
	{
		FResponseChannel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FResponseChannel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimCurveType
// 0x0000
struct FAnimCurveType
{


	static FAnimCurveType ReadAsMe(uintptr_t address)
	{
		FAnimCurveType ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimCurveType& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BookmarkBaseJumpToSettings
// 0x0000
struct FBookmarkBaseJumpToSettings
{


	static FBookmarkBaseJumpToSettings ReadAsMe(uintptr_t address)
	{
		FBookmarkBaseJumpToSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBookmarkBaseJumpToSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GeomSelection
// 0x0000
struct FGeomSelection
{


	static FGeomSelection ReadAsMe(uintptr_t address)
	{
		FGeomSelection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGeomSelection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BookmarkJumpToSettings
// 0x5A3D0DC0
struct FBookmarkJumpToSettings
{
	unsigned char                                      UnknownData00[0x5A3D0DC0];                                // 0x0000(0x5A3D0DC0) MISSED OFFSET

	static FBookmarkJumpToSettings ReadAsMe(uintptr_t address)
	{
		FBookmarkJumpToSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBookmarkJumpToSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BuilderPoly
// 0x0000
struct FBuilderPoly
{


	static FBuilderPoly ReadAsMe(uintptr_t address)
	{
		FBuilderPoly ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBuilderPoly& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CachedAnimTransitionData
// 0x0000
struct FCachedAnimTransitionData
{


	static FCachedAnimTransitionData ReadAsMe(uintptr_t address)
	{
		FCachedAnimTransitionData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedAnimTransitionData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Bookmark2DJumpToSettings
// 0x0000
struct FBookmark2DJumpToSettings
{


	static FBookmark2DJumpToSettings ReadAsMe(uintptr_t address)
	{
		FBookmark2DJumpToSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBookmark2DJumpToSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CachedAnimStateArray
// 0x0000
struct FCachedAnimStateArray
{


	static FCachedAnimStateArray ReadAsMe(uintptr_t address)
	{
		FCachedAnimStateArray ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedAnimStateArray& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CachedAnimAssetPlayerData
// 0x0000
struct FCachedAnimAssetPlayerData
{


	static FCachedAnimAssetPlayerData ReadAsMe(uintptr_t address)
	{
		FCachedAnimAssetPlayerData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedAnimAssetPlayerData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CachedAnimStateData
// 0x0000
struct FCachedAnimStateData
{


	static FCachedAnimStateData ReadAsMe(uintptr_t address)
	{
		FCachedAnimStateData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedAnimStateData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PooledCameraShakes
// 0x0000
struct FPooledCameraShakes
{


	static FPooledCameraShakes ReadAsMe(uintptr_t address)
	{
		FPooledCameraShakes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPooledCameraShakes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CachedAnimRelevancyData
// 0x0000
struct FCachedAnimRelevancyData
{


	static FCachedAnimRelevancyData ReadAsMe(uintptr_t address)
	{
		FCachedAnimRelevancyData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedAnimRelevancyData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FOscillator
// 0x0000
struct FFOscillator
{


	static FFOscillator ReadAsMe(uintptr_t address)
	{
		FFOscillator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFOscillator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VOscillator
// 0x0000
struct FVOscillator
{


	static FVOscillator ReadAsMe(uintptr_t address)
	{
		FVOscillator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVOscillator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ROscillator
// 0x0000
struct FROscillator
{


	static FROscillator ReadAsMe(uintptr_t address)
	{
		FROscillator ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FROscillator& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PostProcessSettings
// 0x0000
struct FPostProcessSettings
{


	static FPostProcessSettings ReadAsMe(uintptr_t address)
	{
		FPostProcessSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPostProcessSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DummySpacerCameraTypes
// 0x0000
struct FDummySpacerCameraTypes
{


	static FDummySpacerCameraTypes ReadAsMe(uintptr_t address)
	{
		FDummySpacerCameraTypes ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDummySpacerCameraTypes& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.WeightedBlendables
// 0x0000
struct FWeightedBlendables
{


	static FWeightedBlendables ReadAsMe(uintptr_t address)
	{
		FWeightedBlendables ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FWeightedBlendables& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.WeightedBlendable
// 0x0000
struct FWeightedBlendable
{


	static FWeightedBlendable ReadAsMe(uintptr_t address)
	{
		FWeightedBlendable ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FWeightedBlendable& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MinimalViewInfo
// 0x0000
struct FMinimalViewInfo
{


	static FMinimalViewInfo ReadAsMe(uintptr_t address)
	{
		FMinimalViewInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMinimalViewInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TextSizingParameters
// 0x0000
struct FTextSizingParameters
{


	static FTextSizingParameters ReadAsMe(uintptr_t address)
	{
		FTextSizingParameters ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTextSizingParameters& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CanvasIcon
// 0x0000
struct FCanvasIcon
{


	static FCanvasIcon ReadAsMe(uintptr_t address)
	{
		FCanvasIcon ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCanvasIcon& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BasedMovementInfo
// 0x0000
struct FBasedMovementInfo
{


	static FBasedMovementInfo ReadAsMe(uintptr_t address)
	{
		FBasedMovementInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBasedMovementInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.WrappedStringElement
// 0x0000
struct FWrappedStringElement
{


	static FWrappedStringElement ReadAsMe(uintptr_t address)
	{
		FWrappedStringElement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FWrappedStringElement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RepRootMotionMontage
// 0x0000
struct FRepRootMotionMontage
{


	static FRepRootMotionMontage ReadAsMe(uintptr_t address)
	{
		FRepRootMotionMontage ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRepRootMotionMontage& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SimulatedRootMotionReplicatedMove
// 0x0000
struct FSimulatedRootMotionReplicatedMove
{


	static FSimulatedRootMotionReplicatedMove ReadAsMe(uintptr_t address)
	{
		FSimulatedRootMotionReplicatedMove ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSimulatedRootMotionReplicatedMove& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSourceGroup
// 0x0000
struct alignas(8) FRootMotionSourceGroup
{


	static FRootMotionSourceGroup ReadAsMe(uintptr_t address)
	{
		FRootMotionSourceGroup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSourceGroup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSourceSettings
// 0x0000
struct FRootMotionSourceSettings
{


	static FRootMotionSourceSettings ReadAsMe(uintptr_t address)
	{
		FRootMotionSourceSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSourceSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CharacterMovementComponentPostPhysicsTickFunction
// 0x5A123C40
struct FCharacterMovementComponentPostPhysicsTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FCharacterMovementComponentPostPhysicsTickFunction ReadAsMe(uintptr_t address)
	{
		FCharacterMovementComponentPostPhysicsTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCharacterMovementComponentPostPhysicsTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FindFloorResult
// 0x0000
struct FFindFloorResult
{


	static FFindFloorResult ReadAsMe(uintptr_t address)
	{
		FFindFloorResult ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFindFloorResult& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ChildActorComponentInstanceData
// 0x5A367480
struct FChildActorComponentInstanceData
{
	unsigned char                                      UnknownData00[0x5A367480];                                // 0x0000(0x5A367480) MISSED OFFSET

	static FChildActorComponentInstanceData ReadAsMe(uintptr_t address)
	{
		FChildActorComponentInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FChildActorComponentInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ChildActorAttachedActorInfo
// 0x0000
struct FChildActorAttachedActorInfo
{


	static FChildActorAttachedActorInfo ReadAsMe(uintptr_t address)
	{
		FChildActorAttachedActorInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FChildActorAttachedActorInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CustomProfile
// 0x0000
struct FCustomProfile
{


	static FCustomProfile ReadAsMe(uintptr_t address)
	{
		FCustomProfile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCustomProfile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CustomChannelSetup
// 0x0000
struct FCustomChannelSetup
{


	static FCustomChannelSetup ReadAsMe(uintptr_t address)
	{
		FCustomChannelSetup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCustomChannelSetup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollisionResponseTemplate
// 0x0000
struct FCollisionResponseTemplate
{


	static FCollisionResponseTemplate ReadAsMe(uintptr_t address)
	{
		FCollisionResponseTemplate ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollisionResponseTemplate& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MeshUVChannelInfo
// 0x0000
struct FMeshUVChannelInfo
{


	static FMeshUVChannelInfo ReadAsMe(uintptr_t address)
	{
		FMeshUVChannelInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMeshUVChannelInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintComponentDelegateBinding
// 0x0000
struct FBlueprintComponentDelegateBinding
{


	static FBlueprintComponentDelegateBinding ReadAsMe(uintptr_t address)
	{
		FBlueprintComponentDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintComponentDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AutoCompleteNode
// 0x0000
struct FAutoCompleteNode
{


	static FAutoCompleteNode ReadAsMe(uintptr_t address)
	{
		FAutoCompleteNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAutoCompleteNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AngularDriveConstraint
// 0x0000
struct FAngularDriveConstraint
{


	static FAngularDriveConstraint ReadAsMe(uintptr_t address)
	{
		FAngularDriveConstraint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAngularDriveConstraint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ConstraintDrive
// 0x0000
struct FConstraintDrive
{


	static FConstraintDrive ReadAsMe(uintptr_t address)
	{
		FConstraintDrive ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConstraintDrive& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LinearDriveConstraint
// 0x0000
struct FLinearDriveConstraint
{


	static FLinearDriveConstraint ReadAsMe(uintptr_t address)
	{
		FLinearDriveConstraint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLinearDriveConstraint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ConstraintInstance
// 0x0000
struct FConstraintInstance
{


	static FConstraintInstance ReadAsMe(uintptr_t address)
	{
		FConstraintInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConstraintInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ConstraintProfileProperties
// 0x0000
struct FConstraintProfileProperties
{


	static FConstraintProfileProperties ReadAsMe(uintptr_t address)
	{
		FConstraintProfileProperties ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConstraintProfileProperties& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ConstraintBaseParams
// 0x0000
struct FConstraintBaseParams
{


	static FConstraintBaseParams ReadAsMe(uintptr_t address)
	{
		FConstraintBaseParams ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConstraintBaseParams& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TwistConstraint
// 0x5A3D1FC0
struct FTwistConstraint
{
	unsigned char                                      UnknownData00[0x5A3D1FC0];                                // 0x0000(0x5A3D1FC0) MISSED OFFSET

	static FTwistConstraint ReadAsMe(uintptr_t address)
	{
		FTwistConstraint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTwistConstraint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ConeConstraint
// 0x5A3D1FC0
struct FConeConstraint
{
	unsigned char                                      UnknownData00[0x5A3D1FC0];                                // 0x0000(0x5A3D1FC0) MISSED OFFSET

	static FConeConstraint ReadAsMe(uintptr_t address)
	{
		FConeConstraint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConeConstraint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LinearConstraint
// 0x5A3D1FC0
struct FLinearConstraint
{
	unsigned char                                      UnknownData00[0x5A3D1FC0];                                // 0x0000(0x5A3D1FC0) MISSED OFFSET

	static FLinearConstraint ReadAsMe(uintptr_t address)
	{
		FLinearConstraint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLinearConstraint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CullDistanceSizePair
// 0x0000
struct FCullDistanceSizePair
{


	static FCullDistanceSizePair ReadAsMe(uintptr_t address)
	{
		FCullDistanceSizePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCullDistanceSizePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RuntimeCurveLinearColor
// 0x0000
struct FRuntimeCurveLinearColor
{


	static FRuntimeCurveLinearColor ReadAsMe(uintptr_t address)
	{
		FRuntimeCurveLinearColor ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRuntimeCurveLinearColor& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NamedCurveValue
// 0x0000
struct FNamedCurveValue
{


	static FNamedCurveValue ReadAsMe(uintptr_t address)
	{
		FNamedCurveValue ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNamedCurveValue& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CurveTableRowHandle
// 0x0000
struct FCurveTableRowHandle
{


	static FCurveTableRowHandle ReadAsMe(uintptr_t address)
	{
		FCurveTableRowHandle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCurveTableRowHandle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DataTableCategoryHandle
// 0x0000
struct FDataTableCategoryHandle
{


	static FDataTableCategoryHandle ReadAsMe(uintptr_t address)
	{
		FDataTableCategoryHandle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDataTableCategoryHandle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DataTableRowHandle
// 0x0000
struct FDataTableRowHandle
{


	static FDataTableRowHandle ReadAsMe(uintptr_t address)
	{
		FDataTableRowHandle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDataTableRowHandle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DebugDisplayProperty
// 0x0000
struct FDebugDisplayProperty
{


	static FDebugDisplayProperty ReadAsMe(uintptr_t address)
	{
		FDebugDisplayProperty ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDebugDisplayProperty& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DebugTextInfo
// 0x0000
struct FDebugTextInfo
{


	static FDebugTextInfo ReadAsMe(uintptr_t address)
	{
		FDebugTextInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDebugTextInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RollbackNetStartupActorInfo
// 0x0000
struct FRollbackNetStartupActorInfo
{


	static FRollbackNetStartupActorInfo ReadAsMe(uintptr_t address)
	{
		FRollbackNetStartupActorInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRollbackNetStartupActorInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LevelNameAndTime
// 0x0000
struct FLevelNameAndTime
{


	static FLevelNameAndTime ReadAsMe(uintptr_t address)
	{
		FLevelNameAndTime ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLevelNameAndTime& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DialogueWaveParameter
// 0x0000
struct FDialogueWaveParameter
{


	static FDialogueWaveParameter ReadAsMe(uintptr_t address)
	{
		FDialogueWaveParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDialogueWaveParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DialogueContext
// 0x0000
struct FDialogueContext
{


	static FDialogueContext ReadAsMe(uintptr_t address)
	{
		FDialogueContext ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDialogueContext& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DialogueContextMapping
// 0x0000
struct FDialogueContextMapping
{


	static FDialogueContextMapping ReadAsMe(uintptr_t address)
	{
		FDialogueContextMapping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDialogueContextMapping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RawDistributionFloat
// 0x5A121A80
struct FRawDistributionFloat
{
	unsigned char                                      UnknownData00[0x5A121A80];                                // 0x0000(0x5A121A80) MISSED OFFSET

	static FRawDistributionFloat ReadAsMe(uintptr_t address)
	{
		FRawDistributionFloat ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRawDistributionFloat& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RawDistributionVector
// 0x5A121A80
struct FRawDistributionVector
{
	unsigned char                                      UnknownData00[0x5A121A80];                                // 0x0000(0x5A121A80) MISSED OFFSET

	static FRawDistributionVector ReadAsMe(uintptr_t address)
	{
		FRawDistributionVector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRawDistributionVector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EdGraphPinReference
// 0x0000
struct FEdGraphPinReference
{


	static FEdGraphPinReference ReadAsMe(uintptr_t address)
	{
		FEdGraphPinReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEdGraphPinReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GraphReference
// 0x0000
struct FGraphReference
{


	static FGraphReference ReadAsMe(uintptr_t address)
	{
		FGraphReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGraphReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EdGraphSchemaAction
// 0x0000
struct FEdGraphSchemaAction
{


	static FEdGraphSchemaAction ReadAsMe(uintptr_t address)
	{
		FEdGraphSchemaAction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEdGraphSchemaAction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EdGraphSchemaAction_NewNode
// 0x5A3D4180
struct FEdGraphSchemaAction_NewNode
{
	unsigned char                                      UnknownData00[0x5A3D4180];                                // 0x0000(0x5A3D4180) MISSED OFFSET

	static FEdGraphSchemaAction_NewNode ReadAsMe(uintptr_t address)
	{
		FEdGraphSchemaAction_NewNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEdGraphSchemaAction_NewNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PluginRedirect
// 0x0000
struct FPluginRedirect
{


	static FPluginRedirect ReadAsMe(uintptr_t address)
	{
		FPluginRedirect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPluginRedirect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StructRedirect
// 0x0000
struct FStructRedirect
{


	static FStructRedirect ReadAsMe(uintptr_t address)
	{
		FStructRedirect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStructRedirect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ClassRedirect
// 0x0000
struct FClassRedirect
{


	static FClassRedirect ReadAsMe(uintptr_t address)
	{
		FClassRedirect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FClassRedirect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GameNameRedirect
// 0x0000
struct FGameNameRedirect
{


	static FGameNameRedirect ReadAsMe(uintptr_t address)
	{
		FGameNameRedirect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGameNameRedirect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ScreenMessageString
// 0x0000
struct FScreenMessageString
{


	static FScreenMessageString ReadAsMe(uintptr_t address)
	{
		FScreenMessageString ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FScreenMessageString& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StatColorMapping
// 0x0000
struct FStatColorMapping
{


	static FStatColorMapping ReadAsMe(uintptr_t address)
	{
		FStatColorMapping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStatColorMapping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StatColorMapEntry
// 0x0000
struct FStatColorMapEntry
{


	static FStatColorMapEntry ReadAsMe(uintptr_t address)
	{
		FStatColorMapEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStatColorMapEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DropNoteInfo
// 0x0000
struct FDropNoteInfo
{


	static FDropNoteInfo ReadAsMe(uintptr_t address)
	{
		FDropNoteInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDropNoteInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.WorldContext
// 0x0000
struct FWorldContext
{


	static FWorldContext ReadAsMe(uintptr_t address)
	{
		FWorldContext ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FWorldContext& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NamedNetDriver
// 0x0000
struct FNamedNetDriver
{


	static FNamedNetDriver ReadAsMe(uintptr_t address)
	{
		FNamedNetDriver ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNamedNetDriver& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LevelStreamingStatus
// 0x0000
struct FLevelStreamingStatus
{


	static FLevelStreamingStatus ReadAsMe(uintptr_t address)
	{
		FLevelStreamingStatus ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLevelStreamingStatus& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FullyLoadedPackagesInfo
// 0x0000
struct FFullyLoadedPackagesInfo
{


	static FFullyLoadedPackagesInfo ReadAsMe(uintptr_t address)
	{
		FFullyLoadedPackagesInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFullyLoadedPackagesInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.URL
// 0x0000
struct FURL
{


	static FURL ReadAsMe(uintptr_t address)
	{
		FURL ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FURL& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NetDriverDefinition
// 0x0000
struct FNetDriverDefinition
{


	static FNetDriverDefinition ReadAsMe(uintptr_t address)
	{
		FNetDriverDefinition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNetDriverDefinition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ExposureSettings
// 0x0000
struct FExposureSettings
{


	static FExposureSettings ReadAsMe(uintptr_t address)
	{
		FExposureSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FExposureSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrimitiveComponentPostPhysicsTickFunction
// 0x5A123C40
struct FPrimitiveComponentPostPhysicsTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FPrimitiveComponentPostPhysicsTickFunction ReadAsMe(uintptr_t address)
	{
		FPrimitiveComponentPostPhysicsTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimitiveComponentPostPhysicsTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TickPrerequisite
// 0x0000
struct FTickPrerequisite
{


	static FTickPrerequisite ReadAsMe(uintptr_t address)
	{
		FTickPrerequisite ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTickPrerequisite& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CanvasUVTri
// 0x0000
struct FCanvasUVTri
{


	static FCanvasUVTri ReadAsMe(uintptr_t address)
	{
		FCanvasUVTri ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCanvasUVTri& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FontRenderInfo
// 0x0000
struct FFontRenderInfo
{


	static FFontRenderInfo ReadAsMe(uintptr_t address)
	{
		FFontRenderInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFontRenderInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DepthFieldGlowInfo
// 0x0000
struct FDepthFieldGlowInfo
{


	static FDepthFieldGlowInfo ReadAsMe(uintptr_t address)
	{
		FDepthFieldGlowInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDepthFieldGlowInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ComponentReference
// 0x0000
struct FComponentReference
{


	static FComponentReference ReadAsMe(uintptr_t address)
	{
		FComponentReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FComponentReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollectionReference
// 0x0000
struct FCollectionReference
{


	static FCollectionReference ReadAsMe(uintptr_t address)
	{
		FCollectionReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollectionReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DamageEvent
// 0x0000
struct FDamageEvent
{


	static FDamageEvent ReadAsMe(uintptr_t address)
	{
		FDamageEvent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDamageEvent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RadialDamageEvent
// 0x5A3D5F80
struct FRadialDamageEvent
{
	unsigned char                                      UnknownData00[0x5A3D5F80];                                // 0x0000(0x5A3D5F80) MISSED OFFSET

	static FRadialDamageEvent ReadAsMe(uintptr_t address)
	{
		FRadialDamageEvent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRadialDamageEvent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RadialDamageParams
// 0x0000
struct FRadialDamageParams
{


	static FRadialDamageParams ReadAsMe(uintptr_t address)
	{
		FRadialDamageParams ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRadialDamageParams& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Redirector
// 0x0000
struct FRedirector
{


	static FRedirector ReadAsMe(uintptr_t address)
	{
		FRedirector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRedirector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PointDamageEvent
// 0x5A3D5F80
struct FPointDamageEvent
{
	unsigned char                                      UnknownData00[0x5A3D5F80];                                // 0x0000(0x5A3D5F80) MISSED OFFSET

	static FPointDamageEvent ReadAsMe(uintptr_t address)
	{
		FPointDamageEvent ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPointDamageEvent& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MeshBuildSettings
// 0x0000
struct FMeshBuildSettings
{


	static FMeshBuildSettings ReadAsMe(uintptr_t address)
	{
		FMeshBuildSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMeshBuildSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.POV
// 0x0000
struct FPOV
{


	static FPOV ReadAsMe(uintptr_t address)
	{
		FPOV ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPOV& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ConstrainComponentPropName
// 0x0000
struct FConstrainComponentPropName
{


	static FConstrainComponentPropName ReadAsMe(uintptr_t address)
	{
		FConstrainComponentPropName ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConstrainComponentPropName& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimUpdateRateParameters
// 0x0000
struct FAnimUpdateRateParameters
{


	static FAnimUpdateRateParameters ReadAsMe(uintptr_t address)
	{
		FAnimUpdateRateParameters ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimUpdateRateParameters& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimSlotInfo
// 0x0000
struct FAnimSlotInfo
{


	static FAnimSlotInfo ReadAsMe(uintptr_t address)
	{
		FAnimSlotInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimSlotInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimSlotDesc
// 0x0000
struct FAnimSlotDesc
{


	static FAnimSlotDesc ReadAsMe(uintptr_t address)
	{
		FAnimSlotDesc ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimSlotDesc& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MTDResult
// 0x0000
struct FMTDResult
{


	static FMTDResult ReadAsMe(uintptr_t address)
	{
		FMTDResult ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMTDResult& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.OverlapResult
// 0x0000
struct FOverlapResult
{


	static FOverlapResult ReadAsMe(uintptr_t address)
	{
		FOverlapResult ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FOverlapResult& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrimitiveMaterialRef
// 0x0000
struct FPrimitiveMaterialRef
{


	static FPrimitiveMaterialRef ReadAsMe(uintptr_t address)
	{
		FPrimitiveMaterialRef ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimitiveMaterialRef& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SwarmDebugOptions
// 0x0000
struct FSwarmDebugOptions
{


	static FSwarmDebugOptions ReadAsMe(uintptr_t address)
	{
		FSwarmDebugOptions ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSwarmDebugOptions& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LightmassDebugOptions
// 0x0000
struct FLightmassDebugOptions
{


	static FLightmassDebugOptions ReadAsMe(uintptr_t address)
	{
		FLightmassDebugOptions ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLightmassDebugOptions& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LightmassPrimitiveSettings
// 0x0000
struct FLightmassPrimitiveSettings
{


	static FLightmassPrimitiveSettings ReadAsMe(uintptr_t address)
	{
		FLightmassPrimitiveSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLightmassPrimitiveSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LightmassLightSettings
// 0x0000
struct FLightmassLightSettings
{


	static FLightmassLightSettings ReadAsMe(uintptr_t address)
	{
		FLightmassLightSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLightmassLightSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BasedPosition
// 0x0000
struct FBasedPosition
{


	static FBasedPosition ReadAsMe(uintptr_t address)
	{
		FBasedPosition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBasedPosition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LightmassDirectionalLightSettings
// 0x5A3D5440
struct FLightmassDirectionalLightSettings
{
	unsigned char                                      UnknownData00[0x5A3D5440];                                // 0x0000(0x5A3D5440) MISSED OFFSET

	static FLightmassDirectionalLightSettings ReadAsMe(uintptr_t address)
	{
		FLightmassDirectionalLightSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLightmassDirectionalLightSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LightmassPointLightSettings
// 0x5A3D5440
struct FLightmassPointLightSettings
{
	unsigned char                                      UnknownData00[0x5A3D5440];                                // 0x0000(0x5A3D5440) MISSED OFFSET

	static FLightmassPointLightSettings ReadAsMe(uintptr_t address)
	{
		FLightmassPointLightSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLightmassPointLightSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FractureEffect
// 0x0000
struct FFractureEffect
{


	static FFractureEffect ReadAsMe(uintptr_t address)
	{
		FFractureEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFractureEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollisionImpactData
// 0x0000
struct FCollisionImpactData
{


	static FCollisionImpactData ReadAsMe(uintptr_t address)
	{
		FCollisionImpactData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollisionImpactData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RigidBodyContactInfo
// 0x0000
struct FRigidBodyContactInfo
{


	static FRigidBodyContactInfo ReadAsMe(uintptr_t address)
	{
		FRigidBodyContactInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRigidBodyContactInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RigidBodyErrorCorrection
// 0x0000
struct FRigidBodyErrorCorrection
{


	static FRigidBodyErrorCorrection ReadAsMe(uintptr_t address)
	{
		FRigidBodyErrorCorrection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRigidBodyErrorCorrection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ExponentialHeightFogData
// 0x0000
struct FExponentialHeightFogData
{


	static FExponentialHeightFogData ReadAsMe(uintptr_t address)
	{
		FExponentialHeightFogData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FExponentialHeightFogData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FontCharacter
// 0x0000
struct FFontCharacter
{


	static FFontCharacter ReadAsMe(uintptr_t address)
	{
		FFontCharacter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFontCharacter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RigidBodyState
// 0x0000
struct FRigidBodyState
{


	static FRigidBodyState ReadAsMe(uintptr_t address)
	{
		FRigidBodyState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRigidBodyState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FontImportOptionsData
// 0x0000
struct FFontImportOptionsData
{


	static FFontImportOptionsData ReadAsMe(uintptr_t address)
	{
		FFontImportOptionsData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFontImportOptionsData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ActiveForceFeedbackEffect
// 0x0000
struct FActiveForceFeedbackEffect
{


	static FActiveForceFeedbackEffect ReadAsMe(uintptr_t address)
	{
		FActiveForceFeedbackEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FActiveForceFeedbackEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ForceFeedbackParameters
// 0x0000
struct FForceFeedbackParameters
{


	static FForceFeedbackParameters ReadAsMe(uintptr_t address)
	{
		FForceFeedbackParameters ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FForceFeedbackParameters& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ForceFeedbackChannelDetails
// 0x0000
struct FForceFeedbackChannelDetails
{


	static FForceFeedbackChannelDetails ReadAsMe(uintptr_t address)
	{
		FForceFeedbackChannelDetails ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FForceFeedbackChannelDetails& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ForceFeedbackAttenuationSettings
// 0x5A36F040
struct FForceFeedbackAttenuationSettings
{
	unsigned char                                      UnknownData00[0x5A36F040];                                // 0x0000(0x5A36F040) MISSED OFFSET

	static FForceFeedbackAttenuationSettings ReadAsMe(uintptr_t address)
	{
		FForceFeedbackAttenuationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FForceFeedbackAttenuationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PredictProjectilePathResult
// 0x0000
struct FPredictProjectilePathResult
{


	static FPredictProjectilePathResult ReadAsMe(uintptr_t address)
	{
		FPredictProjectilePathResult ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPredictProjectilePathResult& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PredictProjectilePathPointData
// 0x0000
struct FPredictProjectilePathPointData
{


	static FPredictProjectilePathPointData ReadAsMe(uintptr_t address)
	{
		FPredictProjectilePathPointData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPredictProjectilePathPointData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PredictProjectilePathParams
// 0x0000
struct FPredictProjectilePathParams
{


	static FPredictProjectilePathParams ReadAsMe(uintptr_t address)
	{
		FPredictProjectilePathParams ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPredictProjectilePathParams& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ActiveHapticFeedbackEffect
// 0x0000
struct FActiveHapticFeedbackEffect
{


	static FActiveHapticFeedbackEffect ReadAsMe(uintptr_t address)
	{
		FActiveHapticFeedbackEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FActiveHapticFeedbackEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ClusterNode
// 0x0000
struct FClusterNode
{


	static FClusterNode ReadAsMe(uintptr_t address)
	{
		FClusterNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FClusterNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.HapticFeedbackDetails_Curve
// 0x0000
struct FHapticFeedbackDetails_Curve
{


	static FHapticFeedbackDetails_Curve ReadAsMe(uintptr_t address)
	{
		FHapticFeedbackDetails_Curve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FHapticFeedbackDetails_Curve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ClusterNode_DEPRECATED
// 0x0000
struct FClusterNode_DEPRECATED
{


	static FClusterNode_DEPRECATED ReadAsMe(uintptr_t address)
	{
		FClusterNode_DEPRECATED ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FClusterNode_DEPRECATED& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.HLODProxyMesh
// 0x0000
struct FHLODProxyMesh
{


	static FHLODProxyMesh ReadAsMe(uintptr_t address)
	{
		FHLODProxyMesh ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FHLODProxyMesh& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ImportanceTexture
// 0x0000
struct FImportanceTexture
{


	static FImportanceTexture ReadAsMe(uintptr_t address)
	{
		FImportanceTexture ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FImportanceTexture& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ComponentKey
// 0x0000
struct FComponentKey
{


	static FComponentKey ReadAsMe(uintptr_t address)
	{
		FComponentKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FComponentKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ComponentOverrideRecord
// 0x0000
struct FComponentOverrideRecord
{


	static FComponentOverrideRecord ReadAsMe(uintptr_t address)
	{
		FComponentOverrideRecord ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FComponentOverrideRecord& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintInputDelegateBinding
// 0x0000
struct FBlueprintInputDelegateBinding
{


	static FBlueprintInputDelegateBinding ReadAsMe(uintptr_t address)
	{
		FBlueprintInputDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintInputDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintInputActionDelegateBinding
// 0x5A3D7180
struct FBlueprintInputActionDelegateBinding
{
	unsigned char                                      UnknownData00[0x5A3D7180];                                // 0x0000(0x5A3D7180) MISSED OFFSET

	static FBlueprintInputActionDelegateBinding ReadAsMe(uintptr_t address)
	{
		FBlueprintInputActionDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintInputActionDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintInputAxisDelegateBinding
// 0x5A3D7180
struct FBlueprintInputAxisDelegateBinding
{
	unsigned char                                      UnknownData00[0x5A3D7180];                                // 0x0000(0x5A3D7180) MISSED OFFSET

	static FBlueprintInputAxisDelegateBinding ReadAsMe(uintptr_t address)
	{
		FBlueprintInputAxisDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintInputAxisDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintInputAxisKeyDelegateBinding
// 0x5A3D7180
struct FBlueprintInputAxisKeyDelegateBinding
{
	unsigned char                                      UnknownData00[0x5A3D7180];                                // 0x0000(0x5A3D7180) MISSED OFFSET

	static FBlueprintInputAxisKeyDelegateBinding ReadAsMe(uintptr_t address)
	{
		FBlueprintInputAxisKeyDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintInputAxisKeyDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintInputKeyDelegateBinding
// 0x5A3D7180
struct FBlueprintInputKeyDelegateBinding
{
	unsigned char                                      UnknownData00[0x5A3D7180];                                // 0x0000(0x5A3D7180) MISSED OFFSET

	static FBlueprintInputKeyDelegateBinding ReadAsMe(uintptr_t address)
	{
		FBlueprintInputKeyDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintInputKeyDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CachedKeyToActionInfo
// 0x0000
struct FCachedKeyToActionInfo
{


	static FCachedKeyToActionInfo ReadAsMe(uintptr_t address)
	{
		FCachedKeyToActionInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCachedKeyToActionInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BlueprintInputTouchDelegateBinding
// 0x5A3D7180
struct FBlueprintInputTouchDelegateBinding
{
	unsigned char                                      UnknownData00[0x5A3D7180];                                // 0x0000(0x5A3D7180) MISSED OFFSET

	static FBlueprintInputTouchDelegateBinding ReadAsMe(uintptr_t address)
	{
		FBlueprintInputTouchDelegateBinding ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBlueprintInputTouchDelegateBinding& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InstancedStaticMeshComponentInstanceData
// 0x5A367480
struct FInstancedStaticMeshComponentInstanceData
{
	unsigned char                                      UnknownData00[0x5A367480];                                // 0x0000(0x5A367480) MISSED OFFSET

	static FInstancedStaticMeshComponentInstanceData ReadAsMe(uintptr_t address)
	{
		FInstancedStaticMeshComponentInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInstancedStaticMeshComponentInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InstancedStaticMeshInstanceData
// 0x0000
struct FInstancedStaticMeshInstanceData
{


	static FInstancedStaticMeshInstanceData ReadAsMe(uintptr_t address)
	{
		FInstancedStaticMeshInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInstancedStaticMeshInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.IntegralCurve
// 0x5A311780
struct FIntegralCurve
{
	unsigned char                                      UnknownData00[0x5A311780];                                // 0x0000(0x5A311780) MISSED OFFSET

	static FIntegralCurve ReadAsMe(uintptr_t address)
	{
		FIntegralCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FIntegralCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InstancedStaticMeshLightMapInstanceData
// 0x0000
struct FInstancedStaticMeshLightMapInstanceData
{


	static FInstancedStaticMeshLightMapInstanceData ReadAsMe(uintptr_t address)
	{
		FInstancedStaticMeshLightMapInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInstancedStaticMeshLightMapInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.IntegralKey
// 0x0000
struct FIntegralKey
{


	static FIntegralKey ReadAsMe(uintptr_t address)
	{
		FIntegralKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FIntegralKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CurveEdTab
// 0x0000
struct FCurveEdTab
{


	static FCurveEdTab ReadAsMe(uintptr_t address)
	{
		FCurveEdTab ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCurveEdTab& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InstancedStaticMeshMappingInfo
// 0x0000
struct FInstancedStaticMeshMappingInfo
{


	static FInstancedStaticMeshMappingInfo ReadAsMe(uintptr_t address)
	{
		FInstancedStaticMeshMappingInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInstancedStaticMeshMappingInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CurveEdEntry
// 0x0000
struct FCurveEdEntry
{


	static FCurveEdEntry ReadAsMe(uintptr_t address)
	{
		FCurveEdEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCurveEdEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InterpEdSelKey
// 0x0000
struct FInterpEdSelKey
{


	static FInterpEdSelKey ReadAsMe(uintptr_t address)
	{
		FInterpEdSelKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpEdSelKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SubTrackGroup
// 0x0000
struct FSubTrackGroup
{


	static FSubTrackGroup ReadAsMe(uintptr_t address)
	{
		FSubTrackGroup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSubTrackGroup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SupportedSubTrackInfo
// 0x0000
struct FSupportedSubTrackInfo
{


	static FSupportedSubTrackInfo ReadAsMe(uintptr_t address)
	{
		FSupportedSubTrackInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSupportedSubTrackInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CameraPreviewInfo
// 0x0000
struct FCameraPreviewInfo
{


	static FCameraPreviewInfo ReadAsMe(uintptr_t address)
	{
		FCameraPreviewInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCameraPreviewInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimControlTrackKey
// 0x0000
struct FAnimControlTrackKey
{


	static FAnimControlTrackKey ReadAsMe(uintptr_t address)
	{
		FAnimControlTrackKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimControlTrackKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BoolTrackKey
// 0x0000
struct FBoolTrackKey
{


	static FBoolTrackKey ReadAsMe(uintptr_t address)
	{
		FBoolTrackKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBoolTrackKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DirectorTrackCut
// 0x0000
struct FDirectorTrackCut
{


	static FDirectorTrackCut ReadAsMe(uintptr_t address)
	{
		FDirectorTrackCut ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDirectorTrackCut& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EventTrackKey
// 0x0000
struct FEventTrackKey
{


	static FEventTrackKey ReadAsMe(uintptr_t address)
	{
		FEventTrackKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEventTrackKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InterpLookupTrack
// 0x0000
struct FInterpLookupTrack
{


	static FInterpLookupTrack ReadAsMe(uintptr_t address)
	{
		FInterpLookupTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpLookupTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InterpLookupPoint
// 0x0000
struct FInterpLookupPoint
{


	static FInterpLookupPoint ReadAsMe(uintptr_t address)
	{
		FInterpLookupPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpLookupPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleReplayTrackKey
// 0x0000
struct FParticleReplayTrackKey
{


	static FParticleReplayTrackKey ReadAsMe(uintptr_t address)
	{
		FParticleReplayTrackKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleReplayTrackKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundTrackKey
// 0x0000
struct FSoundTrackKey
{


	static FSoundTrackKey ReadAsMe(uintptr_t address)
	{
		FSoundTrackKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundTrackKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ToggleTrackKey
// 0x0000
struct FToggleTrackKey
{


	static FToggleTrackKey ReadAsMe(uintptr_t address)
	{
		FToggleTrackKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FToggleTrackKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VisibilityTrackKey
// 0x0000
struct FVisibilityTrackKey
{


	static FVisibilityTrackKey ReadAsMe(uintptr_t address)
	{
		FVisibilityTrackKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVisibilityTrackKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VectorSpringState
// 0x0000
struct FVectorSpringState
{


	static FVectorSpringState ReadAsMe(uintptr_t address)
	{
		FVectorSpringState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVectorSpringState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FloatSpringState
// 0x0000
struct FFloatSpringState
{


	static FFloatSpringState ReadAsMe(uintptr_t address)
	{
		FFloatSpringState ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFloatSpringState& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DrawToRenderTargetContext
// 0x0000
struct FDrawToRenderTargetContext
{


	static FDrawToRenderTargetContext ReadAsMe(uintptr_t address)
	{
		FDrawToRenderTargetContext ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDrawToRenderTargetContext& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LatentActionManager
// 0x0000
struct FLatentActionManager
{


	static FLatentActionManager ReadAsMe(uintptr_t address)
	{
		FLatentActionManager ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLatentActionManager& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LayerActorStats
// 0x0000
struct FLayerActorStats
{


	static FLayerActorStats ReadAsMe(uintptr_t address)
	{
		FLayerActorStats ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLayerActorStats& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LevelSimplificationDetails
// 0x0000
struct FLevelSimplificationDetails
{


	static FLevelSimplificationDetails ReadAsMe(uintptr_t address)
	{
		FLevelSimplificationDetails ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLevelSimplificationDetails& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ReplicatedStaticActorDestructionInfo
// 0x0000
struct FReplicatedStaticActorDestructionInfo
{


	static FReplicatedStaticActorDestructionInfo ReadAsMe(uintptr_t address)
	{
		FReplicatedStaticActorDestructionInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FReplicatedStaticActorDestructionInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialProxySettings
// 0x0000
struct FMaterialProxySettings
{


	static FMaterialProxySettings ReadAsMe(uintptr_t address)
	{
		FMaterialProxySettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialProxySettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DynamicTextureInstance
// 0x5A3D8740
struct FDynamicTextureInstance
{
	unsigned char                                      UnknownData00[0x5A3D8740];                                // 0x0000(0x5A3D8740) MISSED OFFSET

	static FDynamicTextureInstance ReadAsMe(uintptr_t address)
	{
		FDynamicTextureInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDynamicTextureInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StreamableTextureInstance
// 0x0000
struct FStreamableTextureInstance
{


	static FStreamableTextureInstance ReadAsMe(uintptr_t address)
	{
		FStreamableTextureInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStreamableTextureInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BatchedPoint
// 0x0000
struct FBatchedPoint
{


	static FBatchedPoint ReadAsMe(uintptr_t address)
	{
		FBatchedPoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBatchedPoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrecomputedLightInstanceData
// 0x5A367480
struct FPrecomputedLightInstanceData
{
	unsigned char                                      UnknownData00[0x5A367480];                                // 0x0000(0x5A367480) MISSED OFFSET

	static FPrecomputedLightInstanceData ReadAsMe(uintptr_t address)
	{
		FPrecomputedLightInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrecomputedLightInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BatchedLine
// 0x0000
struct FBatchedLine
{


	static FBatchedLine ReadAsMe(uintptr_t address)
	{
		FBatchedLine ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBatchedLine& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ClientReceiveData
// 0x0000
struct FClientReceiveData
{


	static FClientReceiveData ReadAsMe(uintptr_t address)
	{
		FClientReceiveData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FClientReceiveData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParameterGroupData
// 0x0000
struct FParameterGroupData
{


	static FParameterGroupData ReadAsMe(uintptr_t address)
	{
		FParameterGroupData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParameterGroupData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialParameterCollectionInfo
// 0x0000
struct FMaterialParameterCollectionInfo
{


	static FMaterialParameterCollectionInfo ReadAsMe(uintptr_t address)
	{
		FMaterialParameterCollectionInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialParameterCollectionInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialFunctionInfo
// 0x0000
struct FMaterialFunctionInfo
{


	static FMaterialFunctionInfo ReadAsMe(uintptr_t address)
	{
		FMaterialFunctionInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialFunctionInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialSpriteElement
// 0x0000
struct FMaterialSpriteElement
{


	static FMaterialSpriteElement ReadAsMe(uintptr_t address)
	{
		FMaterialSpriteElement ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialSpriteElement& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CustomInput
// 0x0000
struct FCustomInput
{


	static FCustomInput ReadAsMe(uintptr_t address)
	{
		FCustomInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCustomInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FunctionExpressionOutput
// 0x0000
struct FFunctionExpressionOutput
{


	static FFunctionExpressionOutput ReadAsMe(uintptr_t address)
	{
		FFunctionExpressionOutput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFunctionExpressionOutput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FontParameterValue
// 0x0000
struct FFontParameterValue
{


	static FFontParameterValue ReadAsMe(uintptr_t address)
	{
		FFontParameterValue ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFontParameterValue& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FunctionExpressionInput
// 0x0000
struct FFunctionExpressionInput
{


	static FFunctionExpressionInput ReadAsMe(uintptr_t address)
	{
		FFunctionExpressionInput ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFunctionExpressionInput& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialParameterInfo
// 0x0000
struct FMaterialParameterInfo
{


	static FMaterialParameterInfo ReadAsMe(uintptr_t address)
	{
		FMaterialParameterInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialParameterInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TextureParameterValue
// 0x0000
struct FTextureParameterValue
{


	static FTextureParameterValue ReadAsMe(uintptr_t address)
	{
		FTextureParameterValue ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTextureParameterValue& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VectorParameterValue
// 0x0000
struct FVectorParameterValue
{


	static FVectorParameterValue ReadAsMe(uintptr_t address)
	{
		FVectorParameterValue ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVectorParameterValue& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ScalarParameterValue
// 0x0000
struct FScalarParameterValue
{


	static FScalarParameterValue ReadAsMe(uintptr_t address)
	{
		FScalarParameterValue ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FScalarParameterValue& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ScalarParameterAtlasInstanceData
// 0x0000
struct FScalarParameterAtlasInstanceData
{


	static FScalarParameterAtlasInstanceData ReadAsMe(uintptr_t address)
	{
		FScalarParameterAtlasInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FScalarParameterAtlasInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialInstanceBasePropertyOverrides
// 0x0000
struct FMaterialInstanceBasePropertyOverrides
{


	static FMaterialInstanceBasePropertyOverrides ReadAsMe(uintptr_t address)
	{
		FMaterialInstanceBasePropertyOverrides ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialInstanceBasePropertyOverrides& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LightmassMaterialInterfaceSettings
// 0x0000
struct FLightmassMaterialInterfaceSettings
{


	static FLightmassMaterialInterfaceSettings ReadAsMe(uintptr_t address)
	{
		FLightmassMaterialInterfaceSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLightmassMaterialInterfaceSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialTextureInfo
// 0x0000
struct FMaterialTextureInfo
{


	static FMaterialTextureInfo ReadAsMe(uintptr_t address)
	{
		FMaterialTextureInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialTextureInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialLayersFunctions
// 0x0000
struct FMaterialLayersFunctions
{


	static FMaterialLayersFunctions ReadAsMe(uintptr_t address)
	{
		FMaterialLayersFunctions ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialLayersFunctions& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollectionParameterBase
// 0x0000
struct FCollectionParameterBase
{


	static FCollectionParameterBase ReadAsMe(uintptr_t address)
	{
		FCollectionParameterBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollectionParameterBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollectionVectorParameter
// 0x5A3DA780
struct FCollectionVectorParameter
{
	unsigned char                                      UnknownData00[0x5A3DA780];                                // 0x0000(0x5A3DA780) MISSED OFFSET

	static FCollectionVectorParameter ReadAsMe(uintptr_t address)
	{
		FCollectionVectorParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollectionVectorParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CollectionScalarParameter
// 0x5A3DA780
struct FCollectionScalarParameter
{
	unsigned char                                      UnknownData00[0x5A3DA780];                                // 0x0000(0x5A3DA780) MISSED OFFSET

	static FCollectionScalarParameter ReadAsMe(uintptr_t address)
	{
		FCollectionScalarParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCollectionScalarParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InterpGroupActorInfo
// 0x0000
struct FInterpGroupActorInfo
{


	static FInterpGroupActorInfo ReadAsMe(uintptr_t address)
	{
		FInterpGroupActorInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInterpGroupActorInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CameraCutInfo
// 0x0000
struct FCameraCutInfo
{


	static FCameraCutInfo ReadAsMe(uintptr_t address)
	{
		FCameraCutInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCameraCutInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MemberReference
// 0x0000
struct FMemberReference
{


	static FMemberReference ReadAsMe(uintptr_t address)
	{
		FMemberReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMemberReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MeshInstancingSettings
// 0x0000
struct FMeshInstancingSettings
{


	static FMeshInstancingSettings ReadAsMe(uintptr_t address)
	{
		FMeshInstancingSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMeshInstancingSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MeshMergingSettings
// 0x0000
struct FMeshMergingSettings
{


	static FMeshMergingSettings ReadAsMe(uintptr_t address)
	{
		FMeshMergingSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMeshMergingSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MeshProxySettings
// 0x0000
struct FMeshProxySettings
{


	static FMeshProxySettings ReadAsMe(uintptr_t address)
	{
		FMeshProxySettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMeshProxySettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MeshReductionSettings
// 0x0000
struct FMeshReductionSettings
{


	static FMeshReductionSettings ReadAsMe(uintptr_t address)
	{
		FMeshReductionSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMeshReductionSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PurchaseInfo
// 0x0000
struct FPurchaseInfo
{


	static FPurchaseInfo ReadAsMe(uintptr_t address)
	{
		FPurchaseInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPurchaseInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NameCurve
// 0x5A311780
struct FNameCurve
{
	unsigned char                                      UnknownData00[0x5A311780];                                // 0x0000(0x5A311780) MISSED OFFSET

	static FNameCurve ReadAsMe(uintptr_t address)
	{
		FNameCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNameCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NameCurveKey
// 0x0000
struct FNameCurveKey
{


	static FNameCurveKey ReadAsMe(uintptr_t address)
	{
		FNameCurveKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNameCurveKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NavAvoidanceMask
// 0x0000
struct FNavAvoidanceMask
{


	static FNavAvoidanceMask ReadAsMe(uintptr_t address)
	{
		FNavAvoidanceMask ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNavAvoidanceMask& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MovementProperties
// 0x0000
struct FMovementProperties
{


	static FMovementProperties ReadAsMe(uintptr_t address)
	{
		FMovementProperties ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMovementProperties& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NavAgentProperties
// 0x5A3D9D00
struct FNavAgentProperties
{
	unsigned char                                      UnknownData00[0x5A3D9D00];                                // 0x0000(0x5A3D9D00) MISSED OFFSET

	static FNavAgentProperties ReadAsMe(uintptr_t address)
	{
		FNavAgentProperties ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNavAgentProperties& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NavDataConfig
// 0x5A3D9C40
struct FNavDataConfig
{
	unsigned char                                      UnknownData00[0x5A3D9C40];                                // 0x0000(0x5A3D9C40) MISSED OFFSET

	static FNavDataConfig ReadAsMe(uintptr_t address)
	{
		FNavDataConfig ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNavDataConfig& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NavAgentSelector
// 0x0000
struct FNavAgentSelector
{


	static FNavAgentSelector ReadAsMe(uintptr_t address)
	{
		FNavAgentSelector ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNavAgentSelector& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NavigationLinkBase
// 0x0000
struct FNavigationLinkBase
{


	static FNavigationLinkBase ReadAsMe(uintptr_t address)
	{
		FNavigationLinkBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNavigationLinkBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NavigationSegmentLink
// 0x5A3D9A00
struct FNavigationSegmentLink
{
	unsigned char                                      UnknownData00[0x5A3D9A00];                                // 0x0000(0x5A3D9A00) MISSED OFFSET

	static FNavigationSegmentLink ReadAsMe(uintptr_t address)
	{
		FNavigationSegmentLink ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNavigationSegmentLink& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NavigationLink
// 0x5A3D9A00
struct FNavigationLink
{
	unsigned char                                      UnknownData00[0x5A3D9A00];                                // 0x0000(0x5A3D9A00) MISSED OFFSET

	static FNavigationLink ReadAsMe(uintptr_t address)
	{
		FNavigationLink ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNavigationLink& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ChannelDefinition
// 0x0000
struct FChannelDefinition
{


	static FChannelDefinition ReadAsMe(uintptr_t address)
	{
		FChannelDefinition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FChannelDefinition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PacketSimulationSettings
// 0x0000
struct FPacketSimulationSettings
{


	static FPacketSimulationSettings ReadAsMe(uintptr_t address)
	{
		FPacketSimulationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPacketSimulationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NodeItem
// 0x0000
struct FNodeItem
{


	static FNodeItem ReadAsMe(uintptr_t address)
	{
		FNodeItem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNodeItem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleBurst
// 0x0000
struct FParticleBurst
{


	static FParticleBurst ReadAsMe(uintptr_t address)
	{
		FParticleBurst ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleBurst& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleRandomSeedInfo
// 0x0000
struct FParticleRandomSeedInfo
{


	static FParticleRandomSeedInfo ReadAsMe(uintptr_t address)
	{
		FParticleRandomSeedInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleRandomSeedInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleEvent_GenerateInfo
// 0x0000
struct FParticleEvent_GenerateInfo
{


	static FParticleEvent_GenerateInfo ReadAsMe(uintptr_t address)
	{
		FParticleEvent_GenerateInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleEvent_GenerateInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BeamModifierOptions
// 0x0000
struct FBeamModifierOptions
{


	static FBeamModifierOptions ReadAsMe(uintptr_t address)
	{
		FBeamModifierOptions ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBeamModifierOptions& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.OrbitOptions
// 0x0000
struct FOrbitOptions
{


	static FOrbitOptions ReadAsMe(uintptr_t address)
	{
		FOrbitOptions ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FOrbitOptions& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EmitterDynamicParameter
// 0x0000
struct FEmitterDynamicParameter
{


	static FEmitterDynamicParameter ReadAsMe(uintptr_t address)
	{
		FEmitterDynamicParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEmitterDynamicParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LocationBoneSocketInfo
// 0x0000
struct FLocationBoneSocketInfo
{


	static FLocationBoneSocketInfo ReadAsMe(uintptr_t address)
	{
		FLocationBoneSocketInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLocationBoneSocketInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BeamTargetData
// 0x0000
struct FBeamTargetData
{


	static FBeamTargetData ReadAsMe(uintptr_t address)
	{
		FBeamTargetData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBeamTargetData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GPUSpriteResourceData
// 0x0000
struct FGPUSpriteResourceData
{


	static FGPUSpriteResourceData ReadAsMe(uintptr_t address)
	{
		FGPUSpriteResourceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPUSpriteResourceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleCurvePair
// 0x0000
struct FParticleCurvePair
{


	static FParticleCurvePair ReadAsMe(uintptr_t address)
	{
		FParticleCurvePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleCurvePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GPUSpriteEmitterInfo
// 0x0000
struct FGPUSpriteEmitterInfo
{


	static FGPUSpriteEmitterInfo ReadAsMe(uintptr_t address)
	{
		FGPUSpriteEmitterInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPUSpriteEmitterInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GPUSpriteLocalVectorFieldInfo
// 0x0000
struct FGPUSpriteLocalVectorFieldInfo
{


	static FGPUSpriteLocalVectorFieldInfo ReadAsMe(uintptr_t address)
	{
		FGPUSpriteLocalVectorFieldInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGPUSpriteLocalVectorFieldInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NamedEmitterMaterial
// 0x0000
struct FNamedEmitterMaterial
{


	static FNamedEmitterMaterial ReadAsMe(uintptr_t address)
	{
		FNamedEmitterMaterial ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNamedEmitterMaterial& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LODSoloTrack
// 0x0000
struct FLODSoloTrack
{


	static FLODSoloTrack ReadAsMe(uintptr_t address)
	{
		FLODSoloTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLODSoloTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleSysParam
// 0x0000
struct FParticleSysParam
{


	static FParticleSysParam ReadAsMe(uintptr_t address)
	{
		FParticleSysParam ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleSysParam& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleSystemLOD
// 0x0000
struct FParticleSystemLOD
{


	static FParticleSystemLOD ReadAsMe(uintptr_t address)
	{
		FParticleSystemLOD ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleSystemLOD& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleSystemWorldManagerTickFunction
// 0x5A123C40
struct FParticleSystemWorldManagerTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FParticleSystemWorldManagerTickFunction ReadAsMe(uintptr_t address)
	{
		FParticleSystemWorldManagerTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleSystemWorldManagerTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleSystemReplayFrame
// 0x0000
struct FParticleSystemReplayFrame
{


	static FParticleSystemReplayFrame ReadAsMe(uintptr_t address)
	{
		FParticleSystemReplayFrame ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleSystemReplayFrame& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ParticleEmitterReplayFrame
// 0x0000
struct FParticleEmitterReplayFrame
{


	static FParticleEmitterReplayFrame ReadAsMe(uintptr_t address)
	{
		FParticleEmitterReplayFrame ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FParticleEmitterReplayFrame& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PhysicalAnimationData
// 0x0000
struct FPhysicalAnimationData
{


	static FPhysicalAnimationData ReadAsMe(uintptr_t address)
	{
		FPhysicalAnimationData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPhysicalAnimationData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TireFrictionScalePair
// 0x0000
struct FTireFrictionScalePair
{


	static FTireFrictionScalePair ReadAsMe(uintptr_t address)
	{
		FTireFrictionScalePair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTireFrictionScalePair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PhysicalAnimationProfile
// 0x0000
struct FPhysicalAnimationProfile
{


	static FPhysicalAnimationProfile ReadAsMe(uintptr_t address)
	{
		FPhysicalAnimationProfile ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPhysicalAnimationProfile& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PhysicsConstraintProfileHandle
// 0x0000
struct FPhysicsConstraintProfileHandle
{


	static FPhysicsConstraintProfileHandle ReadAsMe(uintptr_t address)
	{
		FPhysicsConstraintProfileHandle ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPhysicsConstraintProfileHandle& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PhysicalSurfaceName
// 0x0000
struct FPhysicalSurfaceName
{


	static FPhysicalSurfaceName ReadAsMe(uintptr_t address)
	{
		FPhysicalSurfaceName ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPhysicalSurfaceName& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ViewTargetTransitionParams
// 0x0000
struct FViewTargetTransitionParams
{


	static FViewTargetTransitionParams ReadAsMe(uintptr_t address)
	{
		FViewTargetTransitionParams ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FViewTargetTransitionParams& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TViewTarget
// 0x0000
struct FTViewTarget
{


	static FTViewTarget ReadAsMe(uintptr_t address)
	{
		FTViewTarget ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTViewTarget& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CameraCacheEntry
// 0x0000
struct FCameraCacheEntry
{


	static FCameraCacheEntry ReadAsMe(uintptr_t address)
	{
		FCameraCacheEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCameraCacheEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DelegateArray
// 0x0000
struct FDelegateArray
{


	static FDelegateArray ReadAsMe(uintptr_t address)
	{
		FDelegateArray ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDelegateArray& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.UpdateLevelVisibilityLevelInfo
// 0x0000
struct FUpdateLevelVisibilityLevelInfo
{


	static FUpdateLevelVisibilityLevelInfo ReadAsMe(uintptr_t address)
	{
		FUpdateLevelVisibilityLevelInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FUpdateLevelVisibilityLevelInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputAxisConfigEntry
// 0x0000
struct FInputAxisConfigEntry
{


	static FInputAxisConfigEntry ReadAsMe(uintptr_t address)
	{
		FInputAxisConfigEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputAxisConfigEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.InputAxisProperties
// 0x0000
struct FInputAxisProperties
{


	static FInputAxisProperties ReadAsMe(uintptr_t address)
	{
		FInputAxisProperties ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FInputAxisProperties& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.UpdateLevelStreamingLevelStatus
// 0x0000
struct FUpdateLevelStreamingLevelStatus
{


	static FUpdateLevelStreamingLevelStatus ReadAsMe(uintptr_t address)
	{
		FUpdateLevelStreamingLevelStatus ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FUpdateLevelStreamingLevelStatus& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.KeyBind
// 0x0000
struct FKeyBind
{


	static FKeyBind ReadAsMe(uintptr_t address)
	{
		FKeyBind ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FKeyBind& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PlayerMuteList
// 0x0000
struct FPlayerMuteList
{


	static FPlayerMuteList ReadAsMe(uintptr_t address)
	{
		FPlayerMuteList ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPlayerMuteList& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PoseDataContainer
// 0x0000
struct FPoseDataContainer
{


	static FPoseDataContainer ReadAsMe(uintptr_t address)
	{
		FPoseDataContainer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPoseDataContainer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PoseData
// 0x0000
struct FPoseData
{


	static FPoseData ReadAsMe(uintptr_t address)
	{
		FPoseData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPoseData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PreviewAssetAttachContainer
// 0x0000
struct FPreviewAssetAttachContainer
{


	static FPreviewAssetAttachContainer ReadAsMe(uintptr_t address)
	{
		FPreviewAssetAttachContainer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPreviewAssetAttachContainer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PreviewMeshCollectionEntry
// 0x0000
struct FPreviewMeshCollectionEntry
{


	static FPreviewMeshCollectionEntry ReadAsMe(uintptr_t address)
	{
		FPreviewMeshCollectionEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPreviewMeshCollectionEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PreviewAttachedObjectPair
// 0x0000
struct FPreviewAttachedObjectPair
{


	static FPreviewAttachedObjectPair ReadAsMe(uintptr_t address)
	{
		FPreviewAttachedObjectPair ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPreviewAttachedObjectPair& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrimitiveComponentInstanceData
// 0x5A367480
struct FPrimitiveComponentInstanceData
{
	unsigned char                                      UnknownData00[0x5A367480];                                // 0x0000(0x5A367480) MISSED OFFSET

	static FPrimitiveComponentInstanceData ReadAsMe(uintptr_t address)
	{
		FPrimitiveComponentInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrimitiveComponentInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CompressedRichCurve
// 0x0000
struct FCompressedRichCurve
{


	static FCompressedRichCurve ReadAsMe(uintptr_t address)
	{
		FCompressedRichCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCompressedRichCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TransformBase
// 0x0000
struct FTransformBase
{


	static FTransformBase ReadAsMe(uintptr_t address)
	{
		FTransformBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTransformBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SpriteCategoryInfo
// 0x0000
struct FSpriteCategoryInfo
{


	static FSpriteCategoryInfo ReadAsMe(uintptr_t address)
	{
		FSpriteCategoryInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSpriteCategoryInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TransformBaseConstraint
// 0x0000
struct FTransformBaseConstraint
{


	static FTransformBaseConstraint ReadAsMe(uintptr_t address)
	{
		FTransformBaseConstraint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTransformBaseConstraint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Node
// 0x0000
struct FNode
{


	static FNode ReadAsMe(uintptr_t address)
	{
		FNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RigTransformConstraint
// 0x0000
struct FRigTransformConstraint
{


	static FRigTransformConstraint ReadAsMe(uintptr_t address)
	{
		FRigTransformConstraint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRigTransformConstraint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionFinishVelocitySettings
// 0x0000
struct FRootMotionFinishVelocitySettings
{


	static FRootMotionFinishVelocitySettings ReadAsMe(uintptr_t address)
	{
		FRootMotionFinishVelocitySettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionFinishVelocitySettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSource
// 0x0000
struct FRootMotionSource
{


	static FRootMotionSource ReadAsMe(uintptr_t address)
	{
		FRootMotionSource ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSource& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSourceStatus
// 0x0000
struct FRootMotionSourceStatus
{


	static FRootMotionSourceStatus ReadAsMe(uintptr_t address)
	{
		FRootMotionSourceStatus ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSourceStatus& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSource_JumpForce
// 0x5A3DD600
struct FRootMotionSource_JumpForce
{
	unsigned char                                      UnknownData00[0x5A3DD600];                                // 0x0000(0x5A3DD600) MISSED OFFSET

	static FRootMotionSource_JumpForce ReadAsMe(uintptr_t address)
	{
		FRootMotionSource_JumpForce ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSource_JumpForce& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSource_MoveToDynamicForce
// 0x5A3DD600
struct FRootMotionSource_MoveToDynamicForce
{
	unsigned char                                      UnknownData00[0x5A3DD600];                                // 0x0000(0x5A3DD600) MISSED OFFSET

	static FRootMotionSource_MoveToDynamicForce ReadAsMe(uintptr_t address)
	{
		FRootMotionSource_MoveToDynamicForce ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSource_MoveToDynamicForce& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSource_MoveToForce
// 0x5A3DD600
struct FRootMotionSource_MoveToForce
{
	unsigned char                                      UnknownData00[0x5A3DD600];                                // 0x0000(0x5A3DD600) MISSED OFFSET

	static FRootMotionSource_MoveToForce ReadAsMe(uintptr_t address)
	{
		FRootMotionSource_MoveToForce ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSource_MoveToForce& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSource_RadialForce
// 0x5A3DD600
struct FRootMotionSource_RadialForce
{
	unsigned char                                      UnknownData00[0x5A3DD600];                                // 0x0000(0x5A3DD600) MISSED OFFSET

	static FRootMotionSource_RadialForce ReadAsMe(uintptr_t address)
	{
		FRootMotionSource_RadialForce ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSource_RadialForce& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RootMotionSource_ConstantForce
// 0x5A3DD600
struct FRootMotionSource_ConstantForce
{
	unsigned char                                      UnknownData00[0x5A3DD600];                                // 0x0000(0x5A3DD600) MISSED OFFSET

	static FRootMotionSource_ConstantForce ReadAsMe(uintptr_t address)
	{
		FRootMotionSource_ConstantForce ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRootMotionSource_ConstantForce& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CameraExposureSettings
// 0x0000
struct FCameraExposureSettings
{


	static FCameraExposureSettings ReadAsMe(uintptr_t address)
	{
		FCameraExposureSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCameraExposureSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LensSettings
// 0x0000
struct FLensSettings
{


	static FLensSettings ReadAsMe(uintptr_t address)
	{
		FLensSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLensSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LensBloomSettings
// 0x0000
struct FLensBloomSettings
{


	static FLensBloomSettings ReadAsMe(uintptr_t address)
	{
		FLensBloomSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLensBloomSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ConvolutionBloomSettings
// 0x0000
struct FConvolutionBloomSettings
{


	static FConvolutionBloomSettings ReadAsMe(uintptr_t address)
	{
		FConvolutionBloomSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FConvolutionBloomSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LensImperfectionSettings
// 0x0000
struct FLensImperfectionSettings
{


	static FLensImperfectionSettings ReadAsMe(uintptr_t address)
	{
		FLensImperfectionSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLensImperfectionSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.GaussianSumBloomSettings
// 0x0000
struct FGaussianSumBloomSettings
{


	static FGaussianSumBloomSettings ReadAsMe(uintptr_t address)
	{
		FGaussianSumBloomSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FGaussianSumBloomSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.FilmStockSettings
// 0x0000
struct FFilmStockSettings
{


	static FFilmStockSettings ReadAsMe(uintptr_t address)
	{
		FFilmStockSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FFilmStockSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ColorGradingSettings
// 0x0000
struct FColorGradingSettings
{


	static FColorGradingSettings ReadAsMe(uintptr_t address)
	{
		FColorGradingSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FColorGradingSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ColorGradePerRangeSettings
// 0x0000
struct FColorGradePerRangeSettings
{


	static FColorGradePerRangeSettings ReadAsMe(uintptr_t address)
	{
		FColorGradePerRangeSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FColorGradePerRangeSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EngineShowFlagsSetting
// 0x0000
struct FEngineShowFlagsSetting
{


	static FEngineShowFlagsSetting ReadAsMe(uintptr_t address)
	{
		FEngineShowFlagsSetting ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEngineShowFlagsSetting& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SimpleCurve
// 0x5A311600
struct FSimpleCurve
{
	unsigned char                                      UnknownData00[0x5A311600];                                // 0x0000(0x5A311600) MISSED OFFSET

	static FSimpleCurve ReadAsMe(uintptr_t address)
	{
		FSimpleCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSimpleCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SimpleCurveKey
// 0x0000
struct FSimpleCurveKey
{


	static FSimpleCurveKey ReadAsMe(uintptr_t address)
	{
		FSimpleCurveKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSimpleCurveKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SingleAnimationPlayData
// 0x0000
struct FSingleAnimationPlayData
{


	static FSingleAnimationPlayData ReadAsMe(uintptr_t address)
	{
		FSingleAnimationPlayData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSingleAnimationPlayData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMaterial
// 0x0000
struct FSkeletalMaterial
{


	static FSkeletalMaterial ReadAsMe(uintptr_t address)
	{
		FSkeletalMaterial ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMaterial& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ClothingAssetData_Legacy
// 0x0000
struct FClothingAssetData_Legacy
{


	static FClothingAssetData_Legacy ReadAsMe(uintptr_t address)
	{
		FClothingAssetData_Legacy ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FClothingAssetData_Legacy& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ClothPhysicsProperties_Legacy
// 0x0000
struct FClothPhysicsProperties_Legacy
{


	static FClothPhysicsProperties_Legacy ReadAsMe(uintptr_t address)
	{
		FClothPhysicsProperties_Legacy ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FClothPhysicsProperties_Legacy& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshLODInfo
// 0x0000
struct FSkeletalMeshLODInfo
{


	static FSkeletalMeshLODInfo ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshLODInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshLODInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshOptimizationSettings
// 0x0000
struct FSkeletalMeshOptimizationSettings
{


	static FSkeletalMeshOptimizationSettings ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshOptimizationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshOptimizationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshClothBuildParams
// 0x0000
struct FSkeletalMeshClothBuildParams
{


	static FSkeletalMeshClothBuildParams ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshClothBuildParams ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshClothBuildParams& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BoneMirrorExport
// 0x0000
struct FBoneMirrorExport
{


	static FBoneMirrorExport ReadAsMe(uintptr_t address)
	{
		FBoneMirrorExport ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBoneMirrorExport& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshComponentClothTickFunction
// 0x5A123C40
struct FSkeletalMeshComponentClothTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FSkeletalMeshComponentClothTickFunction ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshComponentClothTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshComponentClothTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshComponentEndPhysicsTickFunction
// 0x5A123C40
struct FSkeletalMeshComponentEndPhysicsTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FSkeletalMeshComponentEndPhysicsTickFunction ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshComponentEndPhysicsTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshComponentEndPhysicsTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BoneMirrorInfo
// 0x0000
struct FBoneMirrorInfo
{


	static FBoneMirrorInfo ReadAsMe(uintptr_t address)
	{
		FBoneMirrorInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBoneMirrorInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshLODGroupSettings
// 0x0000
struct FSkeletalMeshLODGroupSettings
{


	static FSkeletalMeshLODGroupSettings ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshLODGroupSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshLODGroupSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BoneFilter
// 0x0000
struct FBoneFilter
{


	static FBoneFilter ReadAsMe(uintptr_t address)
	{
		FBoneFilter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBoneFilter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshSamplingInfo
// 0x0000
struct FSkeletalMeshSamplingInfo
{


	static FSkeletalMeshSamplingInfo ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshSamplingInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshSamplingInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshSamplingBuiltData
// 0x0000
struct FSkeletalMeshSamplingBuiltData
{


	static FSkeletalMeshSamplingBuiltData ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshSamplingBuiltData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshSamplingBuiltData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshSamplingRegionBuiltData
// 0x0000
struct FSkeletalMeshSamplingRegionBuiltData
{


	static FSkeletalMeshSamplingRegionBuiltData ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshSamplingRegionBuiltData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshSamplingRegionBuiltData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshSamplingLODBuiltData
// 0x0000
struct FSkeletalMeshSamplingLODBuiltData
{


	static FSkeletalMeshSamplingLODBuiltData ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshSamplingLODBuiltData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshSamplingLODBuiltData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshSamplingRegion
// 0x0000
struct FSkeletalMeshSamplingRegion
{


	static FSkeletalMeshSamplingRegion ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshSamplingRegion ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshSamplingRegion& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshSamplingRegionBoneFilter
// 0x0000
struct FSkeletalMeshSamplingRegionBoneFilter
{


	static FSkeletalMeshSamplingRegionBoneFilter ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshSamplingRegionBoneFilter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshSamplingRegionBoneFilter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletalMeshSamplingRegionMaterialFilter
// 0x0000
struct FSkeletalMeshSamplingRegionMaterialFilter
{


	static FSkeletalMeshSamplingRegionMaterialFilter ReadAsMe(uintptr_t address)
	{
		FSkeletalMeshSamplingRegionMaterialFilter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletalMeshSamplingRegionMaterialFilter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VirtualBone
// 0x0000
struct FVirtualBone
{


	static FVirtualBone ReadAsMe(uintptr_t address)
	{
		FVirtualBone ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVirtualBone& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AnimSlotGroup
// 0x0000
struct FAnimSlotGroup
{


	static FAnimSlotGroup ReadAsMe(uintptr_t address)
	{
		FAnimSlotGroup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAnimSlotGroup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NameMapping
// 0x0000
struct FNameMapping
{


	static FNameMapping ReadAsMe(uintptr_t address)
	{
		FNameMapping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNameMapping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BoneReductionSetting
// 0x0000
struct FBoneReductionSetting
{


	static FBoneReductionSetting ReadAsMe(uintptr_t address)
	{
		FBoneReductionSetting ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBoneReductionSetting& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.RigConfiguration
// 0x0000
struct FRigConfiguration
{


	static FRigConfiguration ReadAsMe(uintptr_t address)
	{
		FRigConfiguration ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FRigConfiguration& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ReferencePose
// 0x0000
struct FReferencePose
{


	static FReferencePose ReadAsMe(uintptr_t address)
	{
		FReferencePose ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FReferencePose& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkeletonToMeshLinkup
// 0x0000
struct FSkeletonToMeshLinkup
{


	static FSkeletonToMeshLinkup ReadAsMe(uintptr_t address)
	{
		FSkeletonToMeshLinkup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkeletonToMeshLinkup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkelMeshComponentLODInfo
// 0x0000
struct FSkelMeshComponentLODInfo
{


	static FSkelMeshComponentLODInfo ReadAsMe(uintptr_t address)
	{
		FSkelMeshComponentLODInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkelMeshComponentLODInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SkelMeshSkinWeightInfo
// 0x0000
struct FSkelMeshSkinWeightInfo
{


	static FSkelMeshSkinWeightInfo ReadAsMe(uintptr_t address)
	{
		FSkelMeshSkinWeightInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSkelMeshSkinWeightInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PrecomputedSkyLightInstanceData
// 0x5A367480
struct FPrecomputedSkyLightInstanceData
{
	unsigned char                                      UnknownData00[0x5A367480];                                // 0x0000(0x5A367480) MISSED OFFSET

	static FPrecomputedSkyLightInstanceData ReadAsMe(uintptr_t address)
	{
		FPrecomputedSkyLightInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPrecomputedSkyLightInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SmartNameMapping
// 0x0000
struct FSmartNameMapping
{


	static FSmartNameMapping ReadAsMe(uintptr_t address)
	{
		FSmartNameMapping ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSmartNameMapping& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.CurveMetaData
// 0x0000
struct FCurveMetaData
{


	static FCurveMetaData ReadAsMe(uintptr_t address)
	{
		FCurveMetaData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FCurveMetaData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SmartNameContainer
// 0x0000
struct FSmartNameContainer
{


	static FSmartNameContainer ReadAsMe(uintptr_t address)
	{
		FSmartNameContainer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSmartNameContainer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundAttenuationSettings
// 0x5A36F040
struct FSoundAttenuationSettings
{
	unsigned char                                      UnknownData00[0x5A36F040];                                // 0x0000(0x5A36F040) MISSED OFFSET

	static FSoundAttenuationSettings ReadAsMe(uintptr_t address)
	{
		FSoundAttenuationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundAttenuationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundAttenuationPluginSettings
// 0x0000
struct FSoundAttenuationPluginSettings
{


	static FSoundAttenuationPluginSettings ReadAsMe(uintptr_t address)
	{
		FSoundAttenuationPluginSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundAttenuationPluginSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BoneNode
// 0x0000
struct FBoneNode
{


	static FBoneNode ReadAsMe(uintptr_t address)
	{
		FBoneNode ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBoneNode& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PassiveSoundMixModifier
// 0x0000
struct FPassiveSoundMixModifier
{


	static FPassiveSoundMixModifier ReadAsMe(uintptr_t address)
	{
		FPassiveSoundMixModifier ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPassiveSoundMixModifier& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundClassProperties
// 0x0000
struct FSoundClassProperties
{


	static FSoundClassProperties ReadAsMe(uintptr_t address)
	{
		FSoundClassProperties ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundClassProperties& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundClassEditorData
// 0x0000
struct FSoundClassEditorData
{


	static FSoundClassEditorData ReadAsMe(uintptr_t address)
	{
		FSoundClassEditorData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundClassEditorData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundConcurrencySettings
// 0x0000
struct FSoundConcurrencySettings
{


	static FSoundConcurrencySettings ReadAsMe(uintptr_t address)
	{
		FSoundConcurrencySettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundConcurrencySettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SourceEffectChainEntry
// 0x0000
struct FSourceEffectChainEntry
{


	static FSourceEffectChainEntry ReadAsMe(uintptr_t address)
	{
		FSourceEffectChainEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSourceEffectChainEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundNodeEditorData
// 0x0000
struct FSoundNodeEditorData
{


	static FSoundNodeEditorData ReadAsMe(uintptr_t address)
	{
		FSoundNodeEditorData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundNodeEditorData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundGroup
// 0x0000
struct FSoundGroup
{


	static FSoundGroup ReadAsMe(uintptr_t address)
	{
		FSoundGroup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundGroup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AudioEQEffect
// 0x0000
struct FAudioEQEffect
{


	static FAudioEQEffect ReadAsMe(uintptr_t address)
	{
		FAudioEQEffect ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAudioEQEffect& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundClassAdjuster
// 0x0000
struct FSoundClassAdjuster
{


	static FSoundClassAdjuster ReadAsMe(uintptr_t address)
	{
		FSoundClassAdjuster ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundClassAdjuster& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.DistanceDatum
// 0x0000
struct FDistanceDatum
{


	static FDistanceDatum ReadAsMe(uintptr_t address)
	{
		FDistanceDatum ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FDistanceDatum& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundSubmixSendInfo
// 0x0000
struct FSoundSubmixSendInfo
{


	static FSoundSubmixSendInfo ReadAsMe(uintptr_t address)
	{
		FSoundSubmixSendInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundSubmixSendInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundSourceBusSendInfo
// 0x0000
struct FSoundSourceBusSendInfo
{


	static FSoundSourceBusSendInfo ReadAsMe(uintptr_t address)
	{
		FSoundSourceBusSendInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundSourceBusSendInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.ModulatorContinuousParams
// 0x0000
struct FModulatorContinuousParams
{


	static FModulatorContinuousParams ReadAsMe(uintptr_t address)
	{
		FModulatorContinuousParams ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FModulatorContinuousParams& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundWaveEnvelopeTimeData
// 0x0000
struct FSoundWaveEnvelopeTimeData
{


	static FSoundWaveEnvelopeTimeData ReadAsMe(uintptr_t address)
	{
		FSoundWaveEnvelopeTimeData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundWaveEnvelopeTimeData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundWaveSpectralTimeData
// 0x0000
struct FSoundWaveSpectralTimeData
{


	static FSoundWaveSpectralTimeData ReadAsMe(uintptr_t address)
	{
		FSoundWaveSpectralTimeData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundWaveSpectralTimeData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundWaveSpectralDataEntry
// 0x0000
struct FSoundWaveSpectralDataEntry
{


	static FSoundWaveSpectralDataEntry ReadAsMe(uintptr_t address)
	{
		FSoundWaveSpectralDataEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundWaveSpectralDataEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SoundWaveSpectralData
// 0x0000
struct FSoundWaveSpectralData
{


	static FSoundWaveSpectralData ReadAsMe(uintptr_t address)
	{
		FSoundWaveSpectralData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSoundWaveSpectralData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StreamedAudioPlatformData
// 0x0000
struct FStreamedAudioPlatformData
{


	static FStreamedAudioPlatformData ReadAsMe(uintptr_t address)
	{
		FStreamedAudioPlatformData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStreamedAudioPlatformData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SplineCurves
// 0x0000
struct FSplineCurves
{


	static FSplineCurves ReadAsMe(uintptr_t address)
	{
		FSplineCurves ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSplineCurves& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SplineInstanceData
// 0x5A367480
struct FSplineInstanceData
{
	unsigned char                                      UnknownData00[0x5A367480];                                // 0x0000(0x5A367480) MISSED OFFSET

	static FSplineInstanceData ReadAsMe(uintptr_t address)
	{
		FSplineInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSplineInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SplinePointData
// 0x0000
struct FSplinePointData
{


	static FSplinePointData ReadAsMe(uintptr_t address)
	{
		FSplinePointData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSplinePointData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SplinePoint
// 0x0000
struct FSplinePoint
{


	static FSplinePoint ReadAsMe(uintptr_t address)
	{
		FSplinePoint ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSplinePoint& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SplineMeshInstanceData
// 0x5A367480
struct FSplineMeshInstanceData
{
	unsigned char                                      UnknownData00[0x5A367480];                                // 0x0000(0x5A367480) MISSED OFFSET

	static FSplineMeshInstanceData ReadAsMe(uintptr_t address)
	{
		FSplineMeshInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSplineMeshInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SplineMeshParams
// 0x0000
struct FSplineMeshParams
{


	static FSplineMeshParams ReadAsMe(uintptr_t address)
	{
		FSplineMeshParams ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSplineMeshParams& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MaterialRemapIndex
// 0x0000
struct FMaterialRemapIndex
{


	static FMaterialRemapIndex ReadAsMe(uintptr_t address)
	{
		FMaterialRemapIndex ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMaterialRemapIndex& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticMaterial
// 0x0000
struct FStaticMaterial
{


	static FStaticMaterial ReadAsMe(uintptr_t address)
	{
		FStaticMaterial ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticMaterial& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MeshSectionInfoMap
// 0x0000
struct FMeshSectionInfoMap
{


	static FMeshSectionInfoMap ReadAsMe(uintptr_t address)
	{
		FMeshSectionInfoMap ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMeshSectionInfoMap& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.MeshSectionInfo
// 0x0000
struct FMeshSectionInfo
{


	static FMeshSectionInfo ReadAsMe(uintptr_t address)
	{
		FMeshSectionInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FMeshSectionInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.AssetEditorOrbitCameraPosition
// 0x0000
struct FAssetEditorOrbitCameraPosition
{


	static FAssetEditorOrbitCameraPosition ReadAsMe(uintptr_t address)
	{
		FAssetEditorOrbitCameraPosition ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FAssetEditorOrbitCameraPosition& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticMeshSourceModel
// 0x0000
struct FStaticMeshSourceModel
{


	static FStaticMeshSourceModel ReadAsMe(uintptr_t address)
	{
		FStaticMeshSourceModel ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticMeshSourceModel& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticMeshOptimizationSettings
// 0x0000
struct FStaticMeshOptimizationSettings
{


	static FStaticMeshOptimizationSettings ReadAsMe(uintptr_t address)
	{
		FStaticMeshOptimizationSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticMeshOptimizationSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticMeshComponentInstanceData
// 0x5A3DDB40
struct FStaticMeshComponentInstanceData
{
	unsigned char                                      UnknownData00[0x5A3DDB40];                                // 0x0000(0x5A3DDB40) MISSED OFFSET

	static FStaticMeshComponentInstanceData ReadAsMe(uintptr_t address)
	{
		FStaticMeshComponentInstanceData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticMeshComponentInstanceData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StreamingTextureBuildInfo
// 0x0000
struct FStreamingTextureBuildInfo
{


	static FStreamingTextureBuildInfo ReadAsMe(uintptr_t address)
	{
		FStreamingTextureBuildInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStreamingTextureBuildInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PaintedVertex
// 0x0000
struct FPaintedVertex
{


	static FPaintedVertex ReadAsMe(uintptr_t address)
	{
		FPaintedVertex ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPaintedVertex& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticMeshComponentLODInfo
// 0x0000
struct FStaticMeshComponentLODInfo
{


	static FStaticMeshComponentLODInfo ReadAsMe(uintptr_t address)
	{
		FStaticMeshComponentLODInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticMeshComponentLODInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticMeshVertexColorLODData
// 0x0000
struct FStaticMeshVertexColorLODData
{


	static FStaticMeshVertexColorLODData ReadAsMe(uintptr_t address)
	{
		FStaticMeshVertexColorLODData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticMeshVertexColorLODData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticParameterSet
// 0x0000
struct FStaticParameterSet
{


	static FStaticParameterSet ReadAsMe(uintptr_t address)
	{
		FStaticParameterSet ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticParameterSet& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticMaterialLayersParameter
// 0x0000
struct FStaticMaterialLayersParameter
{


	static FStaticMaterialLayersParameter ReadAsMe(uintptr_t address)
	{
		FStaticMaterialLayersParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticMaterialLayersParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticTerrainLayerWeightParameter
// 0x0000
struct FStaticTerrainLayerWeightParameter
{


	static FStaticTerrainLayerWeightParameter ReadAsMe(uintptr_t address)
	{
		FStaticTerrainLayerWeightParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticTerrainLayerWeightParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticComponentMaskParameter
// 0x0000
struct FStaticComponentMaskParameter
{


	static FStaticComponentMaskParameter ReadAsMe(uintptr_t address)
	{
		FStaticComponentMaskParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticComponentMaskParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StaticSwitchParameter
// 0x0000
struct FStaticSwitchParameter
{


	static FStaticSwitchParameter ReadAsMe(uintptr_t address)
	{
		FStaticSwitchParameter ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStaticSwitchParameter& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StringCurve
// 0x5A311780
struct FStringCurve
{
	unsigned char                                      UnknownData00[0x5A311780];                                // 0x0000(0x5A311780) MISSED OFFSET

	static FStringCurve ReadAsMe(uintptr_t address)
	{
		FStringCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStringCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StringCurveKey
// 0x0000
struct FStringCurveKey
{


	static FStringCurveKey ReadAsMe(uintptr_t address)
	{
		FStringCurveKey ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStringCurveKey& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.SubsurfaceProfileStruct
// 0x0000
struct FSubsurfaceProfileStruct
{


	static FSubsurfaceProfileStruct ReadAsMe(uintptr_t address)
	{
		FSubsurfaceProfileStruct ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FSubsurfaceProfileStruct& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TexturePlatformData
// 0x0000
struct FTexturePlatformData
{


	static FTexturePlatformData ReadAsMe(uintptr_t address)
	{
		FTexturePlatformData ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTexturePlatformData& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TextureLODGroup
// 0x0000
struct FTextureLODGroup
{


	static FTextureLODGroup ReadAsMe(uintptr_t address)
	{
		FTextureLODGroup ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTextureLODGroup& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TextureSource
// 0x0000
struct FTextureSource
{


	static FTextureSource ReadAsMe(uintptr_t address)
	{
		FTextureSource ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTextureSource& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StreamingTexturePrimitiveInfo
// 0x0000
struct FStreamingTexturePrimitiveInfo
{


	static FStreamingTexturePrimitiveInfo ReadAsMe(uintptr_t address)
	{
		FStreamingTexturePrimitiveInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStreamingTexturePrimitiveInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.Timeline
// 0x0000
struct FTimeline
{


	static FTimeline ReadAsMe(uintptr_t address)
	{
		FTimeline ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimeline& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TimelineLinearColorTrack
// 0x0000
struct FTimelineLinearColorTrack
{


	static FTimelineLinearColorTrack ReadAsMe(uintptr_t address)
	{
		FTimelineLinearColorTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimelineLinearColorTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TimelineFloatTrack
// 0x0000
struct FTimelineFloatTrack
{


	static FTimelineFloatTrack ReadAsMe(uintptr_t address)
	{
		FTimelineFloatTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimelineFloatTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TimelineVectorTrack
// 0x0000
struct FTimelineVectorTrack
{


	static FTimelineVectorTrack ReadAsMe(uintptr_t address)
	{
		FTimelineVectorTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimelineVectorTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TimelineEventEntry
// 0x0000
struct FTimelineEventEntry
{


	static FTimelineEventEntry ReadAsMe(uintptr_t address)
	{
		FTimelineEventEntry ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimelineEventEntry& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TTTrackBase
// 0x0000
struct FTTTrackBase
{


	static FTTTrackBase ReadAsMe(uintptr_t address)
	{
		FTTTrackBase ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTTTrackBase& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TTPropertyTrack
// 0x5A442980
struct FTTPropertyTrack
{
	unsigned char                                      UnknownData00[0x5A442980];                                // 0x0000(0x5A442980) MISSED OFFSET

	static FTTPropertyTrack ReadAsMe(uintptr_t address)
	{
		FTTPropertyTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTTPropertyTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TTLinearColorTrack
// 0x5A4428C0
struct FTTLinearColorTrack
{
	unsigned char                                      UnknownData00[0x5A4428C0];                                // 0x0000(0x5A4428C0) MISSED OFFSET

	static FTTLinearColorTrack ReadAsMe(uintptr_t address)
	{
		FTTLinearColorTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTTLinearColorTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TTFloatTrack
// 0x5A4428C0
struct FTTFloatTrack
{
	unsigned char                                      UnknownData00[0x5A4428C0];                                // 0x0000(0x5A4428C0) MISSED OFFSET

	static FTTFloatTrack ReadAsMe(uintptr_t address)
	{
		FTTFloatTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTTFloatTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TTVectorTrack
// 0x5A4428C0
struct FTTVectorTrack
{
	unsigned char                                      UnknownData00[0x5A4428C0];                                // 0x0000(0x5A4428C0) MISSED OFFSET

	static FTTVectorTrack ReadAsMe(uintptr_t address)
	{
		FTTVectorTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTTVectorTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TTEventTrack
// 0x5A442980
struct FTTEventTrack
{
	unsigned char                                      UnknownData00[0x5A442980];                                // 0x0000(0x5A442980) MISSED OFFSET

	static FTTEventTrack ReadAsMe(uintptr_t address)
	{
		FTTEventTrack ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTTEventTrack& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TimeStretchCurveInstance
// 0x0000
struct FTimeStretchCurveInstance
{


	static FTimeStretchCurveInstance ReadAsMe(uintptr_t address)
	{
		FTimeStretchCurveInstance ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimeStretchCurveInstance& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TimeStretchCurve
// 0x0000
struct FTimeStretchCurve
{


	static FTimeStretchCurve ReadAsMe(uintptr_t address)
	{
		FTimeStretchCurve ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimeStretchCurve& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TimeStretchCurveMarker
// 0x0000
struct FTimeStretchCurveMarker
{


	static FTimeStretchCurveMarker ReadAsMe(uintptr_t address)
	{
		FTimeStretchCurveMarker ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTimeStretchCurveMarker& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.TouchInputControl
// 0x0000
struct FTouchInputControl
{


	static FTouchInputControl ReadAsMe(uintptr_t address)
	{
		FTouchInputControl ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FTouchInputControl& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VirtualTextureLayer
// 0x0000
struct FVirtualTextureLayer
{


	static FVirtualTextureLayer ReadAsMe(uintptr_t address)
	{
		FVirtualTextureLayer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVirtualTextureLayer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.HardwareCursorReference
// 0x0000
struct FHardwareCursorReference
{


	static FHardwareCursorReference ReadAsMe(uintptr_t address)
	{
		FHardwareCursorReference ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FHardwareCursorReference& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StreamingLevelsToConsider
// 0x0000
struct FStreamingLevelsToConsider
{


	static FStreamingLevelsToConsider ReadAsMe(uintptr_t address)
	{
		FStreamingLevelsToConsider ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStreamingLevelsToConsider& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.VoiceSettings
// 0x0000
struct FVoiceSettings
{


	static FVoiceSettings ReadAsMe(uintptr_t address)
	{
		FVoiceSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FVoiceSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LevelStreamingWrapper
// 0x0000
struct FLevelStreamingWrapper
{


	static FLevelStreamingWrapper ReadAsMe(uintptr_t address)
	{
		FLevelStreamingWrapper ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLevelStreamingWrapper& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LevelCollection
// 0x0000
struct FLevelCollection
{


	static FLevelCollection ReadAsMe(uintptr_t address)
	{
		FLevelCollection ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLevelCollection& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.EndPhysicsTickFunction
// 0x5A123C40
struct FEndPhysicsTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FEndPhysicsTickFunction ReadAsMe(uintptr_t address)
	{
		FEndPhysicsTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FEndPhysicsTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.StartPhysicsTickFunction
// 0x5A123C40
struct FStartPhysicsTickFunction
{
	unsigned char                                      UnknownData00[0x5A123C40];                                // 0x0000(0x5A123C40) MISSED OFFSET

	static FStartPhysicsTickFunction ReadAsMe(uintptr_t address)
	{
		FStartPhysicsTickFunction ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FStartPhysicsTickFunction& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.WorldPSCPool
// 0x0000
struct FWorldPSCPool
{


	static FWorldPSCPool ReadAsMe(uintptr_t address)
	{
		FWorldPSCPool ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FWorldPSCPool& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PSCPool
// 0x0000
struct FPSCPool
{


	static FPSCPool ReadAsMe(uintptr_t address)
	{
		FPSCPool ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPSCPool& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LevelViewportInfo
// 0x0000
struct FLevelViewportInfo
{


	static FLevelViewportInfo ReadAsMe(uintptr_t address)
	{
		FLevelViewportInfo ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLevelViewportInfo& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.BroadphaseSettings
// 0x0000
struct FBroadphaseSettings
{


	static FBroadphaseSettings ReadAsMe(uintptr_t address)
	{
		FBroadphaseSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FBroadphaseSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.HierarchicalSimplification
// 0x0000
struct FHierarchicalSimplification
{


	static FHierarchicalSimplification ReadAsMe(uintptr_t address)
	{
		FHierarchicalSimplification ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FHierarchicalSimplification& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.PSCPoolElem
// 0x0000
struct FPSCPoolElem
{


	static FPSCPoolElem ReadAsMe(uintptr_t address)
	{
		FPSCPoolElem ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FPSCPoolElem& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.NetViewer
// 0x0000
struct FNetViewer
{


	static FNetViewer ReadAsMe(uintptr_t address)
	{
		FNetViewer ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FNetViewer& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

// ScriptStruct Engine.LightmassWorldInfoSettings
// 0x0000
struct FLightmassWorldInfoSettings
{


	static FLightmassWorldInfoSettings ReadAsMe(uintptr_t address)
	{
		FLightmassWorldInfoSettings ret;
		Utils::MemoryObj->Read(address, ret);
		return ret;
	}

	static int WriteAsMe(const uintptr_t address, FLightmassWorldInfoSettings& toWrite)
	{
		return Utils::MemoryObj->Write(address, toWrite);
	}

};

}

#ifdef _MSC_VER
	#pragma pack(pop)
#endif
